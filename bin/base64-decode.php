#!/usr/bin/env php
<?php

/**
 * This is the vim argument to split everything into lines
 * :1,$ s/ \([a-z_]\+="\|\[\)/^M\1/g
 */
$base64 = trim(fgets(STDIN));
while ($base64) {
	echo "\n";
	$data = base64_decode($base64);
	$serial = unserialize($data);
	echo "$data\n\n";
	print_r($serial);
	echo "================================================================================\n";
	$base64 = trim(fgets(STDIN));
}

