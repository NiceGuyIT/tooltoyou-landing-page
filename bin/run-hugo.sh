#!/usr/bin/env bash

# Hugo does not clean out the public/ directory when content is moved or deleted.
# https://github.com/gohugoio/hugo/issues/2389

#	--verbose \
#	--baseUrl=http://localhost:8081
#	--baseUrl=http://localhost:1316 \
hugo server \
	--renderToDisk \
	--disableFastRender \
	--appendPort=false \
	--baseUrl=http://tsurani:1316 \
	--bind=tsurani \
	--port=1316 \
	--verbose

