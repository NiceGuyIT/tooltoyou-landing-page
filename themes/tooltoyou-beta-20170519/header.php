<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description"
		  content="A peer-to-peer website that allows users to rent or list tools and equipment locally. Save money on DIY projects or make money on the hardware tools sitting in the garage.">
	<meta name="keywords" content=" Tools to rent,
									Home improvement tools,
									Tools and Equipment,
									Do it Yourself Projects,
									DIY Projects,
									DIY Tools,
									Gardening tools,
									Peer to peer website,
									Renting a ladder,
									Tool rentals near my location,
									Rent garden tools,
									Lawn tool rental,
									Power tool rentals,
									Construction equipment rental companies,
									Tool rental places,
									Heavy equipment rental companies,
									Equipment rental company,
									Renting equipment,
									Construction rentals,
									Garden equipment rental,
									Rental power equipment,
									Landscape tool rental,
									Rent tools and equipment,
									Electrical tool rental">

	<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php /* <!-- Google Analytics --> */ ?>
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-82104990-1', 'auto');
		ga('send', 'pageview');
	</script>

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php echo dslc_hf_get_header(); ?>