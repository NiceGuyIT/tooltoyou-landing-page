<?php

// Delcare Header/Footer compatibility
define( 'DS_LIVE_COMPOSER_HF', true );
define( 'DS_LIVE_COMPOSER_HF_AUTO', false );

// Content Width ( WP requires it and LC uses is to figure out the wrapper width )
if ( ! isset( $content_width ) )
	$content_width = 1180;

// Basic theme setup
if ( ! function_exists( 'lct_theme_setup' ) ) {

	function lct_theme_setup() {

		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		// Enable Post Thumbnails ( Featured Image )
		add_theme_support( 'post-thumbnails' );

		// Enable support for HTML5 markup.
		add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );

	}

} add_action( 'after_setup_theme', 'lct_theme_setup' );

// Load JS and CSS files
function lct_load_scripts() {

	wp_enqueue_style( 'lct-base-style', get_stylesheet_uri(), array(), '1.0' );
//	wp_enqueue_style( 'lct-base-style', get_template_directory_uri() . '/css/style.css', array(), '1.0' );
//	wp_enqueue_style( 'sharetribe-style', get_template_directory_uri() . '/css/sharetribe.css', array(), '1.0' );
//	wp_enqueue_style( 'studiotime-style', get_template_directory_uri() . '/css/studiotime.css', array(), '1.0' );
	wp_enqueue_style( 'webfont-ss-social', get_template_directory_uri() . '/webfonts/ss-social.css', array(), '1.0' );
	wp_enqueue_style( 'webfont-ss-pika', get_template_directory_uri() . '/webfonts/ss-pika.css', array(), '1.0' );
//	wp_enqueue_style( 'webfont-fontawesome', get_template_directory_uri() . '/webfonts/font-awesome.css', array(), '1.0' );
	wp_enqueue_style( 'google-fonts-source-sans-pro', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro', array() );
	wp_enqueue_script( 'modernizr-flex', '/js/modernizr-flex.js', array(), '1.0' );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'site-javascript', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.0' );

} add_action( 'wp_enqueue_scripts', 'lct_load_scripts' );
