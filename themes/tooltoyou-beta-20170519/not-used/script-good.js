jQuery(document).ready(function ($) {
	$(".header-menu-container.toggle").click(function () {
		if ($(".header-menu-container.toggle").hasClass("toggled")) {
			// Menu is shown; hide it
			$(".header-menu-container.toggle").removeClass("toggled");
			$(".toggle-menu.header-toggle-menu-menu").addClass("hidden");
		} else {
			// Menu is hidden; show it
			$(".header-menu-container.toggle").addClass("toggled");
			$(".toggle-menu.header-toggle-menu-menu").removeClass("hidden");

			position_menu_desktop();
		}
	});


	$(".dropdown").click(function () {
		$(".dropdown").toggleClass("toggled")
	});

	$(".dropdown .current-value").click(function () {
		return $(".dropdown").toggleClass("open")
	});

	$(".dropdown li a").click(function (a) {
		var b, c;
		return
		c = $(this).attr("data-value"),
			b = $(this).parents("form"),
			b.find("input[name=category]").val(c),
			b.find(".current-value").text($(this).text()),
			$(".dropdown").removeClass("open")
	});

});

jQuery(window).resize(function () {
	position_menu_desktop();
});

position_menu_desktop = function () {
	// Move the menu under the menu button
	// var element = jQuery( "div[data-toggle='#header-menu-toggle-menu']" );
	var element = jQuery("#header-menu-desktop-anchor");
	var offset = element.offset();
	var height = element.outerHeight();
	var width = element.outerWidth();
	var top = (offset.top + height) + "px";
	var left = (offset.left) + "px";

	jQuery("#header-menu-toggle-menu").css({
		"position": "absolute",
		"left": left,
		"top": top
	});

	console.log("offset.top: " + offset.top);
	console.log("offset.left: " + offset.left);
	console.log("height: " + height);
	console.log("width: " + width);
	console.log("top: " + top);
	console.log("left: " + left);

};

studiotime = function () {
	$(function () {
		return APP.buildPageLanding($("body.page-landing"))
	}),
		APP.buildPageLanding = function (a) {
			return 1 === a.length ? (
				$(".header-menu-container.toggle").click(function (a) {
					var b;
					return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
						top: $(this).outerHeight(),
						left: $(this).offset().left
					})
				}),

					$(".dropdown .current-value").click(function (a) {
						return a.stopPropagation(), a.preventDefault(), $(".dropdown").toggleClass("open")
					}),

					$(".dropdown li a").click(function (a) {
						var b, c;
						return a.stopPropagation(), a.preventDefault(), c = $(this).attr("data-value"), b = $(this).parents("form"), b.find("input[name=category]").val(c), b.find(".current-value").text($(this).text()), $(".dropdown").removeClass("open")
					}),

					$(".featured-artist-list li .info-container").click(function (a) {
						return null != $(this).attr("href") ? (a.stopPropagation(), a.preventDefault(), self.location = $(this).attr("href")) : void 0
					}),

					a.on("click", function (a) {
						return $(".header-menu-container.toggle").each(function () {
							var b;
							return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
						}), $(".dropdown").each(function () {
							return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : $(this).removeClass("open")
						})
					}),

					APP.buildAdditionalComponents = function (a) {
						return a.find(".link-talent-form").click(function () {
							var a, b, c, d, e;
							return b = "", d = $(".talent-form-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").removeClass("wide-medium").addClass("wide-medium"), $("#content-modal").modal("show")
						}), a.find("form#form-talent").unbind("onDone"), a.find("form#form-talent").bind("onDone", function () {
							var a, b, c, d, e;
							return b = "", d = $(".thank-you-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").modal("show")
						}), a.find(".has-link").click(function () {
							return null != $(this).attr("href") ? self.location = $(this).attr("href") : void 0
						})
					},

					APP.resizeCanvasExtra = function () {
						return $(".header-menu-container.toggle").each(function () {
							var a;
							return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
						})
					},

					APP.YTPlayer.onPlayerStateChange = function (a) {
						return a.data === YT.PlayerState.ENDED ? APP.hideContentModal() : void 0
					}
			) : void 0
		}
};

