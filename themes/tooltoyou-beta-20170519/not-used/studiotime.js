function onYouTubeIframeAPIReady() {
}
var APP = {};
if (APP.DEBUG = 1, APP.YTPlayer = {done: !1}, APP.player = !1, window.console || (window.console = {
		log: function () {
		}, warn: function () {
		}, error: function () {
		}
	}), !function (a, b) {
		"object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function (a) {
			if (!a.document) {
				throw new Error("jQuery requires a window with a document");
			}
			return b(a)
		} : b(a)
	}("undefined" != typeof window ? window : this, function (a, b) {
		function c(a) {
			var b = a.length, c = _.type(a);
			return "function" === c || _.isWindow(a) ? !1 : 1 === a.nodeType && b ? !0 : "array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a
		}

		function d(a, b, c) {
			if (_.isFunction(b)) {
				return _.grep(a, function (a, d) {
					return !!b.call(a, d, a) !== c
				});
			}
			if (b.nodeType) {
				return _.grep(a, function (a) {
					return a === b !== c
				});
			}
			if ("string" == typeof b) {
				if (hb.test(b)) {
					return _.filter(b, a, c);
				}
				b = _.filter(b, a)
			}
			return _.grep(a, function (a) {
				return U.call(b, a) >= 0 !== c
			})
		}

		function e(a, b) {
			for (; (a = a[b]) && 1 !== a.nodeType;);
			return a
		}

		function f(a) {
			var b = ob[a] = {};
			return _.each(a.match(nb) || [], function (a, c) {
				b[c] = !0
			}), b
		}

		function g() {
			Z.removeEventListener("DOMContentLoaded", g, !1), a.removeEventListener("load", g, !1), _.ready()
		}

		function h() {
			Object.defineProperty(this.cache = {}, 0, {
				get: function () {
					return {}
				}
			}), this.expando = _.expando + Math.random()
		}

		function i(a, b, c) {
			var d;
			if (void 0 === c && 1 === a.nodeType) {
				if (d = "data-" + b.replace(ub, "-$1").toLowerCase(), c = a.getAttribute(d), "string" == typeof c) {
					try {
						c = "true" === c ? !0 : "false" === c ? !1 : "null" === c ? null : +c + "" === c ? +c : tb.test(c) ? _.parseJSON(c) : c
					} catch (e) {
					}
					sb.set(a, b, c)
				} else {
					c = void 0;
				}
			}
			return c
		}

		function j() {
			return !0
		}

		function k() {
			return !1
		}

		function l() {
			try {
				return Z.activeElement
			} catch (a) {
			}
		}

		function m(a, b) {
			return _.nodeName(a, "table") && _.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
		}

		function n(a) {
			return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a
		}

		function o(a) {
			var b = Kb.exec(a.type);
			return b ? a.type = b[1] : a.removeAttribute("type"), a
		}

		function p(a, b) {
			for (var c = 0, d = a.length; d > c; c++)rb.set(a[c], "globalEval", !b || rb.get(b[c], "globalEval"))
		}

		function q(a, b) {
			var c, d, e, f, g, h, i, j;
			if (1 === b.nodeType) {
				if (rb.hasData(a) && (f = rb.access(a), g = rb.set(b, f), j = f.events)) {
					delete g.handle, g.events = {};
					for (e in j)for (c = 0, d = j[e].length; d > c; c++)_.event.add(b, e, j[e][c])
				}
				sb.hasData(a) && (h = sb.access(a), i = _.extend({}, h), sb.set(b, i))
			}
		}

		function r(a, b) {
			var c = a.getElementsByTagName ? a.getElementsByTagName(b || "*") : a.querySelectorAll ? a.querySelectorAll(b || "*") : [];
			return void 0 === b || b && _.nodeName(a, b) ? _.merge([a], c) : c
		}

		function s(a, b) {
			var c = b.nodeName.toLowerCase();
			"input" === c && yb.test(a.type) ? b.checked = a.checked : ("input" === c || "textarea" === c) && (b.defaultValue = a.defaultValue)
		}

		function t(b, c) {
			var d, e = _(c.createElement(b)).appendTo(c.body), f = a.getDefaultComputedStyle && (d = a.getDefaultComputedStyle(e[0])) ? d.display : _.css(e[0], "display");
			return e.detach(), f
		}

		function u(a) {
			var b = Z, c = Ob[a];
			return c || (c = t(a, b), "none" !== c && c || (Nb = (Nb || _("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement), b = Nb[0].contentDocument, b.write(), b.close(), c = t(a, b), Nb.detach()), Ob[a] = c), c
		}

		function v(a, b, c) {
			var d, e, f, g, h = a.style;
			return c = c || Rb(a), c && (g = c.getPropertyValue(b) || c[b]), c && ("" !== g || _.contains(a.ownerDocument, a) || (g = _.style(a, b)), Qb.test(g) && Pb.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + "" : g
		}

		function w(a, b) {
			return {
				get: function () {
					return a() ? void delete this.get : (this.get = b).apply(this, arguments)
				}
			}
		}

		function x(a, b) {
			if (b in a) {
				return b;
			}
			for (var c = b[0].toUpperCase() + b.slice(1), d = b, e = Xb.length; e--;)if (b = Xb[e] + c, b in a) {
				return b;
			}
			return d
		}

		function y(a, b, c) {
			var d = Tb.exec(b);
			return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b
		}

		function z(a, b, c, d, e) {
			for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2)"margin" === c && (g += _.css(a, c + wb[f], !0, e)), d ? ("content" === c && (g -= _.css(a, "padding" + wb[f], !0, e)), "margin" !== c && (g -= _.css(a, "border" + wb[f] + "Width", !0, e))) : (g += _.css(a, "padding" + wb[f], !0, e), "padding" !== c && (g += _.css(a, "border" + wb[f] + "Width", !0, e)));
			return g
		}

		function A(a, b, c) {
			var d = !0, e = "width" === b ? a.offsetWidth : a.offsetHeight, f = Rb(a), g = "border-box" === _.css(a, "boxSizing", !1, f);
			if (0 >= e || null == e) {
				if (e = v(a, b, f), (0 > e || null == e) && (e = a.style[b]), Qb.test(e)) {
					return e;
				}
				d = g && (Y.boxSizingReliable() || e === a.style[b]), e = parseFloat(e) || 0
			}
			return e + z(a, b, c || (g ? "border" : "content"), d, f) + "px"
		}

		function B(a, b) {
			for (var c, d, e, f = [], g = 0, h = a.length; h > g; g++)d = a[g], d.style && (f[g] = rb.get(d, "olddisplay"), c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && xb(d) && (f[g] = rb.access(d, "olddisplay", u(d.nodeName)))) : (e = xb(d), "none" === c && e || rb.set(d, "olddisplay", e ? c : _.css(d, "display"))));
			for (g = 0; h > g; g++)d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none"));
			return a
		}

		function C(a, b, c, d, e) {
			return new C.prototype.init(a, b, c, d, e)
		}

		function D() {
			return setTimeout(function () {
				Yb = void 0
			}), Yb = _.now()
		}

		function E(a, b) {
			var c, d = 0, e = {height: a};
			for (b = b ? 1 : 0; 4 > d; d += 2 - b)c = wb[d], e["margin" + c] = e["padding" + c] = a;
			return b && (e.opacity = e.width = a), e
		}

		function F(a, b, c) {
			for (var d, e = (cc[b] || []).concat(cc["*"]), f = 0, g = e.length; g > f; f++)if (d = e[f].call(c, b, a)) {
				return d
			}
		}

		function G(a, b, c) {
			var d, e, f, g, h, i, j, k, l = this, m = {}, n = a.style, o = a.nodeType && xb(a), p = rb.get(a, "fxshow");
			c.queue || (h = _._queueHooks(a, "fx"), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, h.empty.fire = function () {
				h.unqueued || i()
			}), h.unqueued++, l.always(function () {
				l.always(function () {
					h.unqueued--, _.queue(a, "fx").length || h.empty.fire()
				})
			})), 1 === a.nodeType && ("height" in b || "width" in b) && (c.overflow = [n.overflow, n.overflowX, n.overflowY], j = _.css(a, "display"), k = "none" === j ? rb.get(a, "olddisplay") || u(a.nodeName) : j, "inline" === k && "none" === _.css(a, "float") && (n.display = "inline-block")), c.overflow && (n.overflow = "hidden", l.always(function () {
				n.overflow = c.overflow[0], n.overflowX = c.overflow[1], n.overflowY = c.overflow[2]
			}));
			for (d in b)if (e = b[d], $b.exec(e)) {
				if (delete b[d], f = f || "toggle" === e, e === (o ? "hide" : "show")) {
					if ("show" !== e || !p || void 0 === p[d]) {
						continue;
					}
					o = !0
				}
				m[d] = p && p[d] || _.style(a, d)
			} else {
				j = void 0;
			}
			if (_.isEmptyObject(m)) {
				"inline" === ("none" === j ? u(a.nodeName) : j) && (n.display = j);
			} else {
				p ? "hidden" in p && (o = p.hidden) : p = rb.access(a, "fxshow", {}), f && (p.hidden = !o), o ? _(a).show() : l.done(function () {
					_(a).hide()
				}), l.done(function () {
					var b;
					rb.remove(a, "fxshow");
					for (b in m)_.style(a, b, m[b])
				});
				for (d in m)g = F(o ? p[d] : 0, d, l), d in p || (p[d] = g.start, o && (g.end = g.start, g.start = "width" === d || "height" === d ? 1 : 0))
			}
		}

		function H(a, b) {
			var c, d, e, f, g;
			for (c in a)if (d = _.camelCase(c), e = b[d], f = a[c], _.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = _.cssHooks[d], g && "expand" in g) {
				f = g.expand(f), delete a[d];
				for (c in f)c in a || (a[c] = f[c], b[c] = e)
			} else {
				b[d] = e
			}
		}

		function I(a, b, c) {
			var d, e, f = 0, g = bc.length, h = _.Deferred().always(function () {
				delete i.elem
			}), i = function () {
				if (e) {
					return !1;
				}
				for (var b = Yb || D(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++)j.tweens[g].run(f);
				return h.notifyWith(a, [j, f, c]), 1 > f && i ? c : (h.resolveWith(a, [j]), !1)
			}, j = h.promise({
				elem: a,
				props: _.extend({}, b),
				opts: _.extend(!0, {specialEasing: {}}, c),
				originalProperties: b,
				originalOptions: c,
				startTime: Yb || D(),
				duration: c.duration,
				tweens: [],
				createTween: function (b, c) {
					var d = _.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
					return j.tweens.push(d), d
				},
				stop: function (b) {
					var c = 0, d = b ? j.tweens.length : 0;
					if (e) {
						return this;
					}
					for (e = !0; d > c; c++)j.tweens[c].run(1);
					return b ? h.resolveWith(a, [j, b]) : h.rejectWith(a, [j, b]), this
				}
			}), k = j.props;
			for (H(k, j.opts.specialEasing); g > f; f++)if (d = bc[f].call(j, a, k, j.opts)) {
				return d;
			}
			return _.map(k, F, j), _.isFunction(j.opts.start) && j.opts.start.call(a, j), _.fx.timer(_.extend(i, {
				elem: a,
				anim: j,
				queue: j.opts.queue
			})), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)
		}

		function J(a) {
			return function (b, c) {
				"string" != typeof b && (c = b, b = "*");
				var d, e = 0, f = b.toLowerCase().match(nb) || [];
				if (_.isFunction(c)) {
					for (; d = f[e++];)"+" === d[0] ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
				}
			}
		}

		function K(a, b, c, d) {
			function e(h) {
				var i;
				return f[h] = !0, _.each(a[h] || [], function (a, h) {
					var j = h(b, c, d);
					return "string" != typeof j || g || f[j] ? g ? !(i = j) : void 0 : (b.dataTypes.unshift(j), e(j), !1)
				}), i
			}

			var f = {}, g = a === vc;
			return e(b.dataTypes[0]) || !f["*"] && e("*")
		}

		function L(a, b) {
			var c, d, e = _.ajaxSettings.flatOptions || {};
			for (c in b)void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);
			return d && _.extend(!0, a, d), a
		}

		function M(a, b, c) {
			for (var d, e, f, g, h = a.contents, i = a.dataTypes; "*" === i[0];)i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
			if (d) {
				for (e in h)if (h[e] && h[e].test(d)) {
					i.unshift(e);
					break
				}
			}
			if (i[0] in c) {
				f = i[0];
			} else {
				for (e in c) {
					if (!i[0] || a.converters[e + " " + i[0]]) {
						f = e;
						break
					}
					g || (g = e)
				}
				f = f || g
			}
			return f ? (f !== i[0] && i.unshift(f), c[f]) : void 0
		}

		function N(a, b, c, d) {
			var e, f, g, h, i, j = {}, k = a.dataTypes.slice();
			if (k[1]) {
				for (g in a.converters)j[g.toLowerCase()] = a.converters[g];
			}
			for (f = k.shift(); f;)if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift()) {
				if ("*" === f) {
					f = i;
				} else if ("*" !== i && i !== f) {
					if (g = j[i + " " + f] || j["* " + f], !g) {
						for (e in j)if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
							g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
							break
						}
					}
					if (g !== !0) {
						if (g && a["throws"]) {
							b = g(b);
						} else {
							try {
								b = g(b)
							} catch (l) {
								return {state: "parsererror", error: g ? l : "No conversion from " + i + " to " + f}
							}
						}
					}
				}
			}
			return {state: "success", data: b}
		}

		function O(a, b, c, d) {
			var e;
			if (_.isArray(b)) {
				_.each(b, function (b, e) {
					c || zc.test(a) ? d(a, e) : O(a + "[" + ("object" == typeof e ? b : "") + "]", e, c, d)
				});
			} else if (c || "object" !== _.type(b)) {
				d(a, b);
			} else {
				for (e in b)O(a + "[" + e + "]", b[e], c, d)
			}
		}

		function P(a) {
			return _.isWindow(a) ? a : 9 === a.nodeType && a.defaultView
		}

		var Q = [], R = Q.slice, S = Q.concat, T = Q.push, U = Q.indexOf, V = {}, W = V.toString, X = V.hasOwnProperty, Y = {}, Z = a.document, $ = "2.1.1", _ = function (a, b) {
			return new _.fn.init(a, b)
		}, ab = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, bb = /^-ms-/, cb = /-([\da-z])/gi, db = function (a, b) {
			return b.toUpperCase()
		};
		_.fn = _.prototype = {
			jquery: $, constructor: _, selector: "", length: 0, toArray: function () {
				return R.call(this)
			}, get: function (a) {
				return null != a ? 0 > a ? this[a + this.length] : this[a] : R.call(this)
			}, pushStack: function (a) {
				var b = _.merge(this.constructor(), a);
				return b.prevObject = this, b.context = this.context, b
			}, each: function (a, b) {
				return _.each(this, a, b)
			}, map: function (a) {
				return this.pushStack(_.map(this, function (b, c) {
					return a.call(b, c, b)
				}))
			}, slice: function () {
				return this.pushStack(R.apply(this, arguments))
			}, first: function () {
				return this.eq(0)
			}, last: function () {
				return this.eq(-1)
			}, eq: function (a) {
				var b = this.length, c = +a + (0 > a ? b : 0);
				return this.pushStack(c >= 0 && b > c ? [this[c]] : [])
			}, end: function () {
				return this.prevObject || this.constructor(null)
			}, push: T, sort: Q.sort, splice: Q.splice
		}, _.extend = _.fn.extend = function () {
			var a, b, c, d, e, f, g = arguments[0] || {}, h = 1, i = arguments.length, j = !1;
			for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || _.isFunction(g) || (g = {}), h === i && (g = this, h--); i > h; h++)if (null != (a = arguments[h])) {
				for (b in a)c = g[b], d = a[b], g !== d && (j && d && (_.isPlainObject(d) || (e = _.isArray(d))) ? (e ? (e = !1, f = c && _.isArray(c) ? c : []) : f = c && _.isPlainObject(c) ? c : {}, g[b] = _.extend(j, f, d)) : void 0 !== d && (g[b] = d));
			}
			return g
		}, _.extend({
			expando: "jQuery" + ($ + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (a) {
				throw new Error(a)
			}, noop: function () {
			}, isFunction: function (a) {
				return "function" === _.type(a)
			}, isArray: Array.isArray, isWindow: function (a) {
				return null != a && a === a.window
			}, isNumeric: function (a) {
				return !_.isArray(a) && a - parseFloat(a) >= 0
			}, isPlainObject: function (a) {
				return "object" !== _.type(a) || a.nodeType || _.isWindow(a) ? !1 : a.constructor && !X.call(a.constructor.prototype, "isPrototypeOf") ? !1 : !0
			}, isEmptyObject: function (a) {
				var b;
				for (b in a)return !1;
				return !0
			}, type: function (a) {
				return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? V[W.call(a)] || "object" : typeof a
			}, globalEval: function (a) {
				var b, c = eval;
				a = _.trim(a), a && (1 === a.indexOf("use strict") ? (b = Z.createElement("script"), b.text = a, Z.head.appendChild(b).parentNode.removeChild(b)) : c(a))
			}, camelCase: function (a) {
				return a.replace(bb, "ms-").replace(cb, db)
			}, nodeName: function (a, b) {
				return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
			}, each: function (a, b, d) {
				var e, f = 0, g = a.length, h = c(a);
				if (d) {
					if (h) {
						for (; g > f && (e = b.apply(a[f], d), e !== !1); f++);
					} else {
						for (f in a)if (e = b.apply(a[f], d), e === !1) {
							break
						}
					}
				} else if (h) {
					for (; g > f && (e = b.call(a[f], f, a[f]), e !== !1); f++);
				} else {
					for (f in a)if (e = b.call(a[f], f, a[f]), e === !1) {
						break;
					}
				}
				return a
			}, trim: function (a) {
				return null == a ? "" : (a + "").replace(ab, "")
			}, makeArray: function (a, b) {
				var d = b || [];
				return null != a && (c(Object(a)) ? _.merge(d, "string" == typeof a ? [a] : a) : T.call(d, a)), d
			}, inArray: function (a, b, c) {
				return null == b ? -1 : U.call(b, a, c)
			}, merge: function (a, b) {
				for (var c = +b.length, d = 0, e = a.length; c > d; d++)a[e++] = b[d];
				return a.length = e, a
			}, grep: function (a, b, c) {
				for (var d, e = [], f = 0, g = a.length, h = !c; g > f; f++)d = !b(a[f], f), d !== h && e.push(a[f]);
				return e
			}, map: function (a, b, d) {
				var e, f = 0, g = a.length, h = c(a), i = [];
				if (h) {
					for (; g > f; f++)e = b(a[f], f, d), null != e && i.push(e);
				} else {
					for (f in a)e = b(a[f], f, d), null != e && i.push(e);
				}
				return S.apply([], i)
			}, guid: 1, proxy: function (a, b) {
				var c, d, e;
				return "string" == typeof b && (c = a[b], b = a, a = c), _.isFunction(a) ? (d = R.call(arguments, 2), e = function () {
					return a.apply(b || this, d.concat(R.call(arguments)))
				}, e.guid = a.guid = a.guid || _.guid++, e) : void 0
			}, now: Date.now, support: Y
		}), _.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (a, b) {
			V["[object " + b + "]"] = b.toLowerCase()
		});
		var eb = function (a) {
			function b(a, b, c, d) {
				var e, f, g, h, i, j, l, n, o, p;
				if ((b ? b.ownerDocument || b : O) !== G && F(b), b = b || G, c = c || [], !a || "string" != typeof a) {
					return c;
				}
				if (1 !== (h = b.nodeType) && 9 !== h) {
					return [];
				}
				if (I && !d) {
					if (e = sb.exec(a))if (g = e[1]) {
						if (9 === h) {
							if (f = b.getElementById(g), !f || !f.parentNode) {
								return c;
							}
							if (f.id === g)return c.push(f), c
						} else if (b.ownerDocument && (f = b.ownerDocument.getElementById(g)) && M(b, f) && f.id === g)return c.push(f), c
					} else {
						if (e[2])return _.apply(c, b.getElementsByTagName(a)), c;
						if ((g = e[3]) && v.getElementsByClassName && b.getElementsByClassName)return _.apply(c, b.getElementsByClassName(g)), c
					}
					if (v.qsa && (!J || !J.test(a))) {
						if (n = l = N, o = b, p = 9 === h && a, 1 === h && "object" !== b.nodeName.toLowerCase()) {
							for (j = z(a), (l = b.getAttribute("id")) ? n = l.replace(ub, "\\$&") : b.setAttribute("id", n), n = "[id='" + n + "'] ", i = j.length; i--;)j[i] = n + m(j[i]);
							o = tb.test(a) && k(b.parentNode) || b, p = j.join(",")
						}
						if (p)try {
							return _.apply(c, o.querySelectorAll(p)), c
						} catch (q) {
						} finally {
							l || b.removeAttribute("id")
						}
					}
				}
				return B(a.replace(ib, "$1"), b, c, d)
			}

			function c() {
				function a(c, d) {
					return b.push(c + " ") > w.cacheLength && delete a[b.shift()], a[c + " "] = d
				}

				var b = [];
				return a
			}

			function d(a) {
				return a[N] = !0, a
			}

			function e(a) {
				var b = G.createElement("div");
				try {
					return !!a(b)
				} catch (c) {
					return !1
				} finally {
					b.parentNode && b.parentNode.removeChild(b), b = null
				}
			}

			function f(a, b) {
				for (var c = a.split("|"), d = a.length; d--;)w.attrHandle[c[d]] = b
			}

			function g(a, b) {
				var c = b && a, d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || W) - (~a.sourceIndex || W);
				if (d)return d;
				if (c)for (; c = c.nextSibling;)if (c === b)return -1;
				return a ? 1 : -1
			}

			function h(a) {
				return function (b) {
					var c = b.nodeName.toLowerCase();
					return "input" === c && b.type === a
				}
			}

			function i(a) {
				return function (b) {
					var c = b.nodeName.toLowerCase();
					return ("input" === c || "button" === c) && b.type === a
				}
			}

			function j(a) {
				return d(function (b) {
					return b = +b, d(function (c, d) {
						for (var e, f = a([], c.length, b), g = f.length; g--;)c[e = f[g]] && (c[e] = !(d[e] = c[e]))
					})
				})
			}

			function k(a) {
				return a && typeof a.getElementsByTagName !== V && a
			}

			function l() {
			}

			function m(a) {
				for (var b = 0, c = a.length, d = ""; c > b; b++)d += a[b].value;
				return d
			}

			function n(a, b, c) {
				var d = b.dir, e = c && "parentNode" === d, f = Q++;
				return b.first ? function (b, c, f) {
					for (; b = b[d];)if (1 === b.nodeType || e)return a(b, c, f)
				} : function (b, c, g) {
					var h, i, j = [P, f];
					if (g) {
						for (; b = b[d];)if ((1 === b.nodeType || e) && a(b, c, g))return !0
					} else for (; b = b[d];)if (1 === b.nodeType || e) {
						if (i = b[N] || (b[N] = {}), (h = i[d]) && h[0] === P && h[1] === f)return j[2] = h[2];
						if (i[d] = j, j[2] = a(b, c, g))return !0
					}
				}
			}

			function o(a) {
				return a.length > 1 ? function (b, c, d) {
					for (var e = a.length; e--;)if (!a[e](b, c, d))return !1;
					return !0
				} : a[0]
			}

			function p(a, c, d) {
				for (var e = 0, f = c.length; f > e; e++)b(a, c[e], d);
				return d
			}

			function q(a, b, c, d, e) {
				for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++)(f = a[h]) && (!c || c(f, d, e)) && (g.push(f), j && b.push(h));
				return g
			}

			function r(a, b, c, e, f, g) {
				return e && !e[N] && (e = r(e)), f && !f[N] && (f = r(f, g)), d(function (d, g, h, i) {
					var j, k, l, m = [], n = [], o = g.length, r = d || p(b || "*", h.nodeType ? [h] : h, []), s = !a || !d && b ? r : q(r, m, a, h, i), t = c ? f || (d ? a : o || e) ? [] : g : s;
					if (c && c(s, t, h, i), e)for (j = q(t, n), e(j, [], h, i), k = j.length; k--;)(l = j[k]) && (t[n[k]] = !(s[n[k]] = l));
					if (d) {
						if (f || a) {
							if (f) {
								for (j = [], k = t.length; k--;)(l = t[k]) && j.push(s[k] = l);
								f(null, t = [], j, i)
							}
							for (k = t.length; k--;)(l = t[k]) && (j = f ? bb.call(d, l) : m[k]) > -1 && (d[j] = !(g[j] = l))
						}
					} else t = q(t === g ? t.splice(o, t.length) : t), f ? f(null, g, t, i) : _.apply(g, t)
				})
			}

			function s(a) {
				for (var b, c, d, e = a.length, f = w.relative[a[0].type], g = f || w.relative[" "], h = f ? 1 : 0, i = n(function (a) {
					return a === b
				}, g, !0), j = n(function (a) {
					return bb.call(b, a) > -1
				}, g, !0), k = [function (a, c, d) {
					return !f && (d || c !== C) || ((b = c).nodeType ? i(a, c, d) : j(a, c, d))
				}]; e > h; h++)if (c = w.relative[a[h].type])k = [n(o(k), c)]; else {
					if (c = w.filter[a[h].type].apply(null, a[h].matches), c[N]) {
						for (d = ++h; e > d && !w.relative[a[d].type]; d++);
						return r(h > 1 && o(k), h > 1 && m(a.slice(0, h - 1).concat({value: " " === a[h - 2].type ? "*" : ""})).replace(ib, "$1"), c, d > h && s(a.slice(h, d)), e > d && s(a = a.slice(d)), e > d && m(a))
					}
					k.push(c)
				}
				return o(k)
			}

			function t(a, c) {
				var e = c.length > 0, f = a.length > 0, g = function (d, g, h, i, j) {
					var k, l, m, n = 0, o = "0", p = d && [], r = [], s = C, t = d || f && w.find.TAG("*", j), u = P += null == s ? 1 : Math.random() || .1, v = t.length;
					for (j && (C = g !== G && g); o !== v && null != (k = t[o]); o++) {
						if (f && k) {
							for (l = 0; m = a[l++];)if (m(k, g, h)) {
								i.push(k);
								break
							}
							j && (P = u)
						}
						e && ((k = !m && k) && n--, d && p.push(k))
					}
					if (n += o, e && o !== n) {
						for (l = 0; m = c[l++];)m(p, r, g, h);
						if (d) {
							if (n > 0)for (; o--;)p[o] || r[o] || (r[o] = Z.call(i));
							r = q(r)
						}
						_.apply(i, r), j && !d && r.length > 0 && n + c.length > 1 && b.uniqueSort(i)
					}
					return j && (P = u, C = s), p
				};
				return e ? d(g) : g
			}

			var u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N = "sizzle" + -new Date, O = a.document, P = 0, Q = 0, R = c(), S = c(), T = c(), U = function (a, b) {
				return a === b && (E = !0), 0
			}, V = "undefined", W = 1 << 31, X = {}.hasOwnProperty, Y = [], Z = Y.pop, $ = Y.push, _ = Y.push, ab = Y.slice, bb = Y.indexOf || function (a) {
					for (var b = 0, c = this.length; c > b; b++)if (this[b] === a)return b;
					return -1
				}, cb = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", db = "[\\x20\\t\\r\\n\\f]", eb = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", fb = eb.replace("w", "w#"), gb = "\\[" + db + "*(" + eb + ")(?:" + db + "*([*^$|!~]?=)" + db + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + fb + "))|)" + db + "*\\]", hb = ":(" + eb + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + gb + ")*)|.*)\\)|)", ib = new RegExp("^" + db + "+|((?:^|[^\\\\])(?:\\\\.)*)" + db + "+$", "g"), jb = new RegExp("^" + db + "*," + db + "*"), kb = new RegExp("^" + db + "*([>+~]|" + db + ")" + db + "*"), lb = new RegExp("=" + db + "*([^\\]'\"]*?)" + db + "*\\]", "g"), mb = new RegExp(hb), nb = new RegExp("^" + fb + "$"), ob = {
				ID: new RegExp("^#(" + eb + ")"),
				CLASS: new RegExp("^\\.(" + eb + ")"),
				TAG: new RegExp("^(" + eb.replace("w", "w*") + ")"),
				ATTR: new RegExp("^" + gb),
				PSEUDO: new RegExp("^" + hb),
				CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + db + "*(even|odd|(([+-]|)(\\d*)n|)" + db + "*(?:([+-]|)" + db + "*(\\d+)|))" + db + "*\\)|)", "i"),
				bool: new RegExp("^(?:" + cb + ")$", "i"),
				needsContext: new RegExp("^" + db + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + db + "*((?:-\\d)?\\d*)" + db + "*\\)|)(?=[^-]|$)", "i")
			}, pb = /^(?:input|select|textarea|button)$/i, qb = /^h\d$/i, rb = /^[^{]+\{\s*\[native \w/, sb = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, tb = /[+~]/, ub = /'|\\/g, vb = new RegExp("\\\\([\\da-f]{1,6}" + db + "?|(" + db + ")|.)", "ig"), wb = function (a, b, c) {
				var d = "0x" + b - 65536;
				return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
			};
			try {
				_.apply(Y = ab.call(O.childNodes), O.childNodes), Y[O.childNodes.length].nodeType
			} catch (xb) {
				_ = {
					apply: Y.length ? function (a, b) {
						$.apply(a, ab.call(b))
					} : function (a, b) {
						for (var c = a.length, d = 0; a[c++] = b[d++];);
						a.length = c - 1
					}
				}
			}
			v = b.support = {}, y = b.isXML = function (a) {
				var b = a && (a.ownerDocument || a).documentElement;
				return b ? "HTML" !== b.nodeName : !1
			}, F = b.setDocument = function (a) {
				var b, c = a ? a.ownerDocument || a : O, d = c.defaultView;
				return c !== G && 9 === c.nodeType && c.documentElement ? (G = c, H = c.documentElement, I = !y(c), d && d !== d.top && (d.addEventListener ? d.addEventListener("unload", function () {
					F()
				}, !1) : d.attachEvent && d.attachEvent("onunload", function () {
					F()
				})), v.attributes = e(function (a) {
					return a.className = "i", !a.getAttribute("className")
				}), v.getElementsByTagName = e(function (a) {
					return a.appendChild(c.createComment("")), !a.getElementsByTagName("*").length
				}), v.getElementsByClassName = rb.test(c.getElementsByClassName) && e(function (a) {
						return a.innerHTML = "<div class='a'></div><div class='a i'></div>", a.firstChild.className = "i", 2 === a.getElementsByClassName("i").length
					}), v.getById = e(function (a) {
					return H.appendChild(a).id = N, !c.getElementsByName || !c.getElementsByName(N).length
				}), v.getById ? (w.find.ID = function (a, b) {
					if (typeof b.getElementById !== V && I) {
						var c = b.getElementById(a);
						return c && c.parentNode ? [c] : []
					}
				}, w.filter.ID = function (a) {
					var b = a.replace(vb, wb);
					return function (a) {
						return a.getAttribute("id") === b
					}
				}) : (delete w.find.ID, w.filter.ID = function (a) {
					var b = a.replace(vb, wb);
					return function (a) {
						var c = typeof a.getAttributeNode !== V && a.getAttributeNode("id");
						return c && c.value === b
					}
				}), w.find.TAG = v.getElementsByTagName ? function (a, b) {
					return typeof b.getElementsByTagName !== V ? b.getElementsByTagName(a) : void 0
				} : function (a, b) {
					var c, d = [], e = 0, f = b.getElementsByTagName(a);
					if ("*" === a) {
						for (; c = f[e++];)1 === c.nodeType && d.push(c);
						return d
					}
					return f
				}, w.find.CLASS = v.getElementsByClassName && function (a, b) {
						return typeof b.getElementsByClassName !== V && I ? b.getElementsByClassName(a) : void 0
					}, K = [], J = [], (v.qsa = rb.test(c.querySelectorAll)) && (e(function (a) {
					a.innerHTML = "<select msallowclip=''><option selected=''></option></select>", a.querySelectorAll("[msallowclip^='']").length && J.push("[*^$]=" + db + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || J.push("\\[" + db + "*(?:value|" + cb + ")"), a.querySelectorAll(":checked").length || J.push(":checked")
				}), e(function (a) {
					var b = c.createElement("input");
					b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && J.push("name" + db + "*[*^$|!~]?="), a.querySelectorAll(":enabled").length || J.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), J.push(",.*:")
				})), (v.matchesSelector = rb.test(L = H.matches || H.webkitMatchesSelector || H.mozMatchesSelector || H.oMatchesSelector || H.msMatchesSelector)) && e(function (a) {
					v.disconnectedMatch = L.call(a, "div"), L.call(a, "[s!='']:x"), K.push("!=", hb)
				}), J = J.length && new RegExp(J.join("|")), K = K.length && new RegExp(K.join("|")), b = rb.test(H.compareDocumentPosition), M = b || rb.test(H.contains) ? function (a, b) {
					var c = 9 === a.nodeType ? a.documentElement : a, d = b && b.parentNode;
					return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
				} : function (a, b) {
					if (b)for (; b = b.parentNode;)if (b === a)return !0;
					return !1
				}, U = b ? function (a, b) {
					if (a === b)return E = !0, 0;
					var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
					return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !v.sortDetached && b.compareDocumentPosition(a) === d ? a === c || a.ownerDocument === O && M(O, a) ? -1 : b === c || b.ownerDocument === O && M(O, b) ? 1 : D ? bb.call(D, a) - bb.call(D, b) : 0 : 4 & d ? -1 : 1)
				} : function (a, b) {
					if (a === b)return E = !0, 0;
					var d, e = 0, f = a.parentNode, h = b.parentNode, i = [a], j = [b];
					if (!f || !h)return a === c ? -1 : b === c ? 1 : f ? -1 : h ? 1 : D ? bb.call(D, a) - bb.call(D, b) : 0;
					if (f === h)return g(a, b);
					for (d = a; d = d.parentNode;)i.unshift(d);
					for (d = b; d = d.parentNode;)j.unshift(d);
					for (; i[e] === j[e];)e++;
					return e ? g(i[e], j[e]) : i[e] === O ? -1 : j[e] === O ? 1 : 0
				}, c) : G
			}, b.matches = function (a, c) {
				return b(a, null, null, c)
			}, b.matchesSelector = function (a, c) {
				if ((a.ownerDocument || a) !== G && F(a), c = c.replace(lb, "='$1']"), !(!v.matchesSelector || !I || K && K.test(c) || J && J.test(c)))try {
					var d = L.call(a, c);
					if (d || v.disconnectedMatch || a.document && 11 !== a.document.nodeType)return d
				} catch (e) {
				}
				return b(c, G, null, [a]).length > 0
			}, b.contains = function (a, b) {
				return (a.ownerDocument || a) !== G && F(a), M(a, b)
			}, b.attr = function (a, b) {
				(a.ownerDocument || a) !== G && F(a);
				var c = w.attrHandle[b.toLowerCase()], d = c && X.call(w.attrHandle, b.toLowerCase()) ? c(a, b, !I) : void 0;
				return void 0 !== d ? d : v.attributes || !I ? a.getAttribute(b) : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
			}, b.error = function (a) {
				throw new Error("Syntax error, unrecognized expression: " + a)
			}, b.uniqueSort = function (a) {
				var b, c = [], d = 0, e = 0;
				if (E = !v.detectDuplicates, D = !v.sortStable && a.slice(0), a.sort(U), E) {
					for (; b = a[e++];)b === a[e] && (d = c.push(e));
					for (; d--;)a.splice(c[d], 1)
				}
				return D = null, a
			}, x = b.getText = function (a) {
				var b, c = "", d = 0, e = a.nodeType;
				if (e) {
					if (1 === e || 9 === e || 11 === e) {
						if ("string" == typeof a.textContent)return a.textContent;
						for (a = a.firstChild; a; a = a.nextSibling)c += x(a)
					} else if (3 === e || 4 === e)return a.nodeValue
				} else for (; b = a[d++];)c += x(b);
				return c
			}, w = b.selectors = {
				cacheLength: 50,
				createPseudo: d,
				match: ob,
				attrHandle: {},
				find: {},
				relative: {
					">": {dir: "parentNode", first: !0},
					" ": {dir: "parentNode"},
					"+": {dir: "previousSibling", first: !0},
					"~": {dir: "previousSibling"}
				},
				preFilter: {
					ATTR: function (a) {
						return a[1] = a[1].replace(vb, wb), a[3] = (a[3] || a[4] || a[5] || "").replace(vb, wb), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
					}, CHILD: function (a) {
						return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || b.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && b.error(a[0]), a
					}, PSEUDO: function (a) {
						var b, c = !a[6] && a[2];
						return ob.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && mb.test(c) && (b = z(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
					}
				},
				filter: {
					TAG: function (a) {
						var b = a.replace(vb, wb).toLowerCase();
						return "*" === a ? function () {
							return !0
						} : function (a) {
							return a.nodeName && a.nodeName.toLowerCase() === b
						}
					}, CLASS: function (a) {
						var b = R[a + " "];
						return b || (b = new RegExp("(^|" + db + ")" + a + "(" + db + "|$)")) && R(a, function (a) {
								return b.test("string" == typeof a.className && a.className || typeof a.getAttribute !== V && a.getAttribute("class") || "")
							})
					}, ATTR: function (a, c, d) {
						return function (e) {
							var f = b.attr(e, a);
							return null == f ? "!=" === c : c ? (f += "", "=" === c ? f === d : "!=" === c ? f !== d : "^=" === c ? d && 0 === f.indexOf(d) : "*=" === c ? d && f.indexOf(d) > -1 : "$=" === c ? d && f.slice(-d.length) === d : "~=" === c ? (" " + f + " ").indexOf(d) > -1 : "|=" === c ? f === d || f.slice(0, d.length + 1) === d + "-" : !1) : !0
						}
					}, CHILD: function (a, b, c, d, e) {
						var f = "nth" !== a.slice(0, 3), g = "last" !== a.slice(-4), h = "of-type" === b;
						return 1 === d && 0 === e ? function (a) {
							return !!a.parentNode
						} : function (b, c, i) {
							var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h;
							if (q) {
								if (f) {
									for (; p;) {
										for (l = b; l = l[p];)if (h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType)return !1;
										o = p = "only" === a && !o && "nextSibling"
									}
									return !0
								}
								if (o = [g ? q.firstChild : q.lastChild], g && s) {
									for (k = q[N] || (q[N] = {}), j = k[a] || [], n = j[0] === P && j[1], m = j[0] === P && j[2], l = n && q.childNodes[n]; l = ++n && l && l[p] || (m = n = 0) || o.pop();)if (1 === l.nodeType && ++m && l === b) {
										k[a] = [P, n, m];
										break
									}
								} else if (s && (j = (b[N] || (b[N] = {}))[a]) && j[0] === P)m = j[1]; else for (; (l = ++n && l && l[p] || (m = n = 0) || o.pop()) && ((h ? l.nodeName.toLowerCase() !== r : 1 !== l.nodeType) || !++m || (s && ((l[N] || (l[N] = {}))[a] = [P, m]), l !== b)););
								return m -= e, m === d || m % d === 0 && m / d >= 0
							}
						}
					}, PSEUDO: function (a, c) {
						var e, f = w.pseudos[a] || w.setFilters[a.toLowerCase()] || b.error("unsupported pseudo: " + a);
						return f[N] ? f(c) : f.length > 1 ? (e = [a, a, "", c], w.setFilters.hasOwnProperty(a.toLowerCase()) ? d(function (a, b) {
							for (var d, e = f(a, c), g = e.length; g--;)d = bb.call(a, e[g]), a[d] = !(b[d] = e[g])
						}) : function (a) {
							return f(a, 0, e)
						}) : f
					}
				},
				pseudos: {
					not: d(function (a) {
						var b = [], c = [], e = A(a.replace(ib, "$1"));
						return e[N] ? d(function (a, b, c, d) {
							for (var f, g = e(a, null, d, []), h = a.length; h--;)(f = g[h]) && (a[h] = !(b[h] = f))
						}) : function (a, d, f) {
							return b[0] = a, e(b, null, f, c), !c.pop()
						}
					}), has: d(function (a) {
						return function (c) {
							return b(a, c).length > 0
						}
					}), contains: d(function (a) {
						return function (b) {
							return (b.textContent || b.innerText || x(b)).indexOf(a) > -1
						}
					}), lang: d(function (a) {
						return nb.test(a || "") || b.error("unsupported lang: " + a), a = a.replace(vb, wb).toLowerCase(), function (b) {
							var c;
							do if (c = I ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang"))return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType);
							return !1
						}
					}), target: function (b) {
						var c = a.location && a.location.hash;
						return c && c.slice(1) === b.id
					}, root: function (a) {
						return a === H
					}, focus: function (a) {
						return a === G.activeElement && (!G.hasFocus || G.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
					}, enabled: function (a) {
						return a.disabled === !1
					}, disabled: function (a) {
						return a.disabled === !0
					}, checked: function (a) {
						var b = a.nodeName.toLowerCase();
						return "input" === b && !!a.checked || "option" === b && !!a.selected
					}, selected: function (a) {
						return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
					}, empty: function (a) {
						for (a = a.firstChild; a; a = a.nextSibling)if (a.nodeType < 6)return !1;
						return !0
					}, parent: function (a) {
						return !w.pseudos.empty(a)
					}, header: function (a) {
						return qb.test(a.nodeName)
					}, input: function (a) {
						return pb.test(a.nodeName)
					}, button: function (a) {
						var b = a.nodeName.toLowerCase();
						return "input" === b && "button" === a.type || "button" === b
					}, text: function (a) {
						var b;
						return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
					}, first: j(function () {
						return [0]
					}), last: j(function (a, b) {
						return [b - 1]
					}), eq: j(function (a, b, c) {
						return [0 > c ? c + b : c]
					}), even: j(function (a, b) {
						for (var c = 0; b > c; c += 2)a.push(c);
						return a
					}), odd: j(function (a, b) {
						for (var c = 1; b > c; c += 2)a.push(c);
						return a
					}), lt: j(function (a, b, c) {
						for (var d = 0 > c ? c + b : c; --d >= 0;)a.push(d);
						return a
					}), gt: j(function (a, b, c) {
						for (var d = 0 > c ? c + b : c; ++d < b;)a.push(d);
						return a
					})
				}
			}, w.pseudos.nth = w.pseudos.eq;
			for (u in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0})w.pseudos[u] = h(u);
			for (u in{submit: !0, reset: !0})w.pseudos[u] = i(u);
			return l.prototype = w.filters = w.pseudos, w.setFilters = new l, z = b.tokenize = function (a, c) {
				var d, e, f, g, h, i, j, k = S[a + " "];
				if (k)return c ? 0 : k.slice(0);
				for (h = a, i = [], j = w.preFilter; h;) {
					(!d || (e = jb.exec(h))) && (e && (h = h.slice(e[0].length) || h), i.push(f = [])), d = !1, (e = kb.exec(h)) && (d = e.shift(), f.push({
						value: d,
						type: e[0].replace(ib, " ")
					}), h = h.slice(d.length));
					for (g in w.filter)!(e = ob[g].exec(h)) || j[g] && !(e = j[g](e)) || (d = e.shift(), f.push({
						value: d,
						type: g,
						matches: e
					}), h = h.slice(d.length));
					if (!d)break
				}
				return c ? h.length : h ? b.error(a) : S(a, i).slice(0)
			}, A = b.compile = function (a, b) {
				var c, d = [], e = [], f = T[a + " "];
				if (!f) {
					for (b || (b = z(a)), c = b.length; c--;)f = s(b[c]), f[N] ? d.push(f) : e.push(f);
					f = T(a, t(e, d)), f.selector = a
				}
				return f
			}, B = b.select = function (a, b, c, d) {
				var e, f, g, h, i, j = "function" == typeof a && a, l = !d && z(a = j.selector || a);
				if (c = c || [], 1 === l.length) {
					if (f = l[0] = l[0].slice(0), f.length > 2 && "ID" === (g = f[0]).type && v.getById && 9 === b.nodeType && I && w.relative[f[1].type]) {
						if (b = (w.find.ID(g.matches[0].replace(vb, wb), b) || [])[0], !b)return c;
						j && (b = b.parentNode), a = a.slice(f.shift().value.length)
					}
					for (e = ob.needsContext.test(a) ? 0 : f.length; e-- && (g = f[e], !w.relative[h = g.type]);)if ((i = w.find[h]) && (d = i(g.matches[0].replace(vb, wb), tb.test(f[0].type) && k(b.parentNode) || b))) {
						if (f.splice(e, 1), a = d.length && m(f), !a)return _.apply(c, d), c;
						break
					}
				}
				return (j || A(a, l))(d, b, !I, c, tb.test(a) && k(b.parentNode) || b), c
			}, v.sortStable = N.split("").sort(U).join("") === N, v.detectDuplicates = !!E, F(), v.sortDetached = e(function (a) {
				return 1 & a.compareDocumentPosition(G.createElement("div"))
			}), e(function (a) {
				return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
			}) || f("type|href|height|width", function (a, b, c) {
				return c ? void 0 : a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
			}), v.attributes && e(function (a) {
				return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
			}) || f("value", function (a, b, c) {
				return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue
			}), e(function (a) {
				return null == a.getAttribute("disabled")
			}) || f(cb, function (a, b, c) {
				var d;
				return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
			}), b
		}(a);
		_.find = eb, _.expr = eb.selectors, _.expr[":"] = _.expr.pseudos, _.unique = eb.uniqueSort, _.text = eb.getText, _.isXMLDoc = eb.isXML, _.contains = eb.contains;
		var fb = _.expr.match.needsContext, gb = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, hb = /^.[^:#\[\.,]*$/;
		_.filter = function (a, b, c) {
			var d = b[0];
			return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? _.find.matchesSelector(d, a) ? [d] : [] : _.find.matches(a, _.grep(b, function (a) {
				return 1 === a.nodeType
			}))
		}, _.fn.extend({
			find: function (a) {
				var b, c = this.length, d = [], e = this;
				if ("string" != typeof a)return this.pushStack(_(a).filter(function () {
					for (b = 0; c > b; b++)if (_.contains(e[b], this))return !0
				}));
				for (b = 0; c > b; b++)_.find(a, e[b], d);
				return d = this.pushStack(c > 1 ? _.unique(d) : d), d.selector = this.selector ? this.selector + " " + a : a, d
			}, filter: function (a) {
				return this.pushStack(d(this, a || [], !1))
			}, not: function (a) {
				return this.pushStack(d(this, a || [], !0))
			}, is: function (a) {
				return !!d(this, "string" == typeof a && fb.test(a) ? _(a) : a || [], !1).length
			}
		});
		var ib, jb = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, kb = _.fn.init = function (a, b) {
			var c, d;
			if (!a)return this;
			if ("string" == typeof a) {
				if (c = "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : jb.exec(a), !c || !c[1] && b)return !b || b.jquery ? (b || ib).find(a) : this.constructor(b).find(a);
				if (c[1]) {
					if (b = b instanceof _ ? b[0] : b, _.merge(this, _.parseHTML(c[1], b && b.nodeType ? b.ownerDocument || b : Z, !0)), gb.test(c[1]) && _.isPlainObject(b))for (c in b)_.isFunction(this[c]) ? this[c](b[c]) : this.attr(c, b[c]);
					return this
				}
				return d = Z.getElementById(c[2]), d && d.parentNode && (this.length = 1, this[0] = d), this.context = Z, this.selector = a, this
			}
			return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : _.isFunction(a) ? "undefined" != typeof ib.ready ? ib.ready(a) : a(_) : (void 0 !== a.selector && (this.selector = a.selector, this.context = a.context), _.makeArray(a, this))
		};
		kb.prototype = _.fn, ib = _(Z);
		var lb = /^(?:parents|prev(?:Until|All))/, mb = {children: !0, contents: !0, next: !0, prev: !0};
		_.extend({
			dir: function (a, b, c) {
				for (var d = [], e = void 0 !== c; (a = a[b]) && 9 !== a.nodeType;)if (1 === a.nodeType) {
					if (e && _(a).is(c))break;
					d.push(a)
				}
				return d
			}, sibling: function (a, b) {
				for (var c = []; a; a = a.nextSibling)1 === a.nodeType && a !== b && c.push(a);
				return c
			}
		}), _.fn.extend({
			has: function (a) {
				var b = _(a, this), c = b.length;
				return this.filter(function () {
					for (var a = 0; c > a; a++)if (_.contains(this, b[a]))return !0
				})
			}, closest: function (a, b) {
				for (var c, d = 0, e = this.length, f = [], g = fb.test(a) || "string" != typeof a ? _(a, b || this.context) : 0; e > d; d++)for (c = this[d]; c && c !== b; c = c.parentNode)if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && _.find.matchesSelector(c, a))) {
					f.push(c);
					break
				}
				return this.pushStack(f.length > 1 ? _.unique(f) : f)
			}, index: function (a) {
				return a ? "string" == typeof a ? U.call(_(a), this[0]) : U.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
			}, add: function (a, b) {
				return this.pushStack(_.unique(_.merge(this.get(), _(a, b))))
			}, addBack: function (a) {
				return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
			}
		}), _.each({
			parent: function (a) {
				var b = a.parentNode;
				return b && 11 !== b.nodeType ? b : null
			}, parents: function (a) {
				return _.dir(a, "parentNode")
			}, parentsUntil: function (a, b, c) {
				return _.dir(a, "parentNode", c)
			}, next: function (a) {
				return e(a, "nextSibling")
			}, prev: function (a) {
				return e(a, "previousSibling")
			}, nextAll: function (a) {
				return _.dir(a, "nextSibling")
			}, prevAll: function (a) {
				return _.dir(a, "previousSibling")
			}, nextUntil: function (a, b, c) {
				return _.dir(a, "nextSibling", c)
			}, prevUntil: function (a, b, c) {
				return _.dir(a, "previousSibling", c)
			}, siblings: function (a) {
				return _.sibling((a.parentNode || {}).firstChild, a)
			}, children: function (a) {
				return _.sibling(a.firstChild)
			}, contents: function (a) {
				return a.contentDocument || _.merge([], a.childNodes)
			}
		}, function (a, b) {
			_.fn[a] = function (c, d) {
				var e = _.map(this, b, c);
				return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = _.filter(d, e)), this.length > 1 && (mb[a] || _.unique(e), lb.test(a) && e.reverse()), this.pushStack(e)
			}
		});
		var nb = /\S+/g, ob = {};
		_.Callbacks = function (a) {
			a = "string" == typeof a ? ob[a] || f(a) : _.extend({}, a);
			var b, c, d, e, g, h, i = [], j = !a.once && [], k = function (f) {
				for (b = a.memory && f, c = !0, h = e || 0, e = 0, g = i.length, d = !0; i && g > h; h++)if (i[h].apply(f[0], f[1]) === !1 && a.stopOnFalse) {
					b = !1;
					break
				}
				d = !1, i && (j ? j.length && k(j.shift()) : b ? i = [] : l.disable())
			}, l = {
				add: function () {
					if (i) {
						var c = i.length;
						!function f(b) {
							_.each(b, function (b, c) {
								var d = _.type(c);
								"function" === d ? a.unique && l.has(c) || i.push(c) : c && c.length && "string" !== d && f(c)
							})
						}(arguments), d ? g = i.length : b && (e = c, k(b))
					}
					return this
				}, remove: function () {
					return i && _.each(arguments, function (a, b) {
						for (var c; (c = _.inArray(b, i, c)) > -1;)i.splice(c, 1), d && (g >= c && g--, h >= c && h--)
					}), this
				}, has: function (a) {
					return a ? _.inArray(a, i) > -1 : !(!i || !i.length)
				}, empty: function () {
					return i = [], g = 0, this
				}, disable: function () {
					return i = j = b = void 0, this
				}, disabled: function () {
					return !i
				}, lock: function () {
					return j = void 0, b || l.disable(), this
				}, locked: function () {
					return !j
				}, fireWith: function (a, b) {
					return !i || c && !j || (b = b || [], b = [a, b.slice ? b.slice() : b], d ? j.push(b) : k(b)), this
				}, fire: function () {
					return l.fireWith(this, arguments), this
				}, fired: function () {
					return !!c
				}
			};
			return l
		}, _.extend({
			Deferred: function (a) {
				var b = [["resolve", "done", _.Callbacks("once memory"), "resolved"], ["reject", "fail", _.Callbacks("once memory"), "rejected"], ["notify", "progress", _.Callbacks("memory")]], c = "pending", d = {
					state: function () {
						return c
					}, always: function () {
						return e.done(arguments).fail(arguments), this
					}, then: function () {
						var a = arguments;
						return _.Deferred(function (c) {
							_.each(b, function (b, f) {
								var g = _.isFunction(a[b]) && a[b];
								e[f[1]](function () {
									var a = g && g.apply(this, arguments);
									a && _.isFunction(a.promise) ? a.promise().done(c.resolve).fail(c.reject).progress(c.notify) : c[f[0] + "With"](this === d ? c.promise() : this, g ? [a] : arguments)
								})
							}), a = null
						}).promise()
					}, promise: function (a) {
						return null != a ? _.extend(a, d) : d
					}
				}, e = {};
				return d.pipe = d.then, _.each(b, function (a, f) {
					var g = f[2], h = f[3];
					d[f[1]] = g.add, h && g.add(function () {
						c = h
					}, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function () {
						return e[f[0] + "With"](this === e ? d : this, arguments), this
					}, e[f[0] + "With"] = g.fireWith
				}), d.promise(e), a && a.call(e, e), e
			}, when: function (a) {
				var b, c, d, e = 0, f = R.call(arguments), g = f.length, h = 1 !== g || a && _.isFunction(a.promise) ? g : 0, i = 1 === h ? a : _.Deferred(), j = function (a, c, d) {
					return function (e) {
						c[a] = this, d[a] = arguments.length > 1 ? R.call(arguments) : e, d === b ? i.notifyWith(c, d) : --h || i.resolveWith(c, d)
					}
				};
				if (g > 1)for (b = new Array(g), c = new Array(g), d = new Array(g); g > e; e++)f[e] && _.isFunction(f[e].promise) ? f[e].promise().done(j(e, d, f)).fail(i.reject).progress(j(e, c, b)) : --h;
				return h || i.resolveWith(d, f), i.promise()
			}
		});
		var pb;
		_.fn.ready = function (a) {
			return _.ready.promise().done(a), this
		}, _.extend({
			isReady: !1, readyWait: 1, holdReady: function (a) {
				a ? _.readyWait++ : _.ready(!0)
			}, ready: function (a) {
				(a === !0 ? --_.readyWait : _.isReady) || (_.isReady = !0, a !== !0 && --_.readyWait > 0 || (pb.resolveWith(Z, [_]), _.fn.triggerHandler && (_(Z).triggerHandler("ready"), _(Z).off("ready"))))
			}
		}), _.ready.promise = function (b) {
			return pb || (pb = _.Deferred(), "complete" === Z.readyState ? setTimeout(_.ready) : (Z.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1))), pb.promise(b)
		}, _.ready.promise();
		var qb = _.access = function (a, b, c, d, e, f, g) {
			var h = 0, i = a.length, j = null == c;
			if ("object" === _.type(c)) {
				e = !0;
				for (h in c)_.access(a, b, h, c[h], !0, f, g)
			} else if (void 0 !== d && (e = !0, _.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function (a, b, c) {
					return j.call(_(a), c)
				})), b))for (; i > h; h++)b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
			return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
		};
		_.acceptData = function (a) {
			return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType
		}, h.uid = 1, h.accepts = _.acceptData, h.prototype = {
			key: function (a) {
				if (!h.accepts(a))return 0;
				var b = {}, c = a[this.expando];
				if (!c) {
					c = h.uid++;
					try {
						b[this.expando] = {value: c}, Object.defineProperties(a, b)
					} catch (d) {
						b[this.expando] = c, _.extend(a, b)
					}
				}
				return this.cache[c] || (this.cache[c] = {}), c
			}, set: function (a, b, c) {
				var d, e = this.key(a), f = this.cache[e];
				if ("string" == typeof b)f[b] = c; else if (_.isEmptyObject(f))_.extend(this.cache[e], b); else for (d in b)f[d] = b[d];
				return f
			}, get: function (a, b) {
				var c = this.cache[this.key(a)];
				return void 0 === b ? c : c[b]
			}, access: function (a, b, c) {
				var d;
				return void 0 === b || b && "string" == typeof b && void 0 === c ? (d = this.get(a, b), void 0 !== d ? d : this.get(a, _.camelCase(b))) : (this.set(a, b, c), void 0 !== c ? c : b)
			}, remove: function (a, b) {
				var c, d, e, f = this.key(a), g = this.cache[f];
				if (void 0 === b)this.cache[f] = {}; else {
					_.isArray(b) ? d = b.concat(b.map(_.camelCase)) : (e = _.camelCase(b), b in g ? d = [b, e] : (d = e, d = d in g ? [d] : d.match(nb) || [])), c = d.length;
					for (; c--;)delete g[d[c]]
				}
			}, hasData: function (a) {
				return !_.isEmptyObject(this.cache[a[this.expando]] || {})
			}, discard: function (a) {
				a[this.expando] && delete this.cache[a[this.expando]]
			}
		};
		var rb = new h, sb = new h, tb = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, ub = /([A-Z])/g;
		_.extend({
			hasData: function (a) {
				return sb.hasData(a) || rb.hasData(a)
			}, data: function (a, b, c) {
				return sb.access(a, b, c)
			}, removeData: function (a, b) {
				sb.remove(a, b)
			}, _data: function (a, b, c) {
				return rb.access(a, b, c)
			}, _removeData: function (a, b) {
				rb.remove(a, b)
			}
		}), _.fn.extend({
			data: function (a, b) {
				var c, d, e, f = this[0], g = f && f.attributes;
				if (void 0 === a) {
					if (this.length && (e = sb.get(f), 1 === f.nodeType && !rb.get(f, "hasDataAttrs"))) {
						for (c = g.length; c--;)g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = _.camelCase(d.slice(5)), i(f, d, e[d])));
						rb.set(f, "hasDataAttrs", !0)
					}
					return e
				}
				return "object" == typeof a ? this.each(function () {
					sb.set(this, a)
				}) : qb(this, function (b) {
					var c, d = _.camelCase(a);
					if (f && void 0 === b) {
						if (c = sb.get(f, a), void 0 !== c)return c;
						if (c = sb.get(f, d), void 0 !== c)return c;
						if (c = i(f, d, void 0), void 0 !== c)return c
					} else this.each(function () {
						var c = sb.get(this, d);
						sb.set(this, d, b), -1 !== a.indexOf("-") && void 0 !== c && sb.set(this, a, b)
					})
				}, null, b, arguments.length > 1, null, !0)
			}, removeData: function (a) {
				return this.each(function () {
					sb.remove(this, a)
				})
			}
		}), _.extend({
			queue: function (a, b, c) {
				var d;
				return a ? (b = (b || "fx") + "queue", d = rb.get(a, b), c && (!d || _.isArray(c) ? d = rb.access(a, b, _.makeArray(c)) : d.push(c)), d || []) : void 0
			}, dequeue: function (a, b) {
				b = b || "fx";
				var c = _.queue(a, b), d = c.length, e = c.shift(), f = _._queueHooks(a, b), g = function () {
					_.dequeue(a, b)
				};
				"inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
			}, _queueHooks: function (a, b) {
				var c = b + "queueHooks";
				return rb.get(a, c) || rb.access(a, c, {
						empty: _.Callbacks("once memory").add(function () {
							rb.remove(a, [b + "queue", c])
						})
					})
			}
		}), _.fn.extend({
			queue: function (a, b) {
				var c = 2;
				return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? _.queue(this[0], a) : void 0 === b ? this : this.each(function () {
					var c = _.queue(this, a, b);
					_._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && _.dequeue(this, a)
				})
			}, dequeue: function (a) {
				return this.each(function () {
					_.dequeue(this, a)
				})
			}, clearQueue: function (a) {
				return this.queue(a || "fx", [])
			}, promise: function (a, b) {
				var c, d = 1, e = _.Deferred(), f = this, g = this.length, h = function () {
					--d || e.resolveWith(f, [f])
				};
				for ("string" != typeof a && (b = a, a = void 0), a = a || "fx"; g--;)c = rb.get(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
				return h(), e.promise(b)
			}
		});
		var vb = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, wb = ["Top", "Right", "Bottom", "Left"], xb = function (a, b) {
			return a = b || a, "none" === _.css(a, "display") || !_.contains(a.ownerDocument, a)
		}, yb = /^(?:checkbox|radio)$/i;
		!function () {
			var a = Z.createDocumentFragment(), b = a.appendChild(Z.createElement("div")), c = Z.createElement("input");
			c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), b.appendChild(c), Y.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, b.innerHTML = "<textarea>x</textarea>", Y.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue
		}();
		var zb = "undefined";
		Y.focusinBubbles = "onfocusin" in a;
		var Ab = /^key/, Bb = /^(?:mouse|pointer|contextmenu)|click/, Cb = /^(?:focusinfocus|focusoutblur)$/, Db = /^([^.]*)(?:\.(.+)|)$/;
		_.event = {
			global: {},
			add: function (a, b, c, d, e) {
				var f, g, h, i, j, k, l, m, n, o, p, q = rb.get(a);
				if (q)for (c.handler && (f = c, c = f.handler, e = f.selector), c.guid || (c.guid = _.guid++), (i = q.events) || (i = q.events = {}), (g = q.handle) || (g = q.handle = function (b) {
					return typeof _ !== zb && _.event.triggered !== b.type ? _.event.dispatch.apply(a, arguments) : void 0
				}), b = (b || "").match(nb) || [""], j = b.length; j--;)h = Db.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n && (l = _.event.special[n] || {}, n = (e ? l.delegateType : l.bindType) || n, l = _.event.special[n] || {}, k = _.extend({
					type: n,
					origType: p,
					data: d,
					handler: c,
					guid: c.guid,
					selector: e,
					needsContext: e && _.expr.match.needsContext.test(e),
					namespace: o.join(".")
				}, f), (m = i[n]) || (m = i[n] = [], m.delegateCount = 0, l.setup && l.setup.call(a, d, o, g) !== !1 || a.addEventListener && a.addEventListener(n, g, !1)), l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), _.event.global[n] = !0)
			},
			remove: function (a, b, c, d, e) {
				var f, g, h, i, j, k, l, m, n, o, p, q = rb.hasData(a) && rb.get(a);
				if (q && (i = q.events)) {
					for (b = (b || "").match(nb) || [""], j = b.length; j--;)if (h = Db.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
						for (l = _.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length; f--;)k = m[f], !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
						g && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || _.removeEvent(a, n, q.handle), delete i[n])
					} else for (n in i)_.event.remove(a, n + b[j], c, d, !0);
					_.isEmptyObject(i) && (delete q.handle, rb.remove(a, "events"))
				}
			},
			trigger: function (b, c, d, e) {
				var f, g, h, i, j, k, l, m = [d || Z], n = X.call(b, "type") ? b.type : b, o = X.call(b, "namespace") ? b.namespace.split(".") : [];
				if (g = h = d = d || Z, 3 !== d.nodeType && 8 !== d.nodeType && !Cb.test(n + _.event.triggered) && (n.indexOf(".") >= 0 && (o = n.split("."), n = o.shift(), o.sort()), j = n.indexOf(":") < 0 && "on" + n, b = b[_.expando] ? b : new _.Event(n, "object" == typeof b && b), b.isTrigger = e ? 2 : 3, b.namespace = o.join("."), b.namespace_re = b.namespace ? new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = d), c = null == c ? [b] : _.makeArray(c, [b]), l = _.event.special[n] || {}, e || !l.trigger || l.trigger.apply(d, c) !== !1)) {
					if (!e && !l.noBubble && !_.isWindow(d)) {
						for (i = l.delegateType || n, Cb.test(i + n) || (g = g.parentNode); g; g = g.parentNode)m.push(g), h = g;
						h === (d.ownerDocument || Z) && m.push(h.defaultView || h.parentWindow || a)
					}
					for (f = 0; (g = m[f++]) && !b.isPropagationStopped();)b.type = f > 1 ? i : l.bindType || n, k = (rb.get(g, "events") || {})[b.type] && rb.get(g, "handle"), k && k.apply(g, c), k = j && g[j], k && k.apply && _.acceptData(g) && (b.result = k.apply(g, c), b.result === !1 && b.preventDefault());
					return b.type = n, e || b.isDefaultPrevented() || l._default && l._default.apply(m.pop(), c) !== !1 || !_.acceptData(d) || j && _.isFunction(d[n]) && !_.isWindow(d) && (h = d[j], h && (d[j] = null), _.event.triggered = n, d[n](), _.event.triggered = void 0, h && (d[j] = h)), b.result
				}
			},
			dispatch: function (a) {
				a = _.event.fix(a);
				var b, c, d, e, f, g = [], h = R.call(arguments), i = (rb.get(this, "events") || {})[a.type] || [], j = _.event.special[a.type] || {};
				if (h[0] = a, a.delegateTarget = this, !j.preDispatch || j.preDispatch.call(this, a) !== !1) {
					for (g = _.event.handlers.call(this, a, i), b = 0; (e = g[b++]) && !a.isPropagationStopped();)for (a.currentTarget = e.elem, c = 0; (f = e.handlers[c++]) && !a.isImmediatePropagationStopped();)(!a.namespace_re || a.namespace_re.test(f.namespace)) && (a.handleObj = f, a.data = f.data, d = ((_.event.special[f.origType] || {}).handle || f.handler).apply(e.elem, h), void 0 !== d && (a.result = d) === !1 && (a.preventDefault(), a.stopPropagation()));
					return j.postDispatch && j.postDispatch.call(this, a), a.result
				}
			},
			handlers: function (a, b) {
				var c, d, e, f, g = [], h = b.delegateCount, i = a.target;
				if (h && i.nodeType && (!a.button || "click" !== a.type))for (; i !== this; i = i.parentNode || this)if (i.disabled !== !0 || "click" !== a.type) {
					for (d = [], c = 0; h > c; c++)f = b[c], e = f.selector + " ", void 0 === d[e] && (d[e] = f.needsContext ? _(e, this).index(i) >= 0 : _.find(e, this, null, [i]).length), d[e] && d.push(f);
					d.length && g.push({elem: i, handlers: d})
				}
				return h < b.length && g.push({elem: this, handlers: b.slice(h)}), g
			},
			props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
			fixHooks: {},
			keyHooks: {
				props: "char charCode key keyCode".split(" "), filter: function (a, b) {
					return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a
				}
			},
			mouseHooks: {
				props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
				filter: function (a, b) {
					var c, d, e, f = b.button;
					return null == a.pageX && null != b.clientX && (c = a.target.ownerDocument || Z, d = c.documentElement, e = c.body, a.pageX = b.clientX + (d && d.scrollLeft || e && e.scrollLeft || 0) - (d && d.clientLeft || e && e.clientLeft || 0), a.pageY = b.clientY + (d && d.scrollTop || e && e.scrollTop || 0) - (d && d.clientTop || e && e.clientTop || 0)), a.which || void 0 === f || (a.which = 1 & f ? 1 : 2 & f ? 3 : 4 & f ? 2 : 0), a
				}
			},
			fix: function (a) {
				if (a[_.expando])return a;
				var b, c, d, e = a.type, f = a, g = this.fixHooks[e];
				for (g || (this.fixHooks[e] = g = Bb.test(e) ? this.mouseHooks : Ab.test(e) ? this.keyHooks : {}), d = g.props ? this.props.concat(g.props) : this.props, a = new _.Event(f), b = d.length; b--;)c = d[b], a[c] = f[c];
				return a.target || (a.target = Z), 3 === a.target.nodeType && (a.target = a.target.parentNode), g.filter ? g.filter(a, f) : a
			},
			special: {
				load: {noBubble: !0}, focus: {
					trigger: function () {
						return this !== l() && this.focus ? (this.focus(), !1) : void 0
					}, delegateType: "focusin"
				}, blur: {
					trigger: function () {
						return this === l() && this.blur ? (this.blur(), !1) : void 0
					}, delegateType: "focusout"
				}, click: {
					trigger: function () {
						return "checkbox" === this.type && this.click && _.nodeName(this, "input") ? (this.click(), !1) : void 0
					}, _default: function (a) {
						return _.nodeName(a.target, "a")
					}
				}, beforeunload: {
					postDispatch: function (a) {
						void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
					}
				}
			},
			simulate: function (a, b, c, d) {
				var e = _.extend(new _.Event, c, {type: a, isSimulated: !0, originalEvent: {}});
				d ? _.event.trigger(e, null, b) : _.event.dispatch.call(b, e), e.isDefaultPrevented() && c.preventDefault()
			}
		}, _.removeEvent = function (a, b, c) {
			a.removeEventListener && a.removeEventListener(b, c, !1)
		}, _.Event = function (a, b) {
			return this instanceof _.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? j : k) : this.type = a, b && _.extend(this, b), this.timeStamp = a && a.timeStamp || _.now(), void(this[_.expando] = !0)) : new _.Event(a, b)
		}, _.Event.prototype = {
			isDefaultPrevented: k,
			isPropagationStopped: k,
			isImmediatePropagationStopped: k,
			preventDefault: function () {
				var a = this.originalEvent;
				this.isDefaultPrevented = j, a && a.preventDefault && a.preventDefault()
			},
			stopPropagation: function () {
				var a = this.originalEvent;
				this.isPropagationStopped = j, a && a.stopPropagation && a.stopPropagation()
			},
			stopImmediatePropagation: function () {
				var a = this.originalEvent;
				this.isImmediatePropagationStopped = j, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), this.stopPropagation()
			}
		}, _.each({
			mouseenter: "mouseover",
			mouseleave: "mouseout",
			pointerenter: "pointerover",
			pointerleave: "pointerout"
		}, function (a, b) {
			_.event.special[a] = {
				delegateType: b, bindType: b, handle: function (a) {
					var c, d = this, e = a.relatedTarget, f = a.handleObj;
					return (!e || e !== d && !_.contains(d, e)) && (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
				}
			}
		}), Y.focusinBubbles || _.each({focus: "focusin", blur: "focusout"}, function (a, b) {
			var c = function (a) {
				_.event.simulate(b, a.target, _.event.fix(a), !0)
			};
			_.event.special[b] = {
				setup: function () {
					var d = this.ownerDocument || this, e = rb.access(d, b);
					e || d.addEventListener(a, c, !0), rb.access(d, b, (e || 0) + 1)
				}, teardown: function () {
					var d = this.ownerDocument || this, e = rb.access(d, b) - 1;
					e ? rb.access(d, b, e) : (d.removeEventListener(a, c, !0), rb.remove(d, b))
				}
			}
		}), _.fn.extend({
			on: function (a, b, c, d, e) {
				var f, g;
				if ("object" == typeof a) {
					"string" != typeof b && (c = c || b, b = void 0);
					for (g in a)this.on(g, b, c, a[g], e);
					return this
				}
				if (null == c && null == d ? (d = b, c = b = void 0) : null == d && ("string" == typeof b ? (d = c, c = void 0) : (d = c, c = b, b = void 0)), d === !1)d = k; else if (!d)return this;
				return 1 === e && (f = d, d = function (a) {
					return _().off(a), f.apply(this, arguments)
				}, d.guid = f.guid || (f.guid = _.guid++)), this.each(function () {
					_.event.add(this, a, d, c, b)
				})
			}, one: function (a, b, c, d) {
				return this.on(a, b, c, d, 1)
			}, off: function (a, b, c) {
				var d, e;
				if (a && a.preventDefault && a.handleObj)return d = a.handleObj, _(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
				if ("object" == typeof a) {
					for (e in a)this.off(e, b, a[e]);
					return this
				}
				return (b === !1 || "function" == typeof b) && (c = b, b = void 0), c === !1 && (c = k), this.each(function () {
					_.event.remove(this, a, c, b)
				})
			}, trigger: function (a, b) {
				return this.each(function () {
					_.event.trigger(a, b, this)
				})
			}, triggerHandler: function (a, b) {
				var c = this[0];
				return c ? _.event.trigger(a, b, c, !0) : void 0
			}
		});
		var Eb = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, Fb = /<([\w:]+)/, Gb = /<|&#?\w+;/, Hb = /<(?:script|style|link)/i, Ib = /checked\s*(?:[^=]|=\s*.checked.)/i, Jb = /^$|\/(?:java|ecma)script/i, Kb = /^true\/(.*)/, Lb = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Mb = {
			option: [1, "<select multiple='multiple'>", "</select>"],
			thead: [1, "<table>", "</table>"],
			col: [2, "<table><colgroup>", "</colgroup></table>"],
			tr: [2, "<table><tbody>", "</tbody></table>"],
			td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
			_default: [0, "", ""]
		};
		Mb.optgroup = Mb.option, Mb.tbody = Mb.tfoot = Mb.colgroup = Mb.caption = Mb.thead, Mb.th = Mb.td, _.extend({
			clone: function (a, b, c) {
				var d, e, f, g, h = a.cloneNode(!0), i = _.contains(a.ownerDocument, a);
				if (!(Y.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || _.isXMLDoc(a)))for (g = r(h), f = r(a), d = 0, e = f.length; e > d; d++)s(f[d], g[d]);
				if (b)if (c)for (f = f || r(a), g = g || r(h), d = 0, e = f.length; e > d; d++)q(f[d], g[d]); else q(a, h);
				return g = r(h, "script"), g.length > 0 && p(g, !i && r(a, "script")), h
			}, buildFragment: function (a, b, c, d) {
				for (var e, f, g, h, i, j, k = b.createDocumentFragment(), l = [], m = 0, n = a.length; n > m; m++)if (e = a[m], e || 0 === e)if ("object" === _.type(e))_.merge(l, e.nodeType ? [e] : e); else if (Gb.test(e)) {
					for (f = f || k.appendChild(b.createElement("div")), g = (Fb.exec(e) || ["", ""])[1].toLowerCase(), h = Mb[g] || Mb._default, f.innerHTML = h[1] + e.replace(Eb, "<$1></$2>") + h[2], j = h[0]; j--;)f = f.lastChild;
					_.merge(l, f.childNodes), f = k.firstChild, f.textContent = ""
				} else l.push(b.createTextNode(e));
				for (k.textContent = "", m = 0; e = l[m++];)if ((!d || -1 === _.inArray(e, d)) && (i = _.contains(e.ownerDocument, e), f = r(k.appendChild(e), "script"), i && p(f), c))for (j = 0; e = f[j++];)Jb.test(e.type || "") && c.push(e);
				return k
			}, cleanData: function (a) {
				for (var b, c, d, e, f = _.event.special, g = 0; void 0 !== (c = a[g]); g++) {
					if (_.acceptData(c) && (e = c[rb.expando], e && (b = rb.cache[e]))) {
						if (b.events)for (d in b.events)f[d] ? _.event.remove(c, d) : _.removeEvent(c, d, b.handle);
						rb.cache[e] && delete rb.cache[e]
					}
					delete sb.cache[c[sb.expando]]
				}
			}
		}), _.fn.extend({
			text: function (a) {
				return qb(this, function (a) {
					return void 0 === a ? _.text(this) : this.empty().each(function () {
						(1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = a)
					})
				}, null, a, arguments.length)
			}, append: function () {
				return this.domManip(arguments, function (a) {
					if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
						var b = m(this, a);
						b.appendChild(a)
					}
				})
			}, prepend: function () {
				return this.domManip(arguments, function (a) {
					if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
						var b = m(this, a);
						b.insertBefore(a, b.firstChild)
					}
				})
			}, before: function () {
				return this.domManip(arguments, function (a) {
					this.parentNode && this.parentNode.insertBefore(a, this)
				})
			}, after: function () {
				return this.domManip(arguments, function (a) {
					this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
				})
			}, remove: function (a, b) {
				for (var c, d = a ? _.filter(a, this) : this, e = 0; null != (c = d[e]); e++)b || 1 !== c.nodeType || _.cleanData(r(c)), c.parentNode && (b && _.contains(c.ownerDocument, c) && p(r(c, "script")), c.parentNode.removeChild(c));
				return this
			}, empty: function () {
				for (var a, b = 0; null != (a = this[b]); b++)1 === a.nodeType && (_.cleanData(r(a, !1)), a.textContent = "");
				return this
			}, clone: function (a, b) {
				return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function () {
					return _.clone(this, a, b)
				})
			}, html: function (a) {
				return qb(this, function (a) {
					var b = this[0] || {}, c = 0, d = this.length;
					if (void 0 === a && 1 === b.nodeType)return b.innerHTML;
					if ("string" == typeof a && !Hb.test(a) && !Mb[(Fb.exec(a) || ["", ""])[1].toLowerCase()]) {
						a = a.replace(Eb, "<$1></$2>");
						try {
							for (; d > c; c++)b = this[c] || {}, 1 === b.nodeType && (_.cleanData(r(b, !1)), b.innerHTML = a);
							b = 0
						} catch (e) {
						}
					}
					b && this.empty().append(a)
				}, null, a, arguments.length)
			}, replaceWith: function () {
				var a = arguments[0];
				return this.domManip(arguments, function (b) {
					a = this.parentNode, _.cleanData(r(this)), a && a.replaceChild(b, this)
				}), a && (a.length || a.nodeType) ? this : this.remove()
			}, detach: function (a) {
				return this.remove(a, !0)
			}, domManip: function (a, b) {
				a = S.apply([], a);
				var c, d, e, f, g, h, i = 0, j = this.length, k = this, l = j - 1, m = a[0], p = _.isFunction(m);
				if (p || j > 1 && "string" == typeof m && !Y.checkClone && Ib.test(m))return this.each(function (c) {
					var d = k.eq(c);
					p && (a[0] = m.call(this, c, d.html())), d.domManip(a, b)
				});
				if (j && (c = _.buildFragment(a, this[0].ownerDocument, !1, this), d = c.firstChild, 1 === c.childNodes.length && (c = d), d)) {
					for (e = _.map(r(c, "script"), n), f = e.length; j > i; i++)g = c, i !== l && (g = _.clone(g, !0, !0), f && _.merge(e, r(g, "script"))), b.call(this[i], g, i);
					if (f)for (h = e[e.length - 1].ownerDocument, _.map(e, o), i = 0; f > i; i++)g = e[i], Jb.test(g.type || "") && !rb.access(g, "globalEval") && _.contains(h, g) && (g.src ? _._evalUrl && _._evalUrl(g.src) : _.globalEval(g.textContent.replace(Lb, "")))
				}
				return this
			}
		}), _.each({
			appendTo: "append",
			prependTo: "prepend",
			insertBefore: "before",
			insertAfter: "after",
			replaceAll: "replaceWith"
		}, function (a, b) {
			_.fn[a] = function (a) {
				for (var c, d = [], e = _(a), f = e.length - 1, g = 0; f >= g; g++)c = g === f ? this : this.clone(!0), _(e[g])[b](c), T.apply(d, c.get());
				return this.pushStack(d)
			}
		});
		var Nb, Ob = {}, Pb = /^margin/, Qb = new RegExp("^(" + vb + ")(?!px)[a-z%]+$", "i"), Rb = function (a) {
			return a.ownerDocument.defaultView.getComputedStyle(a, null)
		};
		!function () {
			function b() {
				g.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", g.innerHTML = "", e.appendChild(f);
				var b = a.getComputedStyle(g, null);
				c = "1%" !== b.top, d = "4px" === b.width, e.removeChild(f)
			}

			var c, d, e = Z.documentElement, f = Z.createElement("div"), g = Z.createElement("div");
			g.style && (g.style.backgroundClip = "content-box", g.cloneNode(!0).style.backgroundClip = "", Y.clearCloneStyle = "content-box" === g.style.backgroundClip, f.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute", f.appendChild(g), a.getComputedStyle && _.extend(Y, {
				pixelPosition: function () {
					return b(), c
				}, boxSizingReliable: function () {
					return null == d && b(), d
				}, reliableMarginRight: function () {
					var b, c = g.appendChild(Z.createElement("div"));
					return c.style.cssText = g.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", c.style.marginRight = c.style.width = "0", g.style.width = "1px", e.appendChild(f), b = !parseFloat(a.getComputedStyle(c, null).marginRight), e.removeChild(f), b
				}
			}))
		}(), _.swap = function (a, b, c, d) {
			var e, f, g = {};
			for (f in b)g[f] = a.style[f], a.style[f] = b[f];
			e = c.apply(a, d || []);
			for (f in b)a.style[f] = g[f];
			return e
		};
		var Sb = /^(none|table(?!-c[ea]).+)/, Tb = new RegExp("^(" + vb + ")(.*)$", "i"), Ub = new RegExp("^([+-])=(" + vb + ")", "i"), Vb = {
			position: "absolute",
			visibility: "hidden",
			display: "block"
		}, Wb = {letterSpacing: "0", fontWeight: "400"}, Xb = ["Webkit", "O", "Moz", "ms"];
		_.extend({
			cssHooks: {
				opacity: {
					get: function (a, b) {
						if (b) {
							var c = v(a, "opacity");
							return "" === c ? "1" : c
						}
					}
				}
			},
			cssNumber: {
				columnCount: !0,
				fillOpacity: !0,
				flexGrow: !0,
				flexShrink: !0,
				fontWeight: !0,
				lineHeight: !0,
				opacity: !0,
				order: !0,
				orphans: !0,
				widows: !0,
				zIndex: !0,
				zoom: !0
			},
			cssProps: {"float": "cssFloat"},
			style: function (a, b, c, d) {
				if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
					var e, f, g, h = _.camelCase(b), i = a.style;
					return b = _.cssProps[h] || (_.cssProps[h] = x(i, h)), g = _.cssHooks[b] || _.cssHooks[h], void 0 === c ? g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b] : (f = typeof c, "string" === f && (e = Ub.exec(c)) && (c = (e[1] + 1) * e[2] + parseFloat(_.css(a, b)), f = "number"), void(null != c && c === c && ("number" !== f || _.cssNumber[h] || (c += "px"), Y.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), g && "set" in g && void 0 === (c = g.set(a, c, d)) || (i[b] = c))))
				}
			},
			css: function (a, b, c, d) {
				var e, f, g, h = _.camelCase(b);
				return b = _.cssProps[h] || (_.cssProps[h] = x(a.style, h)), g = _.cssHooks[b] || _.cssHooks[h], g && "get" in g && (e = g.get(a, !0, c)), void 0 === e && (e = v(a, b, d)), "normal" === e && b in Wb && (e = Wb[b]), "" === c || c ? (f = parseFloat(e), c === !0 || _.isNumeric(f) ? f || 0 : e) : e
			}
		}), _.each(["height", "width"], function (a, b) {
			_.cssHooks[b] = {
				get: function (a, c, d) {
					return c ? Sb.test(_.css(a, "display")) && 0 === a.offsetWidth ? _.swap(a, Vb, function () {
						return A(a, b, d)
					}) : A(a, b, d) : void 0
				}, set: function (a, c, d) {
					var e = d && Rb(a);
					return y(a, c, d ? z(a, b, d, "border-box" === _.css(a, "boxSizing", !1, e), e) : 0)
				}
			}
		}), _.cssHooks.marginRight = w(Y.reliableMarginRight, function (a, b) {
			return b ? _.swap(a, {display: "inline-block"}, v, [a, "marginRight"]) : void 0
		}), _.each({margin: "", padding: "", border: "Width"}, function (a, b) {
			_.cssHooks[a + b] = {
				expand: function (c) {
					for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; 4 > d; d++)e[a + wb[d] + b] = f[d] || f[d - 2] || f[0];
					return e
				}
			}, Pb.test(a) || (_.cssHooks[a + b].set = y)
		}), _.fn.extend({
			css: function (a, b) {
				return qb(this, function (a, b, c) {
					var d, e, f = {}, g = 0;
					if (_.isArray(b)) {
						for (d = Rb(a), e = b.length; e > g; g++)f[b[g]] = _.css(a, b[g], !1, d);
						return f
					}
					return void 0 !== c ? _.style(a, b, c) : _.css(a, b)
				}, a, b, arguments.length > 1)
			}, show: function () {
				return B(this, !0)
			}, hide: function () {
				return B(this)
			}, toggle: function (a) {
				return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function () {
					xb(this) ? _(this).show() : _(this).hide()
				})
			}
		}), _.Tween = C, C.prototype = {
			constructor: C, init: function (a, b, c, d, e, f) {
				this.elem = a, this.prop = c, this.easing = e || "swing", this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (_.cssNumber[c] ? "" : "px")
			}, cur: function () {
				var a = C.propHooks[this.prop];
				return a && a.get ? a.get(this) : C.propHooks._default.get(this)
			}, run: function (a) {
				var b, c = C.propHooks[this.prop];
				return this.pos = b = this.options.duration ? _.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : C.propHooks._default.set(this), this
			}
		}, C.prototype.init.prototype = C.prototype, C.propHooks = {
			_default: {
				get: function (a) {
					var b;
					return null == a.elem[a.prop] || a.elem.style && null != a.elem.style[a.prop] ? (b = _.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0) : a.elem[a.prop]
				}, set: function (a) {
					_.fx.step[a.prop] ? _.fx.step[a.prop](a) : a.elem.style && (null != a.elem.style[_.cssProps[a.prop]] || _.cssHooks[a.prop]) ? _.style(a.elem, a.prop, a.now + a.unit) : a.elem[a.prop] = a.now
				}
			}
		}, C.propHooks.scrollTop = C.propHooks.scrollLeft = {
			set: function (a) {
				a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
			}
		}, _.easing = {
			linear: function (a) {
				return a
			}, swing: function (a) {
				return .5 - Math.cos(a * Math.PI) / 2
			}
		}, _.fx = C.prototype.init, _.fx.step = {};
		var Yb, Zb, $b = /^(?:toggle|show|hide)$/, _b = new RegExp("^(?:([+-])=|)(" + vb + ")([a-z%]*)$", "i"), ac = /queueHooks$/, bc = [G], cc = {
			"*": [function (a, b) {
				var c = this.createTween(a, b), d = c.cur(), e = _b.exec(b), f = e && e[3] || (_.cssNumber[a] ? "" : "px"), g = (_.cssNumber[a] || "px" !== f && +d) && _b.exec(_.css(c.elem, a)), h = 1, i = 20;
				if (g && g[3] !== f) {
					f = f || g[3], e = e || [], g = +d || 1;
					do h = h || ".5", g /= h, _.style(c.elem, a, g + f); while (h !== (h = c.cur() / d) && 1 !== h && --i)
				}
				return e && (g = c.start = +g || +d || 0, c.unit = f, c.end = e[1] ? g + (e[1] + 1) * e[2] : +e[2]), c
			}]
		};
		_.Animation = _.extend(I, {
			tweener: function (a, b) {
				_.isFunction(a) ? (b = a, a = ["*"]) : a = a.split(" ");
				for (var c, d = 0, e = a.length; e > d; d++)c = a[d], cc[c] = cc[c] || [], cc[c].unshift(b)
			}, prefilter: function (a, b) {
				b ? bc.unshift(a) : bc.push(a)
			}
		}), _.speed = function (a, b, c) {
			var d = a && "object" == typeof a ? _.extend({}, a) : {
				complete: c || !c && b || _.isFunction(a) && a,
				duration: a,
				easing: c && b || b && !_.isFunction(b) && b
			};
			return d.duration = _.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in _.fx.speeds ? _.fx.speeds[d.duration] : _.fx.speeds._default, (null == d.queue || d.queue === !0) && (d.queue = "fx"), d.old = d.complete, d.complete = function () {
				_.isFunction(d.old) && d.old.call(this), d.queue && _.dequeue(this, d.queue)
			}, d
		}, _.fn.extend({
			fadeTo: function (a, b, c, d) {
				return this.filter(xb).css("opacity", 0).show().end().animate({opacity: b}, a, c, d)
			}, animate: function (a, b, c, d) {
				var e = _.isEmptyObject(a), f = _.speed(b, c, d), g = function () {
					var b = I(this, _.extend({}, a), f);
					(e || rb.get(this, "finish")) && b.stop(!0)
				};
				return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
			}, stop: function (a, b, c) {
				var d = function (a) {
					var b = a.stop;
					delete a.stop, b(c)
				};
				return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function () {
					var b = !0, e = null != a && a + "queueHooks", f = _.timers, g = rb.get(this);
					if (e)g[e] && g[e].stop && d(g[e]); else for (e in g)g[e] && g[e].stop && ac.test(e) && d(g[e]);
					for (e = f.length; e--;)f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
					(b || !c) && _.dequeue(this, a)
				})
			}, finish: function (a) {
				return a !== !1 && (a = a || "fx"), this.each(function () {
					var b, c = rb.get(this), d = c[a + "queue"], e = c[a + "queueHooks"], f = _.timers, g = d ? d.length : 0;
					for (c.finish = !0, _.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;)f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
					for (b = 0; g > b; b++)d[b] && d[b].finish && d[b].finish.call(this);
					delete c.finish
				})
			}
		}), _.each(["toggle", "show", "hide"], function (a, b) {
			var c = _.fn[b];
			_.fn[b] = function (a, d, e) {
				return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(E(b, !0), a, d, e)
			}
		}), _.each({
			slideDown: E("show"),
			slideUp: E("hide"),
			slideToggle: E("toggle"),
			fadeIn: {opacity: "show"},
			fadeOut: {opacity: "hide"},
			fadeToggle: {opacity: "toggle"}
		}, function (a, b) {
			_.fn[a] = function (a, c, d) {
				return this.animate(b, a, c, d)
			}
		}), _.timers = [], _.fx.tick = function () {
			var a, b = 0, c = _.timers;
			for (Yb = _.now(); b < c.length; b++)a = c[b], a() || c[b] !== a || c.splice(b--, 1);
			c.length || _.fx.stop(), Yb = void 0
		}, _.fx.timer = function (a) {
			_.timers.push(a), a() ? _.fx.start() : _.timers.pop()
		}, _.fx.interval = 13, _.fx.start = function () {
			Zb || (Zb = setInterval(_.fx.tick, _.fx.interval))
		}, _.fx.stop = function () {
			clearInterval(Zb), Zb = null
		}, _.fx.speeds = {slow: 600, fast: 200, _default: 400}, _.fn.delay = function (a, b) {
			return a = _.fx ? _.fx.speeds[a] || a : a, b = b || "fx", this.queue(b, function (b, c) {
				var d = setTimeout(b, a);
				c.stop = function () {
					clearTimeout(d)
				}
			})
		}, function () {
			var a = Z.createElement("input"), b = Z.createElement("select"), c = b.appendChild(Z.createElement("option"));
			a.type = "checkbox", Y.checkOn = "" !== a.value, Y.optSelected = c.selected, b.disabled = !0, Y.optDisabled = !c.disabled, a = Z.createElement("input"), a.value = "t", a.type = "radio", Y.radioValue = "t" === a.value
		}();
		var dc, ec, fc = _.expr.attrHandle;
		_.fn.extend({
			attr: function (a, b) {
				return qb(this, _.attr, a, b, arguments.length > 1)
			}, removeAttr: function (a) {
				return this.each(function () {
					_.removeAttr(this, a)
				})
			}
		}), _.extend({
			attr: function (a, b, c) {
				var d, e, f = a.nodeType;
				return a && 3 !== f && 8 !== f && 2 !== f ? typeof a.getAttribute === zb ? _.prop(a, b, c) : (1 === f && _.isXMLDoc(a) || (b = b.toLowerCase(), d = _.attrHooks[b] || (_.expr.match.bool.test(b) ? ec : dc)), void 0 === c ? d && "get" in d && null !== (e = d.get(a, b)) ? e : (e = _.find.attr(a, b), null == e ? void 0 : e) : null !== c ? d && "set" in d && void 0 !== (e = d.set(a, c, b)) ? e : (a.setAttribute(b, c + ""), c) : void _.removeAttr(a, b)) : void 0
			}, removeAttr: function (a, b) {
				var c, d, e = 0, f = b && b.match(nb);
				if (f && 1 === a.nodeType)for (; c = f[e++];)d = _.propFix[c] || c, _.expr.match.bool.test(c) && (a[d] = !1), a.removeAttribute(c)
			}, attrHooks: {
				type: {
					set: function (a, b) {
						if (!Y.radioValue && "radio" === b && _.nodeName(a, "input")) {
							var c = a.value;
							return a.setAttribute("type", b), c && (a.value = c), b
						}
					}
				}
			}
		}), ec = {
			set: function (a, b, c) {
				return b === !1 ? _.removeAttr(a, c) : a.setAttribute(c, c), c
			}
		}, _.each(_.expr.match.bool.source.match(/\w+/g), function (a, b) {
			var c = fc[b] || _.find.attr;
			fc[b] = function (a, b, d) {
				var e, f;
				return d || (f = fc[b], fc[b] = e, e = null != c(a, b, d) ? b.toLowerCase() : null, fc[b] = f), e
			}
		});
		var gc = /^(?:input|select|textarea|button)$/i;
		_.fn.extend({
			prop: function (a, b) {
				return qb(this, _.prop, a, b, arguments.length > 1)
			}, removeProp: function (a) {
				return this.each(function () {
					delete this[_.propFix[a] || a]
				})
			}
		}), _.extend({
			propFix: {"for": "htmlFor", "class": "className"}, prop: function (a, b, c) {
				var d, e, f, g = a.nodeType;
				return a && 3 !== g && 8 !== g && 2 !== g ? (f = 1 !== g || !_.isXMLDoc(a), f && (b = _.propFix[b] || b, e = _.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]) : void 0
			}, propHooks: {
				tabIndex: {
					get: function (a) {
						return a.hasAttribute("tabindex") || gc.test(a.nodeName) || a.href ? a.tabIndex : -1
					}
				}
			}
		}), Y.optSelected || (_.propHooks.selected = {
			get: function (a) {
				var b = a.parentNode;
				return b && b.parentNode && b.parentNode.selectedIndex, null
			}
		}), _.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
			_.propFix[this.toLowerCase()] = this
		});
		var hc = /[\t\r\n\f]/g;
		_.fn.extend({
			addClass: function (a) {
				var b, c, d, e, f, g, h = "string" == typeof a && a, i = 0, j = this.length;
				if (_.isFunction(a))return this.each(function (b) {
					_(this).addClass(a.call(this, b, this.className))
				});
				if (h)for (b = (a || "").match(nb) || []; j > i; i++)if (c = this[i], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(hc, " ") : " ")) {
					for (f = 0; e = b[f++];)d.indexOf(" " + e + " ") < 0 && (d += e + " ");
					g = _.trim(d), c.className !== g && (c.className = g)
				}
				return this
			}, removeClass: function (a) {
				var b, c, d, e, f, g, h = 0 === arguments.length || "string" == typeof a && a, i = 0, j = this.length;
				if (_.isFunction(a))return this.each(function (b) {
					_(this).removeClass(a.call(this, b, this.className))
				});
				if (h)for (b = (a || "").match(nb) || []; j > i; i++)if (c = this[i], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(hc, " ") : "")) {
					for (f = 0; e = b[f++];)for (; d.indexOf(" " + e + " ") >= 0;)d = d.replace(" " + e + " ", " ");
					g = a ? _.trim(d) : "", c.className !== g && (c.className = g)
				}
				return this
			}, toggleClass: function (a, b) {
				var c = typeof a;
				return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : this.each(_.isFunction(a) ? function (c) {
					_(this).toggleClass(a.call(this, c, this.className, b), b)
				} : function () {
					if ("string" === c)for (var b, d = 0, e = _(this), f = a.match(nb) || []; b = f[d++];)e.hasClass(b) ? e.removeClass(b) : e.addClass(b); else(c === zb || "boolean" === c) && (this.className && rb.set(this, "__className__", this.className), this.className = this.className || a === !1 ? "" : rb.get(this, "__className__") || "")
				})
			}, hasClass: function (a) {
				for (var b = " " + a + " ", c = 0, d = this.length; d > c; c++)if (1 === this[c].nodeType && (" " + this[c].className + " ").replace(hc, " ").indexOf(b) >= 0)return !0;
				return !1
			}
		});
		var ic = /\r/g;
		_.fn.extend({
			val: function (a) {
				var b, c, d, e = this[0];
				return arguments.length ? (d = _.isFunction(a), this.each(function (c) {
					var e;
					1 === this.nodeType && (e = d ? a.call(this, c, _(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : _.isArray(e) && (e = _.map(e, function (a) {
						return null == a ? "" : a + ""
					})), b = _.valHooks[this.type] || _.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e))
				})) : e ? (b = _.valHooks[e.type] || _.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(ic, "") : null == c ? "" : c)) : void 0
			}
		}), _.extend({
			valHooks: {
				option: {
					get: function (a) {
						var b = _.find.attr(a, "value");
						return null != b ? b : _.trim(_.text(a))
					}
				}, select: {
					get: function (a) {
						for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++)if (c = d[i], !(!c.selected && i !== e || (Y.optDisabled ? c.disabled : null !== c.getAttribute("disabled")) || c.parentNode.disabled && _.nodeName(c.parentNode, "optgroup"))) {
							if (b = _(c).val(), f)return b;
							g.push(b)
						}
						return g
					}, set: function (a, b) {
						for (var c, d, e = a.options, f = _.makeArray(b), g = e.length; g--;)d = e[g], (d.selected = _.inArray(d.value, f) >= 0) && (c = !0);
						return c || (a.selectedIndex = -1), f
					}
				}
			}
		}), _.each(["radio", "checkbox"], function () {
			_.valHooks[this] = {
				set: function (a, b) {
					return _.isArray(b) ? a.checked = _.inArray(_(a).val(), b) >= 0 : void 0
				}
			}, Y.checkOn || (_.valHooks[this].get = function (a) {
				return null === a.getAttribute("value") ? "on" : a.value
			})
		}), _.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (a, b) {
			_.fn[b] = function (a, c) {
				return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
			}
		}), _.fn.extend({
			hover: function (a, b) {
				return this.mouseenter(a).mouseleave(b || a)
			}, bind: function (a, b, c) {
				return this.on(a, null, b, c)
			}, unbind: function (a, b) {
				return this.off(a, null, b)
			}, delegate: function (a, b, c, d) {
				return this.on(b, a, c, d)
			}, undelegate: function (a, b, c) {
				return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
			}
		});
		var jc = _.now(), kc = /\?/;
		_.parseJSON = function (a) {
			return JSON.parse(a + "")
		}, _.parseXML = function (a) {
			var b, c;
			if (!a || "string" != typeof a)return null;
			try {
				c = new DOMParser, b = c.parseFromString(a, "text/xml")
			} catch (d) {
				b = void 0
			}
			return (!b || b.getElementsByTagName("parsererror").length) && _.error("Invalid XML: " + a), b
		};
		var lc, mc, nc = /#.*$/, oc = /([?&])_=[^&]*/, pc = /^(.*?):[ \t]*([^\r\n]*)$/gm, qc = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, rc = /^(?:GET|HEAD)$/, sc = /^\/\//, tc = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, uc = {}, vc = {}, wc = "*/".concat("*");
		try {
			mc = location.href
		} catch (xc) {
			mc = Z.createElement("a"), mc.href = "", mc = mc.href
		}
		lc = tc.exec(mc.toLowerCase()) || [], _.extend({
			active: 0,
			lastModified: {},
			etag: {},
			ajaxSettings: {
				url: mc,
				type: "GET",
				isLocal: qc.test(lc[1]),
				global: !0,
				processData: !0,
				async: !0,
				contentType: "application/x-www-form-urlencoded; charset=UTF-8",
				accepts: {
					"*": wc,
					text: "text/plain",
					html: "text/html",
					xml: "application/xml, text/xml",
					json: "application/json, text/javascript"
				},
				contents: {xml: /xml/, html: /html/, json: /json/},
				responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
				converters: {"* text": String, "text html": !0, "text json": _.parseJSON, "text xml": _.parseXML},
				flatOptions: {url: !0, context: !0}
			},
			ajaxSetup: function (a, b) {
				return b ? L(L(a, _.ajaxSettings), b) : L(_.ajaxSettings, a)
			},
			ajaxPrefilter: J(uc),
			ajaxTransport: J(vc),
			ajax: function (a, b) {
				function c(a, b, c, g) {
					var i, k, r, s, u, w = b;
					2 !== t && (t = 2, h && clearTimeout(h), d = void 0, f = g || "", v.readyState = a > 0 ? 4 : 0, i = a >= 200 && 300 > a || 304 === a, c && (s = M(l, v, c)), s = N(l, s, v, i), i ? (l.ifModified && (u = v.getResponseHeader("Last-Modified"), u && (_.lastModified[e] = u), u = v.getResponseHeader("etag"), u && (_.etag[e] = u)), 204 === a || "HEAD" === l.type ? w = "nocontent" : 304 === a ? w = "notmodified" : (w = s.state, k = s.data, r = s.error, i = !r)) : (r = w, (a || !w) && (w = "error", 0 > a && (a = 0))), v.status = a, v.statusText = (b || w) + "", i ? o.resolveWith(m, [k, w, v]) : o.rejectWith(m, [v, w, r]), v.statusCode(q), q = void 0, j && n.trigger(i ? "ajaxSuccess" : "ajaxError", [v, l, i ? k : r]), p.fireWith(m, [v, w]), j && (n.trigger("ajaxComplete", [v, l]), --_.active || _.event.trigger("ajaxStop")))
				}

				"object" == typeof a && (b = a, a = void 0), b = b || {};
				var d, e, f, g, h, i, j, k, l = _.ajaxSetup({}, b), m = l.context || l, n = l.context && (m.nodeType || m.jquery) ? _(m) : _.event, o = _.Deferred(), p = _.Callbacks("once memory"), q = l.statusCode || {}, r = {}, s = {}, t = 0, u = "canceled", v = {
					readyState: 0,
					getResponseHeader: function (a) {
						var b;
						if (2 === t) {
							if (!g)for (g = {}; b = pc.exec(f);)g[b[1].toLowerCase()] = b[2];
							b = g[a.toLowerCase()]
						}
						return null == b ? null : b
					},
					getAllResponseHeaders: function () {
						return 2 === t ? f : null
					},
					setRequestHeader: function (a, b) {
						var c = a.toLowerCase();
						return t || (a = s[c] = s[c] || a, r[a] = b), this
					},
					overrideMimeType: function (a) {
						return t || (l.mimeType = a), this
					},
					statusCode: function (a) {
						var b;
						if (a)if (2 > t)for (b in a)q[b] = [q[b], a[b]]; else v.always(a[v.status]);
						return this
					},
					abort: function (a) {
						var b = a || u;
						return d && d.abort(b), c(0, b), this
					}
				};
				if (o.promise(v).complete = p.add, v.success = v.done, v.error = v.fail, l.url = ((a || l.url || mc) + "").replace(nc, "").replace(sc, lc[1] + "//"), l.type = b.method || b.type || l.method || l.type, l.dataTypes = _.trim(l.dataType || "*").toLowerCase().match(nb) || [""], null == l.crossDomain && (i = tc.exec(l.url.toLowerCase()), l.crossDomain = !(!i || i[1] === lc[1] && i[2] === lc[2] && (i[3] || ("http:" === i[1] ? "80" : "443")) === (lc[3] || ("http:" === lc[1] ? "80" : "443")))), l.data && l.processData && "string" != typeof l.data && (l.data = _.param(l.data, l.traditional)), K(uc, l, b, v), 2 === t)return v;
				j = l.global, j && 0 === _.active++ && _.event.trigger("ajaxStart"), l.type = l.type.toUpperCase(), l.hasContent = !rc.test(l.type), e = l.url, l.hasContent || (l.data && (e = l.url += (kc.test(e) ? "&" : "?") + l.data, delete l.data), l.cache === !1 && (l.url = oc.test(e) ? e.replace(oc, "$1_=" + jc++) : e + (kc.test(e) ? "&" : "?") + "_=" + jc++)), l.ifModified && (_.lastModified[e] && v.setRequestHeader("If-Modified-Since", _.lastModified[e]), _.etag[e] && v.setRequestHeader("If-None-Match", _.etag[e])), (l.data && l.hasContent && l.contentType !== !1 || b.contentType) && v.setRequestHeader("Content-Type", l.contentType), v.setRequestHeader("Accept", l.dataTypes[0] && l.accepts[l.dataTypes[0]] ? l.accepts[l.dataTypes[0]] + ("*" !== l.dataTypes[0] ? ", " + wc + "; q=0.01" : "") : l.accepts["*"]);
				for (k in l.headers)v.setRequestHeader(k, l.headers[k]);
				if (l.beforeSend && (l.beforeSend.call(m, v, l) === !1 || 2 === t))return v.abort();
				u = "abort";
				for (k in{success: 1, error: 1, complete: 1})v[k](l[k]);
				if (d = K(vc, l, b, v)) {
					v.readyState = 1, j && n.trigger("ajaxSend", [v, l]), l.async && l.timeout > 0 && (h = setTimeout(function () {
						v.abort("timeout")
					}, l.timeout));
					try {
						t = 1, d.send(r, c)
					} catch (w) {
						if (!(2 > t))throw w;
						c(-1, w)
					}
				} else c(-1, "No Transport");
				return v
			},
			getJSON: function (a, b, c) {
				return _.get(a, b, c, "json")
			},
			getScript: function (a, b) {
				return _.get(a, void 0, b, "script")
			}
		}), _.each(["get", "post"], function (a, b) {
			_[b] = function (a, c, d, e) {
				return _.isFunction(c) && (e = e || d, d = c, c = void 0), _.ajax({
					url: a,
					type: b,
					dataType: e,
					data: c,
					success: d
				})
			}
		}), _.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) {
			_.fn[b] = function (a) {
				return this.on(b, a)
			}
		}), _._evalUrl = function (a) {
			return _.ajax({url: a, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0})
		}, _.fn.extend({
			wrapAll: function (a) {
				var b;
				return _.isFunction(a) ? this.each(function (b) {
					_(this).wrapAll(a.call(this, b))
				}) : (this[0] && (b = _(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function () {
					for (var a = this; a.firstElementChild;)a = a.firstElementChild;
					return a
				}).append(this)), this)
			}, wrapInner: function (a) {
				return this.each(_.isFunction(a) ? function (b) {
					_(this).wrapInner(a.call(this, b))
				} : function () {
					var b = _(this), c = b.contents();
					c.length ? c.wrapAll(a) : b.append(a)
				})
			}, wrap: function (a) {
				var b = _.isFunction(a);
				return this.each(function (c) {
					_(this).wrapAll(b ? a.call(this, c) : a)
				})
			}, unwrap: function () {
				return this.parent().each(function () {
					_.nodeName(this, "body") || _(this).replaceWith(this.childNodes)
				}).end()
			}
		}), _.expr.filters.hidden = function (a) {
			return a.offsetWidth <= 0 && a.offsetHeight <= 0
		}, _.expr.filters.visible = function (a) {
			return !_.expr.filters.hidden(a)
		};
		var yc = /%20/g, zc = /\[\]$/, Ac = /\r?\n/g, Bc = /^(?:submit|button|image|reset|file)$/i, Cc = /^(?:input|select|textarea|keygen)/i;
		_.param = function (a, b) {
			var c, d = [], e = function (a, b) {
				b = _.isFunction(b) ? b() : null == b ? "" : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
			};
			if (void 0 === b && (b = _.ajaxSettings && _.ajaxSettings.traditional), _.isArray(a) || a.jquery && !_.isPlainObject(a))_.each(a, function () {
				e(this.name, this.value)
			}); else for (c in a)O(c, a[c], b, e);
			return d.join("&").replace(yc, "+")
		}, _.fn.extend({
			serialize: function () {
				return _.param(this.serializeArray())
			}, serializeArray: function () {
				return this.map(function () {
					var a = _.prop(this, "elements");
					return a ? _.makeArray(a) : this
				}).filter(function () {
					var a = this.type;
					return this.name && !_(this).is(":disabled") && Cc.test(this.nodeName) && !Bc.test(a) && (this.checked || !yb.test(a))
				}).map(function (a, b) {
					var c = _(this).val();
					return null == c ? null : _.isArray(c) ? _.map(c, function (a) {
						return {name: b.name, value: a.replace(Ac, "\r\n")}
					}) : {name: b.name, value: c.replace(Ac, "\r\n")}
				}).get()
			}
		}), _.ajaxSettings.xhr = function () {
			try {
				return new XMLHttpRequest
			} catch (a) {
			}
		};
		var Dc = 0, Ec = {}, Fc = {0: 200, 1223: 204}, Gc = _.ajaxSettings.xhr();
		a.ActiveXObject && _(a).on("unload", function () {
			for (var a in Ec)Ec[a]()
		}), Y.cors = !!Gc && "withCredentials" in Gc, Y.ajax = Gc = !!Gc, _.ajaxTransport(function (a) {
			var b;
			return Y.cors || Gc && !a.crossDomain ? {
				send: function (c, d) {
					var e, f = a.xhr(), g = ++Dc;
					if (f.open(a.type, a.url, a.async, a.username, a.password), a.xhrFields)for (e in a.xhrFields)f[e] = a.xhrFields[e];
					a.mimeType && f.overrideMimeType && f.overrideMimeType(a.mimeType), a.crossDomain || c["X-Requested-With"] || (c["X-Requested-With"] = "XMLHttpRequest");
					for (e in c)f.setRequestHeader(e, c[e]);
					b = function (a) {
						return function () {
							b && (delete Ec[g], b = f.onload = f.onerror = null, "abort" === a ? f.abort() : "error" === a ? d(f.status, f.statusText) : d(Fc[f.status] || f.status, f.statusText, "string" == typeof f.responseText ? {text: f.responseText} : void 0, f.getAllResponseHeaders()))
						}
					}, f.onload = b(), f.onerror = b("error"), b = Ec[g] = b("abort");
					try {
						f.send(a.hasContent && a.data || null)
					} catch (h) {
						if (b)throw h
					}
				}, abort: function () {
					b && b()
				}
			} : void 0
		}), _.ajaxSetup({
			accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
			contents: {script: /(?:java|ecma)script/},
			converters: {
				"text script": function (a) {
					return _.globalEval(a), a
				}
			}
		}), _.ajaxPrefilter("script", function (a) {
			void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET")
		}), _.ajaxTransport("script", function (a) {
			if (a.crossDomain) {
				var b, c;
				return {
					send: function (d, e) {
						b = _("<script>").prop({
							async: !0,
							charset: a.scriptCharset,
							src: a.url
						}).on("load error", c = function (a) {
							b.remove(), c = null, a && e("error" === a.type ? 404 : 200, a.type)
						}), Z.head.appendChild(b[0])
					}, abort: function () {
						c && c()
					}
				}
			}
		});
		var Hc = [], Ic = /(=)\?(?=&|$)|\?\?/;
		_.ajaxSetup({
			jsonp: "callback", jsonpCallback: function () {
				var a = Hc.pop() || _.expando + "_" + jc++;
				return this[a] = !0, a
			}
		}), _.ajaxPrefilter("json jsonp", function (b, c, d) {
			var e, f, g, h = b.jsonp !== !1 && (Ic.test(b.url) ? "url" : "string" == typeof b.data && !(b.contentType || "").indexOf("application/x-www-form-urlencoded") && Ic.test(b.data) && "data");
			return h || "jsonp" === b.dataTypes[0] ? (e = b.jsonpCallback = _.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(Ic, "$1" + e) : b.jsonp !== !1 && (b.url += (kc.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function () {
				return g || _.error(e + " was not called"), g[0]
			}, b.dataTypes[0] = "json", f = a[e], a[e] = function () {
				g = arguments
			}, d.always(function () {
				a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, Hc.push(e)), g && _.isFunction(f) && f(g[0]), g = f = void 0
			}), "script") : void 0
		}), _.parseHTML = function (a, b, c) {
			if (!a || "string" != typeof a)return null;
			"boolean" == typeof b && (c = b, b = !1), b = b || Z;
			var d = gb.exec(a), e = !c && [];
			return d ? [b.createElement(d[1])] : (d = _.buildFragment([a], b, e), e && e.length && _(e).remove(), _.merge([], d.childNodes))
		};
		var Jc = _.fn.load;
		_.fn.load = function (a, b, c) {
			if ("string" != typeof a && Jc)return Jc.apply(this, arguments);
			var d, e, f, g = this, h = a.indexOf(" ");
			return h >= 0 && (d = _.trim(a.slice(h)), a = a.slice(0, h)), _.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), g.length > 0 && _.ajax({
				url: a,
				type: e,
				dataType: "html",
				data: b
			}).done(function (a) {
				f = arguments, g.html(d ? _("<div>").append(_.parseHTML(a)).find(d) : a)
			}).complete(c && function (a, b) {
					g.each(c, f || [a.responseText, b, a])
				}), this
		}, _.expr.filters.animated = function (a) {
			return _.grep(_.timers, function (b) {
				return a === b.elem
			}).length
		};
		var Kc = a.document.documentElement;
		_.offset = {
			setOffset: function (a, b, c) {
				var d, e, f, g, h, i, j, k = _.css(a, "position"), l = _(a), m = {};
				"static" === k && (a.style.position = "relative"), h = l.offset(), f = _.css(a, "top"), i = _.css(a, "left"), j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), _.isFunction(b) && (b = b.call(a, c, h)), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
			}
		}, _.fn.extend({
			offset: function (a) {
				if (arguments.length)return void 0 === a ? this : this.each(function (b) {
					_.offset.setOffset(this, a, b)
				});
				var b, c, d = this[0], e = {top: 0, left: 0}, f = d && d.ownerDocument;
				return f ? (b = f.documentElement, _.contains(b, d) ? (typeof d.getBoundingClientRect !== zb && (e = d.getBoundingClientRect()), c = P(f), {
					top: e.top + c.pageYOffset - b.clientTop,
					left: e.left + c.pageXOffset - b.clientLeft
				}) : e) : void 0
			}, position: function () {
				if (this[0]) {
					var a, b, c = this[0], d = {top: 0, left: 0};
					return "fixed" === _.css(c, "position") ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), _.nodeName(a[0], "html") || (d = a.offset()), d.top += _.css(a[0], "borderTopWidth", !0), d.left += _.css(a[0], "borderLeftWidth", !0)), {
						top: b.top - d.top - _.css(c, "marginTop", !0),
						left: b.left - d.left - _.css(c, "marginLeft", !0)
					}
				}
			}, offsetParent: function () {
				return this.map(function () {
					for (var a = this.offsetParent || Kc; a && !_.nodeName(a, "html") && "static" === _.css(a, "position");)a = a.offsetParent;
					return a || Kc
				})
			}
		}), _.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (b, c) {
			var d = "pageYOffset" === c;
			_.fn[b] = function (e) {
				return qb(this, function (b, e, f) {
					var g = P(b);
					return void 0 === f ? g ? g[c] : b[e] : void(g ? g.scrollTo(d ? a.pageXOffset : f, d ? f : a.pageYOffset) : b[e] = f)
				}, b, e, arguments.length, null)
			}
		}), _.each(["top", "left"], function (a, b) {
			_.cssHooks[b] = w(Y.pixelPosition, function (a, c) {
				return c ? (c = v(a, b), Qb.test(c) ? _(a).position()[b] + "px" : c) : void 0
			})
		}), _.each({Height: "height", Width: "width"}, function (a, b) {
			_.each({padding: "inner" + a, content: b, "": "outer" + a}, function (c, d) {
				_.fn[d] = function (d, e) {
					var f = arguments.length && (c || "boolean" != typeof d), g = c || (d === !0 || e === !0 ? "margin" : "border");
					return qb(this, function (b, c, d) {
						var e;
						return _.isWindow(b) ? b.document.documentElement["client" + a] : 9 === b.nodeType ? (e = b.documentElement, Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a])) : void 0 === d ? _.css(b, c, g) : _.style(b, c, d, g)
					}, b, f ? d : void 0, f, null)
				}
			})
		}), _.fn.size = function () {
			return this.length
		}, _.fn.andSelf = _.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
			return _
		});
		var Lc = a.jQuery, Mc = a.$;
		return _.noConflict = function (b) {
			return a.$ === _ && (a.$ = Mc), b && a.jQuery === _ && (a.jQuery = Lc), _
		}, typeof b === zb && (a.jQuery = a.$ = _), _
	}), !jQuery)throw new Error("Bootstrap requires jQuery");
+function (a) {
	"use strict";
	function b() {
		var a = document.createElement("bootstrap"), b = {
			WebkitTransition: "webkitTransitionEnd",
			MozTransition: "transitionend",
			OTransition: "oTransitionEnd otransitionend",
			transition: "transitionend"
		};
		for (var c in b)if (void 0 !== a.style[c])return {end: b[c]}
	}

	a.fn.emulateTransitionEnd = function (b) {
		var c = !1, d = this;
		a(this).one(a.support.transition.end, function () {
			c = !0
		});
		var e = function () {
			c || a(d).trigger(a.support.transition.end)
		};
		return setTimeout(e, b), this
	}, a(function () {
		a.support.transition = b()
	})
}(window.jQuery), +function (a) {
	"use strict";
	var b = '[data-dismiss="alert"]', c = function (c) {
		a(c).on("click", b, this.close)
	};
	c.prototype.close = function (b) {
		function c() {
			f.trigger("closed.bs.alert").remove()
		}

		var d = a(this), e = d.attr("data-target");
		e || (e = d.attr("href"), e = e && e.replace(/.*(?=#[^\s]*$)/, ""));
		var f = a(e);
		b && b.preventDefault(), f.length || (f = d.hasClass("alert") ? d : d.parent()), f.trigger(b = a.Event("close.bs.alert")), b.isDefaultPrevented() || (f.removeClass("in"), a.support.transition && f.hasClass("fade") ? f.one(a.support.transition.end, c).emulateTransitionEnd(150) : c())
	};
	var d = a.fn.alert;
	a.fn.alert = function (b) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.alert");
			e || d.data("bs.alert", e = new c(this)), "string" == typeof b && e[b].call(d)
		})
	}, a.fn.alert.Constructor = c, a.fn.alert.noConflict = function () {
		return a.fn.alert = d, this
	}, a(document).on("click.bs.alert.data-api", b, c.prototype.close)
}(window.jQuery), +function (a) {
	"use strict";
	var b = function (c, d) {
		this.$element = a(c), this.options = a.extend({}, b.DEFAULTS, d)
	};
	b.DEFAULTS = {loadingText: "loading..."}, b.prototype.setState = function (a) {
		var b = "disabled", c = this.$element, d = c.is("input") ? "val" : "html", e = c.data();
		a += "Text", e.resetText || c.data("resetText", c[d]()), c[d](e[a] || this.options[a]), setTimeout(function () {
			"loadingText" == a ? c.addClass(b).attr(b, b) : c.removeClass(b).removeAttr(b)
		}, 0)
	}, b.prototype.toggle = function () {
		var a = this.$element.closest('[data-toggle="buttons"]');
		if (a.length) {
			var b = this.$element.find("input").prop("checked", !this.$element.hasClass("active")).trigger("change");
			"radio" === b.prop("type") && a.find(".active").removeClass("active")
		}
		this.$element.toggleClass("active")
	};
	var c = a.fn.button;
	a.fn.button = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.button"), f = "object" == typeof c && c;
			e || d.data("bs.button", e = new b(this, f)), "toggle" == c ? e.toggle() : c && e.setState(c)
		})
	}, a.fn.button.Constructor = b, a.fn.button.noConflict = function () {
		return a.fn.button = c, this
	}, a(document).on("click.bs.button.data-api", "[data-toggle^=button]", function (b) {
		var c = a(b.target);
		c.hasClass("btn") || (c = c.closest(".btn")), c.button("toggle"), b.preventDefault()
	})
}(window.jQuery), +function (a) {
	"use strict";
	var b = function (b, c) {
		this.$element = a(b), this.$indicators = this.$element.find(".carousel-indicators"), this.options = c, this.paused = this.sliding = this.interval = this.$active = this.$items = null, "hover" == this.options.pause && this.$element.on("mouseenter", a.proxy(this.pause, this)).on("mouseleave", a.proxy(this.cycle, this))
	};
	b.DEFAULTS = {interval: 5e3, pause: "hover", wrap: !0}, b.prototype.cycle = function (b) {
		return b || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(a.proxy(this.next, this), this.options.interval)), this
	}, b.prototype.getActiveIndex = function () {
		return this.$active = this.$element.find(".item.active"), this.$items = this.$active.parent().children(), this.$items.index(this.$active)
	}, b.prototype.to = function (b) {
		var c = this, d = this.getActiveIndex();
		return b > this.$items.length - 1 || 0 > b ? void 0 : this.sliding ? this.$element.one("slid", function () {
			c.to(b)
		}) : d == b ? this.pause().cycle() : this.slide(b > d ? "next" : "prev", a(this.$items[b]))
	}, b.prototype.pause = function (b) {
		return b || (this.paused = !0), this.$element.find(".next, .prev").length && a.support.transition.end && (this.$element.trigger(a.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
	}, b.prototype.next = function () {
		return this.sliding ? void 0 : this.slide("next")
	}, b.prototype.prev = function () {
		return this.sliding ? void 0 : this.slide("prev")
	}, b.prototype.slide = function (b, c) {
		var d = this.$element.find(".item.active"), e = c || d[b](), f = this.interval, g = "next" == b ? "left" : "right", h = "next" == b ? "first" : "last", i = this;
		if (!e.length) {
			if (!this.options.wrap)return;
			e = this.$element.find(".item")[h]()
		}
		this.sliding = !0, f && this.pause();
		var j = a.Event("slide.bs.carousel", {relatedTarget: e[0], direction: g});
		if (!e.hasClass("active")) {
			if (this.$indicators.length && (this.$indicators.find(".active").removeClass("active"), this.$element.one("slid", function () {
					var b = a(i.$indicators.children()[i.getActiveIndex()]);
					b && b.addClass("active")
				})), a.support.transition && this.$element.hasClass("slide")) {
				if (this.$element.trigger(j), j.isDefaultPrevented())return;
				e.addClass(b), e[0].offsetWidth, d.addClass(g), e.addClass(g), d.one(a.support.transition.end, function () {
					e.removeClass([b, g].join(" ")).addClass("active"), d.removeClass(["active", g].join(" ")), i.sliding = !1, setTimeout(function () {
						i.$element.trigger("slid")
					}, 0)
				}).emulateTransitionEnd(600)
			} else {
				if (this.$element.trigger(j), j.isDefaultPrevented())return;
				d.removeClass("active"), e.addClass("active"), this.sliding = !1, this.$element.trigger("slid")
			}
			return f && this.cycle(), this
		}
	};
	var c = a.fn.carousel;
	a.fn.carousel = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.carousel"), f = a.extend({}, b.DEFAULTS, d.data(), "object" == typeof c && c), g = "string" == typeof c ? c : f.slide;
			e || d.data("bs.carousel", e = new b(this, f)), "number" == typeof c ? e.to(c) : g ? e[g]() : f.interval && e.pause().cycle()
		})
	}, a.fn.carousel.Constructor = b, a.fn.carousel.noConflict = function () {
		return a.fn.carousel = c, this
	}, a(document).on("click.bs.carousel.data-api", "[data-slide], [data-slide-to]", function (b) {
		var c, d = a(this), e = a(d.attr("data-target") || (c = d.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, "")), f = a.extend({}, e.data(), d.data()), g = d.attr("data-slide-to");
		g && (f.interval = !1), e.carousel(f), (g = d.attr("data-slide-to")) && e.data("bs.carousel").to(g), b.preventDefault()
	}), a(window).on("load", function () {
		a('[data-ride="carousel"]').each(function () {
			var b = a(this);
			b.carousel(b.data())
		})
	})
}(window.jQuery), +function (a) {
	"use strict";
	var b = function (c, d) {
		this.$element = a(c), this.options = a.extend({}, b.DEFAULTS, d), this.transitioning = null, this.options.parent && (this.$parent = a(this.options.parent)), this.options.toggle && this.toggle()
	};
	b.DEFAULTS = {toggle: !0}, b.prototype.dimension = function () {
		var a = this.$element.hasClass("width");
		return a ? "width" : "height"
	}, b.prototype.show = function () {
		if (!this.transitioning && !this.$element.hasClass("in")) {
			var b = a.Event("show.bs.collapse");
			if (this.$element.trigger(b), !b.isDefaultPrevented()) {
				var c = this.$parent && this.$parent.find("> .panel > .in");
				if (c && c.length) {
					var d = c.data("bs.collapse");
					if (d && d.transitioning)return;
					c.collapse("hide"), d || c.data("bs.collapse", null)
				}
				var e = this.dimension();
				this.$element.removeClass("collapse").addClass("collapsing")[e](0), this.transitioning = 1;
				var f = function () {
					this.$element.removeClass("collapsing").addClass("in")[e]("auto"), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
				};
				if (!a.support.transition)return f.call(this);
				var g = a.camelCase(["scroll", e].join("-"));
				this.$element.one(a.support.transition.end, a.proxy(f, this)).emulateTransitionEnd(350)[e](this.$element[0][g])
			}
		}
	}, b.prototype.hide = function () {
		if (!this.transitioning && this.$element.hasClass("in")) {
			var b = a.Event("hide.bs.collapse");
			if (this.$element.trigger(b), !b.isDefaultPrevented()) {
				var c = this.dimension();
				this.$element[c](this.$element[c]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"), this.transitioning = 1;
				var d = function () {
					this.transitioning = 0, this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")
				};
				return a.support.transition ? void this.$element[c](0).one(a.support.transition.end, a.proxy(d, this)).emulateTransitionEnd(350) : d.call(this)
			}
		}
	}, b.prototype.toggle = function () {
		this[this.$element.hasClass("in") ? "hide" : "show"]()
	};
	var c = a.fn.collapse;
	a.fn.collapse = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.collapse"), f = a.extend({}, b.DEFAULTS, d.data(), "object" == typeof c && c);
			e || d.data("bs.collapse", e = new b(this, f)), "string" == typeof c && e[c]()
		})
	}, a.fn.collapse.Constructor = b, a.fn.collapse.noConflict = function () {
		return a.fn.collapse = c, this
	}, a(document).on("click.bs.collapse.data-api", "[data-toggle=collapse]", function (b) {
		var c, d = a(this), e = d.attr("data-target") || b.preventDefault() || (c = d.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, ""), f = a(e), g = f.data("bs.collapse"), h = g ? "toggle" : d.data(), i = d.attr("data-parent"), j = i && a(i);
		g && g.transitioning || (j && j.find('[data-toggle=collapse][data-parent="' + i + '"]').not(d).addClass("collapsed"), d[f.hasClass("in") ? "addClass" : "removeClass"]("collapsed")), f.collapse(h)
	})
}(window.jQuery), +function (a) {
	"use strict";
	function b() {
		a(d).remove(), a(e).each(function (b) {
			var d = c(a(this));
			d.hasClass("open") && (d.trigger(b = a.Event("hide.bs.dropdown")), b.isDefaultPrevented() || d.removeClass("open").trigger("hidden.bs.dropdown"))
		})
	}

	function c(b) {
		var c = b.attr("data-target");
		c || (c = b.attr("href"), c = c && /#/.test(c) && c.replace(/.*(?=#[^\s]*$)/, ""));
		var d = c && a(c);
		return d && d.length ? d : b.parent()
	}

	var d = ".dropdown-backdrop", e = "[data-toggle=dropdown]", f = function (b) {
		a(b).on("click.bs.dropdown", this.toggle)
	};
	f.prototype.toggle = function (d) {
		var e = a(this);
		if (!e.is(".disabled, :disabled")) {
			var f = c(e), g = f.hasClass("open");
			if (b(), !g) {
				if ("ontouchstart" in document.documentElement && !f.closest(".navbar-nav").length && a('<div class="dropdown-backdrop"/>').insertAfter(a(this)).on("click", b), f.trigger(d = a.Event("show.bs.dropdown")), d.isDefaultPrevented())return;
				f.toggleClass("open").trigger("shown.bs.dropdown"), e.focus()
			}
			return !1
		}
	}, f.prototype.keydown = function (b) {
		if (/(38|40|27)/.test(b.keyCode)) {
			var d = a(this);
			if (b.preventDefault(), b.stopPropagation(), !d.is(".disabled, :disabled")) {
				var f = c(d), g = f.hasClass("open");
				if (!g || g && 27 == b.keyCode)return 27 == b.which && f.find(e).focus(), d.click();
				var h = a("[role=menu] li:not(.divider):visible a", f);
				if (h.length) {
					var i = h.index(h.filter(":focus"));
					38 == b.keyCode && i > 0 && i--, 40 == b.keyCode && i < h.length - 1 && i++, ~i || (i = 0), h.eq(i).focus()
				}
			}
		}
	};
	var g = a.fn.dropdown;
	a.fn.dropdown = function (b) {
		return this.each(function () {
			var c = a(this), d = c.data("dropdown");
			d || c.data("dropdown", d = new f(this)), "string" == typeof b && d[b].call(c)
		})
	}, a.fn.dropdown.Constructor = f, a.fn.dropdown.noConflict = function () {
		return a.fn.dropdown = g, this
	}, a(document).on("click.bs.dropdown.data-api", b).on("click.bs.dropdown.data-api", ".dropdown form", function (a) {
		a.stopPropagation()
	}).on("click.bs.dropdown.data-api", e, f.prototype.toggle).on("keydown.bs.dropdown.data-api", e + ", [role=menu]", f.prototype.keydown)
}(window.jQuery), +function (a) {
	"use strict";
	var b = function (b, c) {
		this.options = c, this.$element = a(b), this.$backdrop = this.isShown = null, this.options.remote && this.$element.load(this.options.remote)
	};
	b.DEFAULTS = {backdrop: !0, keyboard: !0, show: !0}, b.prototype.toggle = function (a) {
		return this[this.isShown ? "hide" : "show"](a)
	}, b.prototype.show = function (b) {
		var c = this, d = a.Event("show.bs.modal", {relatedTarget: b});
		this.$element.trigger(d), this.isShown || d.isDefaultPrevented() || (this.isShown = !0, this.escape(), this.$element.on("click.dismiss.modal", '[data-dismiss="modal"]', a.proxy(this.hide, this)), this.backdrop(function () {
			var d = a.support.transition && c.$element.hasClass("fade");
			c.$element.parent().length || c.$element.appendTo(document.body), c.$element.show(), d && c.$element[0].offsetWidth, c.$element.addClass("in").attr("aria-hidden", !1), c.enforceFocus();
			var e = a.Event("shown.bs.modal", {relatedTarget: b});
			d ? c.$element.find(".modal-dialog").one(a.support.transition.end, function () {
				c.$element.focus().trigger(e)
			}).emulateTransitionEnd(300) : c.$element.focus().trigger(e)
		}))
	}, b.prototype.hide = function (b) {
		b && b.preventDefault(), b = a.Event("hide.bs.modal"), this.$element.trigger(b), this.isShown && !b.isDefaultPrevented() && (this.isShown = !1, this.escape(), a(document).off("focusin.bs.modal"), this.$element.removeClass("in").attr("aria-hidden", !0).off("click.dismiss.modal"), a.support.transition && this.$element.hasClass("fade") ? this.$element.one(a.support.transition.end, a.proxy(this.hideModal, this)).emulateTransitionEnd(300) : this.hideModal())
	}, b.prototype.enforceFocus = function () {
		a(document).off("focusin.bs.modal").on("focusin.bs.modal", a.proxy(function (a) {
			this.$element[0] === a.target || this.$element.has(a.target).length || this.$element.focus()
		}, this))
	}, b.prototype.escape = function () {
		this.isShown && this.options.keyboard ? this.$element.on("keyup.dismiss.bs.modal", a.proxy(function (a) {
			27 == a.which && this.hide()
		}, this)) : this.isShown || this.$element.off("keyup.dismiss.bs.modal")
	}, b.prototype.hideModal = function () {
		var a = this;
		this.$element.hide(), this.backdrop(function () {
			a.removeBackdrop(), a.$element.trigger("hidden.bs.modal")
		})
	}, b.prototype.removeBackdrop = function () {
		this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
	}, b.prototype.backdrop = function (b) {
		var c = this.$element.hasClass("fade") ? "fade" : "";
		if (this.isShown && this.options.backdrop) {
			var d = a.support.transition && c;
			if (this.$backdrop = a('<div class="modal-backdrop ' + c + '" />').appendTo(document.body), this.$element.on("click.dismiss.modal", a.proxy(function (a) {
					a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus.call(this.$element[0]) : this.hide.call(this))
				}, this)), d && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !b)return;
			d ? this.$backdrop.one(a.support.transition.end, b).emulateTransitionEnd(150) : b()
		} else!this.isShown && this.$backdrop ? (this.$backdrop.removeClass("in"), a.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one(a.support.transition.end, b).emulateTransitionEnd(150) : b()) : b && b()
	};
	var c = a.fn.modal;
	a.fn.modal = function (c, d) {
		return this.each(function () {
			var e = a(this), f = e.data("bs.modal"), g = a.extend({}, b.DEFAULTS, e.data(), "object" == typeof c && c);
			f || e.data("bs.modal", f = new b(this, g)), "string" == typeof c ? f[c](d) : g.show && f.show(d)
		})
	}, a.fn.modal.Constructor = b, a.fn.modal.noConflict = function () {
		return a.fn.modal = c, this
	}, a(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (b) {
		var c = a(this), d = c.attr("href"), e = a(c.attr("data-target") || d && d.replace(/.*(?=#[^\s]+$)/, "")), f = e.data("modal") ? "toggle" : a.extend({remote: !/#/.test(d) && d}, e.data(), c.data());
		b.preventDefault(), e.modal(f, this).one("hide", function () {
			c.is(":visible") && c.focus()
		})
	}), a(document).on("show.bs.modal", ".modal", function () {
		a(document.body).addClass("modal-open")
	}).on("hidden.bs.modal", ".modal", function () {
		a(document.body).removeClass("modal-open")
	})
}(window.jQuery), +function (a) {
	"use strict";
	var b = function (a, b) {
		this.type = this.options = this.enabled = this.timeout = this.hoverState = this.$element = null, this.init("tooltip", a, b)
	};
	b.DEFAULTS = {
		animation: !0,
		placement: "top",
		selector: !1,
		template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
		trigger: "hover focus",
		title: "",
		delay: 0,
		html: !1,
		container: !1
	}, b.prototype.init = function (b, c, d) {
		this.enabled = !0, this.type = b, this.$element = a(c), this.options = this.getOptions(d);
		for (var e = this.options.trigger.split(" "), f = e.length; f--;) {
			var g = e[f];
			if ("click" == g)this.$element.on("click." + this.type, this.options.selector, a.proxy(this.toggle, this)); else if ("manual" != g) {
				var h = "hover" == g ? "mouseenter" : "focus", i = "hover" == g ? "mouseleave" : "blur";
				this.$element.on(h + "." + this.type, this.options.selector, a.proxy(this.enter, this)), this.$element.on(i + "." + this.type, this.options.selector, a.proxy(this.leave, this))
			}
		}
		this.options.selector ? this._options = a.extend({}, this.options, {
			trigger: "manual",
			selector: ""
		}) : this.fixTitle()
	}, b.prototype.getDefaults = function () {
		return b.DEFAULTS
	}, b.prototype.getOptions = function (b) {
		return b = a.extend({}, this.getDefaults(), this.$element.data(), b), b.delay && "number" == typeof b.delay && (b.delay = {
			show: b.delay,
			hide: b.delay
		}), b
	}, b.prototype.getDelegateOptions = function () {
		var b = {}, c = this.getDefaults();
		return this._options && a.each(this._options, function (a, d) {
			c[a] != d && (b[a] = d)
		}), b
	}, b.prototype.enter = function (b) {
		var c = b instanceof this.constructor ? b : a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
		return clearTimeout(c.timeout), c.hoverState = "in", c.options.delay && c.options.delay.show ? void(c.timeout = setTimeout(function () {
			"in" == c.hoverState && c.show()
		}, c.options.delay.show)) : c.show()
	}, b.prototype.leave = function (b) {
		var c = b instanceof this.constructor ? b : a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
		return clearTimeout(c.timeout), c.hoverState = "out", c.options.delay && c.options.delay.hide ? void(c.timeout = setTimeout(function () {
			"out" == c.hoverState && c.hide()
		}, c.options.delay.hide)) : c.hide()
	}, b.prototype.show = function () {
		var b = a.Event("show.bs." + this.type);
		if (this.hasContent() && this.enabled) {
			if (this.$element.trigger(b), b.isDefaultPrevented())return;
			var c = this.tip();
			this.setContent(), this.options.animation && c.addClass("fade");
			var d = "function" == typeof this.options.placement ? this.options.placement.call(this, c[0], this.$element[0]) : this.options.placement, e = /\s?auto?\s?/i, f = e.test(d);
			f && (d = d.replace(e, "") || "top"), c.detach().css({
				top: 0,
				left: 0,
				display: "block"
			}).addClass(d), this.options.container ? c.appendTo(this.options.container) : c.insertAfter(this.$element);
			var g = this.getPosition(), h = c[0].offsetWidth, i = c[0].offsetHeight;
			if (f) {
				var j = this.$element.parent(), k = d, l = document.documentElement.scrollTop || document.body.scrollTop, m = "body" == this.options.container ? window.innerWidth : j.outerWidth(), n = "body" == this.options.container ? window.innerHeight : j.outerHeight(), o = "body" == this.options.container ? 0 : j.offset().left;
				d = "bottom" == d && g.top + g.height + i - l > n ? "top" : "top" == d && g.top - l - i < 0 ? "bottom" : "right" == d && g.right + h > m ? "left" : "left" == d && g.left - h < o ? "right" : d, c.removeClass(k).addClass(d)
			}
			var p = this.getCalculatedOffset(d, g, h, i);
			this.applyPlacement(p, d), this.$element.trigger("shown.bs." + this.type)
		}
	}, b.prototype.applyPlacement = function (a, b) {
		var c, d = this.tip(), e = d[0].offsetWidth, f = d[0].offsetHeight, g = parseInt(d.css("margin-top"), 10), h = parseInt(d.css("margin-left"), 10);
		isNaN(g) && (g = 0), isNaN(h) && (h = 0), a.top = a.top + g, a.left = a.left + h, d.offset(a).addClass("in");
		var i = d[0].offsetWidth, j = d[0].offsetHeight;
		if ("top" == b && j != f && (c = !0, a.top = a.top + f - j), /bottom|top/.test(b)) {
			var k = 0;
			a.left < 0 && (k = -2 * a.left, a.left = 0, d.offset(a), i = d[0].offsetWidth, j = d[0].offsetHeight), this.replaceArrow(k - e + i, i, "left")
		} else this.replaceArrow(j - f, j, "top");
		c && d.offset(a)
	}, b.prototype.replaceArrow = function (a, b, c) {
		this.arrow().css(c, a ? 50 * (1 - a / b) + "%" : "")
	}, b.prototype.setContent = function () {
		var a = this.tip(), b = this.getTitle();
		a.find(".tooltip-inner")[this.options.html ? "html" : "text"](b), a.removeClass("fade in top bottom left right")
	}, b.prototype.hide = function () {
		function b() {
			"in" != c.hoverState && d.detach()
		}

		var c = this, d = this.tip(), e = a.Event("hide.bs." + this.type);
		return this.$element.trigger(e), e.isDefaultPrevented() ? void 0 : (d.removeClass("in"), a.support.transition && this.$tip.hasClass("fade") ? d.one(a.support.transition.end, b).emulateTransitionEnd(150) : b(), this.$element.trigger("hidden.bs." + this.type), this)
	}, b.prototype.fixTitle = function () {
		var a = this.$element;
		(a.attr("title") || "string" != typeof a.attr("data-original-title")) && a.attr("data-original-title", a.attr("title") || "").attr("title", "")
	}, b.prototype.hasContent = function () {
		return this.getTitle()
	}, b.prototype.getPosition = function () {
		var b = this.$element[0];
		return a.extend({}, "function" == typeof b.getBoundingClientRect ? b.getBoundingClientRect() : {
			width: b.offsetWidth,
			height: b.offsetHeight
		}, this.$element.offset())
	}, b.prototype.getCalculatedOffset = function (a, b, c, d) {
		return "bottom" == a ? {
			top: b.top + b.height,
			left: b.left + b.width / 2 - c / 2
		} : "top" == a ? {
			top: b.top - d,
			left: b.left + b.width / 2 - c / 2
		} : "left" == a ? {top: b.top + b.height / 2 - d / 2, left: b.left - c} : {
			top: b.top + b.height / 2 - d / 2,
			left: b.left + b.width
		}
	}, b.prototype.getTitle = function () {
		var a, b = this.$element, c = this.options;
		return a = b.attr("data-original-title") || ("function" == typeof c.title ? c.title.call(b[0]) : c.title)
	}, b.prototype.tip = function () {
		return this.$tip = this.$tip || a(this.options.template)
	}, b.prototype.arrow = function () {
		return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
	}, b.prototype.validate = function () {
		this.$element[0].parentNode || (this.hide(), this.$element = null, this.options = null)
	}, b.prototype.enable = function () {
		this.enabled = !0
	}, b.prototype.disable = function () {
		this.enabled = !1
	}, b.prototype.toggleEnabled = function () {
		this.enabled = !this.enabled
	}, b.prototype.toggle = function (b) {
		var c = b ? a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type) : this;
		c.tip().hasClass("in") ? c.leave(c) : c.enter(c)
	}, b.prototype.destroy = function () {
		this.hide().$element.off("." + this.type).removeData("bs." + this.type)
	};
	var c = a.fn.tooltip;
	a.fn.tooltip = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.tooltip"), f = "object" == typeof c && c;
			e || d.data("bs.tooltip", e = new b(this, f)), "string" == typeof c && e[c]()
		})
	}, a.fn.tooltip.Constructor = b, a.fn.tooltip.noConflict = function () {
		return a.fn.tooltip = c, this
	}
}(window.jQuery), +function (a) {
	"use strict";
	var b = function (a, b) {
		this.init("popover", a, b)
	};
	if (!a.fn.tooltip)throw new Error("Popover requires tooltip.js");
	b.DEFAULTS = a.extend({}, a.fn.tooltip.Constructor.DEFAULTS, {
		placement: "right",
		trigger: "click",
		content: "",
		template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
	}), b.prototype = a.extend({}, a.fn.tooltip.Constructor.prototype), b.prototype.constructor = b, b.prototype.getDefaults = function () {
		return b.DEFAULTS
	}, b.prototype.setContent = function () {
		var a = this.tip(), b = this.getTitle(), c = this.getContent();
		a.find(".popover-title")[this.options.html ? "html" : "text"](b), a.find(".popover-content")[this.options.html ? "html" : "text"](c), a.removeClass("fade top bottom left right in"), a.find(".popover-title").html() || a.find(".popover-title").hide()
	}, b.prototype.hasContent = function () {
		return this.getTitle() || this.getContent()
	}, b.prototype.getContent = function () {
		var a = this.$element, b = this.options;
		return a.attr("data-content") || ("function" == typeof b.content ? b.content.call(a[0]) : b.content)
	}, b.prototype.arrow = function () {
		return this.$arrow = this.$arrow || this.tip().find(".arrow")
	}, b.prototype.tip = function () {
		return this.$tip || (this.$tip = a(this.options.template)), this.$tip
	};
	var c = a.fn.popover;
	a.fn.popover = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.popover"), f = "object" == typeof c && c;
			e || d.data("bs.popover", e = new b(this, f)), "string" == typeof c && e[c]()
		})
	}, a.fn.popover.Constructor = b, a.fn.popover.noConflict = function () {
		return a.fn.popover = c, this
	}
}(window.jQuery), +function (a) {
	"use strict";
	function b(c, d) {
		var e, f = a.proxy(this.process, this);
		this.$element = a(a(c).is("body") ? window : c), this.$body = a("body"), this.$scrollElement = this.$element.on("scroll.bs.scroll-spy.data-api", f), this.options = a.extend({}, b.DEFAULTS, d), this.selector = (this.options.target || (e = a(c).attr("href")) && e.replace(/.*(?=#[^\s]+$)/, "") || "") + " .nav li > a", this.offsets = a([]), this.targets = a([]), this.activeTarget = null, this.refresh(), this.process()
	}

	b.DEFAULTS = {offset: 10}, b.prototype.refresh = function () {
		var b = this.$element[0] == window ? "offset" : "position";
		this.offsets = a([]), this.targets = a([]);
		var c = this;
		this.$body.find(this.selector).map(function () {
			var d = a(this), e = d.data("target") || d.attr("href"), f = /^#\w/.test(e) && a(e);
			return f && f.length && [[f[b]().top + (!a.isWindow(c.$scrollElement.get(0)) && c.$scrollElement.scrollTop()), e]] || null
		}).sort(function (a, b) {
			return a[0] - b[0]
		}).each(function () {
			c.offsets.push(this[0]), c.targets.push(this[1])
		})
	}, b.prototype.process = function () {
		var a, b = this.$scrollElement.scrollTop() + this.options.offset, c = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight, d = c - this.$scrollElement.height(), e = this.offsets, f = this.targets, g = this.activeTarget;
		if (b >= d)return g != (a = f.last()[0]) && this.activate(a);
		for (a = e.length; a--;)g != f[a] && b >= e[a] && (!e[a + 1] || b <= e[a + 1]) && this.activate(f[a])
	}, b.prototype.activate = function (b) {
		this.activeTarget = b, a(this.selector).parents(".active").removeClass("active");
		var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]', d = a(c).parents("li").addClass("active");
		d.parent(".dropdown-menu").length && (d = d.closest("li.dropdown").addClass("active")), d.trigger("activate")
	};
	var c = a.fn.scrollspy;
	a.fn.scrollspy = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.scrollspy"), f = "object" == typeof c && c;
			e || d.data("bs.scrollspy", e = new b(this, f)), "string" == typeof c && e[c]()
		})
	}, a.fn.scrollspy.Constructor = b, a.fn.scrollspy.noConflict = function () {
		return a.fn.scrollspy = c, this
	}, a(window).on("load", function () {
		a('[data-spy="scroll"]').each(function () {
			var b = a(this);
			b.scrollspy(b.data())
		})
	})
}(window.jQuery), +function (a) {
	"use strict";
	var b = function (b) {
		this.element = a(b)
	};
	b.prototype.show = function () {
		var b = this.element, c = b.closest("ul:not(.dropdown-menu)"), d = b.attr("data-target");
		if (d || (d = b.attr("href"), d = d && d.replace(/.*(?=#[^\s]*$)/, "")), !b.parent("li").hasClass("active")) {
			var e = c.find(".active:last a")[0], f = a.Event("show.bs.tab", {relatedTarget: e});
			if (b.trigger(f), !f.isDefaultPrevented()) {
				var g = a(d);
				this.activate(b.parent("li"), c), this.activate(g, g.parent(), function () {
					b.trigger({type: "shown.bs.tab", relatedTarget: e})
				})
			}
		}
	}, b.prototype.activate = function (b, c, d) {
		function e() {
			f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"), b.addClass("active"), g ? (b[0].offsetWidth, b.addClass("in")) : b.removeClass("fade"), b.parent(".dropdown-menu") && b.closest("li.dropdown").addClass("active"), d && d()
		}

		var f = c.find("> .active"), g = d && a.support.transition && f.hasClass("fade");
		g ? f.one(a.support.transition.end, e).emulateTransitionEnd(150) : e(), f.removeClass("in")
	};
	var c = a.fn.tab;
	a.fn.tab = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.tab");
			e || d.data("bs.tab", e = new b(this)), "string" == typeof c && e[c]()
		})
	}, a.fn.tab.Constructor = b, a.fn.tab.noConflict = function () {
		return a.fn.tab = c, this
	}, a(document).on("click.bs.tab.data-api", '[data-toggle="tab"], [data-toggle="pill"]', function (b) {
		b.preventDefault(), a(this).tab("show")
	})
}(window.jQuery), +function (a) {
	"use strict";
	var b = function (c, d) {
		this.options = a.extend({}, b.DEFAULTS, d), this.$window = a(window).on("scroll.bs.affix.data-api", a.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", a.proxy(this.checkPositionWithEventLoop, this)), this.$element = a(c), this.affixed = this.unpin = null, this.checkPosition()
	};
	b.RESET = "affix affix-top affix-bottom", b.DEFAULTS = {offset: 0}, b.prototype.checkPositionWithEventLoop = function () {
		setTimeout(a.proxy(this.checkPosition, this), 1)
	}, b.prototype.checkPosition = function () {
		if (this.$element.is(":visible")) {
			var c = a(document).height(), d = this.$window.scrollTop(), e = this.$element.offset(), f = this.options.offset, g = f.top, h = f.bottom;
			"object" != typeof f && (h = g = f), "function" == typeof g && (g = f.top()), "function" == typeof h && (h = f.bottom());
			var i = null != this.unpin && d + this.unpin <= e.top ? !1 : null != h && e.top + this.$element.height() >= c - h ? "bottom" : null != g && g >= d ? "top" : !1;
			this.affixed !== i && (this.unpin && this.$element.css("top", ""), this.affixed = i, this.unpin = "bottom" == i ? e.top - d : null, this.$element.removeClass(b.RESET).addClass("affix" + (i ? "-" + i : "")), "bottom" == i && this.$element.offset({top: document.body.offsetHeight - h - this.$element.height()}))
		}
	};
	var c = a.fn.affix;
	a.fn.affix = function (c) {
		return this.each(function () {
			var d = a(this), e = d.data("bs.affix"), f = "object" == typeof c && c;
			e || d.data("bs.affix", e = new b(this, f)), "string" == typeof c && e[c]()
		})
	}, a.fn.affix.Constructor = b, a.fn.affix.noConflict = function () {
		return a.fn.affix = c, this
	}, a(window).on("load", function () {
		a('[data-spy="affix"]').each(function () {
			var b = a(this), c = b.data();
			c.offset = c.offset || {}, c.offsetBottom && (c.offset.bottom = c.offsetBottom), c.offsetTop && (c.offset.top = c.offsetTop), b.affix(c)
		})
	})
}(window.jQuery), function () {
	var a = this, b = a._, c = {}, d = Array.prototype, e = Object.prototype, f = Function.prototype, g = d.push, h = d.slice, i = d.concat, j = e.toString, k = e.hasOwnProperty, l = d.forEach, m = d.map, n = d.reduce, o = d.reduceRight, p = d.filter, q = d.every, r = d.some, s = d.indexOf, t = d.lastIndexOf, u = Array.isArray, v = Object.keys, w = f.bind, x = function (a) {
		return a instanceof x ? a : this instanceof x ? void(this._wrapped = a) : new x(a)
	};
	"undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = x), exports._ = x) : a._ = x, x.VERSION = "1.6.0";
	var y = x.each = x.forEach = function (a, b, d) {
		if (null == a)return a;
		if (l && a.forEach === l)a.forEach(b, d); else if (a.length === +a.length) {
			for (var e = 0, f = a.length; f > e; e++)if (b.call(d, a[e], e, a) === c)return
		} else for (var g = x.keys(a), e = 0, f = g.length; f > e; e++)if (b.call(d, a[g[e]], g[e], a) === c)return;
		return a
	};
	x.map = x.collect = function (a, b, c) {
		var d = [];
		return null == a ? d : m && a.map === m ? a.map(b, c) : (y(a, function (a, e, f) {
			d.push(b.call(c, a, e, f))
		}), d)
	};
	var z = "Reduce of empty array with no initial value";
	x.reduce = x.foldl = x.inject = function (a, b, c, d) {
		var e = arguments.length > 2;
		if (null == a && (a = []), n && a.reduce === n)return d && (b = x.bind(b, d)), e ? a.reduce(b, c) : a.reduce(b);
		if (y(a, function (a, f, g) {
				e ? c = b.call(d, c, a, f, g) : (c = a, e = !0)
			}), !e)throw new TypeError(z);
		return c
	}, x.reduceRight = x.foldr = function (a, b, c, d) {
		var e = arguments.length > 2;
		if (null == a && (a = []), o && a.reduceRight === o)return d && (b = x.bind(b, d)), e ? a.reduceRight(b, c) : a.reduceRight(b);
		var f = a.length;
		if (f !== +f) {
			var g = x.keys(a);
			f = g.length
		}
		if (y(a, function (h, i, j) {
				i = g ? g[--f] : --f, e ? c = b.call(d, c, a[i], i, j) : (c = a[i], e = !0)
			}), !e)throw new TypeError(z);
		return c
	}, x.find = x.detect = function (a, b, c) {
		var d;
		return A(a, function (a, e, f) {
			return b.call(c, a, e, f) ? (d = a, !0) : void 0
		}), d
	}, x.filter = x.select = function (a, b, c) {
		var d = [];
		return null == a ? d : p && a.filter === p ? a.filter(b, c) : (y(a, function (a, e, f) {
			b.call(c, a, e, f) && d.push(a)
		}), d)
	}, x.reject = function (a, b, c) {
		return x.filter(a, function (a, d, e) {
			return !b.call(c, a, d, e)
		}, c)
	}, x.every = x.all = function (a, b, d) {
		b || (b = x.identity);
		var e = !0;
		return null == a ? e : q && a.every === q ? a.every(b, d) : (y(a, function (a, f, g) {
			return (e = e && b.call(d, a, f, g)) ? void 0 : c
		}), !!e)
	};
	var A = x.some = x.any = function (a, b, d) {
		b || (b = x.identity);
		var e = !1;
		return null == a ? e : r && a.some === r ? a.some(b, d) : (y(a, function (a, f, g) {
			return e || (e = b.call(d, a, f, g)) ? c : void 0
		}), !!e)
	};
	x.contains = x.include = function (a, b) {
		return null == a ? !1 : s && a.indexOf === s ? -1 != a.indexOf(b) : A(a, function (a) {
			return a === b
		})
	}, x.invoke = function (a, b) {
		var c = h.call(arguments, 2), d = x.isFunction(b);
		return x.map(a, function (a) {
			return (d ? b : a[b]).apply(a, c)
		})
	}, x.pluck = function (a, b) {
		return x.map(a, x.property(b))
	}, x.where = function (a, b) {
		return x.filter(a, x.matches(b))
	}, x.findWhere = function (a, b) {
		return x.find(a, x.matches(b))
	}, x.max = function (a, b, c) {
		if (!b && x.isArray(a) && a[0] === +a[0] && a.length < 65535)return Math.max.apply(Math, a);
		var d = -1 / 0, e = -1 / 0;
		return y(a, function (a, f, g) {
			var h = b ? b.call(c, a, f, g) : a;
			h > e && (d = a, e = h)
		}), d
	}, x.min = function (a, b, c) {
		if (!b && x.isArray(a) && a[0] === +a[0] && a.length < 65535)return Math.min.apply(Math, a);
		var d = 1 / 0, e = 1 / 0;
		return y(a, function (a, f, g) {
			var h = b ? b.call(c, a, f, g) : a;
			e > h && (d = a, e = h)
		}), d
	}, x.shuffle = function (a) {
		var b, c = 0, d = [];
		return y(a, function (a) {
			b = x.random(c++), d[c - 1] = d[b], d[b] = a
		}), d
	}, x.sample = function (a, b, c) {
		return null == b || c ? (a.length !== +a.length && (a = x.values(a)), a[x.random(a.length - 1)]) : x.shuffle(a).slice(0, Math.max(0, b))
	};
	var B = function (a) {
		return null == a ? x.identity : x.isFunction(a) ? a : x.property(a)
	};
	x.sortBy = function (a, b, c) {
		return b = B(b), x.pluck(x.map(a, function (a, d, e) {
			return {value: a, index: d, criteria: b.call(c, a, d, e)}
		}).sort(function (a, b) {
			var c = a.criteria, d = b.criteria;
			if (c !== d) {
				if (c > d || void 0 === c)return 1;
				if (d > c || void 0 === d)return -1
			}
			return a.index - b.index
		}), "value")
	};
	var C = function (a) {
		return function (b, c, d) {
			var e = {};
			return c = B(c), y(b, function (f, g) {
				var h = c.call(d, f, g, b);
				a(e, h, f)
			}), e
		}
	};
	x.groupBy = C(function (a, b, c) {
		x.has(a, b) ? a[b].push(c) : a[b] = [c]
	}), x.indexBy = C(function (a, b, c) {
		a[b] = c
	}), x.countBy = C(function (a, b) {
		x.has(a, b) ? a[b]++ : a[b] = 1
	}), x.sortedIndex = function (a, b, c, d) {
		c = B(c);
		for (var e = c.call(d, b), f = 0, g = a.length; g > f;) {
			var h = f + g >>> 1;
			c.call(d, a[h]) < e ? f = h + 1 : g = h
		}
		return f
	}, x.toArray = function (a) {
		return a ? x.isArray(a) ? h.call(a) : a.length === +a.length ? x.map(a, x.identity) : x.values(a) : []
	}, x.size = function (a) {
		return null == a ? 0 : a.length === +a.length ? a.length : x.keys(a).length
	}, x.first = x.head = x.take = function (a, b, c) {
		return null == a ? void 0 : null == b || c ? a[0] : 0 > b ? [] : h.call(a, 0, b)
	}, x.initial = function (a, b, c) {
		return h.call(a, 0, a.length - (null == b || c ? 1 : b))
	}, x.last = function (a, b, c) {
		return null == a ? void 0 : null == b || c ? a[a.length - 1] : h.call(a, Math.max(a.length - b, 0))
	}, x.rest = x.tail = x.drop = function (a, b, c) {
		return h.call(a, null == b || c ? 1 : b)
	}, x.compact = function (a) {
		return x.filter(a, x.identity)
	};
	var D = function (a, b, c) {
		return b && x.every(a, x.isArray) ? i.apply(c, a) : (y(a, function (a) {
			x.isArray(a) || x.isArguments(a) ? b ? g.apply(c, a) : D(a, b, c) : c.push(a)
		}), c)
	};
	x.flatten = function (a, b) {
		return D(a, b, [])
	}, x.without = function (a) {
		return x.difference(a, h.call(arguments, 1))
	}, x.partition = function (a, b) {
		var c = [], d = [];
		return y(a, function (a) {
			(b(a) ? c : d).push(a)
		}), [c, d]
	}, x.uniq = x.unique = function (a, b, c, d) {
		x.isFunction(b) && (d = c, c = b, b = !1);
		var e = c ? x.map(a, c, d) : a, f = [], g = [];
		return y(e, function (c, d) {
			(b ? d && g[g.length - 1] === c : x.contains(g, c)) || (g.push(c), f.push(a[d]))
		}), f
	}, x.union = function () {
		return x.uniq(x.flatten(arguments, !0))
	}, x.intersection = function (a) {
		var b = h.call(arguments, 1);
		return x.filter(x.uniq(a), function (a) {
			return x.every(b, function (b) {
				return x.contains(b, a)
			})
		})
	}, x.difference = function (a) {
		var b = i.apply(d, h.call(arguments, 1));
		return x.filter(a, function (a) {
			return !x.contains(b, a)
		})
	}, x.zip = function () {
		for (var a = x.max(x.pluck(arguments, "length").concat(0)), b = new Array(a), c = 0; a > c; c++)b[c] = x.pluck(arguments, "" + c);
		return b
	}, x.object = function (a, b) {
		if (null == a)return {};
		for (var c = {}, d = 0, e = a.length; e > d; d++)b ? c[a[d]] = b[d] : c[a[d][0]] = a[d][1];
		return c
	}, x.indexOf = function (a, b, c) {
		if (null == a)return -1;
		var d = 0, e = a.length;
		if (c) {
			if ("number" != typeof c)return d = x.sortedIndex(a, b), a[d] === b ? d : -1;
			d = 0 > c ? Math.max(0, e + c) : c
		}
		if (s && a.indexOf === s)return a.indexOf(b, c);
		for (; e > d; d++)if (a[d] === b)return d;
		return -1
	}, x.lastIndexOf = function (a, b, c) {
		if (null == a)return -1;
		var d = null != c;
		if (t && a.lastIndexOf === t)return d ? a.lastIndexOf(b, c) : a.lastIndexOf(b);
		for (var e = d ? c : a.length; e--;)if (a[e] === b)return e;
		return -1
	}, x.range = function (a, b, c) {
		arguments.length <= 1 && (b = a || 0, a = 0), c = arguments[2] || 1;
		for (var d = Math.max(Math.ceil((b - a) / c), 0), e = 0, f = new Array(d); d > e;)f[e++] = a, a += c;
		return f
	};
	var E = function () {
	};
	x.bind = function (a, b) {
		var c, d;
		if (w && a.bind === w)return w.apply(a, h.call(arguments, 1));
		if (!x.isFunction(a))throw new TypeError;
		return c = h.call(arguments, 2), d = function () {
			if (!(this instanceof d))return a.apply(b, c.concat(h.call(arguments)));
			E.prototype = a.prototype;
			var e = new E;
			E.prototype = null;
			var f = a.apply(e, c.concat(h.call(arguments)));
			return Object(f) === f ? f : e
		}
	}, x.partial = function (a) {
		var b = h.call(arguments, 1);
		return function () {
			for (var c = 0, d = b.slice(), e = 0, f = d.length; f > e; e++)d[e] === x && (d[e] = arguments[c++]);
			for (; c < arguments.length;)d.push(arguments[c++]);
			return a.apply(this, d)
		}
	}, x.bindAll = function (a) {
		var b = h.call(arguments, 1);
		if (0 === b.length)throw new Error("bindAll must be passed function names");
		return y(b, function (b) {
			a[b] = x.bind(a[b], a)
		}), a
	}, x.memoize = function (a, b) {
		var c = {};
		return b || (b = x.identity), function () {
			var d = b.apply(this, arguments);
			return x.has(c, d) ? c[d] : c[d] = a.apply(this, arguments)
		}
	}, x.delay = function (a, b) {
		var c = h.call(arguments, 2);
		return setTimeout(function () {
			return a.apply(null, c)
		}, b)
	}, x.defer = function (a) {
		return x.delay.apply(x, [a, 1].concat(h.call(arguments, 1)))
	}, x.throttle = function (a, b, c) {
		var d, e, f, g = null, h = 0;
		c || (c = {});
		var i = function () {
			h = c.leading === !1 ? 0 : x.now(), g = null, f = a.apply(d, e), d = e = null
		};
		return function () {
			var j = x.now();
			h || c.leading !== !1 || (h = j);
			var k = b - (j - h);
			return d = this, e = arguments, 0 >= k ? (clearTimeout(g), g = null, h = j, f = a.apply(d, e), d = e = null) : g || c.trailing === !1 || (g = setTimeout(i, k)), f
		}
	}, x.debounce = function (a, b, c) {
		var d, e, f, g, h, i = function () {
			var j = x.now() - g;
			b > j ? d = setTimeout(i, b - j) : (d = null, c || (h = a.apply(f, e), f = e = null))
		};
		return function () {
			f = this, e = arguments, g = x.now();
			var j = c && !d;
			return d || (d = setTimeout(i, b)), j && (h = a.apply(f, e), f = e = null), h
		}
	}, x.once = function (a) {
		var b, c = !1;
		return function () {
			return c ? b : (c = !0, b = a.apply(this, arguments), a = null, b)
		}
	}, x.wrap = function (a, b) {
		return x.partial(b, a)
	}, x.compose = function () {
		var a = arguments;
		return function () {
			for (var b = arguments, c = a.length - 1; c >= 0; c--)b = [a[c].apply(this, b)];
			return b[0]
		}
	}, x.after = function (a, b) {
		return function () {
			return --a < 1 ? b.apply(this, arguments) : void 0
		}
	}, x.keys = function (a) {
		if (!x.isObject(a))return [];
		if (v)return v(a);
		var b = [];
		for (var c in a)x.has(a, c) && b.push(c);
		return b
	}, x.values = function (a) {
		for (var b = x.keys(a), c = b.length, d = new Array(c), e = 0; c > e; e++)d[e] = a[b[e]];
		return d
	}, x.pairs = function (a) {
		for (var b = x.keys(a), c = b.length, d = new Array(c), e = 0; c > e; e++)d[e] = [b[e], a[b[e]]];
		return d
	}, x.invert = function (a) {
		for (var b = {}, c = x.keys(a), d = 0, e = c.length; e > d; d++)b[a[c[d]]] = c[d];
		return b
	}, x.functions = x.methods = function (a) {
		var b = [];
		for (var c in a)x.isFunction(a[c]) && b.push(c);
		return b.sort()
	}, x.extend = function (a) {
		return y(h.call(arguments, 1), function (b) {
			if (b)for (var c in b)a[c] = b[c]
		}), a
	}, x.pick = function (a) {
		var b = {}, c = i.apply(d, h.call(arguments, 1));
		return y(c, function (c) {
			c in a && (b[c] = a[c])
		}), b
	}, x.omit = function (a) {
		var b = {}, c = i.apply(d, h.call(arguments, 1));
		for (var e in a)x.contains(c, e) || (b[e] = a[e]);
		return b
	}, x.defaults = function (a) {
		return y(h.call(arguments, 1), function (b) {
			if (b)for (var c in b)void 0 === a[c] && (a[c] = b[c])
		}), a
	}, x.clone = function (a) {
		return x.isObject(a) ? x.isArray(a) ? a.slice() : x.extend({}, a) : a
	}, x.tap = function (a, b) {
		return b(a), a
	};
	var F = function (a, b, c, d) {
		if (a === b)return 0 !== a || 1 / a == 1 / b;
		if (null == a || null == b)return a === b;
		a instanceof x && (a = a._wrapped), b instanceof x && (b = b._wrapped);
		var e = j.call(a);
		if (e != j.call(b))return !1;
		switch (e) {
			case"[object String]":
				return a == String(b);
			case"[object Number]":
				return a != +a ? b != +b : 0 == a ? 1 / a == 1 / b : a == +b;
			case"[object Date]":
			case"[object Boolean]":
				return +a == +b;
			case"[object RegExp]":
				return a.source == b.source && a.global == b.global && a.multiline == b.multiline && a.ignoreCase == b.ignoreCase
		}
		if ("object" != typeof a || "object" != typeof b)return !1;
		for (var f = c.length; f--;)if (c[f] == a)return d[f] == b;
		var g = a.constructor, h = b.constructor;
		if (g !== h && !(x.isFunction(g) && g instanceof g && x.isFunction(h) && h instanceof h) && "constructor" in a && "constructor" in b)return !1;
		c.push(a), d.push(b);
		var i = 0, k = !0;
		if ("[object Array]" == e) {
			if (i = a.length, k = i == b.length)for (; i-- && (k = F(a[i], b[i], c, d)););
		} else {
			for (var l in a)if (x.has(a, l) && (i++, !(k = x.has(b, l) && F(a[l], b[l], c, d))))break;
			if (k) {
				for (l in b)if (x.has(b, l) && !i--)break;
				k = !i
			}
		}
		return c.pop(), d.pop(), k
	};
	x.isEqual = function (a, b) {
		return F(a, b, [], [])
	}, x.isEmpty = function (a) {
		if (null == a)return !0;
		if (x.isArray(a) || x.isString(a))return 0 === a.length;
		for (var b in a)if (x.has(a, b))return !1;
		return !0
	}, x.isElement = function (a) {
		return !(!a || 1 !== a.nodeType)
	}, x.isArray = u || function (a) {
			return "[object Array]" == j.call(a)
		}, x.isObject = function (a) {
		return a === Object(a)
	}, y(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function (a) {
		x["is" + a] = function (b) {
			return j.call(b) == "[object " + a + "]"
		}
	}), x.isArguments(arguments) || (x.isArguments = function (a) {
		return !(!a || !x.has(a, "callee"))
	}), "function" != typeof/./ && (x.isFunction = function (a) {
		return "function" == typeof a
	}), x.isFinite = function (a) {
		return isFinite(a) && !isNaN(parseFloat(a))
	}, x.isNaN = function (a) {
		return x.isNumber(a) && a != +a
	}, x.isBoolean = function (a) {
		return a === !0 || a === !1 || "[object Boolean]" == j.call(a)
	}, x.isNull = function (a) {
		return null === a
	}, x.isUndefined = function (a) {
		return void 0 === a
	}, x.has = function (a, b) {
		return k.call(a, b)
	}, x.noConflict = function () {
		return a._ = b, this
	}, x.identity = function (a) {
		return a
	}, x.constant = function (a) {
		return function () {
			return a
		}
	}, x.property = function (a) {
		return function (b) {
			return b[a]
		}
	}, x.matches = function (a) {
		return function (b) {
			if (b === a)return !0;
			for (var c in a)if (a[c] !== b[c])return !1;
			return !0
		}
	}, x.times = function (a, b, c) {
		for (var d = Array(Math.max(0, a)), e = 0; a > e; e++)d[e] = b.call(c, e);
		return d
	}, x.random = function (a, b) {
		return null == b && (b = a, a = 0), a + Math.floor(Math.random() * (b - a + 1))
	}, x.now = Date.now || function () {
			return (new Date).getTime()
		};
	var G = {escape: {"&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#x27;"}};
	G.unescape = x.invert(G.escape);
	var H = {
		escape: new RegExp("[" + x.keys(G.escape).join("") + "]", "g"),
		unescape: new RegExp("(" + x.keys(G.unescape).join("|") + ")", "g")
	};
	x.each(["escape", "unescape"], function (a) {
		x[a] = function (b) {
			return null == b ? "" : ("" + b).replace(H[a], function (b) {
				return G[a][b]
			})
		}
	}), x.result = function (a, b) {
		if (null == a)return void 0;
		var c = a[b];
		return x.isFunction(c) ? c.call(a) : c
	}, x.mixin = function (a) {
		y(x.functions(a), function (b) {
			var c = x[b] = a[b];
			x.prototype[b] = function () {
				var a = [this._wrapped];
				return g.apply(a, arguments), M.call(this, c.apply(x, a))
			}
		})
	};
	var I = 0;
	x.uniqueId = function (a) {
		var b = ++I + "";
		return a ? a + b : b
	}, x.templateSettings = {evaluate: /<%([\s\S]+?)%>/g, interpolate: /<%=([\s\S]+?)%>/g, escape: /<%-([\s\S]+?)%>/g};
	var J = /(.)^/, K = {
		"'": "'",
		"\\": "\\",
		"\r": "r",
		"\n": "n",
		"	": "t",
		"\u2028": "u2028",
		"\u2029": "u2029"
	}, L = /\\|'|\r|\n|\t|\u2028|\u2029/g;
	x.template = function (a, b, c) {
		var d;
		c = x.defaults({}, c, x.templateSettings);
		var e = new RegExp([(c.escape || J).source, (c.interpolate || J).source, (c.evaluate || J).source].join("|") + "|$", "g"), f = 0, g = "__p+='";
		a.replace(e, function (b, c, d, e, h) {
			return g += a.slice(f, h).replace(L, function (a) {
				return "\\" + K[a]
			}), c && (g += "'+\n((__t=(" + c + "))==null?'':_.escape(__t))+\n'"), d && (g += "'+\n((__t=(" + d + "))==null?'':__t)+\n'"), e && (g += "';\n" + e + "\n__p+='"), f = h + b.length, b
		}), g += "';\n", c.variable || (g = "with(obj||{}){\n" + g + "}\n"), g = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + g + "return __p;\n";
		try {
			d = new Function(c.variable || "obj", "_", g)
		} catch (h) {
			throw h.source = g, h
		}
		if (b)return d(b, x);
		var i = function (a) {
			return d.call(this, a, x)
		};
		return i.source = "function(" + (c.variable || "obj") + "){\n" + g + "}", i
	}, x.chain = function (a) {
		return x(a).chain()
	};
	var M = function (a) {
		return this._chain ? x(a).chain() : a
	};
	x.mixin(x), y(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function (a) {
		var b = d[a];
		x.prototype[a] = function () {
			var c = this._wrapped;
			return b.apply(c, arguments), "shift" != a && "splice" != a || 0 !== c.length || delete c[0], M.call(this, c)
		}
	}), y(["concat", "join", "slice"], function (a) {
		var b = d[a];
		x.prototype[a] = function () {
			return M.call(this, b.apply(this._wrapped, arguments))
		}
	}), x.extend(x.prototype, {
		chain: function () {
			return this._chain = !0, this
		}, value: function () {
			return this._wrapped
		}
	}), "function" == typeof define && define.amd && define("underscore", [], function () {
		return x
	})
}.call(this), function (a, b) {
	if ("function" == typeof define && define.amd)define(["underscore", "jquery", "exports"], function (c, d, e) {
		a.Backbone = b(a, e, c, d)
	}); else if ("undefined" != typeof exports) {
		var c = require("underscore");
		b(a, exports, c)
	} else a.Backbone = b(a, {}, a._, a.jQuery || a.Zepto || a.ender || a.$)
}(this, function (a, b, c, d) {
	{
		var e = a.Backbone, f = [], g = (f.push, f.slice);
		f.splice
	}
	b.VERSION = "1.1.2", b.$ = d, b.noConflict = function () {
		return a.Backbone = e, this
	}, b.emulateHTTP = !1, b.emulateJSON = !1;
	var h = b.Events = {
		on: function (a, b, c) {
			if (!j(this, "on", a, [b, c]) || !b)return this;
			this._events || (this._events = {});
			var d = this._events[a] || (this._events[a] = []);
			return d.push({callback: b, context: c, ctx: c || this}), this
		}, once: function (a, b, d) {
			if (!j(this, "once", a, [b, d]) || !b)return this;
			var e = this, f = c.once(function () {
				e.off(a, f), b.apply(this, arguments)
			});
			return f._callback = b, this.on(a, f, d)
		}, off: function (a, b, d) {
			var e, f, g, h, i, k, l, m;
			if (!this._events || !j(this, "off", a, [b, d]))return this;
			if (!a && !b && !d)return this._events = void 0, this;
			for (h = a ? [a] : c.keys(this._events), i = 0, k = h.length; k > i; i++)if (a = h[i], g = this._events[a]) {
				if (this._events[a] = e = [], b || d)for (l = 0, m = g.length; m > l; l++)f = g[l], (b && b !== f.callback && b !== f.callback._callback || d && d !== f.context) && e.push(f);
				e.length || delete this._events[a]
			}
			return this
		}, trigger: function (a) {
			if (!this._events)return this;
			var b = g.call(arguments, 1);
			if (!j(this, "trigger", a, b))return this;
			var c = this._events[a], d = this._events.all;
			return c && k(c, b), d && k(d, arguments), this
		}, stopListening: function (a, b, d) {
			var e = this._listeningTo;
			if (!e)return this;
			var f = !b && !d;
			d || "object" != typeof b || (d = this), a && ((e = {})[a._listenId] = a);
			for (var g in e)a = e[g], a.off(b, d, this), (f || c.isEmpty(a._events)) && delete this._listeningTo[g];
			return this
		}
	}, i = /\s+/, j = function (a, b, c, d) {
		if (!c)return !0;
		if ("object" == typeof c) {
			for (var e in c)a[b].apply(a, [e, c[e]].concat(d));
			return !1
		}
		if (i.test(c)) {
			for (var f = c.split(i), g = 0, h = f.length; h > g; g++)a[b].apply(a, [f[g]].concat(d));
			return !1
		}
		return !0
	}, k = function (a, b) {
		var c, d = -1, e = a.length, f = b[0], g = b[1], h = b[2];
		switch (b.length) {
			case 0:
				for (; ++d < e;)(c = a[d]).callback.call(c.ctx);
				return;
			case 1:
				for (; ++d < e;)(c = a[d]).callback.call(c.ctx, f);
				return;
			case 2:
				for (; ++d < e;)(c = a[d]).callback.call(c.ctx, f, g);
				return;
			case 3:
				for (; ++d < e;)(c = a[d]).callback.call(c.ctx, f, g, h);
				return;
			default:
				for (; ++d < e;)(c = a[d]).callback.apply(c.ctx, b);
				return
		}
	}, l = {listenTo: "on", listenToOnce: "once"};
	c.each(l, function (a, b) {
		h[b] = function (b, d, e) {
			var f = this._listeningTo || (this._listeningTo = {}), g = b._listenId || (b._listenId = c.uniqueId("l"));
			return f[g] = b, e || "object" != typeof d || (e = this), b[a](d, e, this), this
		}
	}), h.bind = h.on, h.unbind = h.off, c.extend(b, h);
	var m = b.Model = function (a, b) {
		var d = a || {};
		b || (b = {}), this.cid = c.uniqueId("c"), this.attributes = {}, b.collection && (this.collection = b.collection), b.parse && (d = this.parse(d, b) || {}), d = c.defaults({}, d, c.result(this, "defaults")), this.set(d, b), this.changed = {}, this.initialize.apply(this, arguments)
	};
	c.extend(m.prototype, h, {
		changed: null, validationError: null, idAttribute: "id", initialize: function () {
		}, toJSON: function () {
			return c.clone(this.attributes)
		}, sync: function () {
			return b.sync.apply(this, arguments)
		}, get: function (a) {
			return this.attributes[a]
		}, escape: function (a) {
			return c.escape(this.get(a))
		}, has: function (a) {
			return null != this.get(a)
		}, set: function (a, b, d) {
			var e, f, g, h, i, j, k, l;
			if (null == a)return this;
			if ("object" == typeof a ? (f = a, d = b) : (f = {})[a] = b, d || (d = {}), !this._validate(f, d))return !1;
			g = d.unset, i = d.silent, h = [], j = this._changing, this._changing = !0, j || (this._previousAttributes = c.clone(this.attributes), this.changed = {}), l = this.attributes, k = this._previousAttributes, this.idAttribute in f && (this.id = f[this.idAttribute]);
			for (e in f)b = f[e], c.isEqual(l[e], b) || h.push(e), c.isEqual(k[e], b) ? delete this.changed[e] : this.changed[e] = b, g ? delete l[e] : l[e] = b;
			if (!i) {
				h.length && (this._pending = d);
				for (var m = 0, n = h.length; n > m; m++)this.trigger("change:" + h[m], this, l[h[m]], d)
			}
			if (j)return this;
			if (!i)for (; this._pending;)d = this._pending, this._pending = !1, this.trigger("change", this, d);
			return this._pending = !1, this._changing = !1, this
		}, unset: function (a, b) {
			return this.set(a, void 0, c.extend({}, b, {unset: !0}))
		}, clear: function (a) {
			var b = {};
			for (var d in this.attributes)b[d] = void 0;
			return this.set(b, c.extend({}, a, {unset: !0}))
		}, hasChanged: function (a) {
			return null == a ? !c.isEmpty(this.changed) : c.has(this.changed, a)
		}, changedAttributes: function (a) {
			if (!a)return this.hasChanged() ? c.clone(this.changed) : !1;
			var b, d = !1, e = this._changing ? this._previousAttributes : this.attributes;
			for (var f in a)c.isEqual(e[f], b = a[f]) || ((d || (d = {}))[f] = b);
			return d
		}, previous: function (a) {
			return null != a && this._previousAttributes ? this._previousAttributes[a] : null
		}, previousAttributes: function () {
			return c.clone(this._previousAttributes)
		}, fetch: function (a) {
			a = a ? c.clone(a) : {}, void 0 === a.parse && (a.parse = !0);
			var b = this, d = a.success;
			return a.success = function (c) {
				return b.set(b.parse(c, a), a) ? (d && d(b, c, a), void b.trigger("sync", b, c, a)) : !1
			}, L(this, a), this.sync("read", this, a)
		}, save: function (a, b, d) {
			var e, f, g, h = this.attributes;
			if (null == a || "object" == typeof a ? (e = a, d = b) : (e = {})[a] = b, d = c.extend({validate: !0}, d), e && !d.wait) {
				if (!this.set(e, d))return !1
			} else if (!this._validate(e, d))return !1;
			e && d.wait && (this.attributes = c.extend({}, h, e)), void 0 === d.parse && (d.parse = !0);
			var i = this, j = d.success;
			return d.success = function (a) {
				i.attributes = h;
				var b = i.parse(a, d);
				return d.wait && (b = c.extend(e || {}, b)), c.isObject(b) && !i.set(b, d) ? !1 : (j && j(i, a, d), void i.trigger("sync", i, a, d))
			}, L(this, d), f = this.isNew() ? "create" : d.patch ? "patch" : "update", "patch" === f && (d.attrs = e), g = this.sync(f, this, d), e && d.wait && (this.attributes = h), g
		}, destroy: function (a) {
			a = a ? c.clone(a) : {};
			var b = this, d = a.success, e = function () {
				b.trigger("destroy", b, b.collection, a)
			};
			if (a.success = function (c) {
					(a.wait || b.isNew()) && e(), d && d(b, c, a), b.isNew() || b.trigger("sync", b, c, a)
				}, this.isNew())return a.success(), !1;
			L(this, a);
			var f = this.sync("delete", this, a);
			return a.wait || e(), f
		}, url: function () {
			var a = c.result(this, "urlRoot") || c.result(this.collection, "url") || K();
			return this.isNew() ? a : a.replace(/([^\/])$/, "$1/") + encodeURIComponent(this.id)
		}, parse: function (a) {
			return a
		}, clone: function () {
			return new this.constructor(this.attributes)
		}, isNew: function () {
			return !this.has(this.idAttribute)
		}, isValid: function (a) {
			return this._validate({}, c.extend(a || {}, {validate: !0}))
		}, _validate: function (a, b) {
			if (!b.validate || !this.validate)return !0;
			a = c.extend({}, this.attributes, a);
			var d = this.validationError = this.validate(a, b) || null;
			return d ? (this.trigger("invalid", this, d, c.extend(b, {validationError: d})), !1) : !0
		}
	});
	var n = ["keys", "values", "pairs", "invert", "pick", "omit"];
	c.each(n, function (a) {
		m.prototype[a] = function () {
			var b = g.call(arguments);
			return b.unshift(this.attributes), c[a].apply(c, b)
		}
	});
	var o = b.Collection = function (a, b) {
		b || (b = {}), b.model && (this.model = b.model), void 0 !== b.comparator && (this.comparator = b.comparator), this._reset(), this.initialize.apply(this, arguments), a && this.reset(a, c.extend({silent: !0}, b))
	}, p = {add: !0, remove: !0, merge: !0}, q = {add: !0, remove: !1};
	c.extend(o.prototype, h, {
		model: m, initialize: function () {
		}, toJSON: function (a) {
			return this.map(function (b) {
				return b.toJSON(a)
			})
		}, sync: function () {
			return b.sync.apply(this, arguments)
		}, add: function (a, b) {
			return this.set(a, c.extend({merge: !1}, b, q))
		}, remove: function (a, b) {
			var d = !c.isArray(a);
			a = d ? [a] : c.clone(a), b || (b = {});
			var e, f, g, h;
			for (e = 0, f = a.length; f > e; e++)h = a[e] = this.get(a[e]), h && (delete this._byId[h.id], delete this._byId[h.cid], g = this.indexOf(h), this.models.splice(g, 1), this.length--, b.silent || (b.index = g, h.trigger("remove", h, this, b)), this._removeReference(h, b));
			return d ? a[0] : a
		}, set: function (a, b) {
			b = c.defaults({}, b, p), b.parse && (a = this.parse(a, b));
			var d = !c.isArray(a);
			a = d ? a ? [a] : [] : c.clone(a);
			var e, f, g, h, i, j, k, l = b.at, n = this.model, o = this.comparator && null == l && b.sort !== !1, q = c.isString(this.comparator) ? this.comparator : null, r = [], s = [], t = {}, u = b.add, v = b.merge, w = b.remove, x = !o && u && w ? [] : !1;
			for (e = 0, f = a.length; f > e; e++) {
				if (i = a[e] || {}, g = i instanceof m ? h = i : i[n.prototype.idAttribute || "id"], j = this.get(g))w && (t[j.cid] = !0), v && (i = i === h ? h.attributes : i, b.parse && (i = j.parse(i, b)), j.set(i, b), o && !k && j.hasChanged(q) && (k = !0)), a[e] = j; else if (u) {
					if (h = a[e] = this._prepareModel(i, b), !h)continue;
					r.push(h), this._addReference(h, b)
				}
				h = j || h, !x || !h.isNew() && t[h.id] || x.push(h), t[h.id] = !0
			}
			if (w) {
				for (e = 0, f = this.length; f > e; ++e)t[(h = this.models[e]).cid] || s.push(h);
				s.length && this.remove(s, b)
			}
			if (r.length || x && x.length)if (o && (k = !0), this.length += r.length, null != l)for (e = 0, f = r.length; f > e; e++)this.models.splice(l + e, 0, r[e]); else {
				x && (this.models.length = 0);
				var y = x || r;
				for (e = 0, f = y.length; f > e; e++)this.models.push(y[e])
			}
			if (k && this.sort({silent: !0}), !b.silent) {
				for (e = 0, f = r.length; f > e; e++)(h = r[e]).trigger("add", h, this, b);
				(k || x && x.length) && this.trigger("sort", this, b)
			}
			return d ? a[0] : a
		}, reset: function (a, b) {
			b || (b = {});
			for (var d = 0, e = this.models.length; e > d; d++)this._removeReference(this.models[d], b);
			return b.previousModels = this.models, this._reset(), a = this.add(a, c.extend({silent: !0}, b)), b.silent || this.trigger("reset", this, b), a
		}, push: function (a, b) {
			return this.add(a, c.extend({at: this.length}, b))
		}, pop: function (a) {
			var b = this.at(this.length - 1);
			return this.remove(b, a), b
		}, unshift: function (a, b) {
			return this.add(a, c.extend({at: 0}, b))
		}, shift: function (a) {
			var b = this.at(0);
			return this.remove(b, a), b
		}, slice: function () {
			return g.apply(this.models, arguments)
		}, get: function (a) {
			return null == a ? void 0 : this._byId[a] || this._byId[a.id] || this._byId[a.cid]
		}, at: function (a) {
			return this.models[a]
		}, where: function (a, b) {
			return c.isEmpty(a) ? b ? void 0 : [] : this[b ? "find" : "filter"](function (b) {
				for (var c in a)if (a[c] !== b.get(c))return !1;
				return !0
			})
		}, findWhere: function (a) {
			return this.where(a, !0)
		}, sort: function (a) {
			if (!this.comparator)throw new Error("Cannot sort a set without a comparator");
			return a || (a = {}), c.isString(this.comparator) || 1 === this.comparator.length ? this.models = this.sortBy(this.comparator, this) : this.models.sort(c.bind(this.comparator, this)), a.silent || this.trigger("sort", this, a), this
		}, pluck: function (a) {
			return c.invoke(this.models, "get", a)
		}, fetch: function (a) {
			a = a ? c.clone(a) : {}, void 0 === a.parse && (a.parse = !0);
			var b = a.success, d = this;
			return a.success = function (c) {
				var e = a.reset ? "reset" : "set";
				d[e](c, a), b && b(d, c, a), d.trigger("sync", d, c, a)
			}, L(this, a), this.sync("read", this, a)
		}, create: function (a, b) {
			if (b = b ? c.clone(b) : {}, !(a = this._prepareModel(a, b)))return !1;
			b.wait || this.add(a, b);
			var d = this, e = b.success;
			return b.success = function (a, c) {
				b.wait && d.add(a, b), e && e(a, c, b)
			}, a.save(null, b), a
		}, parse: function (a) {
			return a
		}, clone: function () {
			return new this.constructor(this.models)
		}, _reset: function () {
			this.length = 0, this.models = [], this._byId = {}
		}, _prepareModel: function (a, b) {
			if (a instanceof m)return a;
			b = b ? c.clone(b) : {}, b.collection = this;
			var d = new this.model(a, b);
			return d.validationError ? (this.trigger("invalid", this, d.validationError, b), !1) : d
		}, _addReference: function (a) {
			this._byId[a.cid] = a, null != a.id && (this._byId[a.id] = a), a.collection || (a.collection = this), a.on("all", this._onModelEvent, this)
		}, _removeReference: function (a) {
			this === a.collection && delete a.collection, a.off("all", this._onModelEvent, this)
		}, _onModelEvent: function (a, b, c, d) {
			("add" !== a && "remove" !== a || c === this) && ("destroy" === a && this.remove(b, d), b && a === "change:" + b.idAttribute && (delete this._byId[b.previous(b.idAttribute)], null != b.id && (this._byId[b.id] = b)), this.trigger.apply(this, arguments))
		}
	});
	var r = ["forEach", "each", "map", "collect", "reduce", "foldl", "inject", "reduceRight", "foldr", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "max", "min", "toArray", "size", "first", "head", "take", "initial", "rest", "tail", "drop", "last", "without", "difference", "indexOf", "shuffle", "lastIndexOf", "isEmpty", "chain", "sample"];
	c.each(r, function (a) {
		o.prototype[a] = function () {
			var b = g.call(arguments);
			return b.unshift(this.models), c[a].apply(c, b)
		}
	});
	var s = ["groupBy", "countBy", "sortBy", "indexBy"];
	c.each(s, function (a) {
		o.prototype[a] = function (b, d) {
			var e = c.isFunction(b) ? b : function (a) {
				return a.get(b)
			};
			return c[a](this.models, e, d)
		}
	});
	var t = b.View = function (a) {
		this.cid = c.uniqueId("view"), a || (a = {}), c.extend(this, c.pick(a, v)), this._ensureElement(), this.initialize.apply(this, arguments), this.delegateEvents()
	}, u = /^(\S+)\s*(.*)$/, v = ["model", "collection", "el", "id", "attributes", "className", "tagName", "events"];
	c.extend(t.prototype, h, {
		tagName: "div", $: function (a) {
			return this.$el.find(a)
		}, initialize: function () {
		}, render: function () {
			return this
		}, remove: function () {
			return this.$el.remove(), this.stopListening(), this
		}, setElement: function (a, c) {
			return this.$el && this.undelegateEvents(), this.$el = a instanceof b.$ ? a : b.$(a), this.el = this.$el[0], c !== !1 && this.delegateEvents(), this
		}, delegateEvents: function (a) {
			if (!a && !(a = c.result(this, "events")))return this;
			this.undelegateEvents();
			for (var b in a) {
				var d = a[b];
				if (c.isFunction(d) || (d = this[a[b]]), d) {
					var e = b.match(u), f = e[1], g = e[2];
					d = c.bind(d, this), f += ".delegateEvents" + this.cid, "" === g ? this.$el.on(f, d) : this.$el.on(f, g, d)
				}
			}
			return this
		}, undelegateEvents: function () {
			return this.$el.off(".delegateEvents" + this.cid), this
		}, _ensureElement: function () {
			if (this.el)this.setElement(c.result(this, "el"), !1); else {
				var a = c.extend({}, c.result(this, "attributes"));
				this.id && (a.id = c.result(this, "id")), this.className && (a["class"] = c.result(this, "className"));
				var d = b.$("<" + c.result(this, "tagName") + ">").attr(a);
				this.setElement(d, !1)
			}
		}
	}), b.sync = function (a, d, e) {
		var f = x[a];
		c.defaults(e || (e = {}), {emulateHTTP: b.emulateHTTP, emulateJSON: b.emulateJSON});
		var g = {type: f, dataType: "json"};
		if (e.url || (g.url = c.result(d, "url") || K()), null != e.data || !d || "create" !== a && "update" !== a && "patch" !== a || (g.contentType = "application/json", g.data = JSON.stringify(e.attrs || d.toJSON(e))), e.emulateJSON && (g.contentType = "application/x-www-form-urlencoded", g.data = g.data ? {model: g.data} : {}), e.emulateHTTP && ("PUT" === f || "DELETE" === f || "PATCH" === f)) {
			g.type = "POST", e.emulateJSON && (g.data._method = f);
			var h = e.beforeSend;
			e.beforeSend = function (a) {
				return a.setRequestHeader("X-HTTP-Method-Override", f), h ? h.apply(this, arguments) : void 0
			}
		}
		"GET" === g.type || e.emulateJSON || (g.processData = !1), "PATCH" === g.type && w && (g.xhr = function () {
			return new ActiveXObject("Microsoft.XMLHTTP")
		});
		var i = e.xhr = b.ajax(c.extend(g, e));
		return d.trigger("request", d, i, e), i
	};
	var w = !("undefined" == typeof window || !window.ActiveXObject || window.XMLHttpRequest && (new XMLHttpRequest).dispatchEvent), x = {
		create: "POST",
		update: "PUT",
		patch: "PATCH",
		"delete": "DELETE",
		read: "GET"
	};
	b.ajax = function () {
		return b.$.ajax.apply(b.$, arguments)
	};
	var y = b.Router = function (a) {
		a || (a = {}), a.routes && (this.routes = a.routes), this._bindRoutes(), this.initialize.apply(this, arguments)
	}, z = /\((.*?)\)/g, A = /(\(\?)?:\w+/g, B = /\*\w+/g, C = /[\-{}\[\]+?.,\\\^$|#\s]/g;
	c.extend(y.prototype, h, {
		initialize: function () {
		}, route: function (a, d, e) {
			c.isRegExp(a) || (a = this._routeToRegExp(a)), c.isFunction(d) && (e = d, d = ""), e || (e = this[d]);
			var f = this;
			return b.history.route(a, function (c) {
				var g = f._extractParameters(a, c);
				f.execute(e, g), f.trigger.apply(f, ["route:" + d].concat(g)), f.trigger("route", d, g), b.history.trigger("route", f, d, g)
			}), this
		}, execute: function (a, b) {
			a && a.apply(this, b)
		}, navigate: function (a, c) {
			return b.history.navigate(a, c), this
		}, _bindRoutes: function () {
			if (this.routes) {
				this.routes = c.result(this, "routes");
				for (var a, b = c.keys(this.routes); null != (a = b.pop());)this.route(a, this.routes[a])
			}
		}, _routeToRegExp: function (a) {
			return a = a.replace(C, "\\$&").replace(z, "(?:$1)?").replace(A, function (a, b) {
				return b ? a : "([^/?]+)"
			}).replace(B, "([^?]*?)"), new RegExp("^" + a + "(?:\\?([\\s\\S]*))?$")
		}, _extractParameters: function (a, b) {
			var d = a.exec(b).slice(1);
			return c.map(d, function (a, b) {
				return b === d.length - 1 ? a || null : a ? decodeURIComponent(a) : null
			})
		}
	});
	var D = b.History = function () {
		this.handlers = [], c.bindAll(this, "checkUrl"), "undefined" != typeof window && (this.location = window.location, this.history = window.history)
	}, E = /^[#\/]|\s+$/g, F = /^\/+|\/+$/g, G = /msie [\w.]+/, H = /\/$/, I = /#.*$/;
	D.started = !1, c.extend(D.prototype, h, {
		interval: 50, atRoot: function () {
			return this.location.pathname.replace(/[^\/]$/, "$&/") === this.root
		}, getHash: function (a) {
			var b = (a || this).location.href.match(/#(.*)$/);
			return b ? b[1] : ""
		}, getFragment: function (a, b) {
			if (null == a)if (this._hasPushState || !this._wantsHashChange || b) {
				a = decodeURI(this.location.pathname + this.location.search);
				var c = this.root.replace(H, "");
				a.indexOf(c) || (a = a.slice(c.length))
			} else a = this.getHash();
			return a.replace(E, "")
		}, start: function (a) {
			if (D.started)throw new Error("Backbone.history has already been started");
			D.started = !0, this.options = c.extend({root: "/"}, this.options, a), this.root = this.options.root, this._wantsHashChange = this.options.hashChange !== !1, this._wantsPushState = !!this.options.pushState, this._hasPushState = !!(this.options.pushState && this.history && this.history.pushState);
			var d = this.getFragment(), e = document.documentMode, f = G.exec(navigator.userAgent.toLowerCase()) && (!e || 7 >= e);
			if (this.root = ("/" + this.root + "/").replace(F, "/"), f && this._wantsHashChange) {
				var g = b.$('<iframe src="javascript:0" tabindex="-1">');
				this.iframe = g.hide().appendTo("body")[0].contentWindow, this.navigate(d)
			}
			this._hasPushState ? b.$(window).on("popstate", this.checkUrl) : this._wantsHashChange && "onhashchange" in window && !f ? b.$(window).on("hashchange", this.checkUrl) : this._wantsHashChange && (this._checkUrlInterval = setInterval(this.checkUrl, this.interval)), this.fragment = d;
			var h = this.location;
			if (this._wantsHashChange && this._wantsPushState) {
				if (!this._hasPushState && !this.atRoot())return this.fragment = this.getFragment(null, !0), this.location.replace(this.root + "#" + this.fragment), !0;
				this._hasPushState && this.atRoot() && h.hash && (this.fragment = this.getHash().replace(E, ""), this.history.replaceState({}, document.title, this.root + this.fragment))
			}
			return this.options.silent ? void 0 : this.loadUrl()
		}, stop: function () {
			b.$(window).off("popstate", this.checkUrl).off("hashchange", this.checkUrl), this._checkUrlInterval && clearInterval(this._checkUrlInterval), D.started = !1
		}, route: function (a, b) {
			this.handlers.unshift({route: a, callback: b})
		}, checkUrl: function () {
			var a = this.getFragment();
			return a === this.fragment && this.iframe && (a = this.getFragment(this.getHash(this.iframe))), a === this.fragment ? !1 : (this.iframe && this.navigate(a), void this.loadUrl())
		}, loadUrl: function (a) {
			return a = this.fragment = this.getFragment(a), c.any(this.handlers, function (b) {
				return b.route.test(a) ? (b.callback(a), !0) : void 0
			})
		}, navigate: function (a, b) {
			if (!D.started)return !1;
			b && b !== !0 || (b = {trigger: !!b});
			var c = this.root + (a = this.getFragment(a || ""));
			if (a = a.replace(I, ""), this.fragment !== a) {
				if (this.fragment = a, "" === a && "/" !== c && (c = c.slice(0, -1)), this._hasPushState)this.history[b.replace ? "replaceState" : "pushState"]({}, document.title, c); else {
					if (!this._wantsHashChange)return this.location.assign(c);
					this._updateHash(this.location, a, b.replace), this.iframe && a !== this.getFragment(this.getHash(this.iframe)) && (b.replace || this.iframe.document.open().close(), this._updateHash(this.iframe.location, a, b.replace))
				}
				return b.trigger ? this.loadUrl(a) : void 0
			}
		}, _updateHash: function (a, b, c) {
			if (c) {
				var d = a.href.replace(/(javascript:|#).*$/, "");
				a.replace(d + "#" + b)
			} else a.hash = "#" + b
		}
	}), b.history = new D;
	var J = function (a, b) {
		var d, e = this;
		d = a && c.has(a, "constructor") ? a.constructor : function () {
			return e.apply(this, arguments)
		}, c.extend(d, e, b);
		var f = function () {
			this.constructor = d
		};
		return f.prototype = e.prototype, d.prototype = new f, a && c.extend(d.prototype, a), d.__super__ = e.prototype, d
	};
	m.extend = o.extend = y.extend = t.extend = D.extend = J;
	var K = function () {
		throw new Error('A "url" property or function must be specified')
	}, L = function (a, b) {
		var c = b.error;
		b.error = function (d) {
			c && c(a, d, b), a.trigger("error", a, d, b)
		}
	};
	return b
});
var Handlebars = function () {
	var a = function () {
		"use strict";
		function a(a) {
			this.string = a
		}

		var b;
		return a.prototype.toString = function () {
			return "" + this.string
		}, b = a
	}(), b = function (a) {
		"use strict";
		function b(a) {
			return h[a] || "&amp;"
		}

		function c(a, b) {
			for (var c in b)Object.prototype.hasOwnProperty.call(b, c) && (a[c] = b[c])
		}

		function d(a) {
			return a instanceof g ? a.toString() : a || 0 === a ? (a = "" + a, j.test(a) ? a.replace(i, b) : a) : ""
		}

		function e(a) {
			return a || 0 === a ? m(a) && 0 === a.length ? !0 : !1 : !0
		}

		var f = {}, g = a, h = {
			"&": "&amp;",
			"<": "&lt;",
			">": "&gt;",
			'"': "&quot;",
			"'": "&#x27;",
			"`": "&#x60;"
		}, i = /[&<>"'`]/g, j = /[&<>"'`]/;
		f.extend = c;
		var k = Object.prototype.toString;
		f.toString = k;
		var l = function (a) {
			return "function" == typeof a
		};
		l(/x/) && (l = function (a) {
			return "function" == typeof a && "[object Function]" === k.call(a)
		});
		var l;
		f.isFunction = l;
		var m = Array.isArray || function (a) {
				return a && "object" == typeof a ? "[object Array]" === k.call(a) : !1
			};
		return f.isArray = m, f.escapeExpression = d, f.isEmpty = e, f
	}(a), c = function () {
		"use strict";
		function a(a, b) {
			var d;
			b && b.firstLine && (d = b.firstLine, a += " - " + d + ":" + b.firstColumn);
			for (var e = Error.prototype.constructor.call(this, a), f = 0; f < c.length; f++)this[c[f]] = e[c[f]];
			d && (this.lineNumber = d, this.column = b.firstColumn)
		}

		var b, c = ["description", "fileName", "lineNumber", "message", "name", "number", "stack"];
		return a.prototype = new Error, b = a
	}(), d = function (a, b) {
		"use strict";
		function c(a, b) {
			this.helpers = a || {}, this.partials = b || {}, d(this)
		}

		function d(a) {
			a.registerHelper("helperMissing", function (a) {
				if (2 === arguments.length)return void 0;
				throw new h("Missing helper: '" + a + "'")
			}), a.registerHelper("blockHelperMissing", function (b, c) {
				var d = c.inverse || function () {
					}, e = c.fn;
				return m(b) && (b = b.call(this)), b === !0 ? e(this) : b === !1 || null == b ? d(this) : l(b) ? b.length > 0 ? a.helpers.each(b, c) : d(this) : e(b)
			}), a.registerHelper("each", function (a, b) {
				var c, d = b.fn, e = b.inverse, f = 0, g = "";
				if (m(a) && (a = a.call(this)), b.data && (c = q(b.data)), a && "object" == typeof a)if (l(a))for (var h = a.length; h > f; f++)c && (c.index = f, c.first = 0 === f, c.last = f === a.length - 1), g += d(a[f], {data: c}); else for (var i in a)a.hasOwnProperty(i) && (c && (c.key = i, c.index = f, c.first = 0 === f), g += d(a[i], {data: c}), f++);
				return 0 === f && (g = e(this)), g
			}), a.registerHelper("if", function (a, b) {
				return m(a) && (a = a.call(this)), !b.hash.includeZero && !a || g.isEmpty(a) ? b.inverse(this) : b.fn(this)
			}), a.registerHelper("unless", function (b, c) {
				return a.helpers["if"].call(this, b, {fn: c.inverse, inverse: c.fn, hash: c.hash})
			}), a.registerHelper("with", function (a, b) {
				return m(a) && (a = a.call(this)), g.isEmpty(a) ? void 0 : b.fn(a)
			}), a.registerHelper("log", function (b, c) {
				var d = c.data && null != c.data.level ? parseInt(c.data.level, 10) : 1;
				a.log(d, b)
			})
		}

		function e(a, b) {
			p.log(a, b)
		}

		var f = {}, g = a, h = b, i = "1.3.0";
		f.VERSION = i;
		var j = 4;
		f.COMPILER_REVISION = j;
		var k = {1: "<= 1.0.rc.2", 2: "== 1.0.0-rc.3", 3: "== 1.0.0-rc.4", 4: ">= 1.0.0"};
		f.REVISION_CHANGES = k;
		var l = g.isArray, m = g.isFunction, n = g.toString, o = "[object Object]";
		f.HandlebarsEnvironment = c, c.prototype = {
			constructor: c,
			logger: p,
			log: e,
			registerHelper: function (a, b, c) {
				if (n.call(a) === o) {
					if (c || b)throw new h("Arg not supported with multiple helpers");
					g.extend(this.helpers, a)
				} else c && (b.not = c), this.helpers[a] = b
			},
			registerPartial: function (a, b) {
				n.call(a) === o ? g.extend(this.partials, a) : this.partials[a] = b
			}
		};
		var p = {
			methodMap: {0: "debug", 1: "info", 2: "warn", 3: "error"},
			DEBUG: 0,
			INFO: 1,
			WARN: 2,
			ERROR: 3,
			level: 3,
			log: function (a, b) {
				if (p.level <= a) {
					var c = p.methodMap[a];
					"undefined" != typeof console && console[c] && console[c].call(console, b)
				}
			}
		};
		f.logger = p, f.log = e;
		var q = function (a) {
			var b = {};
			return g.extend(b, a), b
		};
		return f.createFrame = q, f
	}(b, c), e = function (a, b, c) {
		"use strict";
		function d(a) {
			var b = a && a[0] || 1, c = m;
			if (b !== c) {
				if (c > b) {
					var d = n[c], e = n[b];
					throw new l("Template was precompiled with an older version of Handlebars than the current runtime. Please update your precompiler to a newer version (" + d + ") or downgrade your runtime to an older version (" + e + ").")
				}
				throw new l("Template was precompiled with a newer version of Handlebars than the current runtime. Please update your runtime to a newer version (" + a[1] + ").")
			}
		}

		function e(a, b) {
			if (!b)throw new l("No environment passed to template");
			var c = function (a, c, d, e, f, g) {
				var h = b.VM.invokePartial.apply(this, arguments);
				if (null != h)return h;
				if (b.compile) {
					var i = {helpers: e, partials: f, data: g};
					return f[c] = b.compile(a, {data: void 0 !== g}, b), f[c](d, i)
				}
				throw new l("The partial " + c + " could not be compiled when running in runtime-only mode")
			}, d = {
				escapeExpression: k.escapeExpression, invokePartial: c, programs: [], program: function (a, b, c) {
					var d = this.programs[a];
					return c ? d = g(a, b, c) : d || (d = this.programs[a] = g(a, b)), d
				}, merge: function (a, b) {
					var c = a || b;
					return a && b && a !== b && (c = {}, k.extend(c, b), k.extend(c, a)), c
				}, programWithDepth: b.VM.programWithDepth, noop: b.VM.noop, compilerInfo: null
			};
			return function (c, e) {
				e = e || {};
				var f, g, h = e.partial ? e : b;
				e.partial || (f = e.helpers, g = e.partials);
				var i = a.call(d, h, c, f, g, e.data);
				return e.partial || b.VM.checkRevision(d.compilerInfo), i
			}
		}

		function f(a, b, c) {
			var d = Array.prototype.slice.call(arguments, 3), e = function (a, e) {
				return e = e || {}, b.apply(this, [a, e.data || c].concat(d))
			};
			return e.program = a, e.depth = d.length, e
		}

		function g(a, b, c) {
			var d = function (a, d) {
				return d = d || {}, b(a, d.data || c)
			};
			return d.program = a, d.depth = 0, d
		}

		function h(a, b, c, d, e, f) {
			var g = {partial: !0, helpers: d, partials: e, data: f};
			if (void 0 === a)throw new l("The partial " + b + " could not be found");
			return a instanceof Function ? a(c, g) : void 0
		}

		function i() {
			return ""
		}

		var j = {}, k = a, l = b, m = c.COMPILER_REVISION, n = c.REVISION_CHANGES;
		return j.checkRevision = d, j.template = e, j.programWithDepth = f, j.program = g, j.invokePartial = h, j.noop = i, j
	}(b, c, d), f = function (a, b, c, d, e) {
		"use strict";
		var f, g = a, h = b, i = c, j = d, k = e, l = function () {
			var a = new g.HandlebarsEnvironment;
			return j.extend(a, g), a.SafeString = h, a.Exception = i, a.Utils = j, a.VM = k, a.template = function (b) {
				return k.template(b, a)
			}, a
		}, m = l();
		return m.create = l, f = m
	}(d, a, c, b, e), g = function (a) {
		"use strict";
		function b(a) {
			a = a || {}, this.firstLine = a.first_line, this.firstColumn = a.first_column, this.lastColumn = a.last_column, this.lastLine = a.last_line
		}

		var c, d = a, e = {
			ProgramNode: function (a, c, d, f) {
				var g, h;
				3 === arguments.length ? (f = d, d = null) : 2 === arguments.length && (f = c, c = null), b.call(this, f), this.type = "program", this.statements = a, this.strip = {}, d ? (h = d[0], h ? (g = {
					first_line: h.firstLine,
					last_line: h.lastLine,
					last_column: h.lastColumn,
					first_column: h.firstColumn
				}, this.inverse = new e.ProgramNode(d, c, g)) : this.inverse = new e.ProgramNode(d, c), this.strip.right = c.left) : c && (this.strip.left = c.right)
			}, MustacheNode: function (a, c, d, f, g) {
				if (b.call(this, g), this.type = "mustache", this.strip = f, null != d && d.charAt) {
					var h = d.charAt(3) || d.charAt(2);
					this.escaped = "{" !== h && "&" !== h
				} else this.escaped = !!d;
				this.sexpr = a instanceof e.SexprNode ? a : new e.SexprNode(a, c), this.sexpr.isRoot = !0, this.id = this.sexpr.id, this.params = this.sexpr.params, this.hash = this.sexpr.hash, this.eligibleHelper = this.sexpr.eligibleHelper, this.isHelper = this.sexpr.isHelper
			}, SexprNode: function (a, c, d) {
				b.call(this, d), this.type = "sexpr", this.hash = c;
				var e = this.id = a[0], f = this.params = a.slice(1), g = this.eligibleHelper = e.isSimple;
				this.isHelper = g && (f.length || c)
			}, PartialNode: function (a, c, d, e) {
				b.call(this, e), this.type = "partial", this.partialName = a, this.context = c, this.strip = d
			}, BlockNode: function (a, c, e, f, g) {
				if (b.call(this, g), a.sexpr.id.original !== f.path.original)throw new d(a.sexpr.id.original + " doesn't match " + f.path.original, this);
				this.type = "block", this.mustache = a, this.program = c, this.inverse = e, this.strip = {
					left: a.strip.left,
					right: f.strip.right
				}, (c || e).strip.left = a.strip.right, (e || c).strip.right = f.strip.left, e && !c && (this.isInverse = !0)
			}, ContentNode: function (a, c) {
				b.call(this, c), this.type = "content", this.string = a
			}, HashNode: function (a, c) {
				b.call(this, c), this.type = "hash", this.pairs = a
			}, IdNode: function (a, c) {
				b.call(this, c), this.type = "ID";
				for (var e = "", f = [], g = 0, h = 0, i = a.length; i > h; h++) {
					var j = a[h].part;
					if (e += (a[h].separator || "") + j, ".." === j || "." === j || "this" === j) {
						if (f.length > 0)throw new d("Invalid path: " + e, this);
						".." === j ? g++ : this.isScoped = !0
					} else f.push(j)
				}
				this.original = e, this.parts = f, this.string = f.join("."), this.depth = g, this.isSimple = 1 === a.length && !this.isScoped && 0 === g, this.stringModeValue = this.string
			}, PartialNameNode: function (a, c) {
				b.call(this, c), this.type = "PARTIAL_NAME", this.name = a.original
			}, DataNode: function (a, c) {
				b.call(this, c), this.type = "DATA", this.id = a
			}, StringNode: function (a, c) {
				b.call(this, c), this.type = "STRING", this.original = this.string = this.stringModeValue = a
			}, IntegerNode: function (a, c) {
				b.call(this, c), this.type = "INTEGER", this.original = this.integer = a, this.stringModeValue = Number(a)
			}, BooleanNode: function (a, c) {
				b.call(this, c), this.type = "BOOLEAN", this.bool = a, this.stringModeValue = "true" === a
			}, CommentNode: function (a, c) {
				b.call(this, c), this.type = "comment", this.comment = a
			}
		};
		return c = e
	}(c), h = function () {
		"use strict";
		var a, b = function () {
			function a(a, b) {
				return {left: "~" === a.charAt(2), right: "~" === b.charAt(0) || "~" === b.charAt(1)}
			}

			function b() {
				this.yy = {}
			}

			var c = {
				trace: function () {
				},
				yy: {},
				symbols_: {
					error: 2,
					root: 3,
					statements: 4,
					EOF: 5,
					program: 6,
					simpleInverse: 7,
					statement: 8,
					openInverse: 9,
					closeBlock: 10,
					openBlock: 11,
					mustache: 12,
					partial: 13,
					CONTENT: 14,
					COMMENT: 15,
					OPEN_BLOCK: 16,
					sexpr: 17,
					CLOSE: 18,
					OPEN_INVERSE: 19,
					OPEN_ENDBLOCK: 20,
					path: 21,
					OPEN: 22,
					OPEN_UNESCAPED: 23,
					CLOSE_UNESCAPED: 24,
					OPEN_PARTIAL: 25,
					partialName: 26,
					partial_option0: 27,
					sexpr_repetition0: 28,
					sexpr_option0: 29,
					dataName: 30,
					param: 31,
					STRING: 32,
					INTEGER: 33,
					BOOLEAN: 34,
					OPEN_SEXPR: 35,
					CLOSE_SEXPR: 36,
					hash: 37,
					hash_repetition_plus0: 38,
					hashSegment: 39,
					ID: 40,
					EQUALS: 41,
					DATA: 42,
					pathSegments: 43,
					SEP: 44,
					$accept: 0,
					$end: 1
				},
				terminals_: {
					2: "error",
					5: "EOF",
					14: "CONTENT",
					15: "COMMENT",
					16: "OPEN_BLOCK",
					18: "CLOSE",
					19: "OPEN_INVERSE",
					20: "OPEN_ENDBLOCK",
					22: "OPEN",
					23: "OPEN_UNESCAPED",
					24: "CLOSE_UNESCAPED",
					25: "OPEN_PARTIAL",
					32: "STRING",
					33: "INTEGER",
					34: "BOOLEAN",
					35: "OPEN_SEXPR",
					36: "CLOSE_SEXPR",
					40: "ID",
					41: "EQUALS",
					42: "DATA",
					44: "SEP"
				},
				productions_: [0, [3, 2], [3, 1], [6, 2], [6, 3], [6, 2], [6, 1], [6, 1], [6, 0], [4, 1], [4, 2], [8, 3], [8, 3], [8, 1], [8, 1], [8, 1], [8, 1], [11, 3], [9, 3], [10, 3], [12, 3], [12, 3], [13, 4], [7, 2], [17, 3], [17, 1], [31, 1], [31, 1], [31, 1], [31, 1], [31, 1], [31, 3], [37, 1], [39, 3], [26, 1], [26, 1], [26, 1], [30, 2], [21, 1], [43, 3], [43, 1], [27, 0], [27, 1], [28, 0], [28, 2], [29, 0], [29, 1], [38, 1], [38, 2]],
				performAction: function (b, c, d, e, f, g) {
					var h = g.length - 1;
					switch (f) {
						case 1:
							return new e.ProgramNode(g[h - 1], this._$);
						case 2:
							return new e.ProgramNode([], this._$);
						case 3:
							this.$ = new e.ProgramNode([], g[h - 1], g[h], this._$);
							break;
						case 4:
							this.$ = new e.ProgramNode(g[h - 2], g[h - 1], g[h], this._$);
							break;
						case 5:
							this.$ = new e.ProgramNode(g[h - 1], g[h], [], this._$);
							break;
						case 6:
							this.$ = new e.ProgramNode(g[h], this._$);
							break;
						case 7:
							this.$ = new e.ProgramNode([], this._$);
							break;
						case 8:
							this.$ = new e.ProgramNode([], this._$);
							break;
						case 9:
							this.$ = [g[h]];
							break;
						case 10:
							g[h - 1].push(g[h]), this.$ = g[h - 1];
							break;
						case 11:
							this.$ = new e.BlockNode(g[h - 2], g[h - 1].inverse, g[h - 1], g[h], this._$);
							break;
						case 12:
							this.$ = new e.BlockNode(g[h - 2], g[h - 1], g[h - 1].inverse, g[h], this._$);
							break;
						case 13:
							this.$ = g[h];
							break;
						case 14:
							this.$ = g[h];
							break;
						case 15:
							this.$ = new e.ContentNode(g[h], this._$);
							break;
						case 16:
							this.$ = new e.CommentNode(g[h], this._$);
							break;
						case 17:
							this.$ = new e.MustacheNode(g[h - 1], null, g[h - 2], a(g[h - 2], g[h]), this._$);
							break;
						case 18:
							this.$ = new e.MustacheNode(g[h - 1], null, g[h - 2], a(g[h - 2], g[h]), this._$);
							break;
						case 19:
							this.$ = {path: g[h - 1], strip: a(g[h - 2], g[h])};
							break;
						case 20:
							this.$ = new e.MustacheNode(g[h - 1], null, g[h - 2], a(g[h - 2], g[h]), this._$);
							break;
						case 21:
							this.$ = new e.MustacheNode(g[h - 1], null, g[h - 2], a(g[h - 2], g[h]), this._$);
							break;
						case 22:
							this.$ = new e.PartialNode(g[h - 2], g[h - 1], a(g[h - 3], g[h]), this._$);
							break;
						case 23:
							this.$ = a(g[h - 1], g[h]);
							break;
						case 24:
							this.$ = new e.SexprNode([g[h - 2]].concat(g[h - 1]), g[h], this._$);
							break;
						case 25:
							this.$ = new e.SexprNode([g[h]], null, this._$);
							break;
						case 26:
							this.$ = g[h];
							break;
						case 27:
							this.$ = new e.StringNode(g[h], this._$);
							break;
						case 28:
							this.$ = new e.IntegerNode(g[h], this._$);
							break;
						case 29:
							this.$ = new e.BooleanNode(g[h], this._$);
							break;
						case 30:
							this.$ = g[h];
							break;
						case 31:
							g[h - 1].isHelper = !0, this.$ = g[h - 1];
							break;
						case 32:
							this.$ = new e.HashNode(g[h], this._$);
							break;
						case 33:
							this.$ = [g[h - 2], g[h]];
							break;
						case 34:
							this.$ = new e.PartialNameNode(g[h], this._$);
							break;
						case 35:
							this.$ = new e.PartialNameNode(new e.StringNode(g[h], this._$), this._$);
							break;
						case 36:
							this.$ = new e.PartialNameNode(new e.IntegerNode(g[h], this._$));
							break;
						case 37:
							this.$ = new e.DataNode(g[h], this._$);
							break;
						case 38:
							this.$ = new e.IdNode(g[h], this._$);
							break;
						case 39:
							g[h - 2].push({part: g[h], separator: g[h - 1]}), this.$ = g[h - 2];
							break;
						case 40:
							this.$ = [{part: g[h]}];
							break;
						case 43:
							this.$ = [];
							break;
						case 44:
							g[h - 1].push(g[h]);
							break;
						case 47:
							this.$ = [g[h]];
							break;
						case 48:
							g[h - 1].push(g[h])
					}
				},
				table: [{
					3: 1,
					4: 2,
					5: [1, 3],
					8: 4,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 11],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {1: [3]}, {
					5: [1, 16],
					8: 17,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 11],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {1: [2, 2]}, {
					5: [2, 9],
					14: [2, 9],
					15: [2, 9],
					16: [2, 9],
					19: [2, 9],
					20: [2, 9],
					22: [2, 9],
					23: [2, 9],
					25: [2, 9]
				}, {
					4: 20,
					6: 18,
					7: 19,
					8: 4,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 21],
					20: [2, 8],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {
					4: 20,
					6: 22,
					7: 19,
					8: 4,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 21],
					20: [2, 8],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {
					5: [2, 13],
					14: [2, 13],
					15: [2, 13],
					16: [2, 13],
					19: [2, 13],
					20: [2, 13],
					22: [2, 13],
					23: [2, 13],
					25: [2, 13]
				}, {
					5: [2, 14],
					14: [2, 14],
					15: [2, 14],
					16: [2, 14],
					19: [2, 14],
					20: [2, 14],
					22: [2, 14],
					23: [2, 14],
					25: [2, 14]
				}, {
					5: [2, 15],
					14: [2, 15],
					15: [2, 15],
					16: [2, 15],
					19: [2, 15],
					20: [2, 15],
					22: [2, 15],
					23: [2, 15],
					25: [2, 15]
				}, {
					5: [2, 16],
					14: [2, 16],
					15: [2, 16],
					16: [2, 16],
					19: [2, 16],
					20: [2, 16],
					22: [2, 16],
					23: [2, 16],
					25: [2, 16]
				}, {17: 23, 21: 24, 30: 25, 40: [1, 28], 42: [1, 27], 43: 26}, {
					17: 29,
					21: 24,
					30: 25,
					40: [1, 28],
					42: [1, 27],
					43: 26
				}, {17: 30, 21: 24, 30: 25, 40: [1, 28], 42: [1, 27], 43: 26}, {
					17: 31,
					21: 24,
					30: 25,
					40: [1, 28],
					42: [1, 27],
					43: 26
				}, {21: 33, 26: 32, 32: [1, 34], 33: [1, 35], 40: [1, 28], 43: 26}, {1: [2, 1]}, {
					5: [2, 10],
					14: [2, 10],
					15: [2, 10],
					16: [2, 10],
					19: [2, 10],
					20: [2, 10],
					22: [2, 10],
					23: [2, 10],
					25: [2, 10]
				}, {10: 36, 20: [1, 37]}, {
					4: 38,
					8: 4,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 11],
					20: [2, 7],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {
					7: 39,
					8: 17,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 21],
					20: [2, 6],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {17: 23, 18: [1, 40], 21: 24, 30: 25, 40: [1, 28], 42: [1, 27], 43: 26}, {
					10: 41,
					20: [1, 37]
				}, {18: [1, 42]}, {
					18: [2, 43],
					24: [2, 43],
					28: 43,
					32: [2, 43],
					33: [2, 43],
					34: [2, 43],
					35: [2, 43],
					36: [2, 43],
					40: [2, 43],
					42: [2, 43]
				}, {18: [2, 25], 24: [2, 25], 36: [2, 25]}, {
					18: [2, 38],
					24: [2, 38],
					32: [2, 38],
					33: [2, 38],
					34: [2, 38],
					35: [2, 38],
					36: [2, 38],
					40: [2, 38],
					42: [2, 38],
					44: [1, 44]
				}, {21: 45, 40: [1, 28], 43: 26}, {
					18: [2, 40],
					24: [2, 40],
					32: [2, 40],
					33: [2, 40],
					34: [2, 40],
					35: [2, 40],
					36: [2, 40],
					40: [2, 40],
					42: [2, 40],
					44: [2, 40]
				}, {18: [1, 46]}, {18: [1, 47]}, {24: [1, 48]}, {
					18: [2, 41],
					21: 50,
					27: 49,
					40: [1, 28],
					43: 26
				}, {18: [2, 34], 40: [2, 34]}, {18: [2, 35], 40: [2, 35]}, {18: [2, 36], 40: [2, 36]}, {
					5: [2, 11],
					14: [2, 11],
					15: [2, 11],
					16: [2, 11],
					19: [2, 11],
					20: [2, 11],
					22: [2, 11],
					23: [2, 11],
					25: [2, 11]
				}, {21: 51, 40: [1, 28], 43: 26}, {
					8: 17,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 11],
					20: [2, 3],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {
					4: 52,
					8: 4,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 11],
					20: [2, 5],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {
					14: [2, 23],
					15: [2, 23],
					16: [2, 23],
					19: [2, 23],
					20: [2, 23],
					22: [2, 23],
					23: [2, 23],
					25: [2, 23]
				}, {
					5: [2, 12],
					14: [2, 12],
					15: [2, 12],
					16: [2, 12],
					19: [2, 12],
					20: [2, 12],
					22: [2, 12],
					23: [2, 12],
					25: [2, 12]
				}, {
					14: [2, 18],
					15: [2, 18],
					16: [2, 18],
					19: [2, 18],
					20: [2, 18],
					22: [2, 18],
					23: [2, 18],
					25: [2, 18]
				}, {
					18: [2, 45],
					21: 56,
					24: [2, 45],
					29: 53,
					30: 60,
					31: 54,
					32: [1, 57],
					33: [1, 58],
					34: [1, 59],
					35: [1, 61],
					36: [2, 45],
					37: 55,
					38: 62,
					39: 63,
					40: [1, 64],
					42: [1, 27],
					43: 26
				}, {40: [1, 65]}, {
					18: [2, 37],
					24: [2, 37],
					32: [2, 37],
					33: [2, 37],
					34: [2, 37],
					35: [2, 37],
					36: [2, 37],
					40: [2, 37],
					42: [2, 37]
				}, {
					14: [2, 17],
					15: [2, 17],
					16: [2, 17],
					19: [2, 17],
					20: [2, 17],
					22: [2, 17],
					23: [2, 17],
					25: [2, 17]
				}, {
					5: [2, 20],
					14: [2, 20],
					15: [2, 20],
					16: [2, 20],
					19: [2, 20],
					20: [2, 20],
					22: [2, 20],
					23: [2, 20],
					25: [2, 20]
				}, {
					5: [2, 21],
					14: [2, 21],
					15: [2, 21],
					16: [2, 21],
					19: [2, 21],
					20: [2, 21],
					22: [2, 21],
					23: [2, 21],
					25: [2, 21]
				}, {18: [1, 66]}, {18: [2, 42]}, {18: [1, 67]}, {
					8: 17,
					9: 5,
					11: 6,
					12: 7,
					13: 8,
					14: [1, 9],
					15: [1, 10],
					16: [1, 12],
					19: [1, 11],
					20: [2, 4],
					22: [1, 13],
					23: [1, 14],
					25: [1, 15]
				}, {18: [2, 24], 24: [2, 24], 36: [2, 24]}, {
					18: [2, 44],
					24: [2, 44],
					32: [2, 44],
					33: [2, 44],
					34: [2, 44],
					35: [2, 44],
					36: [2, 44],
					40: [2, 44],
					42: [2, 44]
				}, {18: [2, 46], 24: [2, 46], 36: [2, 46]}, {
					18: [2, 26],
					24: [2, 26],
					32: [2, 26],
					33: [2, 26],
					34: [2, 26],
					35: [2, 26],
					36: [2, 26],
					40: [2, 26],
					42: [2, 26]
				}, {
					18: [2, 27],
					24: [2, 27],
					32: [2, 27],
					33: [2, 27],
					34: [2, 27],
					35: [2, 27],
					36: [2, 27],
					40: [2, 27],
					42: [2, 27]
				}, {
					18: [2, 28],
					24: [2, 28],
					32: [2, 28],
					33: [2, 28],
					34: [2, 28],
					35: [2, 28],
					36: [2, 28],
					40: [2, 28],
					42: [2, 28]
				}, {
					18: [2, 29],
					24: [2, 29],
					32: [2, 29],
					33: [2, 29],
					34: [2, 29],
					35: [2, 29],
					36: [2, 29],
					40: [2, 29],
					42: [2, 29]
				}, {
					18: [2, 30],
					24: [2, 30],
					32: [2, 30],
					33: [2, 30],
					34: [2, 30],
					35: [2, 30],
					36: [2, 30],
					40: [2, 30],
					42: [2, 30]
				}, {17: 68, 21: 24, 30: 25, 40: [1, 28], 42: [1, 27], 43: 26}, {
					18: [2, 32],
					24: [2, 32],
					36: [2, 32],
					39: 69,
					40: [1, 70]
				}, {18: [2, 47], 24: [2, 47], 36: [2, 47], 40: [2, 47]}, {
					18: [2, 40],
					24: [2, 40],
					32: [2, 40],
					33: [2, 40],
					34: [2, 40],
					35: [2, 40],
					36: [2, 40],
					40: [2, 40],
					41: [1, 71],
					42: [2, 40],
					44: [2, 40]
				}, {
					18: [2, 39],
					24: [2, 39],
					32: [2, 39],
					33: [2, 39],
					34: [2, 39],
					35: [2, 39],
					36: [2, 39],
					40: [2, 39],
					42: [2, 39],
					44: [2, 39]
				}, {
					5: [2, 22],
					14: [2, 22],
					15: [2, 22],
					16: [2, 22],
					19: [2, 22],
					20: [2, 22],
					22: [2, 22],
					23: [2, 22],
					25: [2, 22]
				}, {
					5: [2, 19],
					14: [2, 19],
					15: [2, 19],
					16: [2, 19],
					19: [2, 19],
					20: [2, 19],
					22: [2, 19],
					23: [2, 19],
					25: [2, 19]
				}, {36: [1, 72]}, {18: [2, 48], 24: [2, 48], 36: [2, 48], 40: [2, 48]}, {41: [1, 71]}, {
					21: 56,
					30: 60,
					31: 73,
					32: [1, 57],
					33: [1, 58],
					34: [1, 59],
					35: [1, 61],
					40: [1, 28],
					42: [1, 27],
					43: 26
				}, {
					18: [2, 31],
					24: [2, 31],
					32: [2, 31],
					33: [2, 31],
					34: [2, 31],
					35: [2, 31],
					36: [2, 31],
					40: [2, 31],
					42: [2, 31]
				}, {18: [2, 33], 24: [2, 33], 36: [2, 33], 40: [2, 33]}],
				defaultActions: {3: [2, 2], 16: [2, 1], 50: [2, 42]},
				parseError: function (a) {
					throw new Error(a)
				},
				parse: function (a) {
					function b() {
						var a;
						return a = c.lexer.lex() || 1, "number" != typeof a && (a = c.symbols_[a] || a), a
					}

					var c = this, d = [0], e = [null], f = [], g = this.table, h = "", i = 0, j = 0, k = 0;
					this.lexer.setInput(a), this.lexer.yy = this.yy, this.yy.lexer = this.lexer, this.yy.parser = this, "undefined" == typeof this.lexer.yylloc && (this.lexer.yylloc = {});
					var l = this.lexer.yylloc;
					f.push(l);
					var m = this.lexer.options && this.lexer.options.ranges;
					"function" == typeof this.yy.parseError && (this.parseError = this.yy.parseError);
					for (var n, o, p, q, r, s, t, u, v, w = {}; ;) {
						if (p = d[d.length - 1], this.defaultActions[p] ? q = this.defaultActions[p] : ((null === n || "undefined" == typeof n) && (n = b()), q = g[p] && g[p][n]), "undefined" == typeof q || !q.length || !q[0]) {
							var x = "";
							if (!k) {
								v = [];
								for (s in g[p])this.terminals_[s] && s > 2 && v.push("'" + this.terminals_[s] + "'");
								x = this.lexer.showPosition ? "Parse error on line " + (i + 1) + ":\n" + this.lexer.showPosition() + "\nExpecting " + v.join(", ") + ", got '" + (this.terminals_[n] || n) + "'" : "Parse error on line " + (i + 1) + ": Unexpected " + (1 == n ? "end of input" : "'" + (this.terminals_[n] || n) + "'"), this.parseError(x, {
									text: this.lexer.match,
									token: this.terminals_[n] || n,
									line: this.lexer.yylineno,
									loc: l,
									expected: v
								})
							}
						}
						if (q[0] instanceof Array && q.length > 1)throw new Error("Parse Error: multiple actions possible at state: " + p + ", token: " + n);
						switch (q[0]) {
							case 1:
								d.push(n), e.push(this.lexer.yytext), f.push(this.lexer.yylloc), d.push(q[1]), n = null, o ? (n = o, o = null) : (j = this.lexer.yyleng, h = this.lexer.yytext, i = this.lexer.yylineno, l = this.lexer.yylloc, k > 0 && k--);
								break;
							case 2:
								if (t = this.productions_[q[1]][1], w.$ = e[e.length - t], w._$ = {
										first_line: f[f.length - (t || 1)].first_line,
										last_line: f[f.length - 1].last_line,
										first_column: f[f.length - (t || 1)].first_column,
										last_column: f[f.length - 1].last_column
									}, m && (w._$.range = [f[f.length - (t || 1)].range[0], f[f.length - 1].range[1]]), r = this.performAction.call(w, h, j, i, this.yy, q[1], e, f), "undefined" != typeof r)return r;
								t && (d = d.slice(0, -1 * t * 2), e = e.slice(0, -1 * t), f = f.slice(0, -1 * t)), d.push(this.productions_[q[1]][0]), e.push(w.$), f.push(w._$), u = g[d[d.length - 2]][d[d.length - 1]], d.push(u);
								break;
							case 3:
								return !0
						}
					}
					return !0
				}
			}, d = function () {
				var a = {
					EOF: 1, parseError: function (a, b) {
						if (!this.yy.parser)throw new Error(a);
						this.yy.parser.parseError(a, b)
					}, setInput: function (a) {
						return this._input = a, this._more = this._less = this.done = !1, this.yylineno = this.yyleng = 0, this.yytext = this.matched = this.match = "", this.conditionStack = ["INITIAL"], this.yylloc = {
							first_line: 1,
							first_column: 0,
							last_line: 1,
							last_column: 0
						}, this.options.ranges && (this.yylloc.range = [0, 0]), this.offset = 0, this
					}, input: function () {
						var a = this._input[0];
						this.yytext += a, this.yyleng++, this.offset++, this.match += a, this.matched += a;
						var b = a.match(/(?:\r\n?|\n).*/g);
						return b ? (this.yylineno++, this.yylloc.last_line++) : this.yylloc.last_column++, this.options.ranges && this.yylloc.range[1]++, this._input = this._input.slice(1), a
					}, unput: function (a) {
						var b = a.length, c = a.split(/(?:\r\n?|\n)/g);
						this._input = a + this._input, this.yytext = this.yytext.substr(0, this.yytext.length - b - 1), this.offset -= b;
						var d = this.match.split(/(?:\r\n?|\n)/g);
						this.match = this.match.substr(0, this.match.length - 1), this.matched = this.matched.substr(0, this.matched.length - 1), c.length - 1 && (this.yylineno -= c.length - 1);
						var e = this.yylloc.range;
						return this.yylloc = {
							first_line: this.yylloc.first_line,
							last_line: this.yylineno + 1,
							first_column: this.yylloc.first_column,
							last_column: c ? (c.length === d.length ? this.yylloc.first_column : 0) + d[d.length - c.length].length - c[0].length : this.yylloc.first_column - b
						}, this.options.ranges && (this.yylloc.range = [e[0], e[0] + this.yyleng - b]), this
					}, more: function () {
						return this._more = !0, this
					}, less: function (a) {
						this.unput(this.match.slice(a))
					}, pastInput: function () {
						var a = this.matched.substr(0, this.matched.length - this.match.length);
						return (a.length > 20 ? "..." : "") + a.substr(-20).replace(/\n/g, "")
					}, upcomingInput: function () {
						var a = this.match;
						return a.length < 20 && (a += this._input.substr(0, 20 - a.length)), (a.substr(0, 20) + (a.length > 20 ? "..." : "")).replace(/\n/g, "")
					}, showPosition: function () {
						var a = this.pastInput(), b = new Array(a.length + 1).join("-");
						return a + this.upcomingInput() + "\n" + b + "^"
					}, next: function () {
						if (this.done)return this.EOF;
						this._input || (this.done = !0);
						var a, b, c, d, e;
						this._more || (this.yytext = "", this.match = "");
						for (var f = this._currentRules(), g = 0; g < f.length && (c = this._input.match(this.rules[f[g]]), !c || b && !(c[0].length > b[0].length) || (b = c, d = g, this.options.flex)); g++);
						return b ? (e = b[0].match(/(?:\r\n?|\n).*/g), e && (this.yylineno += e.length), this.yylloc = {
							first_line: this.yylloc.last_line,
							last_line: this.yylineno + 1,
							first_column: this.yylloc.last_column,
							last_column: e ? e[e.length - 1].length - e[e.length - 1].match(/\r?\n?/)[0].length : this.yylloc.last_column + b[0].length
						}, this.yytext += b[0], this.match += b[0], this.matches = b, this.yyleng = this.yytext.length, this.options.ranges && (this.yylloc.range = [this.offset, this.offset += this.yyleng]), this._more = !1, this._input = this._input.slice(b[0].length), this.matched += b[0], a = this.performAction.call(this, this.yy, this, f[d], this.conditionStack[this.conditionStack.length - 1]), this.done && this._input && (this.done = !1), a ? a : void 0) : "" === this._input ? this.EOF : this.parseError("Lexical error on line " + (this.yylineno + 1) + ". Unrecognized text.\n" + this.showPosition(), {
							text: "",
							token: null,
							line: this.yylineno
						})
					}, lex: function () {
						var a = this.next();
						return "undefined" != typeof a ? a : this.lex()
					}, begin: function (a) {
						this.conditionStack.push(a)
					}, popState: function () {
						return this.conditionStack.pop()
					}, _currentRules: function () {
						return this.conditions[this.conditionStack[this.conditionStack.length - 1]].rules
					}, topState: function () {
						return this.conditionStack[this.conditionStack.length - 2]
					}, pushState: function (a) {
						this.begin(a)
					}
				};
				return a.options = {}, a.performAction = function (a, b, c, d) {
					function e(a, c) {
						return b.yytext = b.yytext.substr(a, b.yyleng - c)
					}

					switch (c) {
						case 0:
							if ("\\\\" === b.yytext.slice(-2) ? (e(0, 1), this.begin("mu")) : "\\" === b.yytext.slice(-1) ? (e(0, 1), this.begin("emu")) : this.begin("mu"), b.yytext)return 14;
							break;
						case 1:
							return 14;
						case 2:
							return this.popState(), 14;
						case 3:
							return e(0, 4), this.popState(), 15;
						case 4:
							return 35;
						case 5:
							return 36;
						case 6:
							return 25;
						case 7:
							return 16;
						case 8:
							return 20;
						case 9:
							return 19;
						case 10:
							return 19;
						case 11:
							return 23;
						case 12:
							return 22;
						case 13:
							this.popState(), this.begin("com");
							break;
						case 14:
							return e(3, 5), this.popState(), 15;
						case 15:
							return 22;
						case 16:
							return 41;
						case 17:
							return 40;
						case 18:
							return 40;
						case 19:
							return 44;
						case 20:
							break;
						case 21:
							return this.popState(), 24;
						case 22:
							return this.popState(), 18;
						case 23:
							return b.yytext = e(1, 2).replace(/\\"/g, '"'), 32;
						case 24:
							return b.yytext = e(1, 2).replace(/\\'/g, "'"), 32;
						case 25:
							return 42;
						case 26:
							return 34;
						case 27:
							return 34;
						case 28:
							return 33;
						case 29:
							return 40;
						case 30:
							return b.yytext = e(1, 2), 40;
						case 31:
							return "INVALID";
						case 32:
							return 5
					}
				}, a.rules = [/^(?:[^\x00]*?(?=(\{\{)))/, /^(?:[^\x00]+)/, /^(?:[^\x00]{2,}?(?=(\{\{|\\\{\{|\\\\\{\{|$)))/, /^(?:[\s\S]*?--\}\})/, /^(?:\()/, /^(?:\))/, /^(?:\{\{(~)?>)/, /^(?:\{\{(~)?#)/, /^(?:\{\{(~)?\/)/, /^(?:\{\{(~)?\^)/, /^(?:\{\{(~)?\s*else\b)/, /^(?:\{\{(~)?\{)/, /^(?:\{\{(~)?&)/, /^(?:\{\{!--)/, /^(?:\{\{![\s\S]*?\}\})/, /^(?:\{\{(~)?)/, /^(?:=)/, /^(?:\.\.)/, /^(?:\.(?=([=~}\s\/.)])))/, /^(?:[\/.])/, /^(?:\s+)/, /^(?:\}(~)?\}\})/, /^(?:(~)?\}\})/, /^(?:"(\\["]|[^"])*")/, /^(?:'(\\[']|[^'])*')/, /^(?:@)/, /^(?:true(?=([~}\s)])))/, /^(?:false(?=([~}\s)])))/, /^(?:-?[0-9]+(?=([~}\s)])))/, /^(?:([^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=([=~}\s\/.)]))))/, /^(?:\[[^\]]*\])/, /^(?:.)/, /^(?:$)/], a.conditions = {
					mu: {
						rules: [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32],
						inclusive: !1
					},
					emu: {rules: [2], inclusive: !1},
					com: {rules: [3], inclusive: !1},
					INITIAL: {rules: [0, 1, 32], inclusive: !0}
				}, a
			}();
			return c.lexer = d, b.prototype = c, c.Parser = b, new b
		}();
		return a = b
	}(), i = function (a, b) {
		"use strict";
		function c(a) {
			return a.constructor === f.ProgramNode ? a : (e.yy = f, e.parse(a))
		}

		var d = {}, e = a, f = b;
		return d.parser = e, d.parse = c, d
	}(h, g), j = function (a) {
		"use strict";
		function b() {
		}

		function c(a, b, c) {
			if (null == a || "string" != typeof a && a.constructor !== c.AST.ProgramNode)throw new f("You must pass a string or Handlebars AST to Handlebars.precompile. You passed " + a);
			b = b || {}, "data" in b || (b.data = !0);
			var d = c.parse(a), e = (new c.Compiler).compile(d, b);
			return (new c.JavaScriptCompiler).compile(e, b)
		}

		function d(a, b, c) {
			function d() {
				var d = c.parse(a), e = (new c.Compiler).compile(d, b), f = (new c.JavaScriptCompiler).compile(e, b, void 0, !0);
				return c.template(f)
			}

			if (null == a || "string" != typeof a && a.constructor !== c.AST.ProgramNode)throw new f("You must pass a string or Handlebars AST to Handlebars.compile. You passed " + a);
			b = b || {}, "data" in b || (b.data = !0);
			var e;
			return function (a, b) {
				return e || (e = d()), e.call(this, a, b)
			}
		}

		var e = {}, f = a;
		return e.Compiler = b, b.prototype = {
			compiler: b, disassemble: function () {
				for (var a, b, c, d = this.opcodes, e = [], f = 0, g = d.length; g > f; f++)if (a = d[f], "DECLARE" === a.opcode)e.push("DECLARE " + a.name + "=" + a.value); else {
					b = [];
					for (var h = 0; h < a.args.length; h++)c = a.args[h], "string" == typeof c && (c = '"' + c.replace("\n", "\\n") + '"'), b.push(c);
					e.push(a.opcode + " " + b.join(" "))
				}
				return e.join("\n")
			}, equals: function (a) {
				var b = this.opcodes.length;
				if (a.opcodes.length !== b)return !1;
				for (var c = 0; b > c; c++) {
					var d = this.opcodes[c], e = a.opcodes[c];
					if (d.opcode !== e.opcode || d.args.length !== e.args.length)return !1;
					for (var f = 0; f < d.args.length; f++)if (d.args[f] !== e.args[f])return !1
				}
				if (b = this.children.length, a.children.length !== b)return !1;
				for (c = 0; b > c; c++)if (!this.children[c].equals(a.children[c]))return !1;
				return !0
			}, guid: 0, compile: function (a, b) {
				this.opcodes = [], this.children = [], this.depths = {list: []}, this.options = b;
				var c = this.options.knownHelpers;
				if (this.options.knownHelpers = {
						helperMissing: !0,
						blockHelperMissing: !0,
						each: !0,
						"if": !0,
						unless: !0,
						"with": !0,
						log: !0
					}, c)for (var d in c)this.options.knownHelpers[d] = c[d];
				return this.accept(a)
			}, accept: function (a) {
				var b, c = a.strip || {};
				return c.left && this.opcode("strip"), b = this[a.type](a), c.right && this.opcode("strip"), b
			}, program: function (a) {
				for (var b = a.statements, c = 0, d = b.length; d > c; c++)this.accept(b[c]);
				return this.isSimple = 1 === d, this.depths.list = this.depths.list.sort(function (a, b) {
					return a - b
				}), this
			}, compileProgram: function (a) {
				var b, c = (new this.compiler).compile(a, this.options), d = this.guid++;
				this.usePartial = this.usePartial || c.usePartial, this.children[d] = c;
				for (var e = 0, f = c.depths.list.length; f > e; e++)b = c.depths.list[e], 2 > b || this.addDepth(b - 1);
				return d
			}, block: function (a) {
				var b = a.mustache, c = a.program, d = a.inverse;
				c && (c = this.compileProgram(c)), d && (d = this.compileProgram(d));
				var e = b.sexpr, f = this.classifySexpr(e);
				"helper" === f ? this.helperSexpr(e, c, d) : "simple" === f ? (this.simpleSexpr(e), this.opcode("pushProgram", c), this.opcode("pushProgram", d), this.opcode("emptyHash"), this.opcode("blockValue")) : (this.ambiguousSexpr(e, c, d), this.opcode("pushProgram", c), this.opcode("pushProgram", d), this.opcode("emptyHash"), this.opcode("ambiguousBlockValue")), this.opcode("append")
			}, hash: function (a) {
				var b, c, d = a.pairs;
				this.opcode("pushHash");
				for (var e = 0, f = d.length; f > e; e++)b = d[e], c = b[1], this.options.stringParams ? (c.depth && this.addDepth(c.depth), this.opcode("getContext", c.depth || 0), this.opcode("pushStringParam", c.stringModeValue, c.type), "sexpr" === c.type && this.sexpr(c)) : this.accept(c), this.opcode("assignToHash", b[0]);
				this.opcode("popHash")
			}, partial: function (a) {
				var b = a.partialName;
				this.usePartial = !0, a.context ? this.ID(a.context) : this.opcode("push", "depth0"), this.opcode("invokePartial", b.name), this.opcode("append")
			}, content: function (a) {
				this.opcode("appendContent", a.string)
			}, mustache: function (a) {
				this.sexpr(a.sexpr), this.opcode(a.escaped && !this.options.noEscape ? "appendEscaped" : "append")
			}, ambiguousSexpr: function (a, b, c) {
				var d = a.id, e = d.parts[0], f = null != b || null != c;
				this.opcode("getContext", d.depth), this.opcode("pushProgram", b), this.opcode("pushProgram", c), this.opcode("invokeAmbiguous", e, f)
			}, simpleSexpr: function (a) {
				var b = a.id;
				"DATA" === b.type ? this.DATA(b) : b.parts.length ? this.ID(b) : (this.addDepth(b.depth), this.opcode("getContext", b.depth), this.opcode("pushContext")), this.opcode("resolvePossibleLambda")
			}, helperSexpr: function (a, b, c) {
				var d = this.setupFullMustacheParams(a, b, c), e = a.id.parts[0];
				if (this.options.knownHelpers[e])this.opcode("invokeKnownHelper", d.length, e); else {
					if (this.options.knownHelpersOnly)throw new f("You specified knownHelpersOnly, but used the unknown helper " + e, a);
					this.opcode("invokeHelper", d.length, e, a.isRoot)
				}
			}, sexpr: function (a) {
				var b = this.classifySexpr(a);
				"simple" === b ? this.simpleSexpr(a) : "helper" === b ? this.helperSexpr(a) : this.ambiguousSexpr(a)
			}, ID: function (a) {
				this.addDepth(a.depth), this.opcode("getContext", a.depth);
				var b = a.parts[0];
				b ? this.opcode("lookupOnContext", a.parts[0]) : this.opcode("pushContext");
				for (var c = 1, d = a.parts.length; d > c; c++)this.opcode("lookup", a.parts[c])
			}, DATA: function (a) {
				if (this.options.data = !0, a.id.isScoped || a.id.depth)throw new f("Scoped data references are not supported: " + a.original, a);
				this.opcode("lookupData");
				for (var b = a.id.parts, c = 0, d = b.length; d > c; c++)this.opcode("lookup", b[c])
			}, STRING: function (a) {
				this.opcode("pushString", a.string)
			}, INTEGER: function (a) {
				this.opcode("pushLiteral", a.integer)
			}, BOOLEAN: function (a) {
				this.opcode("pushLiteral", a.bool)
			}, comment: function () {
			}, opcode: function (a) {
				this.opcodes.push({opcode: a, args: [].slice.call(arguments, 1)})
			}, declare: function (a, b) {
				this.opcodes.push({opcode: "DECLARE", name: a, value: b})
			}, addDepth: function (a) {
				0 !== a && (this.depths[a] || (this.depths[a] = !0, this.depths.list.push(a)))
			}, classifySexpr: function (a) {
				var b = a.isHelper, c = a.eligibleHelper, d = this.options;
				if (c && !b) {
					var e = a.id.parts[0];
					d.knownHelpers[e] ? b = !0 : d.knownHelpersOnly && (c = !1)
				}
				return b ? "helper" : c ? "ambiguous" : "simple"
			}, pushParams: function (a) {
				for (var b, c = a.length; c--;)b = a[c], this.options.stringParams ? (b.depth && this.addDepth(b.depth), this.opcode("getContext", b.depth || 0), this.opcode("pushStringParam", b.stringModeValue, b.type), "sexpr" === b.type && this.sexpr(b)) : this[b.type](b)
			}, setupFullMustacheParams: function (a, b, c) {
				var d = a.params;
				return this.pushParams(d), this.opcode("pushProgram", b), this.opcode("pushProgram", c), a.hash ? this.hash(a.hash) : this.opcode("emptyHash"), d
			}
		}, e.precompile = c, e.compile = d, e
	}(c), k = function (a, b) {
		"use strict";
		function c(a) {
			this.value = a
		}

		function d() {
		}

		var e, f = a.COMPILER_REVISION, g = a.REVISION_CHANGES, h = a.log, i = b;
		d.prototype = {
			nameLookup: function (a, b) {
				var c, e;
				return 0 === a.indexOf("depth") && (c = !0), e = /^[0-9]+$/.test(b) ? a + "[" + b + "]" : d.isValidJavaScriptVariableName(b) ? a + "." + b : a + "['" + b + "']", c ? "(" + a + " && " + e + ")" : e
			}, compilerInfo: function () {
				var a = f, b = g[a];
				return "this.compilerInfo = [" + a + ",'" + b + "'];\n"
			}, appendToBuffer: function (a) {
				return this.environment.isSimple ? "return " + a + ";" : {
					appendToBuffer: !0,
					content: a,
					toString: function () {
						return "buffer += " + a + ";"
					}
				}
			}, initializeBuffer: function () {
				return this.quotedString("")
			}, namespace: "Handlebars", compile: function (a, b, c, d) {
				this.environment = a, this.options = b || {}, h("debug", this.environment.disassemble() + "\n\n"), this.name = this.environment.name, this.isChild = !!c, this.context = c || {
						programs: [],
						environments: [],
						aliases: {}
					}, this.preamble(), this.stackSlot = 0, this.stackVars = [], this.registers = {list: []}, this.hashes = [], this.compileStack = [], this.inlineStack = [], this.compileChildren(a, b);
				var e, f = a.opcodes;
				this.i = 0;
				for (var g = f.length; this.i < g; this.i++)e = f[this.i], "DECLARE" === e.opcode ? this[e.name] = e.value : this[e.opcode].apply(this, e.args), e.opcode !== this.stripNext && (this.stripNext = !1);
				if (this.pushSource(""), this.stackSlot || this.inlineStack.length || this.compileStack.length)throw new i("Compile completed with content left on stack");
				return this.createFunctionContext(d)
			}, preamble: function () {
				var a = [];
				if (this.isChild)a.push(""); else {
					var b = this.namespace, c = "helpers = this.merge(helpers, " + b + ".helpers);";
					this.environment.usePartial && (c = c + " partials = this.merge(partials, " + b + ".partials);"), this.options.data && (c += " data = data || {};"), a.push(c)
				}
				a.push(this.environment.isSimple ? "" : ", buffer = " + this.initializeBuffer()), this.lastContext = 0, this.source = a
			}, createFunctionContext: function (a) {
				var b = this.stackVars.concat(this.registers.list);
				if (b.length > 0 && (this.source[1] = this.source[1] + ", " + b.join(", ")), !this.isChild)for (var c in this.context.aliases)this.context.aliases.hasOwnProperty(c) && (this.source[1] = this.source[1] + ", " + c + "=" + this.context.aliases[c]);
				this.source[1] && (this.source[1] = "var " + this.source[1].substring(2) + ";"), this.isChild || (this.source[1] += "\n" + this.context.programs.join("\n") + "\n"), this.environment.isSimple || this.pushSource("return buffer;");
				for (var d = this.isChild ? ["depth0", "data"] : ["Handlebars", "depth0", "helpers", "partials", "data"], e = 0, f = this.environment.depths.list.length; f > e; e++)d.push("depth" + this.environment.depths.list[e]);
				var g = this.mergeSource();
				if (this.isChild || (g = this.compilerInfo() + g), a)return d.push(g), Function.apply(this, d);
				var i = "function " + (this.name || "") + "(" + d.join(",") + ") {\n  " + g + "}";
				return h("debug", i + "\n\n"), i
			}, mergeSource: function () {
				for (var a, b = "", c = 0, d = this.source.length; d > c; c++) {
					var e = this.source[c];
					e.appendToBuffer ? a = a ? a + "\n    + " + e.content : e.content : (a && (b += "buffer += " + a + ";\n  ", a = void 0), b += e + "\n  ")
				}
				return b
			}, blockValue: function () {
				this.context.aliases.blockHelperMissing = "helpers.blockHelperMissing";
				var a = ["depth0"];
				this.setupParams(0, a), this.replaceStack(function (b) {
					return a.splice(1, 0, b), "blockHelperMissing.call(" + a.join(", ") + ")"
				})
			}, ambiguousBlockValue: function () {
				this.context.aliases.blockHelperMissing = "helpers.blockHelperMissing";
				var a = ["depth0"];
				this.setupParams(0, a);
				var b = this.topStack();
				a.splice(1, 0, b), this.pushSource("if (!" + this.lastHelper + ") { " + b + " = blockHelperMissing.call(" + a.join(", ") + "); }")
			}, appendContent: function (a) {
				this.pendingContent && (a = this.pendingContent + a), this.stripNext && (a = a.replace(/^\s+/, "")), this.pendingContent = a
			}, strip: function () {
				this.pendingContent && (this.pendingContent = this.pendingContent.replace(/\s+$/, "")), this.stripNext = "strip"
			}, append: function () {
				this.flushInline();
				var a = this.popStack();
				this.pushSource("if(" + a + " || " + a + " === 0) { " + this.appendToBuffer(a) + " }"), this.environment.isSimple && this.pushSource("else { " + this.appendToBuffer("''") + " }")
			}, appendEscaped: function () {
				this.context.aliases.escapeExpression = "this.escapeExpression", this.pushSource(this.appendToBuffer("escapeExpression(" + this.popStack() + ")"))
			}, getContext: function (a) {
				this.lastContext !== a && (this.lastContext = a)
			}, lookupOnContext: function (a) {
				this.push(this.nameLookup("depth" + this.lastContext, a, "context"))
			}, pushContext: function () {
				this.pushStackLiteral("depth" + this.lastContext)
			}, resolvePossibleLambda: function () {
				this.context.aliases.functionType = '"function"', this.replaceStack(function (a) {
					return "typeof " + a + " === functionType ? " + a + ".apply(depth0) : " + a
				})
			}, lookup: function (a) {
				this.replaceStack(function (b) {
					return b + " == null || " + b + " === false ? " + b + " : " + this.nameLookup(b, a, "context")
				})
			}, lookupData: function () {
				this.pushStackLiteral("data")
			}, pushStringParam: function (a, b) {
				this.pushStackLiteral("depth" + this.lastContext), this.pushString(b), "sexpr" !== b && ("string" == typeof a ? this.pushString(a) : this.pushStackLiteral(a))
			}, emptyHash: function () {
				this.pushStackLiteral("{}"), this.options.stringParams && (this.push("{}"), this.push("{}"))
			}, pushHash: function () {
				this.hash && this.hashes.push(this.hash), this.hash = {values: [], types: [], contexts: []}
			}, popHash: function () {
				var a = this.hash;
				this.hash = this.hashes.pop(), this.options.stringParams && (this.push("{" + a.contexts.join(",") + "}"), this.push("{" + a.types.join(",") + "}")), this.push("{\n    " + a.values.join(",\n    ") + "\n  }")
			}, pushString: function (a) {
				this.pushStackLiteral(this.quotedString(a))
			}, push: function (a) {
				return this.inlineStack.push(a), a
			}, pushLiteral: function (a) {
				this.pushStackLiteral(a)
			}, pushProgram: function (a) {
				this.pushStackLiteral(null != a ? this.programExpression(a) : null)
			}, invokeHelper: function (a, b, c) {
				this.context.aliases.helperMissing = "helpers.helperMissing", this.useRegister("helper");
				var d = this.lastHelper = this.setupHelper(a, b, !0), e = this.nameLookup("depth" + this.lastContext, b, "context"), f = "helper = " + d.name + " || " + e;
				d.paramsInit && (f += "," + d.paramsInit), this.push("(" + f + ",helper ? helper.call(" + d.callParams + ") : helperMissing.call(" + d.helperMissingParams + "))"), c || this.flushInline()
			}, invokeKnownHelper: function (a, b) {
				var c = this.setupHelper(a, b);
				this.push(c.name + ".call(" + c.callParams + ")")
			}, invokeAmbiguous: function (a, b) {
				this.context.aliases.functionType = '"function"', this.useRegister("helper"), this.emptyHash();
				var c = this.setupHelper(0, a, b), d = this.lastHelper = this.nameLookup("helpers", a, "helper"), e = this.nameLookup("depth" + this.lastContext, a, "context"), f = this.nextStack();
				c.paramsInit && this.pushSource(c.paramsInit), this.pushSource("if (helper = " + d + ") { " + f + " = helper.call(" + c.callParams + "); }"), this.pushSource("else { helper = " + e + "; " + f + " = typeof helper === functionType ? helper.call(" + c.callParams + ") : helper; }")
			}, invokePartial: function (a) {
				var b = [this.nameLookup("partials", a, "partial"), "'" + a + "'", this.popStack(), "helpers", "partials"];
				this.options.data && b.push("data"), this.context.aliases.self = "this", this.push("self.invokePartial(" + b.join(", ") + ")")
			}, assignToHash: function (a) {
				var b, c, d = this.popStack();
				this.options.stringParams && (c = this.popStack(), b = this.popStack());
				var e = this.hash;
				b && e.contexts.push("'" + a + "': " + b), c && e.types.push("'" + a + "': " + c), e.values.push("'" + a + "': (" + d + ")")
			}, compiler: d, compileChildren: function (a, b) {
				for (var c, d, e = a.children, f = 0, g = e.length; g > f; f++) {
					c = e[f], d = new this.compiler;
					var h = this.matchExistingProgram(c);
					null == h ? (this.context.programs.push(""), h = this.context.programs.length, c.index = h, c.name = "program" + h, this.context.programs[h] = d.compile(c, b, this.context), this.context.environments[h] = c) : (c.index = h, c.name = "program" + h)
				}
			}, matchExistingProgram: function (a) {
				for (var b = 0, c = this.context.environments.length; c > b; b++) {
					var d = this.context.environments[b];
					if (d && d.equals(a))return b
				}
			}, programExpression: function (a) {
				if (this.context.aliases.self = "this", null == a)return "self.noop";
				for (var b, c = this.environment.children[a], d = c.depths.list, e = [c.index, c.name, "data"], f = 0, g = d.length; g > f; f++)b = d[f], e.push(1 === b ? "depth0" : "depth" + (b - 1));
				return (0 === d.length ? "self.program(" : "self.programWithDepth(") + e.join(", ") + ")"
			}, register: function (a, b) {
				this.useRegister(a), this.pushSource(a + " = " + b + ";")
			}, useRegister: function (a) {
				this.registers[a] || (this.registers[a] = !0, this.registers.list.push(a))
			}, pushStackLiteral: function (a) {
				return this.push(new c(a))
			}, pushSource: function (a) {
				this.pendingContent && (this.source.push(this.appendToBuffer(this.quotedString(this.pendingContent))), this.pendingContent = void 0), a && this.source.push(a)
			}, pushStack: function (a) {
				this.flushInline();
				var b = this.incrStack();
				return a && this.pushSource(b + " = " + a + ";"), this.compileStack.push(b), b
			}, replaceStack: function (a) {
				var b, d, e, f = "", g = this.isInline();
				if (g) {
					var h = this.popStack(!0);
					if (h instanceof c)b = h.value, e = !0; else {
						d = !this.stackSlot;
						var i = d ? this.incrStack() : this.topStackName();
						f = "(" + this.push(i) + " = " + h + "),", b = this.topStack()
					}
				} else b = this.topStack();
				var j = a.call(this, b);
				return g ? (e || this.popStack(), d && this.stackSlot--, this.push("(" + f + j + ")")) : (/^stack/.test(b) || (b = this.nextStack()), this.pushSource(b + " = (" + f + j + ");")), b
			}, nextStack: function () {
				return this.pushStack()
			}, incrStack: function () {
				return this.stackSlot++, this.stackSlot > this.stackVars.length && this.stackVars.push("stack" + this.stackSlot), this.topStackName()
			}, topStackName: function () {
				return "stack" + this.stackSlot
			}, flushInline: function () {
				var a = this.inlineStack;
				if (a.length) {
					this.inlineStack = [];
					for (var b = 0, d = a.length; d > b; b++) {
						var e = a[b];
						e instanceof c ? this.compileStack.push(e) : this.pushStack(e)
					}
				}
			}, isInline: function () {
				return this.inlineStack.length
			}, popStack: function (a) {
				var b = this.isInline(), d = (b ? this.inlineStack : this.compileStack).pop();
				if (!a && d instanceof c)return d.value;
				if (!b) {
					if (!this.stackSlot)throw new i("Invalid stack pop");
					this.stackSlot--
				}
				return d
			}, topStack: function (a) {
				var b = this.isInline() ? this.inlineStack : this.compileStack, d = b[b.length - 1];
				return !a && d instanceof c ? d.value : d
			}, quotedString: function (a) {
				return '"' + a.replace(/\\/g, "\\\\").replace(/"/g, '\\"').replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\u2028/g, "\\u2028").replace(/\u2029/g, "\\u2029") + '"'
			}, setupHelper: function (a, b, c) {
				var d = [], e = this.setupParams(a, d, c), f = this.nameLookup("helpers", b, "helper");
				return {
					params: d,
					paramsInit: e,
					name: f,
					callParams: ["depth0"].concat(d).join(", "),
					helperMissingParams: c && ["depth0", this.quotedString(b)].concat(d).join(", ")
				}
			}, setupOptions: function (a, b) {
				var c, d, e, f = [], g = [], h = [];
				f.push("hash:" + this.popStack()), this.options.stringParams && (f.push("hashTypes:" + this.popStack()), f.push("hashContexts:" + this.popStack())), d = this.popStack(), e = this.popStack(), (e || d) && (e || (this.context.aliases.self = "this", e = "self.noop"), d || (this.context.aliases.self = "this", d = "self.noop"), f.push("inverse:" + d), f.push("fn:" + e));
				for (var i = 0; a > i; i++)c = this.popStack(), b.push(c), this.options.stringParams && (h.push(this.popStack()), g.push(this.popStack()));
				return this.options.stringParams && (f.push("contexts:[" + g.join(",") + "]"), f.push("types:[" + h.join(",") + "]")), this.options.data && f.push("data:data"), f
			}, setupParams: function (a, b, c) {
				var d = "{" + this.setupOptions(a, b).join(",") + "}";
				return c ? (this.useRegister("options"), b.push("options"), "options=" + d) : (b.push(d), "")
			}
		};
		for (var j = "break else new var case finally return void catch for switch while continue function this with default if throw delete in try do instanceof typeof abstract enum int short boolean export interface static byte extends long super char final native synchronized class float package throws const goto private transient debugger implements protected volatile double import public let yield".split(" "), k = d.RESERVED_WORDS = {}, l = 0, m = j.length; m > l; l++)k[j[l]] = !0;
		return d.isValidJavaScriptVariableName = function (a) {
			return !d.RESERVED_WORDS[a] && /^[a-zA-Z_$][0-9a-zA-Z_$]*$/.test(a) ? !0 : !1
		}, e = d
	}(d, c), l = function (a, b, c, d, e) {
		"use strict";
		var f, g = a, h = b, i = c.parser, j = c.parse, k = d.Compiler, l = d.compile, m = d.precompile, n = e, o = g.create, p = function () {
			var a = o();
			return a.compile = function (b, c) {
				return l(b, c, a)
			}, a.precompile = function (b, c) {
				return m(b, c, a)
			}, a.AST = h, a.Compiler = k, a.JavaScriptCompiler = n, a.Parser = i, a.parse = j, a
		};
		return g = p(), g.create = p, f = g
	}(f, g, i, j, k);
	return l
}();
!function (a) {
	"function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
	function b(b, d) {
		var e, f, g, h = b.nodeName.toLowerCase();
		return "area" === h ? (e = b.parentNode, f = e.name, b.href && f && "map" === e.nodeName.toLowerCase() ? (g = a("img[usemap=#" + f + "]")[0], !!g && c(g)) : !1) : (/input|select|textarea|button|object/.test(h) ? !b.disabled : "a" === h ? b.href || d : d) && c(b)
	}

	function c(b) {
		return a.expr.filters.visible(b) && !a(b).parents().addBack().filter(function () {
				return "hidden" === a.css(this, "visibility")
			}).length
	}

	a.ui = a.ui || {}, a.extend(a.ui, {
		version: "1.11.0",
		keyCode: {
			BACKSPACE: 8,
			COMMA: 188,
			DELETE: 46,
			DOWN: 40,
			END: 35,
			ENTER: 13,
			ESCAPE: 27,
			HOME: 36,
			LEFT: 37,
			PAGE_DOWN: 34,
			PAGE_UP: 33,
			PERIOD: 190,
			RIGHT: 39,
			SPACE: 32,
			TAB: 9,
			UP: 38
		}
	}), a.fn.extend({
		scrollParent: function () {
			var b = this.css("position"), c = "absolute" === b, d = this.parents().filter(function () {
				var b = a(this);
				return c && "static" === b.css("position") ? !1 : /(auto|scroll)/.test(b.css("overflow") + b.css("overflow-y") + b.css("overflow-x"))
			}).eq(0);
			return "fixed" !== b && d.length ? d : a(this[0].ownerDocument || document)
		}, uniqueId: function () {
			var a = 0;
			return function () {
				return this.each(function () {
					this.id || (this.id = "ui-id-" + ++a)
				})
			}
		}(), removeUniqueId: function () {
			return this.each(function () {
				/^ui-id-\d+$/.test(this.id) && a(this).removeAttr("id")
			})
		}
	}), a.extend(a.expr[":"], {
		data: a.expr.createPseudo ? a.expr.createPseudo(function (b) {
			return function (c) {
				return !!a.data(c, b)
			}
		}) : function (b, c, d) {
			return !!a.data(b, d[3])
		}, focusable: function (c) {
			return b(c, !isNaN(a.attr(c, "tabindex")))
		}, tabbable: function (c) {
			var d = a.attr(c, "tabindex"), e = isNaN(d);
			return (e || d >= 0) && b(c, !e)
		}
	}), a("<a>").outerWidth(1).jquery || a.each(["Width", "Height"], function (b, c) {
		function d(b, c, d, f) {
			return a.each(e, function () {
				c -= parseFloat(a.css(b, "padding" + this)) || 0, d && (c -= parseFloat(a.css(b, "border" + this + "Width")) || 0), f && (c -= parseFloat(a.css(b, "margin" + this)) || 0)
			}), c
		}

		var e = "Width" === c ? ["Left", "Right"] : ["Top", "Bottom"], f = c.toLowerCase(), g = {
			innerWidth: a.fn.innerWidth,
			innerHeight: a.fn.innerHeight,
			outerWidth: a.fn.outerWidth,
			outerHeight: a.fn.outerHeight
		};
		a.fn["inner" + c] = function (b) {
			return void 0 === b ? g["inner" + c].call(this) : this.each(function () {
				a(this).css(f, d(this, b) + "px")
			})
		}, a.fn["outer" + c] = function (b, e) {
			return "number" != typeof b ? g["outer" + c].call(this, b) : this.each(function () {
				a(this).css(f, d(this, b, !0, e) + "px")
			})
		}
	}), a.fn.addBack || (a.fn.addBack = function (a) {
		return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
	}), a("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (a.fn.removeData = function (b) {
		return function (c) {
			return arguments.length ? b.call(this, a.camelCase(c)) : b.call(this)
		}
	}(a.fn.removeData)), a.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), a.fn.extend({
		focus: function (b) {
			return function (c, d) {
				return "number" == typeof c ? this.each(function () {
					var b = this;
					setTimeout(function () {
						a(b).focus(), d && d.call(b)
					}, c)
				}) : b.apply(this, arguments)
			}
		}(a.fn.focus), disableSelection: function () {
			var a = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
			return function () {
				return this.bind(a + ".ui-disableSelection", function (a) {
					a.preventDefault()
				})
			}
		}(), enableSelection: function () {
			return this.unbind(".ui-disableSelection")
		}, zIndex: function (b) {
			if (void 0 !== b)return this.css("zIndex", b);
			if (this.length)for (var c, d, e = a(this[0]); e.length && e[0] !== document;) {
				if (c = e.css("position"), ("absolute" === c || "relative" === c || "fixed" === c) && (d = parseInt(e.css("zIndex"), 10), !isNaN(d) && 0 !== d))return d;
				e = e.parent()
			}
			return 0
		}
	}), a.ui.plugin = {
		add: function (b, c, d) {
			var e, f = a.ui[b].prototype;
			for (e in d)f.plugins[e] = f.plugins[e] || [], f.plugins[e].push([c, d[e]])
		}, call: function (a, b, c, d) {
			var e, f = a.plugins[b];
			if (f && (d || a.element[0].parentNode && 11 !== a.element[0].parentNode.nodeType))for (e = 0; f.length > e; e++)a.options[f[e][0]] && f[e][1].apply(a.element, c)
		}
	};
	var d = 0, e = Array.prototype.slice;
	a.cleanData = function (b) {
		return function (c) {
			for (var d, e = 0; null != (d = c[e]); e++)try {
				a(d).triggerHandler("remove")
			} catch (f) {
			}
			b(c)
		}
	}(a.cleanData), a.widget = function (b, c, d) {
		var e, f, g, h, i = {}, j = b.split(".")[0];
		return b = b.split(".")[1], e = j + "-" + b, d || (d = c, c = a.Widget), a.expr[":"][e.toLowerCase()] = function (b) {
			return !!a.data(b, e)
		}, a[j] = a[j] || {}, f = a[j][b], g = a[j][b] = function (a, b) {
			return this._createWidget ? void(arguments.length && this._createWidget(a, b)) : new g(a, b)
		}, a.extend(g, f, {
			version: d.version,
			_proto: a.extend({}, d),
			_childConstructors: []
		}), h = new c, h.options = a.widget.extend({}, h.options), a.each(d, function (b, d) {
			return a.isFunction(d) ? void(i[b] = function () {
				var a = function () {
					return c.prototype[b].apply(this, arguments)
				}, e = function (a) {
					return c.prototype[b].apply(this, a)
				};
				return function () {
					var b, c = this._super, f = this._superApply;
					return this._super = a, this._superApply = e, b = d.apply(this, arguments), this._super = c, this._superApply = f, b
				}
			}()) : void(i[b] = d)
		}), g.prototype = a.widget.extend(h, {widgetEventPrefix: f ? h.widgetEventPrefix || b : b}, i, {
			constructor: g,
			namespace: j,
			widgetName: b,
			widgetFullName: e
		}), f ? (a.each(f._childConstructors, function (b, c) {
			var d = c.prototype;
			a.widget(d.namespace + "." + d.widgetName, g, c._proto)
		}), delete f._childConstructors) : c._childConstructors.push(g), a.widget.bridge(b, g), g
	}, a.widget.extend = function (b) {
		for (var c, d, f = e.call(arguments, 1), g = 0, h = f.length; h > g; g++)for (c in f[g])d = f[g][c], f[g].hasOwnProperty(c) && void 0 !== d && (b[c] = a.isPlainObject(d) ? a.isPlainObject(b[c]) ? a.widget.extend({}, b[c], d) : a.widget.extend({}, d) : d);
		return b
	}, a.widget.bridge = function (b, c) {
		var d = c.prototype.widgetFullName || b;
		a.fn[b] = function (f) {
			var g = "string" == typeof f, h = e.call(arguments, 1), i = this;
			return f = !g && h.length ? a.widget.extend.apply(null, [f].concat(h)) : f, this.each(g ? function () {
				var c, e = a.data(this, d);
				return "instance" === f ? (i = e, !1) : e ? a.isFunction(e[f]) && "_" !== f.charAt(0) ? (c = e[f].apply(e, h), c !== e && void 0 !== c ? (i = c && c.jquery ? i.pushStack(c.get()) : c, !1) : void 0) : a.error("no such method '" + f + "' for " + b + " widget instance") : a.error("cannot call methods on " + b + " prior to initialization; attempted to call method '" + f + "'")
			} : function () {
				var b = a.data(this, d);
				b ? (b.option(f || {}), b._init && b._init()) : a.data(this, d, new c(f, this))
			}), i
		}
	}, a.Widget = function () {
	}, a.Widget._childConstructors = [], a.Widget.prototype = {
		widgetName: "widget",
		widgetEventPrefix: "",
		defaultElement: "<div>",
		options: {disabled: !1, create: null},
		_createWidget: function (b, c) {
			c = a(c || this.defaultElement || this)[0], this.element = a(c), this.uuid = d++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = a.widget.extend({}, this.options, this._getCreateOptions(), b), this.bindings = a(), this.hoverable = a(), this.focusable = a(), c !== this && (a.data(c, this.widgetFullName, this), this._on(!0, this.element, {
				remove: function (a) {
					a.target === c && this.destroy()
				}
			}), this.document = a(c.style ? c.ownerDocument : c.document || c), this.window = a(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
		},
		_getCreateOptions: a.noop,
		_getCreateEventData: a.noop,
		_create: a.noop,
		_init: a.noop,
		destroy: function () {
			this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(a.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
		},
		_destroy: a.noop,
		widget: function () {
			return this.element
		},
		option: function (b, c) {
			var d, e, f, g = b;
			if (0 === arguments.length)return a.widget.extend({}, this.options);
			if ("string" == typeof b)if (g = {}, d = b.split("."), b = d.shift(), d.length) {
				for (e = g[b] = a.widget.extend({}, this.options[b]), f = 0; d.length - 1 > f; f++)e[d[f]] = e[d[f]] || {}, e = e[d[f]];
				if (b = d.pop(), 1 === arguments.length)return void 0 === e[b] ? null : e[b];
				e[b] = c
			} else {
				if (1 === arguments.length)return void 0 === this.options[b] ? null : this.options[b];
				g[b] = c
			}
			return this._setOptions(g), this
		},
		_setOptions: function (a) {
			var b;
			for (b in a)this._setOption(b, a[b]);
			return this
		},
		_setOption: function (a, b) {
			return this.options[a] = b, "disabled" === a && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!b), b && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this
		},
		enable: function () {
			return this._setOptions({disabled: !1})
		},
		disable: function () {
			return this._setOptions({disabled: !0})
		},
		_on: function (b, c, d) {
			var e, f = this;
			"boolean" != typeof b && (d = c, c = b, b = !1), d ? (c = e = a(c), this.bindings = this.bindings.add(c)) : (d = c, c = this.element, e = this.widget()), a.each(d, function (d, g) {
				function h() {
					return b || f.options.disabled !== !0 && !a(this).hasClass("ui-state-disabled") ? ("string" == typeof g ? f[g] : g).apply(f, arguments) : void 0
				}

				"string" != typeof g && (h.guid = g.guid = g.guid || h.guid || a.guid++);
				var i = d.match(/^([\w:-]*)\s*(.*)$/), j = i[1] + f.eventNamespace, k = i[2];
				k ? e.delegate(k, j, h) : c.bind(j, h)
			})
		},
		_off: function (a, b) {
			b = (b || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, a.unbind(b).undelegate(b)
		},
		_delay: function (a, b) {
			function c() {
				return ("string" == typeof a ? d[a] : a).apply(d, arguments)
			}

			var d = this;
			return setTimeout(c, b || 0)
		},
		_hoverable: function (b) {
			this.hoverable = this.hoverable.add(b), this._on(b, {
				mouseenter: function (b) {
					a(b.currentTarget).addClass("ui-state-hover")
				}, mouseleave: function (b) {
					a(b.currentTarget).removeClass("ui-state-hover")
				}
			})
		},
		_focusable: function (b) {
			this.focusable = this.focusable.add(b), this._on(b, {
				focusin: function (b) {
					a(b.currentTarget).addClass("ui-state-focus")
				}, focusout: function (b) {
					a(b.currentTarget).removeClass("ui-state-focus")
				}
			})
		},
		_trigger: function (b, c, d) {
			var e, f, g = this.options[b];
			if (d = d || {}, c = a.Event(c), c.type = (b === this.widgetEventPrefix ? b : this.widgetEventPrefix + b).toLowerCase(), c.target = this.element[0], f = c.originalEvent)for (e in f)e in c || (c[e] = f[e]);
			return this.element.trigger(c, d), !(a.isFunction(g) && g.apply(this.element[0], [c].concat(d)) === !1 || c.isDefaultPrevented())
		}
	}, a.each({show: "fadeIn", hide: "fadeOut"}, function (b, c) {
		a.Widget.prototype["_" + b] = function (d, e, f) {
			"string" == typeof e && (e = {effect: e});
			var g, h = e ? e === !0 || "number" == typeof e ? c : e.effect || c : b;
			e = e || {}, "number" == typeof e && (e = {duration: e}), g = !a.isEmptyObject(e), e.complete = f, e.delay && d.delay(e.delay), g && a.effects && a.effects.effect[h] ? d[b](e) : h !== b && d[h] ? d[h](e.duration, e.easing, f) : d.queue(function (c) {
				a(this)[b](), f && f.call(d[0]), c()
			})
		}
	}), a.widget;
	var f = !1;
	a(document).mouseup(function () {
		f = !1
	}), a.widget("ui.mouse", {
		version: "1.11.0",
		options: {cancel: "input,textarea,button,select,option", distance: 1, delay: 0},
		_mouseInit: function () {
			var b = this;
			this.element.bind("mousedown." + this.widgetName, function (a) {
				return b._mouseDown(a)
			}).bind("click." + this.widgetName, function (c) {
				return !0 === a.data(c.target, b.widgetName + ".preventClickEvent") ? (a.removeData(c.target, b.widgetName + ".preventClickEvent"), c.stopImmediatePropagation(), !1) : void 0
			}), this.started = !1
		},
		_mouseDestroy: function () {
			this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
		},
		_mouseDown: function (b) {
			if (!f) {
				this._mouseStarted && this._mouseUp(b), this._mouseDownEvent = b;
				var c = this, d = 1 === b.which, e = "string" == typeof this.options.cancel && b.target.nodeName ? a(b.target).closest(this.options.cancel).length : !1;
				return d && !e && this._mouseCapture(b) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () {
					c.mouseDelayMet = !0
				}, this.options.delay)), this._mouseDistanceMet(b) && this._mouseDelayMet(b) && (this._mouseStarted = this._mouseStart(b) !== !1, !this._mouseStarted) ? (b.preventDefault(), !0) : (!0 === a.data(b.target, this.widgetName + ".preventClickEvent") && a.removeData(b.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (a) {
					return c._mouseMove(a)
				}, this._mouseUpDelegate = function (a) {
					return c._mouseUp(a)
				}, this.document.bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), b.preventDefault(), f = !0, !0)) : !0
			}
		},
		_mouseMove: function (b) {
			return a.ui.ie && (!document.documentMode || 9 > document.documentMode) && !b.button ? this._mouseUp(b) : b.which ? this._mouseStarted ? (this._mouseDrag(b), b.preventDefault()) : (this._mouseDistanceMet(b) && this._mouseDelayMet(b) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, b) !== !1, this._mouseStarted ? this._mouseDrag(b) : this._mouseUp(b)), !this._mouseStarted) : this._mouseUp(b)
		},
		_mouseUp: function (b) {
			return this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, b.target === this._mouseDownEvent.target && a.data(b.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(b)), f = !1, !1
		},
		_mouseDistanceMet: function (a) {
			return Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance
		},
		_mouseDelayMet: function () {
			return this.mouseDelayMet
		},
		_mouseStart: function () {
		},
		_mouseDrag: function () {
		},
		_mouseStop: function () {
		},
		_mouseCapture: function () {
			return !0
		}
	}), function () {
		function b(a, b, c) {
			return [parseFloat(a[0]) * (n.test(a[0]) ? b / 100 : 1), parseFloat(a[1]) * (n.test(a[1]) ? c / 100 : 1)]
		}

		function c(b, c) {
			return parseInt(a.css(b, c), 10) || 0
		}

		function d(b) {
			var c = b[0];
			return 9 === c.nodeType ? {
				width: b.width(),
				height: b.height(),
				offset: {top: 0, left: 0}
			} : a.isWindow(c) ? {
				width: b.width(),
				height: b.height(),
				offset: {top: b.scrollTop(), left: b.scrollLeft()}
			} : c.preventDefault ? {
				width: 0,
				height: 0,
				offset: {top: c.pageY, left: c.pageX}
			} : {width: b.outerWidth(), height: b.outerHeight(), offset: b.offset()}
		}

		a.ui = a.ui || {};
		var e, f, g = Math.max, h = Math.abs, i = Math.round, j = /left|center|right/, k = /top|center|bottom/, l = /[\+\-]\d+(\.[\d]+)?%?/, m = /^\w+/, n = /%$/, o = a.fn.position;
		a.position = {
			scrollbarWidth: function () {
				if (void 0 !== e)return e;
				var b, c, d = a("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"), f = d.children()[0];
				return a("body").append(d), b = f.offsetWidth, d.css("overflow", "scroll"), c = f.offsetWidth, b === c && (c = d[0].clientWidth), d.remove(), e = b - c
			}, getScrollInfo: function (b) {
				var c = b.isWindow || b.isDocument ? "" : b.element.css("overflow-x"), d = b.isWindow || b.isDocument ? "" : b.element.css("overflow-y"), e = "scroll" === c || "auto" === c && b.width < b.element[0].scrollWidth, f = "scroll" === d || "auto" === d && b.height < b.element[0].scrollHeight;
				return {width: f ? a.position.scrollbarWidth() : 0, height: e ? a.position.scrollbarWidth() : 0}
			}, getWithinInfo: function (b) {
				var c = a(b || window), d = a.isWindow(c[0]), e = !!c[0] && 9 === c[0].nodeType;
				return {
					element: c,
					isWindow: d,
					isDocument: e,
					offset: c.offset() || {left: 0, top: 0},
					scrollLeft: c.scrollLeft(),
					scrollTop: c.scrollTop(),
					width: d ? c.width() : c.outerWidth(),
					height: d ? c.height() : c.outerHeight()
				}
			}
		}, a.fn.position = function (e) {
			if (!e || !e.of)return o.apply(this, arguments);
			e = a.extend({}, e);
			var n, p, q, r, s, t, u = a(e.of), v = a.position.getWithinInfo(e.within), w = a.position.getScrollInfo(v), x = (e.collision || "flip").split(" "), y = {};
			return t = d(u), u[0].preventDefault && (e.at = "left top"), p = t.width, q = t.height, r = t.offset, s = a.extend({}, r), a.each(["my", "at"], function () {
				var a, b, c = (e[this] || "").split(" ");
				1 === c.length && (c = j.test(c[0]) ? c.concat(["center"]) : k.test(c[0]) ? ["center"].concat(c) : ["center", "center"]), c[0] = j.test(c[0]) ? c[0] : "center", c[1] = k.test(c[1]) ? c[1] : "center", a = l.exec(c[0]), b = l.exec(c[1]), y[this] = [a ? a[0] : 0, b ? b[0] : 0], e[this] = [m.exec(c[0])[0], m.exec(c[1])[0]]
			}), 1 === x.length && (x[1] = x[0]), "right" === e.at[0] ? s.left += p : "center" === e.at[0] && (s.left += p / 2), "bottom" === e.at[1] ? s.top += q : "center" === e.at[1] && (s.top += q / 2), n = b(y.at, p, q), s.left += n[0], s.top += n[1], this.each(function () {
				var d, j, k = a(this), l = k.outerWidth(), m = k.outerHeight(), o = c(this, "marginLeft"), t = c(this, "marginTop"), z = l + o + c(this, "marginRight") + w.width, A = m + t + c(this, "marginBottom") + w.height, B = a.extend({}, s), C = b(y.my, k.outerWidth(), k.outerHeight());
				"right" === e.my[0] ? B.left -= l : "center" === e.my[0] && (B.left -= l / 2), "bottom" === e.my[1] ? B.top -= m : "center" === e.my[1] && (B.top -= m / 2), B.left += C[0], B.top += C[1], f || (B.left = i(B.left), B.top = i(B.top)), d = {
					marginLeft: o,
					marginTop: t
				}, a.each(["left", "top"], function (b, c) {
					a.ui.position[x[b]] && a.ui.position[x[b]][c](B, {
						targetWidth: p,
						targetHeight: q,
						elemWidth: l,
						elemHeight: m,
						collisionPosition: d,
						collisionWidth: z,
						collisionHeight: A,
						offset: [n[0] + C[0], n[1] + C[1]],
						my: e.my,
						at: e.at,
						within: v,
						elem: k
					})
				}), e.using && (j = function (a) {
					var b = r.left - B.left, c = b + p - l, d = r.top - B.top, f = d + q - m, i = {
						target: {
							element: u,
							left: r.left,
							top: r.top,
							width: p,
							height: q
						},
						element: {element: k, left: B.left, top: B.top, width: l, height: m},
						horizontal: 0 > c ? "left" : b > 0 ? "right" : "center",
						vertical: 0 > f ? "top" : d > 0 ? "bottom" : "middle"
					};
					l > p && p > h(b + c) && (i.horizontal = "center"), m > q && q > h(d + f) && (i.vertical = "middle"), i.important = g(h(b), h(c)) > g(h(d), h(f)) ? "horizontal" : "vertical", e.using.call(this, a, i)
				}), k.offset(a.extend(B, {using: j}))
			})
		}, a.ui.position = {
			fit: {
				left: function (a, b) {
					var c, d = b.within, e = d.isWindow ? d.scrollLeft : d.offset.left, f = d.width, h = a.left - b.collisionPosition.marginLeft, i = e - h, j = h + b.collisionWidth - f - e;
					b.collisionWidth > f ? i > 0 && 0 >= j ? (c = a.left + i + b.collisionWidth - f - e, a.left += i - c) : a.left = j > 0 && 0 >= i ? e : i > j ? e + f - b.collisionWidth : e : i > 0 ? a.left += i : j > 0 ? a.left -= j : a.left = g(a.left - h, a.left)
				}, top: function (a, b) {
					var c, d = b.within, e = d.isWindow ? d.scrollTop : d.offset.top, f = b.within.height, h = a.top - b.collisionPosition.marginTop, i = e - h, j = h + b.collisionHeight - f - e;
					b.collisionHeight > f ? i > 0 && 0 >= j ? (c = a.top + i + b.collisionHeight - f - e, a.top += i - c) : a.top = j > 0 && 0 >= i ? e : i > j ? e + f - b.collisionHeight : e : i > 0 ? a.top += i : j > 0 ? a.top -= j : a.top = g(a.top - h, a.top)
				}
			}, flip: {
				left: function (a, b) {
					var c, d, e = b.within, f = e.offset.left + e.scrollLeft, g = e.width, i = e.isWindow ? e.scrollLeft : e.offset.left, j = a.left - b.collisionPosition.marginLeft, k = j - i, l = j + b.collisionWidth - g - i, m = "left" === b.my[0] ? -b.elemWidth : "right" === b.my[0] ? b.elemWidth : 0, n = "left" === b.at[0] ? b.targetWidth : "right" === b.at[0] ? -b.targetWidth : 0, o = -2 * b.offset[0];
					0 > k ? (c = a.left + m + n + o + b.collisionWidth - g - f, (0 > c || h(k) > c) && (a.left += m + n + o)) : l > 0 && (d = a.left - b.collisionPosition.marginLeft + m + n + o - i, (d > 0 || l > h(d)) && (a.left += m + n + o))
				}, top: function (a, b) {
					var c, d, e = b.within, f = e.offset.top + e.scrollTop, g = e.height, i = e.isWindow ? e.scrollTop : e.offset.top, j = a.top - b.collisionPosition.marginTop, k = j - i, l = j + b.collisionHeight - g - i, m = "top" === b.my[1], n = m ? -b.elemHeight : "bottom" === b.my[1] ? b.elemHeight : 0, o = "top" === b.at[1] ? b.targetHeight : "bottom" === b.at[1] ? -b.targetHeight : 0, p = -2 * b.offset[1];
					0 > k ? (d = a.top + n + o + p + b.collisionHeight - g - f, a.top + n + o + p > k && (0 > d || h(k) > d) && (a.top += n + o + p)) : l > 0 && (c = a.top - b.collisionPosition.marginTop + n + o + p - i, a.top + n + o + p > l && (c > 0 || l > h(c)) && (a.top += n + o + p))
				}
			}, flipfit: {
				left: function () {
					a.ui.position.flip.left.apply(this, arguments), a.ui.position.fit.left.apply(this, arguments)
				}, top: function () {
					a.ui.position.flip.top.apply(this, arguments), a.ui.position.fit.top.apply(this, arguments)
				}
			}
		}, function () {
			var b, c, d, e, g, h = document.getElementsByTagName("body")[0], i = document.createElement("div");
			b = document.createElement(h ? "div" : "body"), d = {
				visibility: "hidden",
				width: 0,
				height: 0,
				border: 0,
				margin: 0,
				background: "none"
			}, h && a.extend(d, {position: "absolute", left: "-1000px", top: "-1000px"});
			for (g in d)b.style[g] = d[g];
			b.appendChild(i), c = h || document.documentElement, c.insertBefore(b, c.firstChild), i.style.cssText = "position: absolute; left: 10.7432222px;", e = a(i).offset().left, f = e > 10 && 11 > e, b.innerHTML = "", c.removeChild(b)
		}()
	}(), a.ui.position, a.widget("ui.draggable", a.ui.mouse, {
		version: "1.11.0",
		widgetEventPrefix: "drag",
		options: {
			addClasses: !0,
			appendTo: "parent",
			axis: !1,
			connectToSortable: !1,
			containment: !1,
			cursor: "auto",
			cursorAt: !1,
			grid: !1,
			handle: !1,
			helper: "original",
			iframeFix: !1,
			opacity: !1,
			refreshPositions: !1,
			revert: !1,
			revertDuration: 500,
			scope: "default",
			scroll: !0,
			scrollSensitivity: 20,
			scrollSpeed: 20,
			snap: !1,
			snapMode: "both",
			snapTolerance: 20,
			stack: !1,
			zIndex: !1,
			drag: null,
			start: null,
			stop: null
		},
		_create: function () {
			"original" !== this.options.helper || /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative"), this.options.addClasses && this.element.addClass("ui-draggable"), this.options.disabled && this.element.addClass("ui-draggable-disabled"), this._setHandleClassName(), this._mouseInit()
		},
		_setOption: function (a, b) {
			this._super(a, b), "handle" === a && this._setHandleClassName()
		},
		_destroy: function () {
			return (this.helper || this.element).is(".ui-draggable-dragging") ? void(this.destroyOnClear = !0) : (this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"), this._removeHandleClassName(), void this._mouseDestroy())
		},
		_mouseCapture: function (b) {
			var c = this.document[0], d = this.options;
			try {
				c.activeElement && "body" !== c.activeElement.nodeName.toLowerCase() && a(c.activeElement).blur()
			} catch (e) {
			}
			return this.helper || d.disabled || a(b.target).closest(".ui-resizable-handle").length > 0 ? !1 : (this.handle = this._getHandle(b), this.handle ? (a(d.iframeFix === !0 ? "iframe" : d.iframeFix).each(function () {
				a("<div class='ui-draggable-iframeFix' style='background: #ffffff;'></div>").css({
					width: this.offsetWidth + "px",
					height: this.offsetHeight + "px",
					position: "absolute",
					opacity: "0.001",
					zIndex: 1e3
				}).css(a(this).offset()).appendTo("body")
			}), !0) : !1)
		},
		_mouseStart: function (b) {
			var c = this.options;
			return this.helper = this._createHelper(b), this.helper.addClass("ui-draggable-dragging"), this._cacheHelperProportions(), a.ui.ddmanager && (a.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(), this.offsetParent = this.helper.offsetParent(), this.offsetParentCssPosition = this.offsetParent.css("position"), this.offset = this.positionAbs = this.element.offset(), this.offset = {
				top: this.offset.top - this.margins.top,
				left: this.offset.left - this.margins.left
			}, this.offset.scroll = !1, a.extend(this.offset, {
				click: {
					left: b.pageX - this.offset.left,
					top: b.pageY - this.offset.top
				}, parent: this._getParentOffset(), relative: this._getRelativeOffset()
			}), this.originalPosition = this.position = this._generatePosition(b, !1), this.originalPageX = b.pageX, this.originalPageY = b.pageY, c.cursorAt && this._adjustOffsetFromHelper(c.cursorAt), this._setContainment(), this._trigger("start", b) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), a.ui.ddmanager && !c.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b), this._mouseDrag(b, !0), a.ui.ddmanager && a.ui.ddmanager.dragStart(this, b), !0)
		},
		_mouseDrag: function (b, c) {
			if ("fixed" === this.offsetParentCssPosition && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(b, !0), this.positionAbs = this._convertPositionTo("absolute"), !c) {
				var d = this._uiHash();
				if (this._trigger("drag", b, d) === !1)return this._mouseUp({}), !1;
				this.position = d.position
			}
			return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", a.ui.ddmanager && a.ui.ddmanager.drag(this, b), !1
		},
		_mouseStop: function (b) {
			var c = this, d = !1;
			return a.ui.ddmanager && !this.options.dropBehaviour && (d = a.ui.ddmanager.drop(this, b)), this.dropped && (d = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !d || "valid" === this.options.revert && d || this.options.revert === !0 || a.isFunction(this.options.revert) && this.options.revert.call(this.element, d) ? a(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () {
				c._trigger("stop", b) !== !1 && c._clear()
			}) : this._trigger("stop", b) !== !1 && this._clear(), !1
		},
		_mouseUp: function (b) {
			return a("div.ui-draggable-iframeFix").each(function () {
				this.parentNode.removeChild(this)
			}), a.ui.ddmanager && a.ui.ddmanager.dragStop(this, b), this.element.focus(), a.ui.mouse.prototype._mouseUp.call(this, b)
		},
		cancel: function () {
			return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), this
		},
		_getHandle: function (b) {
			return this.options.handle ? !!a(b.target).closest(this.element.find(this.options.handle)).length : !0
		},
		_setHandleClassName: function () {
			this._removeHandleClassName(), a(this.options.handle || this.element).addClass("ui-draggable-handle")
		},
		_removeHandleClassName: function () {
			this.element.find(".ui-draggable-handle").addBack().removeClass("ui-draggable-handle")
		},
		_createHelper: function (b) {
			var c = this.options, d = a.isFunction(c.helper) ? a(c.helper.apply(this.element[0], [b])) : "clone" === c.helper ? this.element.clone().removeAttr("id") : this.element;
			return d.parents("body").length || d.appendTo("parent" === c.appendTo ? this.element[0].parentNode : c.appendTo), d[0] === this.element[0] || /(fixed|absolute)/.test(d.css("position")) || d.css("position", "absolute"), d
		},
		_adjustOffsetFromHelper: function (b) {
			"string" == typeof b && (b = b.split(" ")), a.isArray(b) && (b = {
				left: +b[0],
				top: +b[1] || 0
			}), "left" in b && (this.offset.click.left = b.left + this.margins.left), "right" in b && (this.offset.click.left = this.helperProportions.width - b.right + this.margins.left), "top" in b && (this.offset.click.top = b.top + this.margins.top), "bottom" in b && (this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top)
		},
		_isRootNode: function (a) {
			return /(html|body)/i.test(a.tagName) || a === this.document[0]
		},
		_getParentOffset: function () {
			var b = this.offsetParent.offset(), c = this.document[0];
			return "absolute" === this.cssPosition && this.scrollParent[0] !== c && a.contains(this.scrollParent[0], this.offsetParent[0]) && (b.left += this.scrollParent.scrollLeft(), b.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (b = {
				top: 0,
				left: 0
			}), {
				top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
				left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
			}
		},
		_getRelativeOffset: function () {
			if ("relative" !== this.cssPosition)return {top: 0, left: 0};
			var a = this.element.position(), b = this._isRootNode(this.scrollParent[0]);
			return {
				top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + (b ? 0 : this.scrollParent.scrollTop()),
				left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + (b ? 0 : this.scrollParent.scrollLeft())
			}
		},
		_cacheMargins: function () {
			this.margins = {
				left: parseInt(this.element.css("marginLeft"), 10) || 0,
				top: parseInt(this.element.css("marginTop"), 10) || 0,
				right: parseInt(this.element.css("marginRight"), 10) || 0,
				bottom: parseInt(this.element.css("marginBottom"), 10) || 0
			}
		},
		_cacheHelperProportions: function () {
			this.helperProportions = {width: this.helper.outerWidth(), height: this.helper.outerHeight()}
		},
		_setContainment: function () {
			var b, c, d, e = this.options, f = this.document[0];
			return this.relative_container = null, e.containment ? "window" === e.containment ? void(this.containment = [a(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, a(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, a(window).scrollLeft() + a(window).width() - this.helperProportions.width - this.margins.left, a(window).scrollTop() + (a(window).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === e.containment ? void(this.containment = [0, 0, a(f).width() - this.helperProportions.width - this.margins.left, (a(f).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : e.containment.constructor === Array ? void(this.containment = e.containment) : ("parent" === e.containment && (e.containment = this.helper[0].parentNode), c = a(e.containment), d = c[0], void(d && (b = "hidden" !== c.css("overflow"), this.containment = [(parseInt(c.css("borderLeftWidth"), 10) || 0) + (parseInt(c.css("paddingLeft"), 10) || 0), (parseInt(c.css("borderTopWidth"), 10) || 0) + (parseInt(c.css("paddingTop"), 10) || 0), (b ? Math.max(d.scrollWidth, d.offsetWidth) : d.offsetWidth) - (parseInt(c.css("borderRightWidth"), 10) || 0) - (parseInt(c.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (b ? Math.max(d.scrollHeight, d.offsetHeight) : d.offsetHeight) - (parseInt(c.css("borderBottomWidth"), 10) || 0) - (parseInt(c.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relative_container = c))) : void(this.containment = null)
		},
		_convertPositionTo: function (a, b) {
			b || (b = this.position);
			var c = "absolute" === a ? 1 : -1, d = this._isRootNode(this.scrollParent[0]);
			return {
				top: b.top + this.offset.relative.top * c + this.offset.parent.top * c - ("fixed" === this.cssPosition ? -this.offset.scroll.top : d ? 0 : this.offset.scroll.top) * c,
				left: b.left + this.offset.relative.left * c + this.offset.parent.left * c - ("fixed" === this.cssPosition ? -this.offset.scroll.left : d ? 0 : this.offset.scroll.left) * c
			}
		},
		_generatePosition: function (a, b) {
			var c, d, e, f, g = this.options, h = this._isRootNode(this.scrollParent[0]), i = a.pageX, j = a.pageY;
			return h && this.offset.scroll || (this.offset.scroll = {
				top: this.scrollParent.scrollTop(),
				left: this.scrollParent.scrollLeft()
			}), b && (this.containment && (this.relative_container ? (d = this.relative_container.offset(), c = [this.containment[0] + d.left, this.containment[1] + d.top, this.containment[2] + d.left, this.containment[3] + d.top]) : c = this.containment, a.pageX - this.offset.click.left < c[0] && (i = c[0] + this.offset.click.left), a.pageY - this.offset.click.top < c[1] && (j = c[1] + this.offset.click.top), a.pageX - this.offset.click.left > c[2] && (i = c[2] + this.offset.click.left), a.pageY - this.offset.click.top > c[3] && (j = c[3] + this.offset.click.top)), g.grid && (e = g.grid[1] ? this.originalPageY + Math.round((j - this.originalPageY) / g.grid[1]) * g.grid[1] : this.originalPageY, j = c ? e - this.offset.click.top >= c[1] || e - this.offset.click.top > c[3] ? e : e - this.offset.click.top >= c[1] ? e - g.grid[1] : e + g.grid[1] : e, f = g.grid[0] ? this.originalPageX + Math.round((i - this.originalPageX) / g.grid[0]) * g.grid[0] : this.originalPageX, i = c ? f - this.offset.click.left >= c[0] || f - this.offset.click.left > c[2] ? f : f - this.offset.click.left >= c[0] ? f - g.grid[0] : f + g.grid[0] : f), "y" === g.axis && (i = this.originalPageX), "x" === g.axis && (j = this.originalPageY)), {
				top: j - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : h ? 0 : this.offset.scroll.top),
				left: i - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : h ? 0 : this.offset.scroll.left)
			}
		},
		_clear: function () {
			this.helper.removeClass("ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy()
		},
		_trigger: function (b, c, d) {
			return d = d || this._uiHash(), a.ui.plugin.call(this, b, [c, d, this], !0), "drag" === b && (this.positionAbs = this._convertPositionTo("absolute")), a.Widget.prototype._trigger.call(this, b, c, d)
		},
		plugins: {},
		_uiHash: function () {
			return {
				helper: this.helper,
				position: this.position,
				originalPosition: this.originalPosition,
				offset: this.positionAbs
			}
		}
	}), a.ui.plugin.add("draggable", "connectToSortable", {
		start: function (b, c, d) {
			var e = d.options, f = a.extend({}, c, {item: d.element});
			d.sortables = [], a(e.connectToSortable).each(function () {
				var c = a(this).sortable("instance");
				c && !c.options.disabled && (d.sortables.push({
					instance: c,
					shouldRevert: c.options.revert
				}), c.refreshPositions(), c._trigger("activate", b, f))
			})
		}, stop: function (b, c, d) {
			var e = a.extend({}, c, {item: d.element});
			a.each(d.sortables, function () {
				this.instance.isOver ? (this.instance.isOver = 0, d.cancelHelperRemoval = !0, this.instance.cancelHelperRemoval = !1, this.shouldRevert && (this.instance.options.revert = this.shouldRevert), this.instance._mouseStop(b), this.instance.options.helper = this.instance.options._helper, "original" === d.options.helper && this.instance.currentItem.css({
					top: "auto",
					left: "auto"
				})) : (this.instance.cancelHelperRemoval = !1, this.instance._trigger("deactivate", b, e))
			})
		}, drag: function (b, c, d) {
			var e = this;
			a.each(d.sortables, function () {
				var f = !1, g = this;
				this.instance.positionAbs = d.positionAbs, this.instance.helperProportions = d.helperProportions, this.instance.offset.click = d.offset.click, this.instance._intersectsWith(this.instance.containerCache) && (f = !0, a.each(d.sortables, function () {
					return this.instance.positionAbs = d.positionAbs, this.instance.helperProportions = d.helperProportions, this.instance.offset.click = d.offset.click, this !== g && this.instance._intersectsWith(this.instance.containerCache) && a.contains(g.instance.element[0], this.instance.element[0]) && (f = !1), f
				})), f ? (this.instance.isOver || (this.instance.isOver = 1, this.instance.currentItem = a(e).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item", !0), this.instance.options._helper = this.instance.options.helper, this.instance.options.helper = function () {
					return c.helper[0]
				}, b.target = this.instance.currentItem[0], this.instance._mouseCapture(b, !0), this.instance._mouseStart(b, !0, !0), this.instance.offset.click.top = d.offset.click.top, this.instance.offset.click.left = d.offset.click.left, this.instance.offset.parent.left -= d.offset.parent.left - this.instance.offset.parent.left, this.instance.offset.parent.top -= d.offset.parent.top - this.instance.offset.parent.top, d._trigger("toSortable", b), d.dropped = this.instance.element, d.currentItem = d.element, this.instance.fromOutside = d), this.instance.currentItem && this.instance._mouseDrag(b)) : this.instance.isOver && (this.instance.isOver = 0, this.instance.cancelHelperRemoval = !0, this.instance.options.revert = !1, this.instance._trigger("out", b, this.instance._uiHash(this.instance)), this.instance._mouseStop(b, !0), this.instance.options.helper = this.instance.options._helper, this.instance.currentItem.remove(), this.instance.placeholder && this.instance.placeholder.remove(), d._trigger("fromSortable", b), d.dropped = !1)
			})
		}
	}), a.ui.plugin.add("draggable", "cursor", {
		start: function (b, c, d) {
			var e = a("body"), f = d.options;
			e.css("cursor") && (f._cursor = e.css("cursor")), e.css("cursor", f.cursor)
		}, stop: function (b, c, d) {
			var e = d.options;
			e._cursor && a("body").css("cursor", e._cursor)
		}
	}), a.ui.plugin.add("draggable", "opacity", {
		start: function (b, c, d) {
			var e = a(c.helper), f = d.options;
			e.css("opacity") && (f._opacity = e.css("opacity")), e.css("opacity", f.opacity)
		}, stop: function (b, c, d) {
			var e = d.options;
			e._opacity && a(c.helper).css("opacity", e._opacity)
		}
	}), a.ui.plugin.add("draggable", "scroll", {
		start: function (a, b, c) {
			c.scrollParent[0] !== c.document[0] && "HTML" !== c.scrollParent[0].tagName && (c.overflowOffset = c.scrollParent.offset())
		}, drag: function (b, c, d) {
			var e = d.options, f = !1, g = d.document[0];
			d.scrollParent[0] !== g && "HTML" !== d.scrollParent[0].tagName ? (e.axis && "x" === e.axis || (d.overflowOffset.top + d.scrollParent[0].offsetHeight - b.pageY < e.scrollSensitivity ? d.scrollParent[0].scrollTop = f = d.scrollParent[0].scrollTop + e.scrollSpeed : b.pageY - d.overflowOffset.top < e.scrollSensitivity && (d.scrollParent[0].scrollTop = f = d.scrollParent[0].scrollTop - e.scrollSpeed)), e.axis && "y" === e.axis || (d.overflowOffset.left + d.scrollParent[0].offsetWidth - b.pageX < e.scrollSensitivity ? d.scrollParent[0].scrollLeft = f = d.scrollParent[0].scrollLeft + e.scrollSpeed : b.pageX - d.overflowOffset.left < e.scrollSensitivity && (d.scrollParent[0].scrollLeft = f = d.scrollParent[0].scrollLeft - e.scrollSpeed))) : (e.axis && "x" === e.axis || (b.pageY - a(g).scrollTop() < e.scrollSensitivity ? f = a(g).scrollTop(a(g).scrollTop() - e.scrollSpeed) : a(window).height() - (b.pageY - a(g).scrollTop()) < e.scrollSensitivity && (f = a(g).scrollTop(a(g).scrollTop() + e.scrollSpeed))), e.axis && "y" === e.axis || (b.pageX - a(g).scrollLeft() < e.scrollSensitivity ? f = a(g).scrollLeft(a(g).scrollLeft() - e.scrollSpeed) : a(window).width() - (b.pageX - a(g).scrollLeft()) < e.scrollSensitivity && (f = a(g).scrollLeft(a(g).scrollLeft() + e.scrollSpeed)))), f !== !1 && a.ui.ddmanager && !e.dropBehaviour && a.ui.ddmanager.prepareOffsets(d, b)
		}
	}), a.ui.plugin.add("draggable", "snap", {
		start: function (b, c, d) {
			var e = d.options;
			d.snapElements = [], a(e.snap.constructor !== String ? e.snap.items || ":data(ui-draggable)" : e.snap).each(function () {
				var b = a(this), c = b.offset();
				this !== d.element[0] && d.snapElements.push({
					item: this,
					width: b.outerWidth(),
					height: b.outerHeight(),
					top: c.top,
					left: c.left
				})
			})
		}, drag: function (b, c, d) {
			var e, f, g, h, i, j, k, l, m, n, o = d.options, p = o.snapTolerance, q = c.offset.left, r = q + d.helperProportions.width, s = c.offset.top, t = s + d.helperProportions.height;
			for (m = d.snapElements.length - 1; m >= 0; m--)i = d.snapElements[m].left, j = i + d.snapElements[m].width, k = d.snapElements[m].top, l = k + d.snapElements[m].height, i - p > r || q > j + p || k - p > t || s > l + p || !a.contains(d.snapElements[m].item.ownerDocument, d.snapElements[m].item) ? (d.snapElements[m].snapping && d.options.snap.release && d.options.snap.release.call(d.element, b, a.extend(d._uiHash(), {snapItem: d.snapElements[m].item})), d.snapElements[m].snapping = !1) : ("inner" !== o.snapMode && (e = p >= Math.abs(k - t), f = p >= Math.abs(l - s), g = p >= Math.abs(i - r), h = p >= Math.abs(j - q), e && (c.position.top = d._convertPositionTo("relative", {
					top: k - d.helperProportions.height,
					left: 0
				}).top - d.margins.top), f && (c.position.top = d._convertPositionTo("relative", {
					top: l,
					left: 0
				}).top - d.margins.top), g && (c.position.left = d._convertPositionTo("relative", {
					top: 0,
					left: i - d.helperProportions.width
				}).left - d.margins.left), h && (c.position.left = d._convertPositionTo("relative", {
					top: 0,
					left: j
				}).left - d.margins.left)), n = e || f || g || h, "outer" !== o.snapMode && (e = p >= Math.abs(k - s), f = p >= Math.abs(l - t), g = p >= Math.abs(i - q), h = p >= Math.abs(j - r), e && (c.position.top = d._convertPositionTo("relative", {
					top: k,
					left: 0
				}).top - d.margins.top), f && (c.position.top = d._convertPositionTo("relative", {
					top: l - d.helperProportions.height,
					left: 0
				}).top - d.margins.top), g && (c.position.left = d._convertPositionTo("relative", {
					top: 0,
					left: i
				}).left - d.margins.left), h && (c.position.left = d._convertPositionTo("relative", {
					top: 0,
					left: j - d.helperProportions.width
				}).left - d.margins.left)), !d.snapElements[m].snapping && (e || f || g || h || n) && d.options.snap.snap && d.options.snap.snap.call(d.element, b, a.extend(d._uiHash(), {snapItem: d.snapElements[m].item})), d.snapElements[m].snapping = e || f || g || h || n)
		}
	}), a.ui.plugin.add("draggable", "stack", {
		start: function (b, c, d) {
			var e, f = d.options, g = a.makeArray(a(f.stack)).sort(function (b, c) {
				return (parseInt(a(b).css("zIndex"), 10) || 0) - (parseInt(a(c).css("zIndex"), 10) || 0)
			});
			g.length && (e = parseInt(a(g[0]).css("zIndex"), 10) || 0, a(g).each(function (b) {
				a(this).css("zIndex", e + b)
			}), this.css("zIndex", e + g.length))
		}
	}), a.ui.plugin.add("draggable", "zIndex", {
		start: function (b, c, d) {
			var e = a(c.helper), f = d.options;
			e.css("zIndex") && (f._zIndex = e.css("zIndex")), e.css("zIndex", f.zIndex)
		}, stop: function (b, c, d) {
			var e = d.options;
			e._zIndex && a(c.helper).css("zIndex", e._zIndex)
		}
	}), a.ui.draggable, a.widget("ui.droppable", {
		version: "1.11.0",
		widgetEventPrefix: "drop",
		options: {
			accept: "*",
			activeClass: !1,
			addClasses: !0,
			greedy: !1,
			hoverClass: !1,
			scope: "default",
			tolerance: "intersect",
			activate: null,
			deactivate: null,
			drop: null,
			out: null,
			over: null
		},
		_create: function () {
			var b, c = this.options, d = c.accept;
			this.isover = !1, this.isout = !0, this.accept = a.isFunction(d) ? d : function (a) {
				return a.is(d)
			}, this.proportions = function () {
				return arguments.length ? void(b = arguments[0]) : b ? b : b = {
					width: this.element[0].offsetWidth,
					height: this.element[0].offsetHeight
				}
			}, this._addToManager(c.scope), c.addClasses && this.element.addClass("ui-droppable")
		},
		_addToManager: function (b) {
			a.ui.ddmanager.droppables[b] = a.ui.ddmanager.droppables[b] || [], a.ui.ddmanager.droppables[b].push(this)
		},
		_splice: function (a) {
			for (var b = 0; a.length > b; b++)a[b] === this && a.splice(b, 1)
		},
		_destroy: function () {
			var b = a.ui.ddmanager.droppables[this.options.scope];
			this._splice(b), this.element.removeClass("ui-droppable ui-droppable-disabled")
		},
		_setOption: function (b, c) {
			if ("accept" === b)this.accept = a.isFunction(c) ? c : function (a) {
				return a.is(c)
			}; else if ("scope" === b) {
				var d = a.ui.ddmanager.droppables[this.options.scope];
				this._splice(d), this._addToManager(c)
			}
			this._super(b, c)
		},
		_activate: function (b) {
			var c = a.ui.ddmanager.current;
			this.options.activeClass && this.element.addClass(this.options.activeClass), c && this._trigger("activate", b, this.ui(c))
		},
		_deactivate: function (b) {
			var c = a.ui.ddmanager.current;
			this.options.activeClass && this.element.removeClass(this.options.activeClass), c && this._trigger("deactivate", b, this.ui(c))
		},
		_over: function (b) {
			var c = a.ui.ddmanager.current;
			c && (c.currentItem || c.element)[0] !== this.element[0] && this.accept.call(this.element[0], c.currentItem || c.element) && (this.options.hoverClass && this.element.addClass(this.options.hoverClass), this._trigger("over", b, this.ui(c)))
		},
		_out: function (b) {
			var c = a.ui.ddmanager.current;
			c && (c.currentItem || c.element)[0] !== this.element[0] && this.accept.call(this.element[0], c.currentItem || c.element) && (this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("out", b, this.ui(c)))
		},
		_drop: function (b, c) {
			var d = c || a.ui.ddmanager.current, e = !1;
			return d && (d.currentItem || d.element)[0] !== this.element[0] ? (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function () {
				var b = a(this).droppable("instance");
				return b.options.greedy && !b.options.disabled && b.options.scope === d.options.scope && b.accept.call(b.element[0], d.currentItem || d.element) && a.ui.intersect(d, a.extend(b, {offset: b.element.offset()}), b.options.tolerance) ? (e = !0, !1) : void 0
			}), e ? !1 : this.accept.call(this.element[0], d.currentItem || d.element) ? (this.options.activeClass && this.element.removeClass(this.options.activeClass), this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("drop", b, this.ui(d)), this.element) : !1) : !1
		},
		ui: function (a) {
			return {
				draggable: a.currentItem || a.element,
				helper: a.helper,
				position: a.position,
				offset: a.positionAbs
			}
		}
	}), a.ui.intersect = function () {
		function a(a, b, c) {
			return a >= b && b + c > a
		}

		return function (b, c, d) {
			if (!c.offset)return !1;
			var e, f, g = (b.positionAbs || b.position.absolute).left, h = (b.positionAbs || b.position.absolute).top, i = g + b.helperProportions.width, j = h + b.helperProportions.height, k = c.offset.left, l = c.offset.top, m = k + c.proportions().width, n = l + c.proportions().height;
			switch (d) {
				case"fit":
					return g >= k && m >= i && h >= l && n >= j;
				case"intersect":
					return g + b.helperProportions.width / 2 > k && m > i - b.helperProportions.width / 2 && h + b.helperProportions.height / 2 > l && n > j - b.helperProportions.height / 2;
				case"pointer":
					return e = (b.positionAbs || b.position.absolute).left + (b.clickOffset || b.offset.click).left, f = (b.positionAbs || b.position.absolute).top + (b.clickOffset || b.offset.click).top, a(f, l, c.proportions().height) && a(e, k, c.proportions().width);
				case"touch":
					return (h >= l && n >= h || j >= l && n >= j || l > h && j > n) && (g >= k && m >= g || i >= k && m >= i || k > g && i > m);
				default:
					return !1
			}
		}
	}(), a.ui.ddmanager = {
		current: null, droppables: {"default": []}, prepareOffsets: function (b, c) {
			var d, e, f = a.ui.ddmanager.droppables[b.options.scope] || [], g = c ? c.type : null, h = (b.currentItem || b.element).find(":data(ui-droppable)").addBack();
			a:for (d = 0; f.length > d; d++)if (!(f[d].options.disabled || b && !f[d].accept.call(f[d].element[0], b.currentItem || b.element))) {
				for (e = 0; h.length > e; e++)if (h[e] === f[d].element[0]) {
					f[d].proportions().height = 0;
					continue a
				}
				f[d].visible = "none" !== f[d].element.css("display"), f[d].visible && ("mousedown" === g && f[d]._activate.call(f[d], c), f[d].offset = f[d].element.offset(), f[d].proportions({
					width: f[d].element[0].offsetWidth,
					height: f[d].element[0].offsetHeight
				}))
			}
		}, drop: function (b, c) {
			var d = !1;
			return a.each((a.ui.ddmanager.droppables[b.options.scope] || []).slice(), function () {
				this.options && (!this.options.disabled && this.visible && a.ui.intersect(b, this, this.options.tolerance) && (d = this._drop.call(this, c) || d), !this.options.disabled && this.visible && this.accept.call(this.element[0], b.currentItem || b.element) && (this.isout = !0, this.isover = !1, this._deactivate.call(this, c)))
			}), d
		}, dragStart: function (b, c) {
			b.element.parentsUntil("body").bind("scroll.droppable", function () {
				b.options.refreshPositions || a.ui.ddmanager.prepareOffsets(b, c)
			})
		}, drag: function (b, c) {
			b.options.refreshPositions && a.ui.ddmanager.prepareOffsets(b, c), a.each(a.ui.ddmanager.droppables[b.options.scope] || [], function () {
				if (!this.options.disabled && !this.greedyChild && this.visible) {
					var d, e, f, g = a.ui.intersect(b, this, this.options.tolerance), h = !g && this.isover ? "isout" : g && !this.isover ? "isover" : null;
					h && (this.options.greedy && (e = this.options.scope, f = this.element.parents(":data(ui-droppable)").filter(function () {
						return a(this).droppable("instance").options.scope === e
					}), f.length && (d = a(f[0]).droppable("instance"), d.greedyChild = "isover" === h)), d && "isover" === h && (d.isover = !1, d.isout = !0, d._out.call(d, c)), this[h] = !0, this["isout" === h ? "isover" : "isout"] = !1, this["isover" === h ? "_over" : "_out"].call(this, c), d && "isout" === h && (d.isout = !1, d.isover = !0, d._over.call(d, c)))
				}
			})
		}, dragStop: function (b, c) {
			b.element.parentsUntil("body").unbind("scroll.droppable"), b.options.refreshPositions || a.ui.ddmanager.prepareOffsets(b, c)
		}
	}, a.ui.droppable, a.widget("ui.resizable", a.ui.mouse, {
		version: "1.11.0",
		widgetEventPrefix: "resize",
		options: {
			alsoResize: !1,
			animate: !1,
			animateDuration: "slow",
			animateEasing: "swing",
			aspectRatio: !1,
			autoHide: !1,
			containment: !1,
			ghost: !1,
			grid: !1,
			handles: "e,s,se",
			helper: !1,
			maxHeight: null,
			maxWidth: null,
			minHeight: 10,
			minWidth: 10,
			zIndex: 90,
			resize: null,
			start: null,
			stop: null
		},
		_num: function (a) {
			return parseInt(a, 10) || 0
		},
		_isNumber: function (a) {
			return !isNaN(parseInt(a, 10))
		},
		_hasScroll: function (b, c) {
			if ("hidden" === a(b).css("overflow"))return !1;
			var d = c && "left" === c ? "scrollLeft" : "scrollTop", e = !1;
			return b[d] > 0 ? !0 : (b[d] = 1, e = b[d] > 0, b[d] = 0, e)
		},
		_create: function () {
			var b, c, d, e, f, g = this, h = this.options;
			if (this.element.addClass("ui-resizable"), a.extend(this, {
					_aspectRatio: !!h.aspectRatio,
					aspectRatio: h.aspectRatio,
					originalElement: this.element,
					_proportionallyResizeElements: [],
					_helper: h.helper || h.ghost || h.animate ? h.helper || "ui-resizable-helper" : null
				}), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap(a("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
					position: this.element.css("position"),
					width: this.element.outerWidth(),
					height: this.element.outerHeight(),
					top: this.element.css("top"),
					left: this.element.css("left")
				})), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), this.elementIsWrapper = !0, this.element.css({
					marginLeft: this.originalElement.css("marginLeft"),
					marginTop: this.originalElement.css("marginTop"),
					marginRight: this.originalElement.css("marginRight"),
					marginBottom: this.originalElement.css("marginBottom")
				}), this.originalElement.css({
					marginLeft: 0,
					marginTop: 0,
					marginRight: 0,
					marginBottom: 0
				}), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
					position: "static",
					zoom: 1,
					display: "block"
				})), this.originalElement.css({margin: this.originalElement.css("margin")}), this._proportionallyResize()), this.handles = h.handles || (a(".ui-resizable-handle", this.element).length ? {
						n: ".ui-resizable-n",
						e: ".ui-resizable-e",
						s: ".ui-resizable-s",
						w: ".ui-resizable-w",
						se: ".ui-resizable-se",
						sw: ".ui-resizable-sw",
						ne: ".ui-resizable-ne",
						nw: ".ui-resizable-nw"
					} : "e,s,se"), this.handles.constructor === String)for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), b = this.handles.split(","), this.handles = {}, c = 0; b.length > c; c++)d = a.trim(b[c]), f = "ui-resizable-" + d, e = a("<div class='ui-resizable-handle " + f + "'></div>"), e.css({zIndex: h.zIndex}), "se" === d && e.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[d] = ".ui-resizable-" + d, this.element.append(e);
			this._renderAxis = function (b) {
				var c, d, e, f;
				b = b || this.element;
				for (c in this.handles)this.handles[c].constructor === String && (this.handles[c] = this.element.children(this.handles[c]).first().show()), this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i) && (d = a(this.handles[c], this.element), f = /sw|ne|nw|se|n|s/.test(c) ? d.outerHeight() : d.outerWidth(), e = ["padding", /ne|nw|n/.test(c) ? "Top" : /se|sw|s/.test(c) ? "Bottom" : /^e$/.test(c) ? "Right" : "Left"].join(""), b.css(e, f), this._proportionallyResize()), a(this.handles[c]).length
			}, this._renderAxis(this.element), this._handles = a(".ui-resizable-handle", this.element).disableSelection(), this._handles.mouseover(function () {
				g.resizing || (this.className && (e = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), g.axis = e && e[1] ? e[1] : "se")
			}), h.autoHide && (this._handles.hide(), a(this.element).addClass("ui-resizable-autohide").mouseenter(function () {
				h.disabled || (a(this).removeClass("ui-resizable-autohide"), g._handles.show())
			}).mouseleave(function () {
				h.disabled || g.resizing || (a(this).addClass("ui-resizable-autohide"), g._handles.hide())
			})), this._mouseInit()
		},
		_destroy: function () {
			this._mouseDestroy();
			var b, c = function (b) {
				a(b).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
			};
			return this.elementIsWrapper && (c(this.element), b = this.element, this.originalElement.css({
				position: b.css("position"),
				width: b.outerWidth(),
				height: b.outerHeight(),
				top: b.css("top"),
				left: b.css("left")
			}).insertAfter(b), b.remove()), this.originalElement.css("resize", this.originalResizeStyle), c(this.originalElement), this
		},
		_mouseCapture: function (b) {
			var c, d, e = !1;
			for (c in this.handles)d = a(this.handles[c])[0], (d === b.target || a.contains(d, b.target)) && (e = !0);
			return !this.options.disabled && e
		},
		_mouseStart: function (b) {
			var c, d, e, f = this.options, g = this.element;
			return this.resizing = !0, this._renderProxy(), c = this._num(this.helper.css("left")), d = this._num(this.helper.css("top")), f.containment && (c += a(f.containment).scrollLeft() || 0, d += a(f.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
				left: c,
				top: d
			}, this.size = this._helper ? {
				width: this.helper.width(),
				height: this.helper.height()
			} : {width: g.width(), height: g.height()}, this.originalSize = this._helper ? {
				width: g.outerWidth(),
				height: g.outerHeight()
			} : {width: g.width(), height: g.height()}, this.originalPosition = {
				left: c,
				top: d
			}, this.sizeDiff = {
				width: g.outerWidth() - g.width(),
				height: g.outerHeight() - g.height()
			}, this.originalMousePosition = {
				left: b.pageX,
				top: b.pageY
			}, this.aspectRatio = "number" == typeof f.aspectRatio ? f.aspectRatio : this.originalSize.width / this.originalSize.height || 1, e = a(".ui-resizable-" + this.axis).css("cursor"), a("body").css("cursor", "auto" === e ? this.axis + "-resize" : e), g.addClass("ui-resizable-resizing"), this._propagate("start", b), !0
		},
		_mouseDrag: function (b) {
			var c, d = this.helper, e = {}, f = this.originalMousePosition, g = this.axis, h = b.pageX - f.left || 0, i = b.pageY - f.top || 0, j = this._change[g];
			return this.prevPosition = {
				top: this.position.top,
				left: this.position.left
			}, this.prevSize = {
				width: this.size.width,
				height: this.size.height
			}, j ? (c = j.apply(this, [b, h, i]), this._updateVirtualBoundaries(b.shiftKey), (this._aspectRatio || b.shiftKey) && (c = this._updateRatio(c, b)), c = this._respectSize(c, b), this._updateCache(c), this._propagate("resize", b), this.position.top !== this.prevPosition.top && (e.top = this.position.top + "px"), this.position.left !== this.prevPosition.left && (e.left = this.position.left + "px"), this.size.width !== this.prevSize.width && (e.width = this.size.width + "px"), this.size.height !== this.prevSize.height && (e.height = this.size.height + "px"), d.css(e), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), a.isEmptyObject(e) || this._trigger("resize", b, this.ui()), !1) : !1
		},
		_mouseStop: function (b) {
			this.resizing = !1;
			var c, d, e, f, g, h, i, j = this.options, k = this;
			return this._helper && (c = this._proportionallyResizeElements, d = c.length && /textarea/i.test(c[0].nodeName), e = d && this._hasScroll(c[0], "left") ? 0 : k.sizeDiff.height, f = d ? 0 : k.sizeDiff.width, g = {
				width: k.helper.width() - f,
				height: k.helper.height() - e
			}, h = parseInt(k.element.css("left"), 10) + (k.position.left - k.originalPosition.left) || null, i = parseInt(k.element.css("top"), 10) + (k.position.top - k.originalPosition.top) || null, j.animate || this.element.css(a.extend(g, {
				top: i,
				left: h
			})), k.helper.height(k.size.height), k.helper.width(k.size.width), this._helper && !j.animate && this._proportionallyResize()), a("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), this._propagate("stop", b), this._helper && this.helper.remove(), !1
		},
		_updateVirtualBoundaries: function (a) {
			var b, c, d, e, f, g = this.options;
			f = {
				minWidth: this._isNumber(g.minWidth) ? g.minWidth : 0,
				maxWidth: this._isNumber(g.maxWidth) ? g.maxWidth : 1 / 0,
				minHeight: this._isNumber(g.minHeight) ? g.minHeight : 0,
				maxHeight: this._isNumber(g.maxHeight) ? g.maxHeight : 1 / 0
			}, (this._aspectRatio || a) && (b = f.minHeight * this.aspectRatio, d = f.minWidth / this.aspectRatio, c = f.maxHeight * this.aspectRatio, e = f.maxWidth / this.aspectRatio, b > f.minWidth && (f.minWidth = b), d > f.minHeight && (f.minHeight = d), f.maxWidth > c && (f.maxWidth = c), f.maxHeight > e && (f.maxHeight = e)), this._vBoundaries = f
		},
		_updateCache: function (a) {
			this.offset = this.helper.offset(), this._isNumber(a.left) && (this.position.left = a.left), this._isNumber(a.top) && (this.position.top = a.top), this._isNumber(a.height) && (this.size.height = a.height), this._isNumber(a.width) && (this.size.width = a.width)
		},
		_updateRatio: function (a) {
			var b = this.position, c = this.size, d = this.axis;
			return this._isNumber(a.height) ? a.width = a.height * this.aspectRatio : this._isNumber(a.width) && (a.height = a.width / this.aspectRatio), "sw" === d && (a.left = b.left + (c.width - a.width), a.top = null), "nw" === d && (a.top = b.top + (c.height - a.height), a.left = b.left + (c.width - a.width)), a
		},
		_respectSize: function (a) {
			var b = this._vBoundaries, c = this.axis, d = this._isNumber(a.width) && b.maxWidth && b.maxWidth < a.width, e = this._isNumber(a.height) && b.maxHeight && b.maxHeight < a.height, f = this._isNumber(a.width) && b.minWidth && b.minWidth > a.width, g = this._isNumber(a.height) && b.minHeight && b.minHeight > a.height, h = this.originalPosition.left + this.originalSize.width, i = this.position.top + this.size.height, j = /sw|nw|w/.test(c), k = /nw|ne|n/.test(c);
			return f && (a.width = b.minWidth), g && (a.height = b.minHeight), d && (a.width = b.maxWidth), e && (a.height = b.maxHeight), f && j && (a.left = h - b.minWidth), d && j && (a.left = h - b.maxWidth), g && k && (a.top = i - b.minHeight), e && k && (a.top = i - b.maxHeight), a.width || a.height || a.left || !a.top ? a.width || a.height || a.top || !a.left || (a.left = null) : a.top = null, a
		},
		_proportionallyResize: function () {
			if (this._proportionallyResizeElements.length) {
				var a, b, c, d, e, f = this.helper || this.element;
				for (a = 0; this._proportionallyResizeElements.length > a; a++) {
					if (e = this._proportionallyResizeElements[a], !this.borderDif)for (this.borderDif = [], c = [e.css("borderTopWidth"), e.css("borderRightWidth"), e.css("borderBottomWidth"), e.css("borderLeftWidth")], d = [e.css("paddingTop"), e.css("paddingRight"), e.css("paddingBottom"), e.css("paddingLeft")], b = 0; c.length > b; b++)this.borderDif[b] = (parseInt(c[b], 10) || 0) + (parseInt(d[b], 10) || 0);
					e.css({
						height: f.height() - this.borderDif[0] - this.borderDif[2] || 0,
						width: f.width() - this.borderDif[1] - this.borderDif[3] || 0
					})
				}
			}
		},
		_renderProxy: function () {
			var b = this.element, c = this.options;
			this.elementOffset = b.offset(), this._helper ? (this.helper = this.helper || a("<div style='overflow:hidden;'></div>"), this.helper.addClass(this._helper).css({
				width: this.element.outerWidth() - 1,
				height: this.element.outerHeight() - 1,
				position: "absolute",
				left: this.elementOffset.left + "px",
				top: this.elementOffset.top + "px",
				zIndex: ++c.zIndex
			}), this.helper.appendTo("body").disableSelection()) : this.helper = this.element
		},
		_change: {
			e: function (a, b) {
				return {width: this.originalSize.width + b}
			}, w: function (a, b) {
				var c = this.originalSize, d = this.originalPosition;
				return {left: d.left + b, width: c.width - b}
			}, n: function (a, b, c) {
				var d = this.originalSize, e = this.originalPosition;
				return {top: e.top + c, height: d.height - c}
			}, s: function (a, b, c) {
				return {height: this.originalSize.height + c}
			}, se: function (b, c, d) {
				return a.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [b, c, d]))
			}, sw: function (b, c, d) {
				return a.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [b, c, d]))
			}, ne: function (b, c, d) {
				return a.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [b, c, d]))
			}, nw: function (b, c, d) {
				return a.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [b, c, d]))
			}
		},
		_propagate: function (b, c) {
			a.ui.plugin.call(this, b, [c, this.ui()]), "resize" !== b && this._trigger(b, c, this.ui())
		},
		plugins: {},
		ui: function () {
			return {
				originalElement: this.originalElement,
				element: this.element,
				helper: this.helper,
				position: this.position,
				size: this.size,
				originalSize: this.originalSize,
				originalPosition: this.originalPosition,
				prevSize: this.prevSize,
				prevPosition: this.prevPosition
			}
		}
	}), a.ui.plugin.add("resizable", "animate", {
		stop: function (b) {
			var c = a(this).resizable("instance"), d = c.options, e = c._proportionallyResizeElements, f = e.length && /textarea/i.test(e[0].nodeName), g = f && c._hasScroll(e[0], "left") ? 0 : c.sizeDiff.height, h = f ? 0 : c.sizeDiff.width, i = {
				width: c.size.width - h,
				height: c.size.height - g
			}, j = parseInt(c.element.css("left"), 10) + (c.position.left - c.originalPosition.left) || null, k = parseInt(c.element.css("top"), 10) + (c.position.top - c.originalPosition.top) || null;
			c.element.animate(a.extend(i, k && j ? {top: k, left: j} : {}), {
				duration: d.animateDuration,
				easing: d.animateEasing,
				step: function () {
					var d = {
						width: parseInt(c.element.css("width"), 10),
						height: parseInt(c.element.css("height"), 10),
						top: parseInt(c.element.css("top"), 10),
						left: parseInt(c.element.css("left"), 10)
					};
					e && e.length && a(e[0]).css({
						width: d.width,
						height: d.height
					}), c._updateCache(d), c._propagate("resize", b)
				}
			})
		}
	}), a.ui.plugin.add("resizable", "containment", {
		start: function () {
			var b, c, d, e, f, g, h, i = a(this).resizable("instance"), j = i.options, k = i.element, l = j.containment, m = l instanceof a ? l.get(0) : /parent/.test(l) ? k.parent().get(0) : l;
			m && (i.containerElement = a(m), /document/.test(l) || l === document ? (i.containerOffset = {
				left: 0,
				top: 0
			}, i.containerPosition = {left: 0, top: 0}, i.parentData = {
				element: a(document),
				left: 0,
				top: 0,
				width: a(document).width(),
				height: a(document).height() || document.body.parentNode.scrollHeight
			}) : (b = a(m), c = [], a(["Top", "Right", "Left", "Bottom"]).each(function (a, d) {
				c[a] = i._num(b.css("padding" + d))
			}), i.containerOffset = b.offset(), i.containerPosition = b.position(), i.containerSize = {
				height: b.innerHeight() - c[3],
				width: b.innerWidth() - c[1]
			}, d = i.containerOffset, e = i.containerSize.height, f = i.containerSize.width, g = i._hasScroll(m, "left") ? m.scrollWidth : f, h = i._hasScroll(m) ? m.scrollHeight : e, i.parentData = {
				element: m,
				left: d.left,
				top: d.top,
				width: g,
				height: h
			}))
		}, resize: function (b, c) {
			var d, e, f, g, h = a(this).resizable("instance"), i = h.options, j = h.containerOffset, k = h.position, l = h._aspectRatio || b.shiftKey, m = {
				top: 0,
				left: 0
			}, n = h.containerElement, o = !0;
			n[0] !== document && /static/.test(n.css("position")) && (m = j), k.left < (h._helper ? j.left : 0) && (h.size.width = h.size.width + (h._helper ? h.position.left - j.left : h.position.left - m.left), l && (h.size.height = h.size.width / h.aspectRatio, o = !1), h.position.left = i.helper ? j.left : 0), k.top < (h._helper ? j.top : 0) && (h.size.height = h.size.height + (h._helper ? h.position.top - j.top : h.position.top), l && (h.size.width = h.size.height * h.aspectRatio, o = !1), h.position.top = h._helper ? j.top : 0), h.offset.left = h.parentData.left + h.position.left, h.offset.top = h.parentData.top + h.position.top, d = Math.abs((h._helper ? h.offset.left - m.left : h.offset.left - j.left) + h.sizeDiff.width), e = Math.abs((h._helper ? h.offset.top - m.top : h.offset.top - j.top) + h.sizeDiff.height), f = h.containerElement.get(0) === h.element.parent().get(0), g = /relative|absolute/.test(h.containerElement.css("position")), f && g && (d -= Math.abs(h.parentData.left)), d + h.size.width >= h.parentData.width && (h.size.width = h.parentData.width - d, l && (h.size.height = h.size.width / h.aspectRatio, o = !1)), e + h.size.height >= h.parentData.height && (h.size.height = h.parentData.height - e, l && (h.size.width = h.size.height * h.aspectRatio, o = !1)), o || (h.position.left = c.prevPosition.left, h.position.top = c.prevPosition.top, h.size.width = c.prevSize.width, h.size.height = c.prevSize.height)
		}, stop: function () {
			var b = a(this).resizable("instance"), c = b.options, d = b.containerOffset, e = b.containerPosition, f = b.containerElement, g = a(b.helper), h = g.offset(), i = g.outerWidth() - b.sizeDiff.width, j = g.outerHeight() - b.sizeDiff.height;
			b._helper && !c.animate && /relative/.test(f.css("position")) && a(this).css({
				left: h.left - e.left - d.left,
				width: i,
				height: j
			}), b._helper && !c.animate && /static/.test(f.css("position")) && a(this).css({
				left: h.left - e.left - d.left,
				width: i,
				height: j
			})
		}
	}), a.ui.plugin.add("resizable", "alsoResize", {
		start: function () {
			var b = a(this).resizable("instance"), c = b.options, d = function (b) {
				a(b).each(function () {
					var b = a(this);
					b.data("ui-resizable-alsoresize", {
						width: parseInt(b.width(), 10),
						height: parseInt(b.height(), 10),
						left: parseInt(b.css("left"), 10),
						top: parseInt(b.css("top"), 10)
					})
				})
			};
			"object" != typeof c.alsoResize || c.alsoResize.parentNode ? d(c.alsoResize) : c.alsoResize.length ? (c.alsoResize = c.alsoResize[0], d(c.alsoResize)) : a.each(c.alsoResize, function (a) {
				d(a)
			})
		}, resize: function (b, c) {
			var d = a(this).resizable("instance"), e = d.options, f = d.originalSize, g = d.originalPosition, h = {
				height: d.size.height - f.height || 0,
				width: d.size.width - f.width || 0,
				top: d.position.top - g.top || 0,
				left: d.position.left - g.left || 0
			}, i = function (b, d) {
				a(b).each(function () {
					var b = a(this), e = a(this).data("ui-resizable-alsoresize"), f = {}, g = d && d.length ? d : b.parents(c.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
					a.each(g, function (a, b) {
						var c = (e[b] || 0) + (h[b] || 0);
						c && c >= 0 && (f[b] = c || null)
					}), b.css(f)
				})
			};
			"object" != typeof e.alsoResize || e.alsoResize.nodeType ? i(e.alsoResize) : a.each(e.alsoResize, function (a, b) {
				i(a, b)
			})
		}, stop: function () {
			a(this).removeData("resizable-alsoresize")
		}
	}), a.ui.plugin.add("resizable", "ghost", {
		start: function () {
			var b = a(this).resizable("instance"), c = b.options, d = b.size;
			b.ghost = b.originalElement.clone(), b.ghost.css({
				opacity: .25,
				display: "block",
				position: "relative",
				height: d.height,
				width: d.width,
				margin: 0,
				left: 0,
				top: 0
			}).addClass("ui-resizable-ghost").addClass("string" == typeof c.ghost ? c.ghost : ""), b.ghost.appendTo(b.helper)
		}, resize: function () {
			var b = a(this).resizable("instance");
			b.ghost && b.ghost.css({position: "relative", height: b.size.height, width: b.size.width})
		}, stop: function () {
			var b = a(this).resizable("instance");
			b.ghost && b.helper && b.helper.get(0).removeChild(b.ghost.get(0))
		}
	}), a.ui.plugin.add("resizable", "grid", {
		resize: function () {
			var b = a(this).resizable("instance"), c = b.options, d = b.size, e = b.originalSize, f = b.originalPosition, g = b.axis, h = "number" == typeof c.grid ? [c.grid, c.grid] : c.grid, i = h[0] || 1, j = h[1] || 1, k = Math.round((d.width - e.width) / i) * i, l = Math.round((d.height - e.height) / j) * j, m = e.width + k, n = e.height + l, o = c.maxWidth && m > c.maxWidth, p = c.maxHeight && n > c.maxHeight, q = c.minWidth && c.minWidth > m, r = c.minHeight && c.minHeight > n;
			c.grid = h, q && (m += i), r && (n += j), o && (m -= i), p && (n -= j), /^(se|s|e)$/.test(g) ? (b.size.width = m, b.size.height = n) : /^(ne)$/.test(g) ? (b.size.width = m, b.size.height = n, b.position.top = f.top - l) : /^(sw)$/.test(g) ? (b.size.width = m, b.size.height = n, b.position.left = f.left - k) : (n - j > 0 ? (b.size.height = n, b.position.top = f.top - l) : (b.size.height = j, b.position.top = f.top + e.height - j), m - i > 0 ? (b.size.width = m, b.position.left = f.left - k) : (b.size.width = i, b.position.left = f.left + e.width - i))
		}
	}), a.ui.resizable, a.widget("ui.selectable", a.ui.mouse, {
		version: "1.11.0",
		options: {
			appendTo: "body",
			autoRefresh: !0,
			distance: 0,
			filter: "*",
			tolerance: "touch",
			selected: null,
			selecting: null,
			start: null,
			stop: null,
			unselected: null,
			unselecting: null
		},
		_create: function () {
			var b, c = this;
			this.element.addClass("ui-selectable"), this.dragged = !1, this.refresh = function () {
				b = a(c.options.filter, c.element[0]), b.addClass("ui-selectee"), b.each(function () {
					var b = a(this), c = b.offset();
					a.data(this, "selectable-item", {
						element: this,
						$element: b,
						left: c.left,
						top: c.top,
						right: c.left + b.outerWidth(),
						bottom: c.top + b.outerHeight(),
						startselected: !1,
						selected: b.hasClass("ui-selected"),
						selecting: b.hasClass("ui-selecting"),
						unselecting: b.hasClass("ui-unselecting")
					})
				})
			}, this.refresh(), this.selectees = b.addClass("ui-selectee"), this._mouseInit(), this.helper = a("<div class='ui-selectable-helper'></div>")
		},
		_destroy: function () {
			this.selectees.removeClass("ui-selectee").removeData("selectable-item"), this.element.removeClass("ui-selectable ui-selectable-disabled"), this._mouseDestroy()
		},
		_mouseStart: function (b) {
			var c = this, d = this.options;
			this.opos = [b.pageX, b.pageY], this.options.disabled || (this.selectees = a(d.filter, this.element[0]), this._trigger("start", b), a(d.appendTo).append(this.helper), this.helper.css({
				left: b.pageX,
				top: b.pageY,
				width: 0,
				height: 0
			}), d.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function () {
				var d = a.data(this, "selectable-item");
				d.startselected = !0, b.metaKey || b.ctrlKey || (d.$element.removeClass("ui-selected"), d.selected = !1, d.$element.addClass("ui-unselecting"), d.unselecting = !0, c._trigger("unselecting", b, {unselecting: d.element}))
			}), a(b.target).parents().addBack().each(function () {
				var d, e = a.data(this, "selectable-item");
				return e ? (d = !b.metaKey && !b.ctrlKey || !e.$element.hasClass("ui-selected"), e.$element.removeClass(d ? "ui-unselecting" : "ui-selected").addClass(d ? "ui-selecting" : "ui-unselecting"), e.unselecting = !d, e.selecting = d, e.selected = d, d ? c._trigger("selecting", b, {selecting: e.element}) : c._trigger("unselecting", b, {unselecting: e.element}), !1) : void 0
			}))
		},
		_mouseDrag: function (b) {
			if (this.dragged = !0, !this.options.disabled) {
				var c, d = this, e = this.options, f = this.opos[0], g = this.opos[1], h = b.pageX, i = b.pageY;
				return f > h && (c = h, h = f, f = c), g > i && (c = i, i = g, g = c), this.helper.css({
					left: f,
					top: g,
					width: h - f,
					height: i - g
				}), this.selectees.each(function () {
					var c = a.data(this, "selectable-item"), j = !1;
					c && c.element !== d.element[0] && ("touch" === e.tolerance ? j = !(c.left > h || f > c.right || c.top > i || g > c.bottom) : "fit" === e.tolerance && (j = c.left > f && h > c.right && c.top > g && i > c.bottom), j ? (c.selected && (c.$element.removeClass("ui-selected"), c.selected = !1), c.unselecting && (c.$element.removeClass("ui-unselecting"), c.unselecting = !1), c.selecting || (c.$element.addClass("ui-selecting"), c.selecting = !0, d._trigger("selecting", b, {selecting: c.element}))) : (c.selecting && ((b.metaKey || b.ctrlKey) && c.startselected ? (c.$element.removeClass("ui-selecting"), c.selecting = !1, c.$element.addClass("ui-selected"), c.selected = !0) : (c.$element.removeClass("ui-selecting"), c.selecting = !1, c.startselected && (c.$element.addClass("ui-unselecting"), c.unselecting = !0), d._trigger("unselecting", b, {unselecting: c.element}))), c.selected && (b.metaKey || b.ctrlKey || c.startselected || (c.$element.removeClass("ui-selected"), c.selected = !1, c.$element.addClass("ui-unselecting"), c.unselecting = !0, d._trigger("unselecting", b, {unselecting: c.element})))))
				}), !1
			}
		},
		_mouseStop: function (b) {
			var c = this;
			return this.dragged = !1, a(".ui-unselecting", this.element[0]).each(function () {
				var d = a.data(this, "selectable-item");
				d.$element.removeClass("ui-unselecting"), d.unselecting = !1, d.startselected = !1, c._trigger("unselected", b, {unselected: d.element})
			}), a(".ui-selecting", this.element[0]).each(function () {
				var d = a.data(this, "selectable-item");
				d.$element.removeClass("ui-selecting").addClass("ui-selected"), d.selecting = !1, d.selected = !0, d.startselected = !0, c._trigger("selected", b, {selected: d.element})
			}), this._trigger("stop", b), this.helper.remove(), !1
		}
	}), a.widget("ui.sortable", a.ui.mouse, {
		version: "1.11.0",
		widgetEventPrefix: "sort",
		ready: !1,
		options: {
			appendTo: "parent",
			axis: !1,
			connectWith: !1,
			containment: !1,
			cursor: "auto",
			cursorAt: !1,
			dropOnEmpty: !0,
			forcePlaceholderSize: !1,
			forceHelperSize: !1,
			grid: !1,
			handle: !1,
			helper: "original",
			items: "> *",
			opacity: !1,
			placeholder: !1,
			revert: !1,
			scroll: !0,
			scrollSensitivity: 20,
			scrollSpeed: 20,
			scope: "default",
			tolerance: "intersect",
			zIndex: 1e3,
			activate: null,
			beforeStop: null,
			change: null,
			deactivate: null,
			out: null,
			over: null,
			receive: null,
			remove: null,
			sort: null,
			start: null,
			stop: null,
			update: null
		},
		_isOverAxis: function (a, b, c) {
			return a >= b && b + c > a
		},
		_isFloating: function (a) {
			return /left|right/.test(a.css("float")) || /inline|table-cell/.test(a.css("display"))
		},
		_create: function () {
			var a = this.options;
			this.containerCache = {}, this.element.addClass("ui-sortable"), this.refresh(), this.floating = this.items.length ? "x" === a.axis || this._isFloating(this.items[0].item) : !1, this.offset = this.element.offset(), this._mouseInit(), this._setHandleClassName(), this.ready = !0
		},
		_setOption: function (a, b) {
			this._super(a, b), "handle" === a && this._setHandleClassName()
		},
		_setHandleClassName: function () {
			this.element.find(".ui-sortable-handle").removeClass("ui-sortable-handle"), a.each(this.items, function () {
				(this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item).addClass("ui-sortable-handle")
			})
		},
		_destroy: function () {
			this.element.removeClass("ui-sortable ui-sortable-disabled").find(".ui-sortable-handle").removeClass("ui-sortable-handle"), this._mouseDestroy();
			for (var a = this.items.length - 1; a >= 0; a--)this.items[a].item.removeData(this.widgetName + "-item");
			return this
		},
		_mouseCapture: function (b, c) {
			var d = null, e = !1, f = this;
			return this.reverting ? !1 : this.options.disabled || "static" === this.options.type ? !1 : (this._refreshItems(b), a(b.target).parents().each(function () {
				return a.data(this, f.widgetName + "-item") === f ? (d = a(this), !1) : void 0
			}), a.data(b.target, f.widgetName + "-item") === f && (d = a(b.target)), d && (!this.options.handle || c || (a(this.options.handle, d).find("*").addBack().each(function () {
				this === b.target && (e = !0)
			}), e)) ? (this.currentItem = d, this._removeCurrentsFromItems(), !0) : !1)
		},
		_mouseStart: function (b, c, d) {
			var e, f, g = this.options;
			if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(b), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {
					top: this.offset.top - this.margins.top,
					left: this.offset.left - this.margins.left
				}, a.extend(this.offset, {
					click: {left: b.pageX - this.offset.left, top: b.pageY - this.offset.top},
					parent: this._getParentOffset(),
					relative: this._getRelativeOffset()
				}), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(b), this.originalPageX = b.pageX, this.originalPageY = b.pageY, g.cursorAt && this._adjustOffsetFromHelper(g.cursorAt), this.domPosition = {
					prev: this.currentItem.prev()[0],
					parent: this.currentItem.parent()[0]
				}, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), g.containment && this._setContainment(), g.cursor && "auto" !== g.cursor && (f = this.document.find("body"), this.storedCursor = f.css("cursor"), f.css("cursor", g.cursor), this.storedStylesheet = a("<style>*{ cursor: " + g.cursor + " !important; }</style>").appendTo(f)), g.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", g.opacity)), g.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", g.zIndex)), this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", b, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !d)for (e = this.containers.length - 1; e >= 0; e--)this.containers[e]._trigger("activate", b, this._uiHash(this));
			return a.ui.ddmanager && (a.ui.ddmanager.current = this), a.ui.ddmanager && !g.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b), this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(b), !0
		},
		_mouseDrag: function (b) {
			var c, d, e, f, g = this.options, h = !1;
			for (this.position = this._generatePosition(b), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - b.pageY < g.scrollSensitivity ? this.scrollParent[0].scrollTop = h = this.scrollParent[0].scrollTop + g.scrollSpeed : b.pageY - this.overflowOffset.top < g.scrollSensitivity && (this.scrollParent[0].scrollTop = h = this.scrollParent[0].scrollTop - g.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - b.pageX < g.scrollSensitivity ? this.scrollParent[0].scrollLeft = h = this.scrollParent[0].scrollLeft + g.scrollSpeed : b.pageX - this.overflowOffset.left < g.scrollSensitivity && (this.scrollParent[0].scrollLeft = h = this.scrollParent[0].scrollLeft - g.scrollSpeed)) : (b.pageY - a(document).scrollTop() < g.scrollSensitivity ? h = a(document).scrollTop(a(document).scrollTop() - g.scrollSpeed) : a(window).height() - (b.pageY - a(document).scrollTop()) < g.scrollSensitivity && (h = a(document).scrollTop(a(document).scrollTop() + g.scrollSpeed)), b.pageX - a(document).scrollLeft() < g.scrollSensitivity ? h = a(document).scrollLeft(a(document).scrollLeft() - g.scrollSpeed) : a(window).width() - (b.pageX - a(document).scrollLeft()) < g.scrollSensitivity && (h = a(document).scrollLeft(a(document).scrollLeft() + g.scrollSpeed))), h !== !1 && a.ui.ddmanager && !g.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b)), this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), c = this.items.length - 1; c >= 0; c--)if (d = this.items[c], e = d.item[0], f = this._intersectsWithPointer(d), f && d.instance === this.currentContainer && e !== this.currentItem[0] && this.placeholder[1 === f ? "next" : "prev"]()[0] !== e && !a.contains(this.placeholder[0], e) && ("semi-dynamic" === this.options.type ? !a.contains(this.element[0], e) : !0)) {
				if (this.direction = 1 === f ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(d))break;
				this._rearrange(b, d), this._trigger("change", b, this._uiHash());
				break
			}
			return this._contactContainers(b), a.ui.ddmanager && a.ui.ddmanager.drag(this, b), this._trigger("sort", b, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
		},
		_mouseStop: function (b, c) {
			if (b) {
				if (a.ui.ddmanager && !this.options.dropBehaviour && a.ui.ddmanager.drop(this, b), this.options.revert) {
					var d = this, e = this.placeholder.offset(), f = this.options.axis, g = {};
					f && "x" !== f || (g.left = e.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft)), f && "y" !== f || (g.top = e.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop)), this.reverting = !0, a(this.helper).animate(g, parseInt(this.options.revert, 10) || 500, function () {
						d._clear(b)
					})
				} else this._clear(b, c);
				return !1
			}
		},
		cancel: function () {
			if (this.dragging) {
				this._mouseUp({target: null}), "original" === this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
				for (var b = this.containers.length - 1; b >= 0; b--)this.containers[b]._trigger("deactivate", null, this._uiHash(this)), this.containers[b].containerCache.over && (this.containers[b]._trigger("out", null, this._uiHash(this)), this.containers[b].containerCache.over = 0)
			}
			return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), a.extend(this, {
				helper: null,
				dragging: !1,
				reverting: !1,
				_noFinalSort: null
			}), this.domPosition.prev ? a(this.domPosition.prev).after(this.currentItem) : a(this.domPosition.parent).prepend(this.currentItem)), this
		},
		serialize: function (b) {
			var c = this._getItemsAsjQuery(b && b.connected), d = [];
			return b = b || {}, a(c).each(function () {
				var c = (a(b.item || this).attr(b.attribute || "id") || "").match(b.expression || /(.+)[\-=_](.+)/);
				c && d.push((b.key || c[1] + "[]") + "=" + (b.key && b.expression ? c[1] : c[2]))
			}), !d.length && b.key && d.push(b.key + "="), d.join("&")
		},
		toArray: function (b) {
			var c = this._getItemsAsjQuery(b && b.connected), d = [];
			return b = b || {}, c.each(function () {
				d.push(a(b.item || this).attr(b.attribute || "id") || "")
			}), d
		},
		_intersectsWith: function (a) {
			var b = this.positionAbs.left, c = b + this.helperProportions.width, d = this.positionAbs.top, e = d + this.helperProportions.height, f = a.left, g = f + a.width, h = a.top, i = h + a.height, j = this.offset.click.top, k = this.offset.click.left, l = "x" === this.options.axis || d + j > h && i > d + j, m = "y" === this.options.axis || b + k > f && g > b + k, n = l && m;
			return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > a[this.floating ? "width" : "height"] ? n : b + this.helperProportions.width / 2 > f && g > c - this.helperProportions.width / 2 && d + this.helperProportions.height / 2 > h && i > e - this.helperProportions.height / 2
		},
		_intersectsWithPointer: function (a) {
			var b = "x" === this.options.axis || this._isOverAxis(this.positionAbs.top + this.offset.click.top, a.top, a.height), c = "y" === this.options.axis || this._isOverAxis(this.positionAbs.left + this.offset.click.left, a.left, a.width), d = b && c, e = this._getDragVerticalDirection(), f = this._getDragHorizontalDirection();
			return d ? this.floating ? f && "right" === f || "down" === e ? 2 : 1 : e && ("down" === e ? 2 : 1) : !1
		},
		_intersectsWithSides: function (a) {
			var b = this._isOverAxis(this.positionAbs.top + this.offset.click.top, a.top + a.height / 2, a.height), c = this._isOverAxis(this.positionAbs.left + this.offset.click.left, a.left + a.width / 2, a.width), d = this._getDragVerticalDirection(), e = this._getDragHorizontalDirection();
			return this.floating && e ? "right" === e && c || "left" === e && !c : d && ("down" === d && b || "up" === d && !b)
		},
		_getDragVerticalDirection: function () {
			var a = this.positionAbs.top - this.lastPositionAbs.top;
			return 0 !== a && (a > 0 ? "down" : "up")
		},
		_getDragHorizontalDirection: function () {
			var a = this.positionAbs.left - this.lastPositionAbs.left;
			return 0 !== a && (a > 0 ? "right" : "left")
		},
		refresh: function (a) {
			return this._refreshItems(a), this._setHandleClassName(), this.refreshPositions(), this
		},
		_connectWith: function () {
			var a = this.options;
			return a.connectWith.constructor === String ? [a.connectWith] : a.connectWith
		},
		_getItemsAsjQuery: function (b) {
			function c() {
				h.push(this)
			}

			var d, e, f, g, h = [], i = [], j = this._connectWith();
			if (j && b)for (d = j.length - 1; d >= 0; d--)for (f = a(j[d]), e = f.length - 1; e >= 0; e--)g = a.data(f[e], this.widgetFullName), g && g !== this && !g.options.disabled && i.push([a.isFunction(g.options.items) ? g.options.items.call(g.element) : a(g.options.items, g.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), g]);
			for (i.push([a.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
				options: this.options,
				item: this.currentItem
			}) : a(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]), d = i.length - 1; d >= 0; d--)i[d][0].each(c);
			return a(h)
		},
		_removeCurrentsFromItems: function () {
			var b = this.currentItem.find(":data(" + this.widgetName + "-item)");
			this.items = a.grep(this.items, function (a) {
				for (var c = 0; b.length > c; c++)if (b[c] === a.item[0])return !1;
				return !0
			})
		},
		_refreshItems: function (b) {
			this.items = [], this.containers = [this];
			var c, d, e, f, g, h, i, j, k = this.items, l = [[a.isFunction(this.options.items) ? this.options.items.call(this.element[0], b, {item: this.currentItem}) : a(this.options.items, this.element), this]], m = this._connectWith();
			if (m && this.ready)for (c = m.length - 1; c >= 0; c--)for (e = a(m[c]), d = e.length - 1; d >= 0; d--)f = a.data(e[d], this.widgetFullName), f && f !== this && !f.options.disabled && (l.push([a.isFunction(f.options.items) ? f.options.items.call(f.element[0], b, {item: this.currentItem}) : a(f.options.items, f.element), f]), this.containers.push(f));
			for (c = l.length - 1; c >= 0; c--)for (g = l[c][1], h = l[c][0], d = 0, j = h.length; j > d; d++)i = a(h[d]), i.data(this.widgetName + "-item", g), k.push({
				item: i,
				instance: g,
				width: 0,
				height: 0,
				left: 0,
				top: 0
			})
		},
		refreshPositions: function (b) {
			this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
			var c, d, e, f;
			for (c = this.items.length - 1; c >= 0; c--)d = this.items[c], d.instance !== this.currentContainer && this.currentContainer && d.item[0] !== this.currentItem[0] || (e = this.options.toleranceElement ? a(this.options.toleranceElement, d.item) : d.item, b || (d.width = e.outerWidth(), d.height = e.outerHeight()), f = e.offset(), d.left = f.left, d.top = f.top);
			if (this.options.custom && this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this); else for (c = this.containers.length - 1; c >= 0; c--)f = this.containers[c].element.offset(), this.containers[c].containerCache.left = f.left, this.containers[c].containerCache.top = f.top, this.containers[c].containerCache.width = this.containers[c].element.outerWidth(), this.containers[c].containerCache.height = this.containers[c].element.outerHeight();
			return this
		},
		_createPlaceholder: function (b) {
			b = b || this;
			var c, d = b.options;
			d.placeholder && d.placeholder.constructor !== String || (c = d.placeholder, d.placeholder = {
				element: function () {
					var d = b.currentItem[0].nodeName.toLowerCase(), e = a("<" + d + ">", b.document[0]).addClass(c || b.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
					return "tr" === d ? b.currentItem.children().each(function () {
						a("<td>&#160;</td>", b.document[0]).attr("colspan", a(this).attr("colspan") || 1).appendTo(e)
					}) : "img" === d && e.attr("src", b.currentItem.attr("src")), c || e.css("visibility", "hidden"), e
				}, update: function (a, e) {
					(!c || d.forcePlaceholderSize) && (e.height() || e.height(b.currentItem.innerHeight() - parseInt(b.currentItem.css("paddingTop") || 0, 10) - parseInt(b.currentItem.css("paddingBottom") || 0, 10)), e.width() || e.width(b.currentItem.innerWidth() - parseInt(b.currentItem.css("paddingLeft") || 0, 10) - parseInt(b.currentItem.css("paddingRight") || 0, 10)))
				}
			}), b.placeholder = a(d.placeholder.element.call(b.element, b.currentItem)), b.currentItem.after(b.placeholder), d.placeholder.update(b, b.placeholder)
		},
		_contactContainers: function (b) {
			var c, d, e, f, g, h, i, j, k, l, m = null, n = null;
			for (c = this.containers.length - 1; c >= 0; c--)if (!a.contains(this.currentItem[0], this.containers[c].element[0]))if (this._intersectsWith(this.containers[c].containerCache)) {
				if (m && a.contains(this.containers[c].element[0], m.element[0]))continue;
				m = this.containers[c], n = c
			} else this.containers[c].containerCache.over && (this.containers[c]._trigger("out", b, this._uiHash(this)), this.containers[c].containerCache.over = 0);
			if (m)if (1 === this.containers.length)this.containers[n].containerCache.over || (this.containers[n]._trigger("over", b, this._uiHash(this)), this.containers[n].containerCache.over = 1); else {
				for (e = 1e4, f = null, k = m.floating || this._isFloating(this.currentItem), g = k ? "left" : "top", h = k ? "width" : "height", l = k ? "clientX" : "clientY", d = this.items.length - 1; d >= 0; d--)a.contains(this.containers[n].element[0], this.items[d].item[0]) && this.items[d].item[0] !== this.currentItem[0] && (i = this.items[d].item.offset()[g], j = !1, b[l] - i > this.items[d][h] / 2 && (j = !0), e > Math.abs(b[l] - i) && (e = Math.abs(b[l] - i), f = this.items[d], this.direction = j ? "up" : "down"));
				if (!f && !this.options.dropOnEmpty)return;
				if (this.currentContainer === this.containers[n])return;
				f ? this._rearrange(b, f, null, !0) : this._rearrange(b, null, this.containers[n].element, !0), this._trigger("change", b, this._uiHash()), this.containers[n]._trigger("change", b, this._uiHash(this)), this.currentContainer = this.containers[n], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[n]._trigger("over", b, this._uiHash(this)), this.containers[n].containerCache.over = 1
			}
		},
		_createHelper: function (b) {
			var c = this.options, d = a.isFunction(c.helper) ? a(c.helper.apply(this.element[0], [b, this.currentItem])) : "clone" === c.helper ? this.currentItem.clone() : this.currentItem;
			return d.parents("body").length || a("parent" !== c.appendTo ? c.appendTo : this.currentItem[0].parentNode)[0].appendChild(d[0]), d[0] === this.currentItem[0] && (this._storedCSS = {
				width: this.currentItem[0].style.width,
				height: this.currentItem[0].style.height,
				position: this.currentItem.css("position"),
				top: this.currentItem.css("top"),
				left: this.currentItem.css("left")
			}), (!d[0].style.width || c.forceHelperSize) && d.width(this.currentItem.width()), (!d[0].style.height || c.forceHelperSize) && d.height(this.currentItem.height()), d
		},
		_adjustOffsetFromHelper: function (b) {
			"string" == typeof b && (b = b.split(" ")), a.isArray(b) && (b = {
				left: +b[0],
				top: +b[1] || 0
			}), "left" in b && (this.offset.click.left = b.left + this.margins.left), "right" in b && (this.offset.click.left = this.helperProportions.width - b.right + this.margins.left), "top" in b && (this.offset.click.top = b.top + this.margins.top), "bottom" in b && (this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top)
		},
		_getParentOffset: function () {
			this.offsetParent = this.helper.offsetParent();
			var b = this.offsetParent.offset();
			return "absolute" === this.cssPosition && this.scrollParent[0] !== document && a.contains(this.scrollParent[0], this.offsetParent[0]) && (b.left += this.scrollParent.scrollLeft(), b.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && a.ui.ie) && (b = {
				top: 0,
				left: 0
			}), {
				top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
				left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
			}
		},
		_getRelativeOffset: function () {
			if ("relative" === this.cssPosition) {
				var a = this.currentItem.position();
				return {
					top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
					left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
				}
			}
			return {top: 0, left: 0}
		},
		_cacheMargins: function () {
			this.margins = {
				left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
				top: parseInt(this.currentItem.css("marginTop"), 10) || 0
			}
		},
		_cacheHelperProportions: function () {
			this.helperProportions = {width: this.helper.outerWidth(), height: this.helper.outerHeight()}
		},
		_setContainment: function () {
			var b, c, d, e = this.options;
			"parent" === e.containment && (e.containment = this.helper[0].parentNode), ("document" === e.containment || "window" === e.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, a("document" === e.containment ? document : window).width() - this.helperProportions.width - this.margins.left, (a("document" === e.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(e.containment) || (b = a(e.containment)[0], c = a(e.containment).offset(), d = "hidden" !== a(b).css("overflow"), this.containment = [c.left + (parseInt(a(b).css("borderLeftWidth"), 10) || 0) + (parseInt(a(b).css("paddingLeft"), 10) || 0) - this.margins.left, c.top + (parseInt(a(b).css("borderTopWidth"), 10) || 0) + (parseInt(a(b).css("paddingTop"), 10) || 0) - this.margins.top, c.left + (d ? Math.max(b.scrollWidth, b.offsetWidth) : b.offsetWidth) - (parseInt(a(b).css("borderLeftWidth"), 10) || 0) - (parseInt(a(b).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, c.top + (d ? Math.max(b.scrollHeight, b.offsetHeight) : b.offsetHeight) - (parseInt(a(b).css("borderTopWidth"), 10) || 0) - (parseInt(a(b).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top])
		},
		_convertPositionTo: function (b, c) {
			c || (c = this.position);
			var d = "absolute" === b ? 1 : -1, e = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && a.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, f = /(html|body)/i.test(e[0].tagName);
			return {
				top: c.top + this.offset.relative.top * d + this.offset.parent.top * d - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : f ? 0 : e.scrollTop()) * d,
				left: c.left + this.offset.relative.left * d + this.offset.parent.left * d - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : f ? 0 : e.scrollLeft()) * d
			}
		},
		_generatePosition: function (b) {
			var c, d, e = this.options, f = b.pageX, g = b.pageY, h = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && a.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, i = /(html|body)/i.test(h[0].tagName);
			return "relative" !== this.cssPosition || this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (b.pageX - this.offset.click.left < this.containment[0] && (f = this.containment[0] + this.offset.click.left), b.pageY - this.offset.click.top < this.containment[1] && (g = this.containment[1] + this.offset.click.top), b.pageX - this.offset.click.left > this.containment[2] && (f = this.containment[2] + this.offset.click.left), b.pageY - this.offset.click.top > this.containment[3] && (g = this.containment[3] + this.offset.click.top)), e.grid && (c = this.originalPageY + Math.round((g - this.originalPageY) / e.grid[1]) * e.grid[1], g = this.containment ? c - this.offset.click.top >= this.containment[1] && c - this.offset.click.top <= this.containment[3] ? c : c - this.offset.click.top >= this.containment[1] ? c - e.grid[1] : c + e.grid[1] : c, d = this.originalPageX + Math.round((f - this.originalPageX) / e.grid[0]) * e.grid[0], f = this.containment ? d - this.offset.click.left >= this.containment[0] && d - this.offset.click.left <= this.containment[2] ? d : d - this.offset.click.left >= this.containment[0] ? d - e.grid[0] : d + e.grid[0] : d)), {
				top: g - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : i ? 0 : h.scrollTop()),
				left: f - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : i ? 0 : h.scrollLeft())
			}
		},
		_rearrange: function (a, b, c, d) {
			c ? c[0].appendChild(this.placeholder[0]) : b.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? b.item[0] : b.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
			var e = this.counter;
			this._delay(function () {
				e === this.counter && this.refreshPositions(!d)
			})
		},
		_clear: function (a, b) {
			function c(a, b, c) {
				return function (d) {
					c._trigger(a, d, b._uiHash(b))
				}
			}

			this.reverting = !1;
			var d, e = [];
			if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
				for (d in this._storedCSS)("auto" === this._storedCSS[d] || "static" === this._storedCSS[d]) && (this._storedCSS[d] = "");
				this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
			} else this.currentItem.show();
			for (this.fromOutside && !b && e.push(function (a) {
				this._trigger("receive", a, this._uiHash(this.fromOutside))
			}), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || b || e.push(function (a) {
				this._trigger("update", a, this._uiHash())
			}), this !== this.currentContainer && (b || (e.push(function (a) {
				this._trigger("remove", a, this._uiHash())
			}), e.push(function (a) {
				return function (b) {
					a._trigger("receive", b, this._uiHash(this))
				}
			}.call(this, this.currentContainer)), e.push(function (a) {
				return function (b) {
					a._trigger("update", b, this._uiHash(this))
				}
			}.call(this, this.currentContainer)))), d = this.containers.length - 1; d >= 0; d--)b || e.push(c("deactivate", this, this.containers[d])), this.containers[d].containerCache.over && (e.push(c("out", this, this.containers[d])), this.containers[d].containerCache.over = 0);
			if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, this.cancelHelperRemoval) {
				if (!b) {
					for (this._trigger("beforeStop", a, this._uiHash()), d = 0; e.length > d; d++)e[d].call(this, a);
					this._trigger("stop", a, this._uiHash())
				}
				return this.fromOutside = !1, !1
			}
			if (b || this._trigger("beforeStop", a, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null, !b) {
				for (d = 0; e.length > d; d++)e[d].call(this, a);
				this._trigger("stop", a, this._uiHash())
			}
			return this.fromOutside = !1, !0
		},
		_trigger: function () {
			a.Widget.prototype._trigger.apply(this, arguments) === !1 && this.cancel()
		},
		_uiHash: function (b) {
			var c = b || this;
			return {
				helper: c.helper,
				placeholder: c.placeholder || a([]),
				position: c.position,
				originalPosition: c.originalPosition,
				offset: c.positionAbs,
				item: c.currentItem,
				sender: b ? b.element : null
			}
		}
	})
}), function (a) {
	function b(a, b, c) {
		switch (arguments.length) {
			case 2:
				return null != a ? a : b;
			case 3:
				return null != a ? a : null != b ? b : c;
			default:
				throw new Error("Implement me")
		}
	}

	function c() {
		return {
			empty: !1,
			unusedTokens: [],
			unusedInput: [],
			overflow: -2,
			charsLeftOver: 0,
			nullInput: !1,
			invalidMonth: null,
			invalidFormat: !1,
			userInvalidated: !1,
			iso: !1
		}
	}

	function d(a, b) {
		function c() {
			mb.suppressDeprecationWarnings === !1 && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + a)
		}

		var d = !0;
		return j(function () {
			return d && (c(), d = !1), b.apply(this, arguments)
		}, b)
	}

	function e(a, b) {
		return function (c) {
			return m(a.call(this, c), b)
		}
	}

	function f(a, b) {
		return function (c) {
			return this.lang().ordinal(a.call(this, c), b)
		}
	}

	function g() {
	}

	function h(a) {
		z(a), j(this, a)
	}

	function i(a) {
		var b = s(a), c = b.year || 0, d = b.quarter || 0, e = b.month || 0, f = b.week || 0, g = b.day || 0, h = b.hour || 0, i = b.minute || 0, j = b.second || 0, k = b.millisecond || 0;
		this._milliseconds = +k + 1e3 * j + 6e4 * i + 36e5 * h, this._days = +g + 7 * f, this._months = +e + 3 * d + 12 * c, this._data = {}, this._bubble()
	}

	function j(a, b) {
		for (var c in b)b.hasOwnProperty(c) && (a[c] = b[c]);
		return b.hasOwnProperty("toString") && (a.toString = b.toString), b.hasOwnProperty("valueOf") && (a.valueOf = b.valueOf), a
	}

	function k(a) {
		var b, c = {};
		for (b in a)a.hasOwnProperty(b) && Ab.hasOwnProperty(b) && (c[b] = a[b]);
		return c
	}

	function l(a) {
		return 0 > a ? Math.ceil(a) : Math.floor(a)
	}

	function m(a, b, c) {
		for (var d = "" + Math.abs(a), e = a >= 0; d.length < b;)d = "0" + d;
		return (e ? c ? "+" : "" : "-") + d
	}

	function n(a, b, c, d) {
		var e = b._milliseconds, f = b._days, g = b._months;
		d = null == d ? !0 : d, e && a._d.setTime(+a._d + e * c), f && hb(a, "Date", gb(a, "Date") + f * c), g && fb(a, gb(a, "Month") + g * c), d && mb.updateOffset(a, f || g)
	}

	function o(a) {
		return "[object Array]" === Object.prototype.toString.call(a)
	}

	function p(a) {
		return "[object Date]" === Object.prototype.toString.call(a) || a instanceof Date
	}

	function q(a, b, c) {
		var d, e = Math.min(a.length, b.length), f = Math.abs(a.length - b.length), g = 0;
		for (d = 0; e > d; d++)(c && a[d] !== b[d] || !c && u(a[d]) !== u(b[d])) && g++;
		return g + f
	}

	function r(a) {
		if (a) {
			var b = a.toLowerCase().replace(/(.)s$/, "$1");
			a = bc[a] || cc[b] || b
		}
		return a
	}

	function s(a) {
		var b, c, d = {};
		for (c in a)a.hasOwnProperty(c) && (b = r(c), b && (d[b] = a[c]));
		return d
	}

	function t(b) {
		var c, d;
		if (0 === b.indexOf("week"))c = 7, d = "day"; else {
			if (0 !== b.indexOf("month"))return;
			c = 12, d = "month"
		}
		mb[b] = function (e, f) {
			var g, h, i = mb.fn._lang[b], j = [];
			if ("number" == typeof e && (f = e, e = a), h = function (a) {
					var b = mb().utc().set(d, a);
					return i.call(mb.fn._lang, b, e || "")
				}, null != f)return h(f);
			for (g = 0; c > g; g++)j.push(h(g));
			return j
		}
	}

	function u(a) {
		var b = +a, c = 0;
		return 0 !== b && isFinite(b) && (c = b >= 0 ? Math.floor(b) : Math.ceil(b)), c
	}

	function v(a, b) {
		return new Date(Date.UTC(a, b + 1, 0)).getUTCDate()
	}

	function w(a, b, c) {
		return bb(mb([a, 11, 31 + b - c]), b, c).week
	}

	function x(a) {
		return y(a) ? 366 : 365
	}

	function y(a) {
		return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0
	}

	function z(a) {
		var b;
		a._a && -2 === a._pf.overflow && (b = a._a[tb] < 0 || a._a[tb] > 11 ? tb : a._a[ub] < 1 || a._a[ub] > v(a._a[sb], a._a[tb]) ? ub : a._a[vb] < 0 || a._a[vb] > 23 ? vb : a._a[wb] < 0 || a._a[wb] > 59 ? wb : a._a[xb] < 0 || a._a[xb] > 59 ? xb : a._a[yb] < 0 || a._a[yb] > 999 ? yb : -1, a._pf._overflowDayOfYear && (sb > b || b > ub) && (b = ub), a._pf.overflow = b)
	}

	function A(a) {
		return null == a._isValid && (a._isValid = !isNaN(a._d.getTime()) && a._pf.overflow < 0 && !a._pf.empty && !a._pf.invalidMonth && !a._pf.nullInput && !a._pf.invalidFormat && !a._pf.userInvalidated, a._strict && (a._isValid = a._isValid && 0 === a._pf.charsLeftOver && 0 === a._pf.unusedTokens.length)), a._isValid
	}

	function B(a) {
		return a ? a.toLowerCase().replace("_", "-") : a
	}

	function C(a, b) {
		return b._isUTC ? mb(a).zone(b._offset || 0) : mb(a).local()
	}

	function D(a, b) {
		return b.abbr = a, zb[a] || (zb[a] = new g), zb[a].set(b), zb[a]
	}

	function E(a) {
		delete zb[a]
	}

	function F(a) {
		var b, c, d, e, f = 0, g = function (a) {
			if (!zb[a] && Bb)try {
				require("./lang/" + a)
			} catch (b) {
			}
			return zb[a]
		};
		if (!a)return mb.fn._lang;
		if (!o(a)) {
			if (c = g(a))return c;
			a = [a]
		}
		for (; f < a.length;) {
			for (e = B(a[f]).split("-"), b = e.length, d = B(a[f + 1]), d = d ? d.split("-") : null; b > 0;) {
				if (c = g(e.slice(0, b).join("-")))return c;
				if (d && d.length >= b && q(e, d, !0) >= b - 1)break;
				b--
			}
			f++
		}
		return mb.fn._lang
	}

	function G(a) {
		return a.match(/\[[\s\S]/) ? a.replace(/^\[|\]$/g, "") : a.replace(/\\/g, "")
	}

	function H(a) {
		var b, c, d = a.match(Fb);
		for (b = 0, c = d.length; c > b; b++)d[b] = hc[d[b]] ? hc[d[b]] : G(d[b]);
		return function (e) {
			var f = "";
			for (b = 0; c > b; b++)f += d[b] instanceof Function ? d[b].call(e, a) : d[b];
			return f
		}
	}

	function I(a, b) {
		return a.isValid() ? (b = J(b, a.lang()), dc[b] || (dc[b] = H(b)), dc[b](a)) : a.lang().invalidDate()
	}

	function J(a, b) {
		function c(a) {
			return b.longDateFormat(a) || a
		}

		var d = 5;
		for (Gb.lastIndex = 0; d >= 0 && Gb.test(a);)a = a.replace(Gb, c), Gb.lastIndex = 0, d -= 1;
		return a
	}

	function K(a, b) {
		var c, d = b._strict;
		switch (a) {
			case"Q":
				return Rb;
			case"DDDD":
				return Tb;
			case"YYYY":
			case"GGGG":
			case"gggg":
				return d ? Ub : Jb;
			case"Y":
			case"G":
			case"g":
				return Wb;
			case"YYYYYY":
			case"YYYYY":
			case"GGGGG":
			case"ggggg":
				return d ? Vb : Kb;
			case"S":
				if (d)return Rb;
			case"SS":
				if (d)return Sb;
			case"SSS":
				if (d)return Tb;
			case"DDD":
				return Ib;
			case"MMM":
			case"MMMM":
			case"dd":
			case"ddd":
			case"dddd":
				return Mb;
			case"a":
			case"A":
				return F(b._l)._meridiemParse;
			case"X":
				return Pb;
			case"Z":
			case"ZZ":
				return Nb;
			case"T":
				return Ob;
			case"SSSS":
				return Lb;
			case"MM":
			case"DD":
			case"YY":
			case"GG":
			case"gg":
			case"HH":
			case"hh":
			case"mm":
			case"ss":
			case"ww":
			case"WW":
				return d ? Sb : Hb;
			case"M":
			case"D":
			case"d":
			case"H":
			case"h":
			case"m":
			case"s":
			case"w":
			case"W":
			case"e":
			case"E":
				return Hb;
			case"Do":
				return Qb;
			default:
				return c = new RegExp(T(S(a.replace("\\", "")), "i"))
		}
	}

	function L(a) {
		a = a || "";
		var b = a.match(Nb) || [], c = b[b.length - 1] || [], d = (c + "").match(_b) || ["-", 0, 0], e = +(60 * d[1]) + u(d[2]);
		return "+" === d[0] ? -e : e
	}

	function M(a, b, c) {
		var d, e = c._a;
		switch (a) {
			case"Q":
				null != b && (e[tb] = 3 * (u(b) - 1));
				break;
			case"M":
			case"MM":
				null != b && (e[tb] = u(b) - 1);
				break;
			case"MMM":
			case"MMMM":
				d = F(c._l).monthsParse(b), null != d ? e[tb] = d : c._pf.invalidMonth = b;
				break;
			case"D":
			case"DD":
				null != b && (e[ub] = u(b));
				break;
			case"Do":
				null != b && (e[ub] = u(parseInt(b, 10)));
				break;
			case"DDD":
			case"DDDD":
				null != b && (c._dayOfYear = u(b));
				break;
			case"YY":
				e[sb] = mb.parseTwoDigitYear(b);
				break;
			case"YYYY":
			case"YYYYY":
			case"YYYYYY":
				e[sb] = u(b);
				break;
			case"a":
			case"A":
				c._isPm = F(c._l).isPM(b);
				break;
			case"H":
			case"HH":
			case"h":
			case"hh":
				e[vb] = u(b);
				break;
			case"m":
			case"mm":
				e[wb] = u(b);
				break;
			case"s":
			case"ss":
				e[xb] = u(b);
				break;
			case"S":
			case"SS":
			case"SSS":
			case"SSSS":
				e[yb] = u(1e3 * ("0." + b));
				break;
			case"X":
				c._d = new Date(1e3 * parseFloat(b));
				break;
			case"Z":
			case"ZZ":
				c._useUTC = !0, c._tzm = L(b);
				break;
			case"dd":
			case"ddd":
			case"dddd":
				d = F(c._l).weekdaysParse(b), null != d ? (c._w = c._w || {}, c._w.d = d) : c._pf.invalidWeekday = b;
				break;
			case"w":
			case"ww":
			case"W":
			case"WW":
			case"d":
			case"e":
			case"E":
				a = a.substr(0, 1);
			case"gggg":
			case"GGGG":
			case"GGGGG":
				a = a.substr(0, 2), b && (c._w = c._w || {}, c._w[a] = u(b));
				break;
			case"gg":
			case"GG":
				c._w = c._w || {}, c._w[a] = mb.parseTwoDigitYear(b)
		}
	}

	function N(a) {
		var c, d, e, f, g, h, i, j;
		c = a._w, null != c.GG || null != c.W || null != c.E ? (g = 1, h = 4, d = b(c.GG, a._a[sb], bb(mb(), 1, 4).year), e = b(c.W, 1), f = b(c.E, 1)) : (j = F(a._l), g = j._week.dow, h = j._week.doy, d = b(c.gg, a._a[sb], bb(mb(), g, h).year), e = b(c.w, 1), null != c.d ? (f = c.d, g > f && ++e) : f = null != c.e ? c.e + g : g), i = cb(d, e, f, h, g), a._a[sb] = i.year, a._dayOfYear = i.dayOfYear
	}

	function O(a) {
		var c, d, e, f, g = [];
		if (!a._d) {
			for (e = Q(a), a._w && null == a._a[ub] && null == a._a[tb] && N(a), a._dayOfYear && (f = b(a._a[sb], e[sb]), a._dayOfYear > x(f) && (a._pf._overflowDayOfYear = !0), d = Z(f, 0, a._dayOfYear), a._a[tb] = d.getUTCMonth(), a._a[ub] = d.getUTCDate()), c = 0; 3 > c && null == a._a[c]; ++c)a._a[c] = g[c] = e[c];
			for (; 7 > c; c++)a._a[c] = g[c] = null == a._a[c] ? 2 === c ? 1 : 0 : a._a[c];
			a._d = (a._useUTC ? Z : Y).apply(null, g), null != a._tzm && a._d.setUTCMinutes(a._d.getUTCMinutes() + a._tzm)
		}
	}

	function P(a) {
		var b;
		a._d || (b = s(a._i), a._a = [b.year, b.month, b.day, b.hour, b.minute, b.second, b.millisecond], O(a))
	}

	function Q(a) {
		var b = new Date;
		return a._useUTC ? [b.getUTCFullYear(), b.getUTCMonth(), b.getUTCDate()] : [b.getFullYear(), b.getMonth(), b.getDate()]
	}

	function R(a) {
		if (a._f === mb.ISO_8601)return void V(a);
		a._a = [], a._pf.empty = !0;
		var b, c, d, e, f, g = F(a._l), h = "" + a._i, i = h.length, j = 0;
		for (d = J(a._f, g).match(Fb) || [], b = 0; b < d.length; b++)e = d[b], c = (h.match(K(e, a)) || [])[0], c && (f = h.substr(0, h.indexOf(c)), f.length > 0 && a._pf.unusedInput.push(f), h = h.slice(h.indexOf(c) + c.length), j += c.length), hc[e] ? (c ? a._pf.empty = !1 : a._pf.unusedTokens.push(e), M(e, c, a)) : a._strict && !c && a._pf.unusedTokens.push(e);
		a._pf.charsLeftOver = i - j, h.length > 0 && a._pf.unusedInput.push(h), a._isPm && a._a[vb] < 12 && (a._a[vb] += 12), a._isPm === !1 && 12 === a._a[vb] && (a._a[vb] = 0), O(a), z(a)
	}

	function S(a) {
		return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (a, b, c, d, e) {
			return b || c || d || e
		})
	}

	function T(a) {
		return a.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
	}

	function U(a) {
		var b, d, e, f, g;
		if (0 === a._f.length)return a._pf.invalidFormat = !0, void(a._d = new Date(0 / 0));
		for (f = 0; f < a._f.length; f++)g = 0, b = j({}, a), b._pf = c(), b._f = a._f[f], R(b), A(b) && (g += b._pf.charsLeftOver, g += 10 * b._pf.unusedTokens.length, b._pf.score = g, (null == e || e > g) && (e = g, d = b));
		j(a, d || b)
	}

	function V(a) {
		var b, c, d = a._i, e = Xb.exec(d);
		if (e) {
			for (a._pf.iso = !0, b = 0, c = Zb.length; c > b; b++)if (Zb[b][1].exec(d)) {
				a._f = Zb[b][0] + (e[6] || " ");
				break
			}
			for (b = 0, c = $b.length; c > b; b++)if ($b[b][1].exec(d)) {
				a._f += $b[b][0];
				break
			}
			d.match(Nb) && (a._f += "Z"), R(a)
		} else a._isValid = !1
	}

	function W(a) {
		V(a), a._isValid === !1 && (delete a._isValid, mb.createFromInputFallback(a))
	}

	function X(b) {
		var c = b._i, d = Cb.exec(c);
		c === a ? b._d = new Date : d ? b._d = new Date(+d[1]) : "string" == typeof c ? W(b) : o(c) ? (b._a = c.slice(0), O(b)) : p(c) ? b._d = new Date(+c) : "object" == typeof c ? P(b) : "number" == typeof c ? b._d = new Date(c) : mb.createFromInputFallback(b)
	}

	function Y(a, b, c, d, e, f, g) {
		var h = new Date(a, b, c, d, e, f, g);
		return 1970 > a && h.setFullYear(a), h
	}

	function Z(a) {
		var b = new Date(Date.UTC.apply(null, arguments));
		return 1970 > a && b.setUTCFullYear(a), b
	}

	function $(a, b) {
		if ("string" == typeof a)if (isNaN(a)) {
			if (a = b.weekdaysParse(a), "number" != typeof a)return null
		} else a = parseInt(a, 10);
		return a
	}

	function _(a, b, c, d, e) {
		return e.relativeTime(b || 1, !!c, a, d)
	}

	function ab(a, b, c) {
		var d = rb(Math.abs(a) / 1e3), e = rb(d / 60), f = rb(e / 60), g = rb(f / 24), h = rb(g / 365), i = d < ec.s && ["s", d] || 1 === e && ["m"] || e < ec.m && ["mm", e] || 1 === f && ["h"] || f < ec.h && ["hh", f] || 1 === g && ["d"] || g <= ec.dd && ["dd", g] || g <= ec.dm && ["M"] || g < ec.dy && ["MM", rb(g / 30)] || 1 === h && ["y"] || ["yy", h];
		return i[2] = b, i[3] = a > 0, i[4] = c, _.apply({}, i)
	}

	function bb(a, b, c) {
		var d, e = c - b, f = c - a.day();
		return f > e && (f -= 7), e - 7 > f && (f += 7), d = mb(a).add("d", f), {
			week: Math.ceil(d.dayOfYear() / 7),
			year: d.year()
		}
	}

	function cb(a, b, c, d, e) {
		var f, g, h = Z(a, 0, 1).getUTCDay();
		return h = 0 === h ? 7 : h, c = null != c ? c : e, f = e - h + (h > d ? 7 : 0) - (e > h ? 7 : 0), g = 7 * (b - 1) + (c - e) + f + 1, {
			year: g > 0 ? a : a - 1,
			dayOfYear: g > 0 ? g : x(a - 1) + g
		}
	}

	function db(b) {
		var c = b._i, d = b._f;
		return null === c || d === a && "" === c ? mb.invalid({nullInput: !0}) : ("string" == typeof c && (b._i = c = F().preparse(c)), mb.isMoment(c) ? (b = k(c), b._d = new Date(+c._d)) : d ? o(d) ? U(b) : R(b) : X(b), new h(b))
	}

	function eb(a, b) {
		var c, d;
		if (1 === b.length && o(b[0]) && (b = b[0]), !b.length)return mb();
		for (c = b[0], d = 1; d < b.length; ++d)b[d][a](c) && (c = b[d]);
		return c
	}

	function fb(a, b) {
		var c;
		return "string" == typeof b && (b = a.lang().monthsParse(b), "number" != typeof b) ? a : (c = Math.min(a.date(), v(a.year(), b)), a._d["set" + (a._isUTC ? "UTC" : "") + "Month"](b, c), a)
	}

	function gb(a, b) {
		return a._d["get" + (a._isUTC ? "UTC" : "") + b]()
	}

	function hb(a, b, c) {
		return "Month" === b ? fb(a, c) : a._d["set" + (a._isUTC ? "UTC" : "") + b](c)
	}

	function ib(a, b) {
		return function (c) {
			return null != c ? (hb(this, a, c), mb.updateOffset(this, b), this) : gb(this, a)
		}
	}

	function jb(a) {
		mb.duration.fn[a] = function () {
			return this._data[a]
		}
	}

	function kb(a, b) {
		mb.duration.fn["as" + a] = function () {
			return +this / b
		}
	}

	function lb(a) {
		"undefined" == typeof ender && (nb = qb.moment, qb.moment = a ? d("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.", mb) : mb)
	}

	for (var mb, nb, ob, pb = "2.7.0", qb = "undefined" != typeof global ? global : this, rb = Math.round, sb = 0, tb = 1, ub = 2, vb = 3, wb = 4, xb = 5, yb = 6, zb = {}, Ab = {
		_isAMomentObject: null,
		_i: null,
		_f: null,
		_l: null,
		_strict: null,
		_tzm: null,
		_isUTC: null,
		_offset: null,
		_pf: null,
		_lang: null
	}, Bb = "undefined" != typeof module && module.exports, Cb = /^\/?Date\((\-?\d+)/i, Db = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/, Eb = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/, Fb = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g, Gb = /(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g, Hb = /\d\d?/, Ib = /\d{1,3}/, Jb = /\d{1,4}/, Kb = /[+\-]?\d{1,6}/, Lb = /\d+/, Mb = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, Nb = /Z|[\+\-]\d\d:?\d\d/gi, Ob = /T/i, Pb = /[\+\-]?\d+(\.\d{1,3})?/, Qb = /\d{1,2}/, Rb = /\d/, Sb = /\d\d/, Tb = /\d{3}/, Ub = /\d{4}/, Vb = /[+-]?\d{6}/, Wb = /[+-]?\d+/, Xb = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/, Yb = "YYYY-MM-DDTHH:mm:ssZ", Zb = [["YYYYYY-MM-DD", /[+-]\d{6}-\d{2}-\d{2}/], ["YYYY-MM-DD", /\d{4}-\d{2}-\d{2}/], ["GGGG-[W]WW-E", /\d{4}-W\d{2}-\d/], ["GGGG-[W]WW", /\d{4}-W\d{2}/], ["YYYY-DDD", /\d{4}-\d{3}/]], $b = [["HH:mm:ss.SSSS", /(T| )\d\d:\d\d:\d\d\.\d+/], ["HH:mm:ss", /(T| )\d\d:\d\d:\d\d/], ["HH:mm", /(T| )\d\d:\d\d/], ["HH", /(T| )\d\d/]], _b = /([\+\-]|\d\d)/gi, ac = ("Date|Hours|Minutes|Seconds|Milliseconds".split("|"), {
		Milliseconds: 1,
		Seconds: 1e3,
		Minutes: 6e4,
		Hours: 36e5,
		Days: 864e5,
		Months: 2592e6,
		Years: 31536e6
	}), bc = {
		ms: "millisecond",
		s: "second",
		m: "minute",
		h: "hour",
		d: "day",
		D: "date",
		w: "week",
		W: "isoWeek",
		M: "month",
		Q: "quarter",
		y: "year",
		DDD: "dayOfYear",
		e: "weekday",
		E: "isoWeekday",
		gg: "weekYear",
		GG: "isoWeekYear"
	}, cc = {
		dayofyear: "dayOfYear",
		isoweekday: "isoWeekday",
		isoweek: "isoWeek",
		weekyear: "weekYear",
		isoweekyear: "isoWeekYear"
	}, dc = {}, ec = {
		s: 45,
		m: 45,
		h: 22,
		dd: 25,
		dm: 45,
		dy: 345
	}, fc = "DDD w W M D d".split(" "), gc = "M D H h m s w W".split(" "), hc = {
		M: function () {
			return this.month() + 1
		}, MMM: function (a) {
			return this.lang().monthsShort(this, a)
		}, MMMM: function (a) {
			return this.lang().months(this, a)
		}, D: function () {
			return this.date()
		}, DDD: function () {
			return this.dayOfYear()
		}, d: function () {
			return this.day()
		}, dd: function (a) {
			return this.lang().weekdaysMin(this, a)
		}, ddd: function (a) {
			return this.lang().weekdaysShort(this, a)
		}, dddd: function (a) {
			return this.lang().weekdays(this, a)
		}, w: function () {
			return this.week()
		}, W: function () {
			return this.isoWeek()
		}, YY: function () {
			return m(this.year() % 100, 2)
		}, YYYY: function () {
			return m(this.year(), 4)
		}, YYYYY: function () {
			return m(this.year(), 5)
		}, YYYYYY: function () {
			var a = this.year(), b = a >= 0 ? "+" : "-";
			return b + m(Math.abs(a), 6)
		}, gg: function () {
			return m(this.weekYear() % 100, 2)
		}, gggg: function () {
			return m(this.weekYear(), 4)
		}, ggggg: function () {
			return m(this.weekYear(), 5)
		}, GG: function () {
			return m(this.isoWeekYear() % 100, 2)
		}, GGGG: function () {
			return m(this.isoWeekYear(), 4)
		}, GGGGG: function () {
			return m(this.isoWeekYear(), 5)
		}, e: function () {
			return this.weekday()
		}, E: function () {
			return this.isoWeekday()
		}, a: function () {
			return this.lang().meridiem(this.hours(), this.minutes(), !0)
		}, A: function () {
			return this.lang().meridiem(this.hours(), this.minutes(), !1)
		}, H: function () {
			return this.hours()
		}, h: function () {
			return this.hours() % 12 || 12
		}, m: function () {
			return this.minutes()
		}, s: function () {
			return this.seconds()
		}, S: function () {
			return u(this.milliseconds() / 100)
		}, SS: function () {
			return m(u(this.milliseconds() / 10), 2)
		}, SSS: function () {
			return m(this.milliseconds(), 3)
		}, SSSS: function () {
			return m(this.milliseconds(), 3)
		}, Z: function () {
			var a = -this.zone(), b = "+";
			return 0 > a && (a = -a, b = "-"), b + m(u(a / 60), 2) + ":" + m(u(a) % 60, 2)
		}, ZZ: function () {
			var a = -this.zone(), b = "+";
			return 0 > a && (a = -a, b = "-"), b + m(u(a / 60), 2) + m(u(a) % 60, 2)
		}, z: function () {
			return this.zoneAbbr()
		}, zz: function () {
			return this.zoneName()
		}, X: function () {
			return this.unix()
		}, Q: function () {
			return this.quarter()
		}
	}, ic = ["months", "monthsShort", "weekdays", "weekdaysShort", "weekdaysMin"]; fc.length;)ob = fc.pop(), hc[ob + "o"] = f(hc[ob], ob);
	for (; gc.length;)ob = gc.pop(), hc[ob + ob] = e(hc[ob], 2);
	for (hc.DDDD = e(hc.DDD, 3), j(g.prototype, {
		set: function (a) {
			var b, c;
			for (c in a)b = a[c], "function" == typeof b ? this[c] = b : this["_" + c] = b
		},
		_months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
		months: function (a) {
			return this._months[a.month()]
		},
		_monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
		monthsShort: function (a) {
			return this._monthsShort[a.month()]
		},
		monthsParse: function (a) {
			var b, c, d;
			for (this._monthsParse || (this._monthsParse = []), b = 0; 12 > b; b++)if (this._monthsParse[b] || (c = mb.utc([2e3, b]), d = "^" + this.months(c, "") + "|^" + this.monthsShort(c, ""), this._monthsParse[b] = new RegExp(d.replace(".", ""), "i")), this._monthsParse[b].test(a))return b
		},
		_weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
		weekdays: function (a) {
			return this._weekdays[a.day()]
		},
		_weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
		weekdaysShort: function (a) {
			return this._weekdaysShort[a.day()]
		},
		_weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
		weekdaysMin: function (a) {
			return this._weekdaysMin[a.day()]
		},
		weekdaysParse: function (a) {
			var b, c, d;
			for (this._weekdaysParse || (this._weekdaysParse = []), b = 0; 7 > b; b++)if (this._weekdaysParse[b] || (c = mb([2e3, 1]).day(b), d = "^" + this.weekdays(c, "") + "|^" + this.weekdaysShort(c, "") + "|^" + this.weekdaysMin(c, ""), this._weekdaysParse[b] = new RegExp(d.replace(".", ""), "i")), this._weekdaysParse[b].test(a))return b
		},
		_longDateFormat: {
			LT: "h:mm A",
			L: "MM/DD/YYYY",
			LL: "MMMM D YYYY",
			LLL: "MMMM D YYYY LT",
			LLLL: "dddd, MMMM D YYYY LT"
		},
		longDateFormat: function (a) {
			var b = this._longDateFormat[a];
			return !b && this._longDateFormat[a.toUpperCase()] && (b = this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function (a) {
				return a.slice(1)
			}), this._longDateFormat[a] = b), b
		},
		isPM: function (a) {
			return "p" === (a + "").toLowerCase().charAt(0)
		},
		_meridiemParse: /[ap]\.?m?\.?/i,
		meridiem: function (a, b, c) {
			return a > 11 ? c ? "pm" : "PM" : c ? "am" : "AM"
		},
		_calendar: {
			sameDay: "[Today at] LT",
			nextDay: "[Tomorrow at] LT",
			nextWeek: "dddd [at] LT",
			lastDay: "[Yesterday at] LT",
			lastWeek: "[Last] dddd [at] LT",
			sameElse: "L"
		},
		calendar: function (a, b) {
			var c = this._calendar[a];
			return "function" == typeof c ? c.apply(b) : c
		},
		_relativeTime: {
			future: "in %s",
			past: "%s ago",
			s: "a few seconds",
			m: "a minute",
			mm: "%d minutes",
			h: "an hour",
			hh: "%d hours",
			d: "a day",
			dd: "%d days",
			M: "a month",
			MM: "%d months",
			y: "a year",
			yy: "%d years"
		},
		relativeTime: function (a, b, c, d) {
			var e = this._relativeTime[c];
			return "function" == typeof e ? e(a, b, c, d) : e.replace(/%d/i, a)
		},
		pastFuture: function (a, b) {
			var c = this._relativeTime[a > 0 ? "future" : "past"];
			return "function" == typeof c ? c(b) : c.replace(/%s/i, b)
		},
		ordinal: function (a) {
			return this._ordinal.replace("%d", a)
		},
		_ordinal: "%d",
		preparse: function (a) {
			return a
		},
		postformat: function (a) {
			return a
		},
		week: function (a) {
			return bb(a, this._week.dow, this._week.doy).week
		},
		_week: {dow: 0, doy: 6},
		_invalidDate: "Invalid date",
		invalidDate: function () {
			return this._invalidDate
		}
	}), mb = function (b, d, e, f) {
		var g;
		return "boolean" == typeof e && (f = e, e = a), g = {}, g._isAMomentObject = !0, g._i = b, g._f = d, g._l = e, g._strict = f, g._isUTC = !1, g._pf = c(), db(g)
	}, mb.suppressDeprecationWarnings = !1, mb.createFromInputFallback = d("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.", function (a) {
		a._d = new Date(a._i)
	}), mb.min = function () {
		var a = [].slice.call(arguments, 0);
		return eb("isBefore", a)
	}, mb.max = function () {
		var a = [].slice.call(arguments, 0);
		return eb("isAfter", a)
	}, mb.utc = function (b, d, e, f) {
		var g;
		return "boolean" == typeof e && (f = e, e = a), g = {}, g._isAMomentObject = !0, g._useUTC = !0, g._isUTC = !0, g._l = e, g._i = b, g._f = d, g._strict = f, g._pf = c(), db(g).utc()
	}, mb.unix = function (a) {
		return mb(1e3 * a)
	}, mb.duration = function (a, b) {
		var c, d, e, f = a, g = null;
		return mb.isDuration(a) ? f = {
			ms: a._milliseconds,
			d: a._days,
			M: a._months
		} : "number" == typeof a ? (f = {}, b ? f[b] = a : f.milliseconds = a) : (g = Db.exec(a)) ? (c = "-" === g[1] ? -1 : 1, f = {
			y: 0,
			d: u(g[ub]) * c,
			h: u(g[vb]) * c,
			m: u(g[wb]) * c,
			s: u(g[xb]) * c,
			ms: u(g[yb]) * c
		}) : (g = Eb.exec(a)) && (c = "-" === g[1] ? -1 : 1, e = function (a) {
			var b = a && parseFloat(a.replace(",", "."));
			return (isNaN(b) ? 0 : b) * c
		}, f = {
			y: e(g[2]),
			M: e(g[3]),
			d: e(g[4]),
			h: e(g[5]),
			m: e(g[6]),
			s: e(g[7]),
			w: e(g[8])
		}), d = new i(f), mb.isDuration(a) && a.hasOwnProperty("_lang") && (d._lang = a._lang), d
	}, mb.version = pb, mb.defaultFormat = Yb, mb.ISO_8601 = function () {
	}, mb.momentProperties = Ab, mb.updateOffset = function () {
	}, mb.relativeTimeThreshold = function (b, c) {
		return ec[b] === a ? !1 : (ec[b] = c, !0)
	}, mb.lang = function (a, b) {
		var c;
		return a ? (b ? D(B(a), b) : null === b ? (E(a), a = "en") : zb[a] || F(a), c = mb.duration.fn._lang = mb.fn._lang = F(a), c._abbr) : mb.fn._lang._abbr
	}, mb.langData = function (a) {
		return a && a._lang && a._lang._abbr && (a = a._lang._abbr), F(a)
	}, mb.isMoment = function (a) {
		return a instanceof h || null != a && a.hasOwnProperty("_isAMomentObject")
	}, mb.isDuration = function (a) {
		return a instanceof i
	}, ob = ic.length - 1; ob >= 0; --ob)t(ic[ob]);
	mb.normalizeUnits = function (a) {
		return r(a)
	}, mb.invalid = function (a) {
		var b = mb.utc(0 / 0);
		return null != a ? j(b._pf, a) : b._pf.userInvalidated = !0, b
	}, mb.parseZone = function () {
		return mb.apply(null, arguments).parseZone()
	}, mb.parseTwoDigitYear = function (a) {
		return u(a) + (u(a) > 68 ? 1900 : 2e3)
	}, j(mb.fn = h.prototype, {
		clone: function () {
			return mb(this)
		},
		valueOf: function () {
			return +this._d + 6e4 * (this._offset || 0)
		},
		unix: function () {
			return Math.floor(+this / 1e3)
		},
		toString: function () {
			return this.clone().lang("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
		},
		toDate: function () {
			return this._offset ? new Date(+this) : this._d
		},
		toISOString: function () {
			var a = mb(this).utc();
			return 0 < a.year() && a.year() <= 9999 ? I(a, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : I(a, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
		},
		toArray: function () {
			var a = this;
			return [a.year(), a.month(), a.date(), a.hours(), a.minutes(), a.seconds(), a.milliseconds()]
		},
		isValid: function () {
			return A(this)
		},
		isDSTShifted: function () {
			return this._a ? this.isValid() && q(this._a, (this._isUTC ? mb.utc(this._a) : mb(this._a)).toArray()) > 0 : !1
		},
		parsingFlags: function () {
			return j({}, this._pf)
		},
		invalidAt: function () {
			return this._pf.overflow
		},
		utc: function () {
			return this.zone(0)
		},
		local: function () {
			return this.zone(0), this._isUTC = !1, this
		},
		format: function (a) {
			var b = I(this, a || mb.defaultFormat);
			return this.lang().postformat(b)
		},
		add: function (a, b) {
			var c;
			return c = "string" == typeof a && "string" == typeof b ? mb.duration(isNaN(+b) ? +a : +b, isNaN(+b) ? b : a) : "string" == typeof a ? mb.duration(+b, a) : mb.duration(a, b), n(this, c, 1), this
		},
		subtract: function (a, b) {
			var c;
			return c = "string" == typeof a && "string" == typeof b ? mb.duration(isNaN(+b) ? +a : +b, isNaN(+b) ? b : a) : "string" == typeof a ? mb.duration(+b, a) : mb.duration(a, b), n(this, c, -1), this
		},
		diff: function (a, b, c) {
			var d, e, f = C(a, this), g = 6e4 * (this.zone() - f.zone());
			return b = r(b), "year" === b || "month" === b ? (d = 432e5 * (this.daysInMonth() + f.daysInMonth()), e = 12 * (this.year() - f.year()) + (this.month() - f.month()), e += (this - mb(this).startOf("month") - (f - mb(f).startOf("month"))) / d, e -= 6e4 * (this.zone() - mb(this).startOf("month").zone() - (f.zone() - mb(f).startOf("month").zone())) / d, "year" === b && (e /= 12)) : (d = this - f, e = "second" === b ? d / 1e3 : "minute" === b ? d / 6e4 : "hour" === b ? d / 36e5 : "day" === b ? (d - g) / 864e5 : "week" === b ? (d - g) / 6048e5 : d), c ? e : l(e)
		},
		from: function (a, b) {
			return mb.duration(this.diff(a)).lang(this.lang()._abbr).humanize(!b)
		},
		fromNow: function (a) {
			return this.from(mb(), a)
		},
		calendar: function (a) {
			var b = a || mb(), c = C(b, this).startOf("day"), d = this.diff(c, "days", !0), e = -6 > d ? "sameElse" : -1 > d ? "lastWeek" : 0 > d ? "lastDay" : 1 > d ? "sameDay" : 2 > d ? "nextDay" : 7 > d ? "nextWeek" : "sameElse";
			return this.format(this.lang().calendar(e, this))
		},
		isLeapYear: function () {
			return y(this.year())
		},
		isDST: function () {
			return this.zone() < this.clone().month(0).zone() || this.zone() < this.clone().month(5).zone()
		},
		day: function (a) {
			var b = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
			return null != a ? (a = $(a, this.lang()), this.add({d: a - b})) : b
		},
		month: ib("Month", !0),
		startOf: function (a) {
			switch (a = r(a)) {
				case"year":
					this.month(0);
				case"quarter":
				case"month":
					this.date(1);
				case"week":
				case"isoWeek":
				case"day":
					this.hours(0);
				case"hour":
					this.minutes(0);
				case"minute":
					this.seconds(0);
				case"second":
					this.milliseconds(0)
			}
			return "week" === a ? this.weekday(0) : "isoWeek" === a && this.isoWeekday(1), "quarter" === a && this.month(3 * Math.floor(this.month() / 3)), this
		},
		endOf: function (a) {
			return a = r(a), this.startOf(a).add("isoWeek" === a ? "week" : a, 1).subtract("ms", 1)
		},
		isAfter: function (a, b) {
			return b = "undefined" != typeof b ? b : "millisecond", +this.clone().startOf(b) > +mb(a).startOf(b)
		},
		isBefore: function (a, b) {
			return b = "undefined" != typeof b ? b : "millisecond", +this.clone().startOf(b) < +mb(a).startOf(b)
		},
		isSame: function (a, b) {
			return b = b || "ms", +this.clone().startOf(b) === +C(a, this).startOf(b)
		},
		min: d("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548", function (a) {
			return a = mb.apply(null, arguments), this > a ? this : a
		}),
		max: d("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548", function (a) {
			return a = mb.apply(null, arguments), a > this ? this : a
		}),
		zone: function (a, b) {
			var c = this._offset || 0;
			return null == a ? this._isUTC ? c : this._d.getTimezoneOffset() : ("string" == typeof a && (a = L(a)), Math.abs(a) < 16 && (a = 60 * a), this._offset = a, this._isUTC = !0, c !== a && (!b || this._changeInProgress ? n(this, mb.duration(c - a, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, mb.updateOffset(this, !0), this._changeInProgress = null)), this)
		},
		zoneAbbr: function () {
			return this._isUTC ? "UTC" : ""
		},
		zoneName: function () {
			return this._isUTC ? "Coordinated Universal Time" : ""
		},
		parseZone: function () {
			return this._tzm ? this.zone(this._tzm) : "string" == typeof this._i && this.zone(this._i), this
		},
		hasAlignedHourOffset: function (a) {
			return a = a ? mb(a).zone() : 0, (this.zone() - a) % 60 === 0
		},
		daysInMonth: function () {
			return v(this.year(), this.month())
		},
		dayOfYear: function (a) {
			var b = rb((mb(this).startOf("day") - mb(this).startOf("year")) / 864e5) + 1;
			return null == a ? b : this.add("d", a - b)
		},
		quarter: function (a) {
			return null == a ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (a - 1) + this.month() % 3)
		},
		weekYear: function (a) {
			var b = bb(this, this.lang()._week.dow, this.lang()._week.doy).year;
			return null == a ? b : this.add("y", a - b)
		},
		isoWeekYear: function (a) {
			var b = bb(this, 1, 4).year;
			return null == a ? b : this.add("y", a - b)
		},
		week: function (a) {
			var b = this.lang().week(this);
			return null == a ? b : this.add("d", 7 * (a - b))
		},
		isoWeek: function (a) {
			var b = bb(this, 1, 4).week;
			return null == a ? b : this.add("d", 7 * (a - b))
		},
		weekday: function (a) {
			var b = (this.day() + 7 - this.lang()._week.dow) % 7;
			return null == a ? b : this.add("d", a - b)
		},
		isoWeekday: function (a) {
			return null == a ? this.day() || 7 : this.day(this.day() % 7 ? a : a - 7)
		},
		isoWeeksInYear: function () {
			return w(this.year(), 1, 4)
		},
		weeksInYear: function () {
			var a = this._lang._week;
			return w(this.year(), a.dow, a.doy)
		},
		get: function (a) {
			return a = r(a), this[a]()
		},
		set: function (a, b) {
			return a = r(a), "function" == typeof this[a] && this[a](b), this
		},
		lang: function (b) {
			return b === a ? this._lang : (this._lang = F(b), this)
		}
	}), mb.fn.millisecond = mb.fn.milliseconds = ib("Milliseconds", !1), mb.fn.second = mb.fn.seconds = ib("Seconds", !1), mb.fn.minute = mb.fn.minutes = ib("Minutes", !1), mb.fn.hour = mb.fn.hours = ib("Hours", !0), mb.fn.date = ib("Date", !0), mb.fn.dates = d("dates accessor is deprecated. Use date instead.", ib("Date", !0)), mb.fn.year = ib("FullYear", !0), mb.fn.years = d("years accessor is deprecated. Use year instead.", ib("FullYear", !0)), mb.fn.days = mb.fn.day, mb.fn.months = mb.fn.month, mb.fn.weeks = mb.fn.week, mb.fn.isoWeeks = mb.fn.isoWeek, mb.fn.quarters = mb.fn.quarter, mb.fn.toJSON = mb.fn.toISOString, j(mb.duration.fn = i.prototype, {
		_bubble: function () {
			var a, b, c, d, e = this._milliseconds, f = this._days, g = this._months, h = this._data;
			h.milliseconds = e % 1e3, a = l(e / 1e3), h.seconds = a % 60, b = l(a / 60), h.minutes = b % 60, c = l(b / 60), h.hours = c % 24, f += l(c / 24), h.days = f % 30, g += l(f / 30), h.months = g % 12, d = l(g / 12), h.years = d
		}, weeks: function () {
			return l(this.days() / 7)
		}, valueOf: function () {
			return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * u(this._months / 12)
		}, humanize: function (a) {
			var b = +this, c = ab(b, !a, this.lang());
			return a && (c = this.lang().pastFuture(b, c)), this.lang().postformat(c)
		}, add: function (a, b) {
			var c = mb.duration(a, b);
			return this._milliseconds += c._milliseconds, this._days += c._days, this._months += c._months, this._bubble(), this
		}, subtract: function (a, b) {
			var c = mb.duration(a, b);
			return this._milliseconds -= c._milliseconds, this._days -= c._days, this._months -= c._months, this._bubble(), this
		}, get: function (a) {
			return a = r(a), this[a.toLowerCase() + "s"]()
		}, as: function (a) {
			return a = r(a), this["as" + a.charAt(0).toUpperCase() + a.slice(1) + "s"]()
		}, lang: mb.fn.lang, toIsoString: function () {
			var a = Math.abs(this.years()), b = Math.abs(this.months()), c = Math.abs(this.days()), d = Math.abs(this.hours()), e = Math.abs(this.minutes()), f = Math.abs(this.seconds() + this.milliseconds() / 1e3);
			return this.asSeconds() ? (this.asSeconds() < 0 ? "-" : "") + "P" + (a ? a + "Y" : "") + (b ? b + "M" : "") + (c ? c + "D" : "") + (d || e || f ? "T" : "") + (d ? d + "H" : "") + (e ? e + "M" : "") + (f ? f + "S" : "") : "P0D"
		}
	});
	for (ob in ac)ac.hasOwnProperty(ob) && (kb(ob, ac[ob]), jb(ob.toLowerCase()));
	kb("Weeks", 6048e5), mb.duration.fn.asMonths = function () {
		return (+this - 31536e6 * this.years()) / 2592e6 + 12 * this.years()
	}, mb.lang("en", {
		ordinal: function (a) {
			var b = a % 10, c = 1 === u(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
			return a + c
		}
	}), Bb ? module.exports = mb : "function" == typeof define && define.amd ? (define("moment", function (a, b, c) {
		return c.config && c.config() && c.config().noGlobal === !0 && (qb.moment = nb), mb
	}), lb(!0)) : lb()
}.call(this), function () {
	"undefined" != typeof Pusher && null !== Pusher && (APP.webSockets = {pusher: new Pusher("")}, Pusher.log = function (a) {
		return window.console && window.console.log ? window.console.log(a) : void 0
	}), APP.urlRoot = "/", APP.userData = {}, APP.pageData = {}, APP.timers = {}, APP.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], APP.days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
}.call(this), function () {
}.call(this), !function (a) {
	var b = function (b, c, d) {
		d && (d.stopPropagation(), d.preventDefault()), this.$element = a(b), this.$newElement = null, this.button = null, this.label = null, this.labelPrepend = null, this.options = a.extend({}, a.fn.checkbox.defaults, this.$element.data(), "object" == typeof c && c), this.displayAsButton = this.options.displayAsButton, this.buttonStyle = this.options.buttonStyle, this.buttonStyleChecked = this.options.buttonStyleChecked, this.defaultState = this.options.defaultState, this.defaultEnabled = this.options.defaultEnabled, this.indeterminate = this.options.indeterminate, this.init()
	};
	b.prototype = {
		constructor: b, init: function () {
			this.$element.hide(), this.$element.attr("autocomplete", "off");
			var a = void 0 !== this.$element.attr("class") ? this.$element.attr("class").split(/\s+/) : "", b = this.getTemplate();
			this.$element.after(b), this.$newElement = this.$element.next(".bootstrap-checkbox"), this.button = this.$newElement.find("button"), this.label = this.$newElement.find("span.label-checkbox"), this.labelPrepend = this.$newElement.find("span.label-prepend-checkbox");
			for (var c = 0; c < a.length; c++)"checkbox" != a[c] && this.$newElement.addClass(a[c]);
			this.button.addClass(this.buttonStyle), void 0 != this.$element.data("default-state") && (this.defaultState = this.$element.data("default-state")), void 0 != this.$element.data("default-enabled") && (this.defaultEnabled = this.$element.data("default-enabled")), void 0 != this.$element.data("display-as-button") && (this.displayAsButton = this.$element.data("display-as-button")), void 0 != this.$element.data("indeterminate") && (this.indeterminate = this.$element.data("indeterminate")), this.indeterminate && this.$element.prop("indeterminate", !0), this.checkEnabled(), this.checkChecked(), this.checkTabIndex(), this.clickListener()
		}, getTemplate: function () {
			var a = this.displayAsButton ? " displayAsButton" : "", b = this.$element.data("label") ? '<span class="label-checkbox">' + this.$element.data("label") + "</span>" : "", c = this.$element.data("label-prepend") ? '<span class="label-prepend-checkbox">' + this.$element.data("label-prepend") + "</span>" : "", d = '<span class="button-checkbox bootstrap-checkbox"><button type="button" class="btn clearfix' + a + '">' + (this.$element.data("label-prepend") && this.displayAsButton ? c : "") + '<span class="icon ' + this.options.checkedClass + '" style="display:none;"></span><span class="icon ' + this.options.uncheckedClass + '" style="display:inline-block;"></span><span class="icon ' + this.options.indeterminateClass + '" style="display:none;"></span>' + (this.$element.data("label") && this.displayAsButton ? b : "") + "</button></span>";
			return this.displayAsButton || !this.$element.data("label") && !this.$element.data("label-prepend") || (d = '<label class="' + this.options.labelClass + '">' + c + d + b + "</label>"), d
		}, checkEnabled: function () {
			this.button.attr("disabled", this.$element.is(":disabled")), this.$newElement.toggleClass("disabled", this.$element.is(":disabled"))
		}, checkTabIndex: function () {
			if (this.$element.is("[tabindex]")) {
				var a = this.$element.attr("tabindex");
				this.button.attr("tabindex", a)
			}
		}, checkChecked: function () {
			var b = /\s/g, c = ".";
			1 == this.$element.prop("indeterminate") ? (this.button.find("span." + this.options.checkedClass.replace(b, c)).hide(), this.button.find("span." + this.options.uncheckedClass.replace(b, c)).hide(), this.button.find("span." + this.options.indeterminateClass.replace(b, c)).show()) : (this.$element.is(":checked") ? (this.button.find("span." + this.options.checkedClass.replace(b, c)).show(), this.button.find("span." + this.options.uncheckedClass.replace(b, c)).hide()) : (this.button.find("span." + this.options.checkedClass.replace(b, c)).hide(), this.button.find("span." + this.options.uncheckedClass.replace(b, c)).show()), this.button.find("span." + this.options.indeterminateClass.replace(b, c)).hide()), this.$element.is(":checked") ? this.buttonStyleChecked && (this.button.removeClass(this.buttonStyle), this.button.addClass(this.buttonStyleChecked)) : this.buttonStyleChecked && (this.button.removeClass(this.buttonStyleChecked), this.button.addClass(this.buttonStyle)), this.$element.is(":checked") ? this.options.labelClassChecked && a(this.$element).next("label").addClass(this.options.labelClassChecked) : this.options.labelClassChecked && a(this.$element).next("label").removeClass(this.options.labelClassChecked)
		}, clickListener: function () {
			var a = this;
			this.button.on("click", function (b) {
				b.preventDefault(), a.$element.prop("indeterminate", !1), a.$element[0].click(), a.checkChecked()
			}), this.$element.on("change", function () {
				a.checkChecked()
			}), this.$element.parents("form").on("reset", function (b) {
				null == a.defaultState ? a.$element.prop("indeterminate", !0) : a.$element.prop("checked", a.defaultState), a.$element.prop("disabled", !a.defaultEnabled), a.checkEnabled(), a.checkChecked(), b.preventDefault()
			})
		}, setOptions: function (a) {
			void 0 != a.checked && this.setChecked(a.checked), void 0 != a.enabled && this.setEnabled(a.enabled), void 0 != a.indeterminate && this.setIndeterminate(a.indeterminate)
		}, setChecked: function (a) {
			this.$element.prop("checked", a), this.$element.prop("indeterminate", !1), this.checkChecked()
		}, setIndeterminate: function (a) {
			this.$element.prop("indeterminate", a), this.checkChecked()
		}, click: function () {
			this.$element.prop("indeterminate", !1), this.$element[0].click(), this.checkChecked()
		}, change: function () {
			this.$element.change()
		}, setEnabled: function (a) {
			this.$element.attr("disabled", !a), this.checkEnabled()
		}, toggleEnabled: function () {
			this.$element.attr("disabled", !this.$element.is(":disabled")), this.checkEnabled()
		}, refresh: function () {
			this.checkEnabled(), this.checkChecked()
		}
	}, a.fn.checkbox = function (c, d) {
		return this.each(function () {
			var e = a(this), f = e.data("checkbox"), g = "object" == typeof c && c;
			f ? "string" == typeof c ? f[c](d) : "undefined" != typeof c && f.setOptions(c, d) : (e.data("checkbox", f = new b(this, g, d)), void 0 != f.options.constructorCallback && f.options.constructorCallback(f.$element, f.button, f.label, f.labelPrepend))
		})
	}, a.fn.checkbox.defaults = {
		displayAsButton: !1,
		indeterminate: !1,
		buttonStyle: "btn-link",
		buttonStyleChecked: null,
		checkedClass: "cb-icon-check",
		uncheckedClass: "cb-icon-check-empty",
		indeterminateClass: "cb-icon-check-indeterminate",
		defaultState: !1,
		defaultEnabled: !0,
		constructorCallback: null,
		labelClass: "checkbox bootstrap-checkbox",
		labelClassChecked: "active"
	}
}(window.jQuery), function () {
	APP.scrollToTop = function () {
		return navigator.userAgent.match(/(iPhone|iPod)/) ? $("body").scrollTop(0) : void 0
	}, APP.centerFixElement = function (a, b) {
		var c, d;
		return b = b || 0, a ? (c = Math.ceil(($(window).width() - $(a).width()) / 2), d = Math.ceil(($(window).height() - $(a).height()) / 2), navigator.userAgent.match(/(iPhone|iPod|Android)/) ? $(a).css("position", "absolute") : $(window).height() - $(a).height() > 50 && $(window).width() - $(a).width() > 50 ? $(a).css("position", "fixed") : ($(a).css("position", "absolute"), d = $(window).scrollTop(), $(".main_container").length && d + $(a).height() > $(".main_container").height() && (d = $(".main_container").height() - $(a).height() - 50)), 0 > c && (c = 0), 0 > d && (d = 0), b ? $(a).animate({
			left: c,
			top: d
		}, 300, "swing") : ($(a).css("left", c), $(a).css("top", d))) : void 0
	}, APP.trim = function (a) {
		return null != a ? a.replace(/^\s+|\s+$/g, "") : ""
	}, $.fn.isBlank = function () {
		var a, b;
		return b = $(this), a = !1, b.length > 0 && "" === APP.trim(b.val()) ? a = !0 : b.length <= 0 && (a = !0), a
	}, $.fn.serializeObject = function () {
		var a, b;
		return b = {}, a = this.serializeArray(), $.each(a, function () {
			return null != b[this.name] ? (b[this.name].push || (b[this.name] = [b[this.name]]), b[this.name].push(this.value || "")) : b[this.name] = this.value || ""
		}), b
	}, $.fn.isBlankOrZero = function () {
		var a, b;
		return b = $(this), a = !1, b.length > 0 && "" === APP.trim(b.val()) ? a = !0 : b.length <= 0 ? a = !0 : 0 === parseInt(b.val()) && (a = !0), a
	}, $.fn.serializeObject = function () {
		var a, b;
		return b = {}, a = this.serializeArray(), $.each(a, function () {
			return null != b[this.name] ? (b[this.name].push || (b[this.name] = [b[this.name]]), b[this.name].push(this.value || "")) : b[this.name] = this.value || ""
		}), b
	}, $.fn.getBackgroundImageURL = function () {
		var a, b;
		return a = $(this).css("background-image"), b = /\"|\'|\)/g, a.replace("url(", "").replace(")", "").replace(b, "")
	}, APP.alignHeights = function (a) {
		var b;
		return b = 0, $(a).css({height: "auto"}), $(a).each(function () {
			return $(this).height() > b ? b = $(this).height() : void 0
		}), b > 0 ? $(a).height(b) : void 0
	}, APP.URLParameter = function (a) {
		var b, c, d;
		return a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]"), c = "[\\?&]" + a + "=([^&#]*)", b = new RegExp(c), d = b.exec(window.location.search), null === d ? "" : decodeURIComponent(d[1].replace(/\+/g, " "))
	}, APP.parseRSS = function (a, b) {
		return $.ajax({
			url: document.location.protocol + "//ajax.googleapis.com/ajax/services/feed/load?v=2.0&num=10&callback=?&q=" + encodeURIComponent(a),
			dataType: "json",
			success: function (a) {
				return b(a.responseData.feed)
			}
		})
	}, APP.htmlEntities = function (a) {
		return String(a).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;")
	}, APP.filterStringForInput = function (a) {
		var b;
		return null != a ? (b = a.replace(/(<([^>]+)>)/gi, ""), b = APP.htmlEntities(b)) : ""
	}, APP.convertTimeAMPM = function (a) {
		var b, c, d, e, f;
		return null != a && "" !== APP.trim(a) ? (c = Number(a.match(/^(\d+)/)[1]), d = Number(a.match(/:(\d+)/)[1]), b = APP.trim(a).slice(-2), window.console.log(b), ("PM" === b || "pm" === b) && 12 > c && (c += 12), "AM" !== b && "am" !== b || 12 !== c || (c -= 12), e = c.toString(), f = d.toString(), 10 > c && (e = "0" + e), 10 > d && (f = "0" + f), e + ":" + f + ":00") : ""
	}, APP.escapeRegExp = function (a) {
		return a.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1")
	}, APP.replaceAll = function (a, b, c) {
		return a.replace(new RegExp(APP.escapeRegExp(b), "g"), c)
	}, APP.onMobileBrowser = function () {
		return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
	}, $.fn.mobileFix = function (a) {
		var b, c;
		return c = $(this), b = $(a.fixedElements), $(document).on("focus", a.inputElements, function () {
			return c.addClass(a.addClass)
		}).on("blur", a.inputElements, function () {
			return c.removeClass(a.addClass), setTimeout(function () {
				return $(document).scrollTop($(document).scrollTop())
			}, 1)
		}), this
	}, APP.calculateCirclePercentagePosition = function (a, b, c, d, e, f) {
		var g, h, i;
		return g = .5 * Math.PI - 2 * d * Math.PI, h = a + c * Math.cos(g), i = b + c * Math.sin(g), i = -1 * i, h -= e / 2, i -= f / 2, {
			x: Math.round(h),
			y: Math.round(i)
		}
	}, APP.numberWithCommas = function (a) {
		var b;
		return b = a.toString().split("."), b[0] = b[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","), b.join(".")
	}
}.call(this), APP.buildComponents = function (a) {
	a.removeClass("components-built").addClass("components-built"), a.find(".button-2").each(function () {
		$(this).find(".button-content").length <= 0 && $(this).wrapInner('<span class="button-content">')
	}), a.find("input.custom-checkbox").each(function () {
		var a = $(this);
		if ($(this).parents(".tooltip-content").length <= 0) {
			var b = $(this).parents(".popover"), c = [];
			if (b.length > 0) {
				var d = b.parent().find(".tooltip-content");
				c = d.find("input[name=" + a.attr("name") + "]"), c.length > 0 && c.is(":checked") && $(this).prop("checked", !0).attr("checked", !0)
			}
			if ($(this).checkbox(), void 0 !== $(this).attr("data-toggle-show-class")) {
				var e = $("." + $(this).attr("data-toggle-show-class"));
				$(this).is(":checked") ? e.show() : e.hide(), $(this).on("change", function () {
					$(this).is(":checked") ? e.show() : e.hide()
				})
			}
			if (void 0 !== $(this).attr("data-toggle-readonly-id")) {
				var e = $("#" + $(this).attr("data-toggle-readonly-id"));
				$(this).is(":checked") ? (e.attr("readonly", !1), e.removeClass("disabled")) : (e.attr("readonly", !0), e.removeClass("disabled").addClass("disabled")), $(this).on("change", function () {
					$(this).is(":checked") ? (e.attr("readonly", !1), e.removeClass("disabled")) : (e.attr("readonly", !0), e.removeClass("disabled").addClass("disabled"))
				})
			}
			c.length > 0 && $(this).on("change", function () {
				$(this).is(":checked") ? c.prop("checked", !0).attr("checked", !0) : c.prop("checked", !1).attr("checked", !1)
			})
		}
	}), a.find(".force-one-row").each(function () {
		row_element = $(this), row_width = 0, row_element.children().each(function () {
			row_width += $(this).outerWidth(), row_width += parseInt($(this).css("marginLeft").substring(0, $(this).css("marginLeft").length - 1)), row_width += parseInt($(this).css("marginRight").substring(0, $(this).css("marginRight").length - 1)), row_width += 5
		}), row_element.width(row_width)
	}), a.find(".button-copy").on("mouseup", function (a) {
		a.preventDefault(), a.stopPropagation(), target_element = $($(this).attr("data-rel")), target_element.focus(), target_element.select()
	}), a.find(".message-box .close-button").click(function (a) {
		a.preventDefault(), a.stopPropagation(), message_box = $(this).parents(".message-box"), message_box.hasClass("show-only-on-initial-view") && void 0 !== message_box.attr("data-admin-id") && void 0 !== message_box.attr("data-page") ? jqxhr = $.ajax({
			type: "POST",
			url: "/company/showhelp",
			cache: !1,
			data: {admin_id: message_box.attr("data-admin-id"), page: message_box.attr("data-page")}
		}).done(function (a) {
			"success" == a.response ? message_box.fadeOut("slow", function () {
				$(this).remove()
			}) : window.console.log("Request failed." + a.message)
		}).fail(function () {
			window.console.log("Request failed.")
		}) : $(this).parents(".message-box").fadeOut("slow", function () {
			$(this).remove()
		})
	}), a.find("#page-notification .close-button").click(function (a) {
		a.preventDefault(), a.stopPropagation(), APP.hidePageNotification()
	}), a.find("#page-alert-container").hover(function () {
		void 0 !== APP.timers.pageAlert && clearTimeout(APP.timers.pageAlert)
	}), a.find("#page-alert-container").mouseout(function () {
	}), a.find("#page-alert-container .close-button").click(function (a) {
		a.preventDefault(), a.stopPropagation(), APP.hidePageAlert()
	}), a.find("#content-modal-alert-container .close-button").click(function (a) {
		a.preventDefault(), a.stopPropagation(), APP.hideContentModalAlert()
	}), a.find(".modal-close-button").click(function (a) {
		a.preventDefault(), a.stopPropagation(), $(this).parents("#content-modal").modal("hide")
	}), a.find("#content-modal").on("shown.bs.modal", function () {
		"" == APP.trim($(this).find(".modal-header-content").text()) ? $(this).removeClass("hidden-header").addClass("hidden-header") : $(this).removeClass("hidden-header")
	}), a.find(".content-toggle").each(function () {
		content_toggler = $(this), void 0 !== content_toggler.attr("data-rel") && (target_element = $(content_toggler.attr("data-rel")), content_toggler.hasClass("active") ? target_element.show() : target_element.hide(), content_toggler.click(function (a) {
			a.preventDefault(), a.stopPropagation(), target_element = $($(this).attr("data-rel")), $(this).hasClass("active") ? ($(this).removeClass("active"), target_element.hide()) : ($(this).addClass("active"), target_element.show()), APP.resizeCanvas($("body"))
		}))
	}), a.find(".mixpanel-track-click").click(function () {
		$(this).hasClass("disabled") || APP.mixpanel_track($(this).attr("data-mp-event"), {})
	}), a.find('[data-toggle="tooltip"]').each(function () {
		$(this).tooltip()
	}), a.find(".tooltip-trigger").each(function () {
		var a = $(this), b = "top", c = "hover focus", d = !1;
		void 0 !== $(this).attr("data-tooltip-placement") && (b = $(this).attr("data-tooltip-placement")), void 0 !== $(this).attr("data-tooltip-trigger") && (c = $(this).attr("data-tooltip-trigger")), void 0 !== $(this).attr("data-tooltip-container") && (d = $(this).attr("data-tooltip-container")), a.popover({
			trigger: c,
			placement: b,
			container: d,
			content: function () {
				return a.find(".tooltip-content").html()
			},
			html: 1
		}), a.on("shown.bs.popover", function () {
			var b = a.parent().find(".popover");
			1 != b.length && a.next().hasClass("popover") && (b = a.next()), $(this).removeClass("active").addClass("active"), b.length > 0 && (b.find("form").removeClass("ready"), APP.buildComponents(b))
		}), a.on("hidden.bs.popover", function () {
			$(this).removeClass("active")
		})
	}), APP.enableScreens(a), a.find(".feedback-link").click(function () {
		APP.showFeedbackModal()
	}), a.find(".close-modal-after-submit").bind("onSuccess", function () {
		APP.hideContentModal()
	}), a.find("a.smooth-scroll").click(function (a) {
		a.preventDefault();
		var b = $(this), c = b.attr("href");
		"undefined" != typeof c && (c = c.replace("/", ""));
		var d = b.attr("data-screen");
		"undefined" != typeof d && APP.jumpToScreen(parseInt(d)), selector = $(c), selector.length > 0 ? $("html, body").stop().animate({scrollTop: selector.offset().top - 50}, 500, "swing") : self.location = b.attr("href")
	}), a.find(".time-picker").timepicker({scrollDefault: "09:00"}), a.find(".time-picker.start").each(function () {
		time_picker_element = $(this), time_pickers_element = time_picker_element.parents(".time-pickers-container"), time_pickers_element.length > 0 && (end_time_picker = time_pickers_element.find(".time-picker.end"), end_time_picker.length > 0 && time_picker_element.on("changeTime", function () {
			end_time_picker = $(this).parents(".time-pickers-container").find(".time-picker.end"), "" == APP.trim(end_time_picker.val()) && (current_time = $(this).timepicker("getTime", new Date), later = new Date(current_time.getTime() + 36e5), end_time_picker.timepicker("setTime", later))
		}))
	}), a.find(".time-picker.end").each(function () {
		time_picker_element = $(this), time_pickers_element = time_picker_element.parents(".time-pickers-container"), time_pickers_element.length > 0 && (start_time_picker = time_pickers_element.find(".time-picker.start"), start_time_picker.length > 0 && time_picker_element.on("changeTime", function () {
			start_time_picker = $(this).parents(".time-pickers-container").find(".time-picker.start"), "" == APP.trim(start_time_picker.val()) && (current_time = $(this).timepicker("getTime", new Date), before = new Date(current_time.getTime() - 36e5), start_time_picker.timepicker("setTime", before))
		}))
	}), a.find(".flexslider").flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	}), a.find("a").click(function (a) {
		$(this).hasClass("disabled") && a.preventDefault()
	}), $(".main-nav-list li .smooth-scroll").click(function () {
		$(".main-nav-list li").removeClass("active"), $(this).parents("li").addClass("active")
	}), a.find(".use-s3-upload").each(function () {
		image_container = $(this), APP.s3upload = null != APP.s3upload ? APP.s3upload : new S3Upload({
			s3_sign_put_url: "/signS3put",
			file_dom_selector: ".use-s3-upload input[type=file]",
			onProgress: function () {
				window.console.log("making progress")
			},
			onFinishS3Put: function (a) {
				image_container.find("input[type=file]");
				image_container.css("background-image", "url(" + a + "?v=" + Math.random() + ")"), image_container.css("background-size", "cover"), image_container.css("background-position", "0px 0px"), image_container.find(".image-public-url").val(a)
			},
			onError: function (a) {
				window.console.log(a)
			}
		}), image_container.find("input[type=file]").change(function () {
			if ($(this)[0].files[0]) {
				file = $(this)[0].files[0];
				var a = !0, b = ["image/gif", "images/gif", "image/jpeg", "image/jpg", "image/pjpeg", "image/png"];
				file.size > 2e6 ? (a = !1, APP.showPageAlert(3, "The file you have selected is too large. Must be under 2MB.", "", 1)) : _.contains(b, file.type) || (a = !1, APP.showPageAlert(3, "The file you have selected is not in an approved format. Must be of type JPG, PNG or GIF.", "", 1)), a && (ext = file.name.split(".")[1], APP.s3upload.s3_object_name = image_container.attr("data-s3-object-name") + "." + ext, APP.s3upload.uploadFile(file))
			}
		})
	}), a.find(".read-more").each(function () {
		read_more_container = $(this), read_more_link_element = $(this).find(".read-more-link"), max_height = parseInt(read_more_container.attr("data-start-height")), read_more_link_element.length <= 0 && read_more_container.height() > max_height && (read_more_container.wrapInner('<div class="collapsible-content">'), read_more_container.find(".collapsible-content").css({maxHeight: max_height}), read_more_container.append('<a href="#" class="read-more-link">Read </a>'), read_more_link_element = read_more_container.find(".read-more-link"), read_more_link_element.click(function (a) {
			a.preventDefault(), a.stopPropagation(), read_more_container.toggleClass("expanded")
		}))
	}), a.find(".video-modal-link").click(function () {
		var a = "", b = $(".video-modal-template").html(), c = Handlebars.compile(b), d = {
			video_id: $(this).attr("data-video-id"),
			end: $(this).attr("data-video-end")
		};
		a = c(d);
		var e = $("#content-modal .modal-body");
		e.html(a), APP.buildComponents(e), APP.player = new YT.Player("video-modal-container", {
			height: "90%",
			width: "100%",
			videoId: $(this).attr("data-video-id"),
			playerVars: {rel: 0, showinfo: 0},
			events: {onReady: APP.YTPlayer.onPlayerReady, onStateChange: APP.YTPlayer.onPlayerStateChange}
		}), void 0 !== $(this).attr("data-header-title") ? ($("#content-modal .modal-header-content").text($(this).attr("data-header-title")), $("#content-modal").removeClass("hidden-header")) : $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").removeClass("full-screen").addClass("full-screen"), $("body").removeClass("blackout").addClass("blackout"), $("#content-modal").modal("show")
	}), a.find("select").each(function () {
		$(this).on("focus", function () {
			$(this).find("option").each(function () {
				$(this).html(APP.replaceAll($(this).html(), " ", "&nbsp;"))
			})
		}), $(this).on("blur", function () {
			$(this).find("option").each(function () {
				$(this).html(APP.replaceAll($(this).html(), "&nbsp;", " "))
			})
		})
	}), a.find(".dropdown-multiselect").each(function () {
		var a = $(this);
		a.find(".value").click(function () {
			a.toggleClass("active")
		}), a.find("input[type=radio]").click(function () {
			a.find("> ul > li").removeClass("active"), $(this).is(":checked") && ($(this).parents("li").addClass("active"), value_element = a.find(".value"), value_element.text($(this).parent().find("label").text()), sublist_element = $(this).parents("li").find("ul"), sublist_element.length > 0 && (selected_values = [], sublist_element.find("input[type=checkbox]:checked").each(function () {
				selected_values.push($(this).parent().find("label").text())
			}), value_element.text(selected_values.length > 0 ? selected_values.join(", ") : value_element.attr("data-default-value"))))
		}), a.find("input[type=checkbox]").click(function () {
			selected_values = [], a.find("input[type=checkbox]").each(function () {
				$(this).is(":checked") && selected_values.push($(this).parent().find("label").text())
			}), value_element = a.find(".value"), value_element.text(selected_values.length > 0 ? selected_values.join(", ") : value_element.attr("data-default-value"))
		})
	}), a.find(".fb_js_sdk_share_link").click(function (a) {
		a.preventDefault(), a.stopPropagation();
		var b = "";
		void 0 !== $(this).attr("data-fbshare-description-target") ? b = $($(this).attr("data-fbshare-description-target")).val() : void 0 !== $(this).attr("data-fbshare-description") && (b = $(this).attr("data-fbshare-description"));
		try {
			obj = {
				method: "feed",
				link: $(this).attr("href"),
				picture: $(this).attr("data-fbshare-picture"),
				name: $(this).attr("data-fbshare-name"),
				caption: "Powered by Codeity",
				description: b
			}, FB.ui(obj, function () {
			})
		} catch (c) {
			window.console.log(c.message)
		}
	}), a.find(".fb_js_sdk_share_basic_link").click(function (a) {
		a.preventDefault(), a.stopPropagation();
		try {
			obj = {method: "share", href: $(this).attr("href")}, FB.ui(obj, function () {
			})
		} catch (b) {
			window.console.log(b.message)
		}
	}), a.find("#content-modal").on("hide.bs.modal", function () {
		$(this).removeClass("wide wide-medium full-screen"), $("body").removeClass("blackout");
		var a = $(this).find(".video-container iframe");
		a.length > 0 && a.get(0).contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', "*"), APP.player && APP.player.pauseVideo()
	}), a.find(".trigger-reamaze").click(function (a) {
		a.preventDefault(), a.stopPropagation(), $("#reamaze-widget").trigger("click")
	}), a.find(".modal-show-current-plan-button").click(function (a) {
		a.preventDefault(), a.stopPropagation(), APP.showUpgradeSubscriptionModal({header: "UPGRADE PLAN"})
	}), a.find("#payment-form .payment-plans-container > div").click(function (a) {
		a.preventDefault(), a.stopPropagation(), $(this).hasClass("selected") || void 0 === $(this).attr("data-plan-value") ? void 0 !== $(this).attr("href") && (self.location = $(this).attr("href")) : ($("#content-modal").removeClass("wide hidden-header"), $("#content-modal .modal-dialog").attr("style", ""), $("#content-modal .modal-header-content").html("Confirm your plan"), modal_body_element = $("#content-modal .modal-body"), template = $(".change-plan-confirm-template"), source = template.html(), template = Handlebars.compile(source), context = {
			plan: $(this).attr("data-plan-name"),
			plan_value: $(this).attr("data-plan-value"),
			plan_description: $(this).attr("data-plan-description")
		}, html = template(context), modal_body_element.html(html), APP.buildComponents(modal_body_element))
	}), a.find("form#form-upgrade-plan").unbind("onSuccess"), a.find("form#form-upgrade-plan").bind("onSuccess", function () {
		$("#content-modal").removeClass("wide hidden-header"), $("#content-modal .modal-header-content").html("Congratulations!"), modal_body_element = $("#content-modal .modal-body"), template = $(".change-plan-success-template"), source = template.html(), template = Handlebars.compile(source), context = {}, html = template(context), modal_body_element.html(html), APP.buildComponents(modal_body_element), setTimeout(function () {
			location.reload()
		}, 2e3)
	}), APP.addFormValidation(a), a.find("[data-toggle=collapse]").on("hidden.bs.collapse", function () {
		APP.resizeCanvas(a)
	}), a.on("click", function (a) {
		$("[data-toggle=popover]").each(function () {
			$(this).is(a.target) || 0 !== $(this).has(a.target).length || 0 !== $(".popover").has(a.target).length || $(this).popover("hide")
		}), $(".dropdown-multiselect").each(function () {
			$(this).is(a.target) || 0 !== $(this).has(a.target).length || $(this).removeClass("active")
		})
	}), a.on("mouseup", function (a) {
		$(".time-picker").each(function () {
			$(this).is(a.target) || 0 !== $(this).has(a.target).length || $(this).parents(".time-picker-container").find(".time-picker-dd").removeClass("active")
		})
	}), APP.buildAdditionalComponents(a)
}, APP.closeAllPopovers = function () {
	$("[data-toggle=popover]").each(function () {
		$(this).popover("hide")
	})
}, APP.buildAdditionalComponents = function () {
}, APP.showPageAlert = function (a, b, c, d) {
	if ("" == APP.trim(b))return !0;
	var e = "";
	switch (a) {
		case 1:
			e = "success";
			break;
		case 2:
			e = "info";
			break;
		case 3:
			e = "warning";
			break;
		case 4:
			e = "danger"
	}
	$("#page-alert-container").removeClass("success info warning danger").addClass(e), $("#page-alert-message").html(b), height = $("#page-alert-message .content").height(), d ? d > 1 ? $("#page-alert-container").animate({maxHeight: 100}, "ease", function () {
		APP.timers.pageAlert = setTimeout(function () {
			$("#page-alert-container").animate({maxHeight: 0}, "ease", function () {
				$("#page-alert-message").html("")
			})
		}, d)
	}) : $("#page-alert-container").animate({maxHeight: 100}, "ease", function () {
		APP.timers.pageAlert = setTimeout(function () {
			$("#page-alert-container").animate({maxHeight: 0}, "ease", function () {
				$("#page-alert-message").html("")
			})
		}, 3e3)
	}) : $("#page-alert-container").animate({maxHeight: 100}, "ease", function () {
	})
}, APP.hidePageAlert = function () {
	$("#page-alert-container").animate({maxHeight: 0}, "ease", function () {
		$("#page-alert-message").html("")
	})
}, APP.hidePageNotification = function () {
	var a = $("#page-notification");
	if (a.removeClass("closed").addClass("closed"), admin_id = a.attr("data-admin-id"), user_id = a.attr("data-user-id"), message_box = a.find(".message"), message_box.length > 0) {
		var b = "/company/showhelp";
		"" == APP.trim(admin_id) && "" != APP.trim(user_id) && (b = "/developer/" + user_id + "/helpbox/" + message_box.attr("data-page")), jqxhr = $.ajax({
			type: "POST",
			url: b,
			cache: !1,
			data: {admin_id: admin_id, page: message_box.attr("data-page")}
		}).done(function (a) {
			"success" == a.response ? message_box.fadeOut("slow", function () {
				$(this).remove()
			}) : window.console.log("Request failed." + a.message)
		}).fail(function () {
			window.console.log("Request failed.")
		})
	}
}, APP.showContentModalAlert = function (a, b, c, d) {
	if ("" == APP.trim(b))return !0;
	var e = "";
	switch (a) {
		case 1:
			e = "success";
			break;
		case 2:
			e = "info";
			break;
		case 3:
			e = "warning";
			break;
		case 4:
			e = "danger"
	}
	$("#content-modal-alert-container").removeClass("success info warning danger").addClass(e), $("#content-modal-alert-message").html(b), height = $("#content-modal-alert-message .content").height(), d ? d > 1 ? $("#content-modal-alert-container").animate({maxHeight: 100}, "ease", function () {
		setTimeout(function () {
			$("#content-modal-alert-container").animate({maxHeight: 0}, "ease", function () {
				$("#content-modal-alert-message").html("")
			})
		}, d)
	}) : $("#content-modal-alert-container").animate({maxHeight: 100}, "ease", function () {
		setTimeout(function () {
			$("#content-modal-alert-container").animate({maxHeight: 0}, "ease", function () {
				$("#content-modal-alert-message").html("")
			})
		}, 3e3)
	}) : $("#content-modal-alert-container").animate({maxHeight: 100}, "ease", function () {
	})
}, APP.hideContentModalAlert = function () {
	$("#content-modal-alert-container").animate({maxHeight: 0}, "ease", function () {
		$("#content-modal-alert-message").html("")
	})
}, APP.hideContentModal = function () {
	$("#content-modal").modal("hide")
}, APP.clearContentModal = function () {
	$("#content-modal").modal("hide"), $("#content-modal .modal-header .modal-header-content").text(""), $("#content-modal .modal-body").empty()
}, APP.circleAnimationTimer = [], APP.circleAnimationAngle = [], APP.circleAnimationRadius = [], APP.circleAnimationPercentage = [], APP.drawCircle = function (a, b) {
	APP.circleAnimationPercentage[b] = parseInt(a.attr("data-percentage")), APP.circleAnimationAngle[b] = 0, APP.circleAnimationRadius[b] = parseInt(a.attr("data-radius")), APP.circleAnimationTimer[b] = window.setInterval(function () {
		APP.circleAnimationAngle[b] += 2, radians = APP.circleAnimationAngle[b] / 180 * Math.PI, x1 = APP.circleAnimationRadius[b] + Math.sin(Math.PI) * APP.circleAnimationRadius[b], y1 = APP.circleAnimationRadius[b] + Math.cos(Math.PI) * APP.circleAnimationRadius[b], x2 = APP.circleAnimationRadius[b] + Math.sin(Math.PI - radians) * APP.circleAnimationRadius[b], y2 = APP.circleAnimationRadius[b] + Math.cos(Math.PI - radians) * APP.circleAnimationRadius[b], x3 = APP.circleAnimationRadius[b] + Math.sin(Math.PI - 359.99 / 180 * Math.PI) * APP.circleAnimationRadius[b], y3 = APP.circleAnimationRadius[b] + Math.cos(Math.PI - 359.99 / 180 * Math.PI) * APP.circleAnimationRadius[b], d = APP.circleAnimationAngle[b] > 357 ? "M" + APP.circleAnimationRadius[b] + "," + APP.circleAnimationRadius[b] + " L" + x1 + "," + y1 + " A" + APP.circleAnimationRadius[b] + "," + APP.circleAnimationRadius[b] + " 0 1,1 " + x3 + "," + y3 + "z" : APP.circleAnimationAngle[b] > 180 ? "M" + APP.circleAnimationRadius[b] + "," + APP.circleAnimationRadius[b] + " L" + x1 + "," + y1 + " A" + APP.circleAnimationRadius[b] + "," + APP.circleAnimationRadius[b] + " 0 1,1 " + x2 + "," + y2 + "z" : "M" + APP.circleAnimationRadius[b] + "," + APP.circleAnimationRadius[b] + " L" + x1 + "," + y1 + " A" + APP.circleAnimationRadius[b] + "," + APP.circleAnimationRadius[b] + " 0 0,1 " + x2 + "," + y2 + "z", a.attr("d", d), APP.circleAnimationAngle[b] >= 3.6 * APP.circleAnimationPercentage[b] && window.clearInterval(APP.circleAnimationTimer[b])
	}, 10)
}, APP.drawStaticCircle = function (a, b) {
	APP.circleAnimationPercentage[b] = parseInt(a.attr("data-percentage")), angle = 3.6 * parseInt(a.attr("data-percentage")), radius = parseInt(a.attr("data-radius")), radians = angle / 180 * Math.PI, x1 = radius + Math.sin(Math.PI) * radius, y1 = radius + Math.cos(Math.PI) * radius, x2 = radius + Math.sin(Math.PI - radians) * radius, y2 = radius + Math.cos(Math.PI - radians) * radius, x3 = radius + Math.sin(Math.PI - 359.99 / 180 * Math.PI) * radius, y3 = radius + Math.cos(Math.PI - 359.99 / 180 * Math.PI) * radius, d = angle > 357 ? "M" + radius + "," + radius + " L" + x1 + "," + y1 + " A" + radius + "," + radius + " 0 1,1 " + x3 + "," + y3 + "z" : angle > 180 ? "M" + radius + "," + radius + " L" + x1 + "," + y1 + " A" + radius + "," + radius + " 0 1,1 " + x2 + "," + y2 + "z" : "M" + radius + "," + radius + " L" + x1 + "," + y1 + " A" + radius + "," + radius + " 0 0,1 " + x2 + "," + y2 + "z", a.attr("d", d)
}, APP.updateScrollTopStatus = function () {
	$(window).scrollTop() <= 0 ? $("body").removeClass("not-at-top") : $("body").removeClass("not-at-top").addClass("not-at-top")
}, String.prototype.formatMoney = function (a, b, c) {
	var d = Number(this.replace(/[^0-9\.]+/g, "")), a = isNaN(a = Math.abs(a)) ? 2 : a, b = void 0 == b ? "." : b, c = void 0 == c ? "," : c, e = 0 > d ? "-" : "", f = parseInt(d = Math.abs(+d || 0).toFixed(a)) + "", g = (g = f.length) > 3 ? g % 3 : 0;
	return "$" + e + (g ? f.substr(0, g) + c : "") + f.substr(g).replace(/(\d{3})(?=\d)/g, "$1" + c) + (a ? b + Math.abs(d - f).toFixed(a).slice(2) : "")
}, function () {
	APP.removeComponents = function (a) {
		return a.removeClass("components-built"), a.unbind(), a.find("a, div, span, ul, li, form").unbind()
	}, APP.showFeedbackModal = function () {
		var a, b, c, d, e, f;
		return b = "Feedback", e = $(".feedback-modal-template").html(), f = Handlebars.compile(e), a = {}, c = f(a), d = $("#content-modal .modal-body"), $("#content-modal .modal-header-content").html(b), d.html(c), APP.buildComponents(d), $("#content-modal").removeClass("hidden-header"), $("#content-modal").modal("show")
	}, APP.mixpanel_track = function (a, b) {
		return "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.track(a, b), window.console.log("Mixpanel Track:" + a)
	}, APP.timeAgo = function (a) {
		var b, c, d, e, f;
		if (e = [{name: "second", limit: 60, in_seconds: 1}, {
				name: "minute",
				limit: 3600,
				in_seconds: 60
			}, {name: "hour", limit: 86400, in_seconds: 3600}, {
				name: "day",
				limit: 604800,
				in_seconds: 86400
			}, {name: "week", limit: 2629743, in_seconds: 604800}, {
				name: "month",
				limit: 31556926,
				in_seconds: 2629743
			}, {
				name: "year",
				limit: null,
				in_seconds: 31556926
			}], b = (new Date - new Date(1e3 * a)) / 1e3, 5 > b)return "now";
		for (c = 0, d = 0; d = e[c++];)if (b < d.limit || !d.limit)return b = Math.floor(b / d.in_seconds), b + " " + d.name + (null != (f = b > 1) ? f : {s: ""})
	}, APP.YTPlayer.onPlayerReady = function (a) {
		return a.target.playVideo()
	}, APP.YTPlayer.onPlayerStateChange = function () {
	}
}.call(this), function () {
	APP.addFormValidation = function (a) {
		return a.find("input[type=submit]").click(function (a) {
			return $(this).hasClass("disabled") ? a.preventDefault() : void 0
		}), a.find("form.use-form-validation").each(function () {
			var a;
			return a = $(this), a.hasClass("ready") ? void 0 : (a.find("input[type=text], input[type=email], input[type=password], textarea, select").each(function () {
				var a, b;
				return b = $(this).parents(".form-group"), a = $(this).val(), $(this).on("blur", function (a) {
					return function () {
						var c;
						return c = APP.validateInput($(a)), c.is_valid ? b.removeClass("has-error") : b.removeClass("has-error").addClass("has-error")
					}
				}(this))
			}), a.hasClass("without-submit") ? a.submit(function (b) {
				return APP.validateForm(a) && APP.validateForm(a) ? void 0 : b.preventDefault()
			}) : (a.unbind("submit"), a.submit(function (b) {
				var c, d, e, f, g, h;
				return b.preventDefault(), d = a.find("input[type=submit]"), h = !1, d.button("loading"), APP.validateForm(a) && (h = !0, a.trigger("additionalValidation"), null != a.attr("rel-additional-validation") && "false" === a.attr("rel-additional-validation") && (h = !1)), h ? h ? (g = a.serialize(), a.hasClass("question-groups") && (c = [], $("textarea[name='q\\[\\]']").each(function () {
					var a;
					return a = {
						answer: $(this).val(),
						id: $(this).attr("id").split("|")[0],
						question: $(this).attr("id").split("|")[1]
					}, c.push(a)
				}), g = {answers: c}), f = "POST", null != a.attr("method") && (f = a.attr("method")), e = $.ajax({
					type: f,
					url: a.attr("action"),
					cache: !1,
					data: g
				}).done(function (b) {
					return "success" === b.response ? (d.button("reset"), a.trigger("onSuccess", b), null != a.attr("data-success-callback") && (window.location = a.attr("data-success-callback"))) : (d.button("reset"), window.console.log("there was an error"), a.parents("#content-modal").length > 0 ? APP.showContentModalAlert(4, b.message, "", 1) : APP.showPageAlert(4, b.message, "", 1)), a.trigger("onDone", b)
				}).fail(function (b) {
					return d.button("reset"), window.console.log("Request failed."), a.trigger("onFail", b), a.parents("#content-modal").length > 0 ? APP.showContentModalAlert(4, "An Error Occurred", "", 1) : APP.showPageAlert(4, "An Error Occurred", "", 1)
				})) : void 0 : d.button("reset")
			})), a.addClass("ready"))
		})
	}, APP.validateForm = function (a) {
		var b;
		return b = !0, a.find("input[type=text], input[type=email], input[type=password], input[type=hidden], textarea, select").each(function () {
			var c, d, e;
			return d = $(this).parents(".form-group"), c = $(this).val(), e = APP.validateInput($(this)), e.is_valid ? d.removeClass("has-error") : (d.removeClass("has-error").addClass("has-error"), b = !1, a.parents("#content-modal").length > 0 ? APP.showContentModalAlert(4, e.message, "", 1) : APP.showPageAlert(4, e.message, "", 1), b)
		}), a.find(".dropdown-multiselect").each(function () {
			var a;
			return a = $(this).parents(".form-group"), $(this).hasClass("required") ? $(this).find("input[type=checkbox]:checked").length <= 0 ? (a.removeClass("has-error").addClass("has-error"), b = !1, APP.showPageAlert(4, "Required Field Missing", "", 1), b) : a.removeClass("has-error") : void 0
		}), a.find(".btn-group").each(function () {
			var a;
			return a = $(this).parents(".form-group"), $(this).hasClass("required") ? $(this).find("input[type=radio]:checked").length <= 0 ? (a.removeClass("has-error").addClass("has-error"), b = !1, APP.showPageAlert(4, "Required Field Missing", "", 1), b) : a.removeClass("has-error") : void 0
		}), b
	}, APP.validateInput = function (a) {
		var b, c, d;
		return c = a.parents(".form-group"), b = a.val(), d = {
			is_valid: !0,
			message: ""
		}, null == a || a.length <= 0 ? (d.is_valid = !1, d.message = "Field does not exist", d) : a.hasClass("required") && "" === APP.trim(b) ? (d.is_valid = !1, d.message = null != a.attr("data-error-message") ? a.attr("data-error-message") : "Required Field Missing", d) : a.hasClass("required-email") && !APP.checkEmail(b) ? (d.is_valid = !1, d.message = "Email is invalid.", d) : a.hasClass("numeric-only") && "" !== APP.trim(b) && $.isNumeric(b) === !1 ? (d.is_valid = !1, d.message = "Invalid Number.", d) : a.hasClass("required-numeric") && $.isNumeric(b) === !1 ? (d.is_valid = !1, d.message = "Invalid Number.", d) : a.hasClass("required-zip") && (b = APP.trim(a.val()), a.val(b), b.length < 5 || $.isNumeric(b) === !1) ? (d.is_valid = !1, d.message = "Invalid Zipcode.", d) : a.hasClass("required-time") && !APP.checkTime(b) ? (d.is_valid = !1, d.message = "Time is invalid.", d) : a.hasClass("required-password") && b.length < 6 ? (d.is_valid = !1, d.message = "Password must be longer than 5 characters.", d) : a.hasClass("required-retype-password") && b !== $("input[name=password]").val() ? (d.is_valid = !1, d.message = "Passwords do not match.", d) : a.hasClass("required-money") && !APP.checkMoney(b) ? (d.is_valid = !1, d.message = "Invalid money format.", d) : a.hasClass("required-money-or-blank") && "" !== APP.trim(b) && !APP.checkMoney(b) ? (d.is_valid = !1, d.message = "Invalid money format.", d) : d
	}, APP.checkUsername = function (a) {
		var b;
		return b = $.ajax({
			type: "POST",
			url: "/developer/checkuname",
			data: {username: $("#email").val()}
		}).done(function (b) {
			var c;
			return "error" === b.response ? ($("#email").parent().addClass("has-error"), c = !1, APP.showPageAlert(4, "The email " + a + " address is already being used.", "", 1)) : void 0
		}).fail(function () {
			return window.console.log("fail")
		})
	}, APP.checkEmail = function (a) {
		var b;
		return b = /^([a-zA-Z0-9\+_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/, b.test(a) ? !0 : !1
	}, APP.checkMoney = function (a) {
		var b;
		return b = /^\$?\s*[0-9][0-9,]*[0-9]\.?[0-9]{0,2}$/i, b.test(a) ? !0 : !1
	}, APP.checkZip = function (a) {
		var b;
		return b = /^\d{5}$|^\d{5}-\d{4}$/, b.test(a) ? !0 : !1
	}, APP.checkTime = function (a) {
		var b;
		return b = /^(0?[1-9]|1[012])(:[0-5]\d)\s*[APap][mM]$/, b.test(a) ? !0 : !1
	}
}.call(this), Handlebars.registerHelper("ifCond", function (a, b, c, d) {
	switch (b) {
		case"==":
			return a == c ? d.fn(this) : d.inverse(this);
		case"!=":
			return a != c ? d.fn(this) : d.inverse(this);
		case"===":
			return a === c ? d.fn(this) : d.inverse(this);
		case"<":
			return c > a ? d.fn(this) : d.inverse(this);
		case"<=":
			return c >= a ? d.fn(this) : d.inverse(this);
		case">":
			return a > c ? d.fn(this) : d.inverse(this);
		case">=":
			return a >= c ? d.fn(this) : d.inverse(this);
		case"&&":
			return a && c ? d.fn(this) : d.inverse(this);
		case"||":
			return a || c ? d.fn(this) : d.inverse(this);
		default:
			return d.inverse(this)
	}
}), Handlebars.registerHelper("getPrescreenQuestion", function (a) {
	var b = _.find(APP.job_prescreen_questions, function (b) {
		return b.id == a
	});
	return b.value
}), Handlebars.registerHelper("compare", function (a, b, c) {
	if (arguments.length < 3)throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
	operator = c.hash.operator || "==";
	var d = {
		"==": function (a, b) {
			return a == b
		}, "===": function (a, b) {
			return a === b
		}, "!=": function (a, b) {
			return a != b
		}, "<": function (a, b) {
			return b > a
		}, ">": function (a, b) {
			return a > b
		}, "<=": function (a, b) {
			return b >= a
		}, ">=": function (a, b) {
			return a >= b
		}, "typeof": function (a, b) {
			return typeof a == b
		}
	};
	if (!d[operator])throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
	var e = d[operator](a, b);
	return e ? c.fn(this) : c.inverse(this)
}), function (a) {
	a.flexslider = function (b, c) {
		var d = a(b);
		d.vars = a.extend({}, a.flexslider.defaults, c);
		var e, f = d.vars.namespace, g = window.navigator && window.navigator.msPointerEnabled && window.MSGesture, h = ("ontouchstart" in window || g || window.DocumentTouch && document instanceof DocumentTouch) && d.vars.touch, i = "click touchend MSPointerUp", j = "", k = "vertical" === d.vars.direction, l = d.vars.reverse, m = d.vars.itemWidth > 0, n = "fade" === d.vars.animation, o = "" !== d.vars.asNavFor, p = {}, q = !0;
		a.data(b, "flexslider", d), p = {
			init: function () {
				d.animating = !1, d.currentSlide = parseInt(d.vars.startAt ? d.vars.startAt : 0), isNaN(d.currentSlide) && (d.currentSlide = 0), d.animatingTo = d.currentSlide, d.atEnd = 0 === d.currentSlide || d.currentSlide === d.last, d.containerSelector = d.vars.selector.substr(0, d.vars.selector.search(" ")), d.slides = a(d.vars.selector, d), d.container = a(d.containerSelector, d), d.count = d.slides.length, d.syncExists = a(d.vars.sync).length > 0, "slide" === d.vars.animation && (d.vars.animation = "swing"), d.prop = k ? "top" : "marginLeft", d.args = {}, d.manualPause = !1, d.stopped = !1, d.started = !1, d.startTimeout = null, d.transitions = !d.vars.video && !n && d.vars.useCSS && function () {
						var a = document.createElement("div"), b = ["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
						for (var c in b)if (void 0 !== a.style[b[c]])return d.pfx = b[c].replace("Perspective", "").toLowerCase(), d.prop = "-" + d.pfx + "-transform", !0;
						return !1
					}(), "" !== d.vars.controlsContainer && (d.controlsContainer = a(d.vars.controlsContainer).length > 0 && a(d.vars.controlsContainer)), "" !== d.vars.manualControls && (d.manualControls = a(d.vars.manualControls).length > 0 && a(d.vars.manualControls)), d.vars.randomize && (d.slides.sort(function () {
					return Math.round(Math.random()) - .5
				}), d.container.empty().append(d.slides)), d.doMath(), d.setup("init"), d.vars.controlNav && p.controlNav.setup(), d.vars.directionNav && p.directionNav.setup(), d.vars.keyboard && (1 === a(d.containerSelector).length || d.vars.multipleKeyboard) && a(document).bind("keyup", function (a) {
					var b = a.keyCode;
					if (!d.animating && (39 === b || 37 === b)) {
						var c = 39 === b ? d.getTarget("next") : 37 === b ? d.getTarget("prev") : !1;
						d.flexAnimate(c, d.vars.pauseOnAction)
					}
				}), d.vars.mousewheel && d.bind("mousewheel", function (a, b) {
					a.preventDefault();
					var c = d.getTarget(0 > b ? "next" : "prev");
					d.flexAnimate(c, d.vars.pauseOnAction)
				}), d.vars.pausePlay && p.pausePlay.setup(), d.vars.slideshow && d.vars.pauseInvisible && p.pauseInvisible.init(), d.vars.slideshow && (d.vars.pauseOnHover && d.hover(function () {
					!d.manualPlay && !d.manualPause && d.pause()
				}, function () {
					!d.manualPause && !d.manualPlay && !d.stopped && d.play()
				}), d.vars.pauseInvisible && p.pauseInvisible.isHidden() || (d.vars.initDelay > 0 ? d.startTimeout = setTimeout(d.play, d.vars.initDelay) : d.play())), o && p.asNav.setup(), h && d.vars.touch && p.touch(), (!n || n && d.vars.smoothHeight) && a(window).bind("resize orientationchange focus", p.resize), d.find("img").attr("draggable", "false"), setTimeout(function () {
					d.vars.start(d)
				}, 200)
			}, asNav: {
				setup: function () {
					d.asNav = !0, d.animatingTo = Math.floor(d.currentSlide / d.move), d.currentItem = d.currentSlide, d.slides.removeClass(f + "active-slide").eq(d.currentItem).addClass(f + "active-slide"), g ? (b._slider = d, d.slides.each(function () {
						var b = this;
						b._gesture = new MSGesture, b._gesture.target = b, b.addEventListener("MSPointerDown", function (a) {
							a.preventDefault(), a.currentTarget._gesture && a.currentTarget._gesture.addPointer(a.pointerId)
						}, !1), b.addEventListener("MSGestureTap", function (b) {
							b.preventDefault();
							var c = a(this), e = c.index();
							a(d.vars.asNavFor).data("flexslider").animating || c.hasClass("active") || (d.direction = d.currentItem < e ? "next" : "prev", d.flexAnimate(e, d.vars.pauseOnAction, !1, !0, !0))
						})
					})) : d.slides.click(function (b) {
						b.preventDefault();
						var c = a(this), e = c.index(), g = c.offset().left - a(d).scrollLeft();
						0 >= g && c.hasClass(f + "active-slide") ? d.flexAnimate(d.getTarget("prev"), !0) : a(d.vars.asNavFor).data("flexslider").animating || c.hasClass(f + "active-slide") || (d.direction = d.currentItem < e ? "next" : "prev", d.flexAnimate(e, d.vars.pauseOnAction, !1, !0, !0))
					})
				}
			}, controlNav: {
				setup: function () {
					d.manualControls ? p.controlNav.setupManual() : p.controlNav.setupPaging()
				}, setupPaging: function () {
					var b, c, e = "thumbnails" === d.vars.controlNav ? "control-thumbs" : "control-paging", g = 1;
					if (d.controlNavScaffold = a('<ol class="' + f + "control-nav " + f + e + '"></ol>'), d.pagingCount > 1)for (var h = 0; h < d.pagingCount; h++) {
						if (c = d.slides.eq(h), b = "thumbnails" === d.vars.controlNav ? '<img src="' + c.attr("data-thumb") + '"/>' : "<a>" + g + "</a>", "thumbnails" === d.vars.controlNav && !0 === d.vars.thumbCaptions) {
							var k = c.attr("data-thumbcaption");
							"" != k && void 0 != k && (b += '<span class="' + f + 'caption">' + k + "</span>")
						}
						d.controlNavScaffold.append("<li>" + b + "</li>"), g++
					}
					d.controlsContainer ? a(d.controlsContainer).append(d.controlNavScaffold) : d.append(d.controlNavScaffold), p.controlNav.set(), p.controlNav.active(), d.controlNavScaffold.delegate("a, img", i, function (b) {
						if (b.preventDefault(), "" === j || j === b.type) {
							var c = a(this), e = d.controlNav.index(c);
							c.hasClass(f + "active") || (d.direction = e > d.currentSlide ? "next" : "prev", d.flexAnimate(e, d.vars.pauseOnAction))
						}
						"" === j && (j = b.type), p.setToClearWatchedEvent()
					})
				}, setupManual: function () {
					d.controlNav = d.manualControls, p.controlNav.active(), d.controlNav.bind(i, function (b) {
						if (b.preventDefault(), "" === j || j === b.type) {
							var c = a(this), e = d.controlNav.index(c);
							c.hasClass(f + "active") || (d.direction = e > d.currentSlide ? "next" : "prev", d.flexAnimate(e, d.vars.pauseOnAction))
						}
						"" === j && (j = b.type), p.setToClearWatchedEvent()
					})
				}, set: function () {
					var b = "thumbnails" === d.vars.controlNav ? "img" : "a";
					d.controlNav = a("." + f + "control-nav li " + b, d.controlsContainer ? d.controlsContainer : d)
				}, active: function () {
					d.controlNav.removeClass(f + "active").eq(d.animatingTo).addClass(f + "active")
				}, update: function (b, c) {
					d.pagingCount > 1 && "add" === b ? d.controlNavScaffold.append(a("<li><a>" + d.count + "</a></li>")) : 1 === d.pagingCount ? d.controlNavScaffold.find("li").remove() : d.controlNav.eq(c).closest("li").remove(), p.controlNav.set(), d.pagingCount > 1 && d.pagingCount !== d.controlNav.length ? d.update(c, b) : p.controlNav.active()
				}
			}, directionNav: {
				setup: function () {
					var b = a('<ul class="' + f + 'direction-nav"><li><a class="' + f + 'prev" href="#">' + d.vars.prevText + '</a></li><li><a class="' + f + 'next" href="#">' + d.vars.nextText + "</a></li></ul>");
					d.controlsContainer ? (a(d.controlsContainer).append(b), d.directionNav = a("." + f + "direction-nav li a", d.controlsContainer)) : (d.append(b), d.directionNav = a("." + f + "direction-nav li a", d)), p.directionNav.update(), d.directionNav.bind(i, function (b) {
						b.preventDefault();
						var c;
						("" === j || j === b.type) && (c = d.getTarget(a(this).hasClass(f + "next") ? "next" : "prev"), d.flexAnimate(c, d.vars.pauseOnAction)), "" === j && (j = b.type), p.setToClearWatchedEvent()
					})
				}, update: function () {
					var a = f + "disabled";
					1 === d.pagingCount ? d.directionNav.addClass(a).attr("tabindex", "-1") : d.vars.animationLoop ? d.directionNav.removeClass(a).removeAttr("tabindex") : 0 === d.animatingTo ? d.directionNav.removeClass(a).filter("." + f + "prev").addClass(a).attr("tabindex", "-1") : d.animatingTo === d.last ? d.directionNav.removeClass(a).filter("." + f + "next").addClass(a).attr("tabindex", "-1") : d.directionNav.removeClass(a).removeAttr("tabindex")
				}
			}, pausePlay: {
				setup: function () {
					var b = a('<div class="' + f + 'pauseplay"><a></a></div>');
					d.controlsContainer ? (d.controlsContainer.append(b), d.pausePlay = a("." + f + "pauseplay a", d.controlsContainer)) : (d.append(b), d.pausePlay = a("." + f + "pauseplay a", d)), p.pausePlay.update(d.vars.slideshow ? f + "pause" : f + "play"), d.pausePlay.bind(i, function (b) {
						b.preventDefault(), ("" === j || j === b.type) && (a(this).hasClass(f + "pause") ? (d.manualPause = !0, d.manualPlay = !1, d.pause()) : (d.manualPause = !1, d.manualPlay = !0, d.play())), "" === j && (j = b.type), p.setToClearWatchedEvent()
					})
				}, update: function (a) {
					"play" === a ? d.pausePlay.removeClass(f + "pause").addClass(f + "play").html(d.vars.playText) : d.pausePlay.removeClass(f + "play").addClass(f + "pause").html(d.vars.pauseText)
				}
			}, touch: function () {
				function a(a) {
					d.animating ? a.preventDefault() : (window.navigator.msPointerEnabled || 1 === a.touches.length) && (d.pause(), q = k ? d.h : d.w, s = Number(new Date), u = a.touches[0].pageX, v = a.touches[0].pageY, p = m && l && d.animatingTo === d.last ? 0 : m && l ? d.limit - (d.itemW + d.vars.itemMargin) * d.move * d.animatingTo : m && d.currentSlide === d.last ? d.limit : m ? (d.itemW + d.vars.itemMargin) * d.move * d.currentSlide : l ? (d.last - d.currentSlide + d.cloneOffset) * q : (d.currentSlide + d.cloneOffset) * q, j = k ? v : u, o = k ? u : v, b.addEventListener("touchmove", c, !1), b.addEventListener("touchend", e, !1))
				}

				function c(a) {
					u = a.touches[0].pageX, v = a.touches[0].pageY, r = k ? j - v : j - u, t = k ? Math.abs(r) < Math.abs(u - o) : Math.abs(r) < Math.abs(v - o);
					var b = 500;
					(!t || Number(new Date) - s > b) && (a.preventDefault(), !n && d.transitions && (d.vars.animationLoop || (r /= 0 === d.currentSlide && 0 > r || d.currentSlide === d.last && r > 0 ? Math.abs(r) / q + 2 : 1), d.setProps(p + r, "setTouch")))
				}

				function e() {
					if (b.removeEventListener("touchmove", c, !1), d.animatingTo === d.currentSlide && !t && null !== r) {
						var a = l ? -r : r, f = d.getTarget(a > 0 ? "next" : "prev");
						d.canAdvance(f) && (Number(new Date) - s < 550 && Math.abs(a) > 50 || Math.abs(a) > q / 2) ? d.flexAnimate(f, d.vars.pauseOnAction) : n || d.flexAnimate(d.currentSlide, d.vars.pauseOnAction, !0)
					}
					b.removeEventListener("touchend", e, !1), j = null, o = null, r = null, p = null
				}

				function f(a) {
					a.stopPropagation(), d.animating ? a.preventDefault() : (d.pause(), b._gesture.addPointer(a.pointerId), w = 0, q = k ? d.h : d.w, s = Number(new Date), p = m && l && d.animatingTo === d.last ? 0 : m && l ? d.limit - (d.itemW + d.vars.itemMargin) * d.move * d.animatingTo : m && d.currentSlide === d.last ? d.limit : m ? (d.itemW + d.vars.itemMargin) * d.move * d.currentSlide : l ? (d.last - d.currentSlide + d.cloneOffset) * q : (d.currentSlide + d.cloneOffset) * q)
				}

				function h(a) {
					a.stopPropagation();
					var c = a.target._slider;
					if (c) {
						var d = -a.translationX, e = -a.translationY;
						return w += k ? e : d, r = w, t = k ? Math.abs(w) < Math.abs(-d) : Math.abs(w) < Math.abs(-e), a.detail === a.MSGESTURE_FLAG_INERTIA ? void setImmediate(function () {
							b._gesture.stop()
						}) : void((!t || Number(new Date) - s > 500) && (a.preventDefault(), !n && c.transitions && (c.vars.animationLoop || (r = w / (0 === c.currentSlide && 0 > w || c.currentSlide === c.last && w > 0 ? Math.abs(w) / q + 2 : 1)), c.setProps(p + r, "setTouch"))))
					}
				}

				function i(a) {
					a.stopPropagation();
					var b = a.target._slider;
					if (b) {
						if (b.animatingTo === b.currentSlide && !t && null !== r) {
							var c = l ? -r : r, d = b.getTarget(c > 0 ? "next" : "prev");
							b.canAdvance(d) && (Number(new Date) - s < 550 && Math.abs(c) > 50 || Math.abs(c) > q / 2) ? b.flexAnimate(d, b.vars.pauseOnAction) : n || b.flexAnimate(b.currentSlide, b.vars.pauseOnAction, !0)
						}
						j = null, o = null, r = null, p = null, w = 0
					}
				}

				var j, o, p, q, r, s, t = !1, u = 0, v = 0, w = 0;
				g ? (b.style.msTouchAction = "none", b._gesture = new MSGesture, b._gesture.target = b, b.addEventListener("MSPointerDown", f, !1), b._slider = d, b.addEventListener("MSGestureChange", h, !1), b.addEventListener("MSGestureEnd", i, !1)) : b.addEventListener("touchstart", a, !1)
			}, resize: function () {
				!d.animating && d.is(":visible") && (m || d.doMath(), n ? p.smoothHeight() : m ? (d.slides.width(d.computedW), d.update(d.pagingCount), d.setProps()) : k ? (d.viewport.height(d.h), d.setProps(d.h, "setTotal")) : (d.vars.smoothHeight && p.smoothHeight(), d.newSlides.width(d.computedW), d.setProps(d.computedW, "setTotal")))
			}, smoothHeight: function (a) {
				if (!k || n) {
					var b = n ? d : d.viewport;
					a ? b.animate({height: d.slides.eq(d.animatingTo).height()}, a) : b.height(d.slides.eq(d.animatingTo).height())
				}
			}, sync: function (b) {
				var c = a(d.vars.sync).data("flexslider"), e = d.animatingTo;
				switch (b) {
					case"animate":
						c.flexAnimate(e, d.vars.pauseOnAction, !1, !0);
						break;
					case"play":
						!c.playing && !c.asNav && c.play();
						break;
					case"pause":
						c.pause()
				}
			}, pauseInvisible: {
				visProp: null, init: function () {
					var a = ["webkit", "moz", "ms", "o"];
					if ("hidden" in document)return "hidden";
					for (var b = 0; b < a.length; b++)a[b] + "Hidden" in document && (p.pauseInvisible.visProp = a[b] + "Hidden");
					if (p.pauseInvisible.visProp) {
						var c = p.pauseInvisible.visProp.replace(/[H|h]idden/, "") + "visibilitychange";
						document.addEventListener(c, function () {
							p.pauseInvisible.isHidden() ? d.startTimeout ? clearTimeout(d.startTimeout) : d.pause() : d.started ? d.play() : d.vars.initDelay > 0 ? setTimeout(d.play, d.vars.initDelay) : d.play()
						})
					}
				}, isHidden: function () {
					return document[p.pauseInvisible.visProp] || !1
				}
			}, setToClearWatchedEvent: function () {
				clearTimeout(e), e = setTimeout(function () {
					j = ""
				}, 3e3)
			}
		}, d.flexAnimate = function (b, c, e, g, i) {
			if (!d.vars.animationLoop && b !== d.currentSlide && (d.direction = b > d.currentSlide ? "next" : "prev"), o && 1 === d.pagingCount && (d.direction = d.currentItem < b ? "next" : "prev"), !d.animating && (d.canAdvance(b, i) || e) && d.is(":visible")) {
				if (o && g) {
					var j = a(d.vars.asNavFor).data("flexslider");
					if (d.atEnd = 0 === b || b === d.count - 1, j.flexAnimate(b, !0, !1, !0, i), d.direction = d.currentItem < b ? "next" : "prev", j.direction = d.direction, Math.ceil((b + 1) / d.visible) - 1 === d.currentSlide || 0 === b)return d.currentItem = b, d.slides.removeClass(f + "active-slide").eq(b).addClass(f + "active-slide"), !1;
					d.currentItem = b, d.slides.removeClass(f + "active-slide").eq(b).addClass(f + "active-slide"), b = Math.floor(b / d.visible)
				}
				if (d.animating = !0, d.animatingTo = b, c && d.pause(), d.vars.before(d), d.syncExists && !i && p.sync("animate"), d.vars.controlNav && p.controlNav.active(), m || d.slides.removeClass(f + "active-slide").eq(b).addClass(f + "active-slide"), d.atEnd = 0 === b || b === d.last, d.vars.directionNav && p.directionNav.update(), b === d.last && (d.vars.end(d), d.vars.animationLoop || d.pause()), n)h ? (d.slides.eq(d.currentSlide).css({
					opacity: 0,
					zIndex: 1
				}), d.slides.eq(b).css({
					opacity: 1,
					zIndex: 2
				}), d.wrapup(t)) : (d.slides.eq(d.currentSlide).css({zIndex: 1}).animate({opacity: 0}, d.vars.animationSpeed, d.vars.easing), d.slides.eq(b).css({zIndex: 2}).animate({opacity: 1}, d.vars.animationSpeed, d.vars.easing, d.wrapup)); else {
					var q, r, s, t = k ? d.slides.filter(":first").height() : d.computedW;
					m ? (q = d.vars.itemMargin, s = (d.itemW + q) * d.move * d.animatingTo, r = s > d.limit && 1 !== d.visible ? d.limit : s) : r = 0 === d.currentSlide && b === d.count - 1 && d.vars.animationLoop && "next" !== d.direction ? l ? (d.count + d.cloneOffset) * t : 0 : d.currentSlide === d.last && 0 === b && d.vars.animationLoop && "prev" !== d.direction ? l ? 0 : (d.count + 1) * t : l ? (d.count - 1 - b + d.cloneOffset) * t : (b + d.cloneOffset) * t, d.setProps(r, "", d.vars.animationSpeed), d.transitions ? (d.vars.animationLoop && d.atEnd || (d.animating = !1, d.currentSlide = d.animatingTo), d.container.unbind("webkitTransitionEnd transitionend"), d.container.bind("webkitTransitionEnd transitionend", function () {
						d.wrapup(t)
					})) : d.container.animate(d.args, d.vars.animationSpeed, d.vars.easing, function () {
						d.wrapup(t)
					})
				}
				d.vars.smoothHeight && p.smoothHeight(d.vars.animationSpeed)
			}
		}, d.wrapup = function (a) {
			!n && !m && (0 === d.currentSlide && d.animatingTo === d.last && d.vars.animationLoop ? d.setProps(a, "jumpEnd") : d.currentSlide === d.last && 0 === d.animatingTo && d.vars.animationLoop && d.setProps(a, "jumpStart")), d.animating = !1, d.currentSlide = d.animatingTo, d.vars.after(d)
		}, d.animateSlides = function () {
			!d.animating && q && d.flexAnimate(d.getTarget("next"))
		}, d.pause = function () {
			clearInterval(d.animatedSlides), d.animatedSlides = null, d.playing = !1, d.vars.pausePlay && p.pausePlay.update("play"), d.syncExists && p.sync("pause")
		}, d.play = function () {
			d.playing && clearInterval(d.animatedSlides), d.animatedSlides = d.animatedSlides || setInterval(d.animateSlides, d.vars.slideshowSpeed), d.started = d.playing = !0, d.vars.pausePlay && p.pausePlay.update("pause"), d.syncExists && p.sync("play")
		}, d.stop = function () {
			d.pause(), d.stopped = !0
		}, d.canAdvance = function (a, b) {
			var c = o ? d.pagingCount - 1 : d.last;
			return b ? !0 : o && d.currentItem === d.count - 1 && 0 === a && "prev" === d.direction ? !0 : o && 0 === d.currentItem && a === d.pagingCount - 1 && "next" !== d.direction ? !1 : a !== d.currentSlide || o ? d.vars.animationLoop ? !0 : d.atEnd && 0 === d.currentSlide && a === c && "next" !== d.direction ? !1 : d.atEnd && d.currentSlide === c && 0 === a && "next" === d.direction ? !1 : !0 : !1
		}, d.getTarget = function (a) {
			return d.direction = a, "next" === a ? d.currentSlide === d.last ? 0 : d.currentSlide + 1 : 0 === d.currentSlide ? d.last : d.currentSlide - 1
		}, d.setProps = function (a, b, c) {
			var e = function () {
				var c = a ? a : (d.itemW + d.vars.itemMargin) * d.move * d.animatingTo, e = function () {
					if (m)return "setTouch" === b ? a : l && d.animatingTo === d.last ? 0 : l ? d.limit - (d.itemW + d.vars.itemMargin) * d.move * d.animatingTo : d.animatingTo === d.last ? d.limit : c;
					switch (b) {
						case"setTotal":
							return l ? (d.count - 1 - d.currentSlide + d.cloneOffset) * a : (d.currentSlide + d.cloneOffset) * a;
						case"setTouch":
							return l ? a : a;
						case"jumpEnd":
							return l ? a : d.count * a;
						case"jumpStart":
							return l ? d.count * a : a;
						default:
							return a
					}
				}();
				return -1 * e + "px"
			}();
			d.transitions && (e = k ? "translate3d(0," + e + ",0)" : "translate3d(" + e + ",0,0)", c = void 0 !== c ? c / 1e3 + "s" : "0s", d.container.css("-" + d.pfx + "-transition-duration", c)), d.args[d.prop] = e, (d.transitions || void 0 === c) && d.container.css(d.args)
		}, d.setup = function (b) {
			if (n)d.slides.css({
				width: "100%",
				"float": "left",
				marginRight: "-100%",
				position: "relative"
			}), "init" === b && (h ? d.slides.css({
				opacity: 0,
				display: "block",
				webkitTransition: "opacity " + d.vars.animationSpeed / 1e3 + "s ease",
				zIndex: 1
			}).eq(d.currentSlide).css({opacity: 1, zIndex: 2}) : d.slides.css({
				opacity: 0,
				display: "block",
				zIndex: 1
			}).eq(d.currentSlide).css({zIndex: 2}).animate({opacity: 1}, d.vars.animationSpeed, d.vars.easing)), d.vars.smoothHeight && p.smoothHeight(); else {
				var c, e;
				"init" === b && (d.viewport = a('<div class="' + f + 'viewport"></div>').css({
					overflow: "hidden",
					position: "relative"
				}).appendTo(d).append(d.container), d.cloneCount = 0, d.cloneOffset = 0, l && (e = a.makeArray(d.slides).reverse(), d.slides = a(e), d.container.empty().append(d.slides))), d.vars.animationLoop && !m && (d.cloneCount = 2, d.cloneOffset = 1, "init" !== b && d.container.find(".clone").remove(), d.container.append(d.slides.first().clone().addClass("clone").attr("aria-hidden", "true")).prepend(d.slides.last().clone().addClass("clone").attr("aria-hidden", "true"))), d.newSlides = a(d.vars.selector, d), c = l ? d.count - 1 - d.currentSlide + d.cloneOffset : d.currentSlide + d.cloneOffset, k && !m ? (d.container.height(200 * (d.count + d.cloneCount) + "%").css("position", "absolute").width("100%"), setTimeout(function () {
					d.newSlides.css({display: "block"}), d.doMath(), d.viewport.height(d.h), d.setProps(c * d.h, "init")
				}, "init" === b ? 100 : 0)) : (d.container.width(200 * (d.count + d.cloneCount) + "%"), d.setProps(c * d.computedW, "init"), setTimeout(function () {
					d.doMath(), d.newSlides.css({
						width: d.computedW,
						"float": "left",
						display: "block"
					}), d.vars.smoothHeight && p.smoothHeight()
				}, "init" === b ? 100 : 0))
			}
			m || d.slides.removeClass(f + "active-slide").eq(d.currentSlide).addClass(f + "active-slide")
		}, d.doMath = function () {
			var a = d.slides.first(), b = d.vars.itemMargin, c = d.vars.minItems, e = d.vars.maxItems;
			d.w = void 0 === d.viewport ? d.width() : d.viewport.width(), d.h = a.height(), d.boxPadding = a.outerWidth() - a.width(), m ? (d.itemT = d.vars.itemWidth + b, d.minW = c ? c * d.itemT : d.w, d.maxW = e ? e * d.itemT - b : d.w, d.itemW = d.minW > d.w ? (d.w - b * (c - 1)) / c : d.maxW < d.w ? (d.w - b * (e - 1)) / e : d.vars.itemWidth > d.w ? d.w : d.vars.itemWidth, d.visible = Math.floor(d.w / d.itemW), d.move = d.vars.move > 0 && d.vars.move < d.visible ? d.vars.move : d.visible, d.pagingCount = Math.ceil((d.count - d.visible) / d.move + 1), d.last = d.pagingCount - 1, d.limit = 1 === d.pagingCount ? 0 : d.vars.itemWidth > d.w ? d.itemW * (d.count - 1) + b * (d.count - 1) : (d.itemW + b) * d.count - d.w - b) : (d.itemW = d.w, d.pagingCount = d.count, d.last = d.count - 1), d.computedW = d.itemW - d.boxPadding
		}, d.update = function (a, b) {
			d.doMath(), m || (a < d.currentSlide ? d.currentSlide += 1 : a <= d.currentSlide && 0 !== a && (d.currentSlide -= 1), d.animatingTo = d.currentSlide), d.vars.controlNav && !d.manualControls && ("add" === b && !m || d.pagingCount > d.controlNav.length ? p.controlNav.update("add") : ("remove" === b && !m || d.pagingCount < d.controlNav.length) && (m && d.currentSlide > d.last && (d.currentSlide -= 1, d.animatingTo -= 1), p.controlNav.update("remove", d.last))), d.vars.directionNav && p.directionNav.update()
		}, d.addSlide = function (b, c) {
			var e = a(b);
			d.count += 1, d.last = d.count - 1, k && l ? void 0 !== c ? d.slides.eq(d.count - c).after(e) : d.container.prepend(e) : void 0 !== c ? d.slides.eq(c).before(e) : d.container.append(e), d.update(c, "add"), d.slides = a(d.vars.selector + ":not(.clone)", d), d.setup(), d.vars.added(d)
		}, d.removeSlide = function (b) {
			var c = isNaN(b) ? d.slides.index(a(b)) : b;
			d.count -= 1, d.last = d.count - 1, isNaN(b) ? a(b, d.slides).remove() : k && l ? d.slides.eq(d.last).remove() : d.slides.eq(b).remove(), d.doMath(), d.update(c, "remove"), d.slides = a(d.vars.selector + ":not(.clone)", d), d.setup(), d.vars.removed(d)
		}, p.init()
	}, a(window).blur(function () {
		focused = !1
	}).focus(function () {
		focused = !0
	}), a.flexslider.defaults = {
		namespace: "flex-",
		selector: ".slides > li",
		animation: "fade",
		easing: "swing",
		direction: "horizontal",
		reverse: !1,
		animationLoop: !0,
		smoothHeight: !1,
		startAt: 0,
		slideshow: !0,
		slideshowSpeed: 7e3,
		animationSpeed: 600,
		initDelay: 0,
		randomize: !1,
		thumbCaptions: !1,
		pauseOnAction: !0,
		pauseOnHover: !1,
		pauseInvisible: !0,
		useCSS: !0,
		touch: !0,
		video: !1,
		controlNav: !0,
		directionNav: !0,
		prevText: "Previous",
		nextText: "Next",
		keyboard: !0,
		multipleKeyboard: !1,
		mousewheel: !1,
		pausePlay: !1,
		pauseText: "Pause",
		playText: "Play",
		controlsContainer: "",
		manualControls: "",
		sync: "",
		asNavFor: "",
		itemWidth: 0,
		itemMargin: 0,
		minItems: 1,
		maxItems: 0,
		move: 0,
		allowOneSlide: !0,
		start: function () {
		},
		before: function () {
		},
		after: function () {
		},
		end: function () {
		},
		added: function () {
		},
		removed: function () {
		}
	}, a.fn.flexslider = function (b) {
		if (void 0 === b && (b = {}), "object" == typeof b)return this.each(function () {
			var c = a(this), d = b.selector ? b.selector : ".slides > li", e = c.find(d);
			1 === e.length && b.allowOneSlide === !0 || 0 === e.length ? (e.fadeIn(400), b.start && b.start(c)) : void 0 === c.data("flexslider") && new a.flexslider(this, b)
		});
		var c = a(this).data("flexslider");
		switch (b) {
			case"play":
				c.play();
				break;
			case"pause":
				c.pause();
				break;
			case"stop":
				c.stop();
				break;
			case"next":
				c.flexAnimate(c.getTarget("next"), !0);
				break;
			case"prev":
			case"previous":
				c.flexAnimate(c.getTarget("prev"), !0);
				break;
			default:
				"number" == typeof b && c.flexAnimate(b, !0)
		}
	}
}(jQuery), window.JSON || (window.JSON = {}), function () {
	function f(a) {
		return 10 > a ? "0" + a : a
	}

	function quote(a) {
		return escapable.lastIndex = 0, escapable.test(a) ? '"' + a.replace(escapable, function (a) {
			var b = meta[a];
			return "string" == typeof b ? b : "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4)
		}) + '"' : '"' + a + '"'
	}

	function str(a, b) {
		var c, d, e, f, g, h = gap, i = b[a];
		switch (i && "object" == typeof i && "function" == typeof i.toJSON && (i = i.toJSON(a)), "function" == typeof rep && (i = rep.call(b, a, i)), typeof i) {
			case"string":
				return quote(i);
			case"number":
				return isFinite(i) ? String(i) : "null";
			case"boolean":
			case"null":
				return String(i);
			case"object":
				if (!i)return "null";
				if (gap += indent, g = [], "[object Array]" === Object.prototype.toString.apply(i)) {
					for (f = i.length, c = 0; f > c; c += 1)g[c] = str(c, i) || "null";
					return e = 0 === g.length ? "[]" : gap ? "[\n" + gap + g.join(",\n" + gap) + "\n" + h + "]" : "[" + g.join(",") + "]", gap = h, e
				}
				if (rep && "object" == typeof rep)for (f = rep.length, c = 0; f > c; c += 1)d = rep[c], "string" == typeof d && (e = str(d, i), e && g.push(quote(d) + (gap ? ": " : ":") + e)); else for (d in i)Object.hasOwnProperty.call(i, d) && (e = str(d, i), e && g.push(quote(d) + (gap ? ": " : ":") + e));
				return e = 0 === g.length ? "{}" : gap ? "{\n" + gap + g.join(",\n" + gap) + "\n" + h + "}" : "{" + g.join(",") + "}", gap = h, e
		}
	}

	"function" != typeof Date.prototype.toJSON && (Date.prototype.toJSON = function () {
		return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-" + f(this.getUTCMonth() + 1) + "-" + f(this.getUTCDate()) + "T" + f(this.getUTCHours()) + ":" + f(this.getUTCMinutes()) + ":" + f(this.getUTCSeconds()) + "Z" : null
	}, String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function () {
		return this.valueOf()
	});
	var JSON = window.JSON, cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, gap, indent, meta = {
		"\b": "\\b",
		"	": "\\t",
		"\n": "\\n",
		"\f": "\\f",
		"\r": "\\r",
		'"': '\\"',
		"\\": "\\\\"
	}, rep;
	"function" != typeof JSON.stringify && (JSON.stringify = function (a, b, c) {
		var d;
		if (gap = "", indent = "", "number" == typeof c)for (d = 0; c > d; d += 1)indent += " "; else"string" == typeof c && (indent = c);
		if (rep = b, !b || "function" == typeof b || "object" == typeof b && "number" == typeof b.length)return str("", {"": a});
		throw new Error("JSON.stringify")
	}), "function" != typeof JSON.parse && (JSON.parse = function (text, reviver) {
		function walk(a, b) {
			var c, d, e = a[b];
			if (e && "object" == typeof e)for (c in e)Object.hasOwnProperty.call(e, c) && (d = walk(e, c), void 0 !== d ? e[c] = d : delete e[c]);
			return reviver.call(a, b, e)
		}

		var j;
		if (text = String(text), cx.lastIndex = 0, cx.test(text) && (text = text.replace(cx, function (a) {
				return "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4)
			})), /^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, "")))return j = eval("(" + text + ")"), "function" == typeof reviver ? walk({"": j}, "") : j;
		throw new SyntaxError("JSON.parse")
	})
}(), function (a, b) {
	"use strict";
	var c = a.History = a.History || {}, d = a.jQuery;
	if ("undefined" != typeof c.Adapter)throw new Error("History.js Adapter has already been loaded...");
	c.Adapter = {
		bind: function (a, b, c) {
			d(a).bind(b, c)
		}, trigger: function (a, b, c) {
			d(a).trigger(b, c)
		}, extractEventData: function (a, c, d) {
			var e = c && c.originalEvent && c.originalEvent[a] || d && d[a] || b;
			return e
		}, onDomLoad: function (a) {
			d(a)
		}
	}, "undefined" != typeof c.init && c.init()
}(window), function (a) {
	"use strict";
	var b = a.document, c = a.setTimeout || c, d = a.clearTimeout || d, e = a.setInterval || e, f = a.History = a.History || {};
	if ("undefined" != typeof f.initHtml4)throw new Error("History.js HTML4 Support has already been loaded...");
	f.initHtml4 = function () {
		return "undefined" != typeof f.initHtml4.initialized ? !1 : (f.initHtml4.initialized = !0, f.enabled = !0, f.savedHashes = [], f.isLastHash = function (a) {
			var b, c = f.getHashByIndex();
			return b = a === c
		}, f.saveHash = function (a) {
			return f.isLastHash(a) ? !1 : (f.savedHashes.push(a), !0)
		}, f.getHashByIndex = function (a) {
			var b = null;
			return b = "undefined" == typeof a ? f.savedHashes[f.savedHashes.length - 1] : 0 > a ? f.savedHashes[f.savedHashes.length + a] : f.savedHashes[a]
		}, f.discardedHashes = {}, f.discardedStates = {}, f.discardState = function (a, b, c) {
			var d, e = f.getHashByState(a);
			return d = {discardedState: a, backState: c, forwardState: b}, f.discardedStates[e] = d, !0
		}, f.discardHash = function (a, b, c) {
			var d = {discardedHash: a, backState: c, forwardState: b};
			return f.discardedHashes[a] = d, !0
		}, f.discardedState = function (a) {
			var b, c = f.getHashByState(a);
			return b = f.discardedStates[c] || !1
		}, f.discardedHash = function (a) {
			var b = f.discardedHashes[a] || !1;
			return b
		}, f.recycleState = function (a) {
			var b = f.getHashByState(a);
			return f.discardedState(a) && delete f.discardedStates[b], !0
		}, f.emulated.hashChange && (f.hashChangeInit = function () {
			f.checkerFunction = null;
			var c, d, g, h, i = "";
			return f.isInternetExplorer() ? (c = "historyjs-iframe", d = b.createElement("iframe"), d.setAttribute("id", c), d.style.display = "none", b.body.appendChild(d), d.contentWindow.document.open(), d.contentWindow.document.close(), g = "", h = !1, f.checkerFunction = function () {
				if (h)return !1;
				h = !0;
				var b = f.getHash() || "", c = f.unescapeHash(d.contentWindow.document.location.hash) || "";
				return b !== i ? (i = b, c !== b && (g = c = b, d.contentWindow.document.open(), d.contentWindow.document.close(), d.contentWindow.document.location.hash = f.escapeHash(b)), f.Adapter.trigger(a, "hashchange")) : c !== g && (g = c, f.setHash(c, !1)), h = !1, !0
			}) : f.checkerFunction = function () {
				var b = f.getHash();
				return b !== i && (i = b, f.Adapter.trigger(a, "hashchange")), !0
			}, f.intervalList.push(e(f.checkerFunction, f.options.hashChangeInterval)), !0
		}, f.Adapter.onDomLoad(f.hashChangeInit)), f.emulated.pushState && (f.onHashChange = function (c) {
			var d, e = c && c.newURL || b.location.href, g = f.getHashByUrl(e), h = null, i = null;
			return f.isLastHash(g) ? (f.busy(!1), !1) : (f.doubleCheckComplete(), f.saveHash(g), g && f.isTraditionalAnchor(g) ? (f.Adapter.trigger(a, "anchorchange"), f.busy(!1), !1) : (h = f.extractState(f.getFullUrl(g || b.location.href, !1), !0), f.isLastSavedState(h) ? (f.busy(!1), !1) : (i = f.getHashByState(h), d = f.discardedState(h), d ? (f.getHashByIndex(-2) === f.getHashByState(d.forwardState) ? f.back(!1) : f.forward(!1), !1) : (f.pushState(h.data, h.title, h.url, !1), !0))))
		}, f.Adapter.bind(a, "hashchange", f.onHashChange), f.pushState = function (c, d, e, g) {
			if (f.getHashByUrl(e))throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
			if (g !== !1 && f.busy())return f.pushQueue({
				scope: f,
				callback: f.pushState,
				args: arguments,
				queue: g
			}), !1;
			f.busy(!0);
			var h = f.createStateObject(c, d, e), i = f.getHashByState(h), j = f.getState(!1), k = f.getHashByState(j), l = f.getHash();
			return f.storeState(h), f.expectedStateId = h.id, f.recycleState(h), f.setTitle(h), i === k ? (f.busy(!1), !1) : i !== l && i !== f.getShortUrl(b.location.href) ? (f.setHash(i, !1), !1) : (f.saveState(h), f.Adapter.trigger(a, "statechange"), f.busy(!1), !0)
		}, f.replaceState = function (a, b, c, d) {
			if (f.getHashByUrl(c))throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
			if (d !== !1 && f.busy())return f.pushQueue({
				scope: f,
				callback: f.replaceState,
				args: arguments,
				queue: d
			}), !1;
			f.busy(!0);
			var e = f.createStateObject(a, b, c), g = f.getState(!1), h = f.getStateByIndex(-2);
			return f.discardState(g, e, h), f.pushState(e.data, e.title, e.url, !1), !0
		}), f.emulated.pushState && f.getHash() && !f.emulated.hashChange && f.Adapter.onDomLoad(function () {
			f.Adapter.trigger(a, "hashchange")
		}), void 0)
	}, "undefined" != typeof f.init && f.init()
}(window), function (a, b) {
	"use strict";
	var c = a.console || b, d = a.document, e = a.navigator, f = a.sessionStorage || !1, g = a.setTimeout, h = a.clearTimeout, i = a.setInterval, j = a.clearInterval, k = a.JSON, l = a.alert, m = a.History = a.History || {}, n = a.history;
	if (k.stringify = k.stringify || k.encode, k.parse = k.parse || k.decode, "undefined" != typeof m.init)throw new Error("History.js Core has already been loaded...");
	m.init = function () {
		return "undefined" == typeof m.Adapter ? !1 : ("undefined" != typeof m.initCore && m.initCore(), "undefined" != typeof m.initHtml4 && m.initHtml4(), !0)
	}, m.initCore = function () {
		if ("undefined" != typeof m.initCore.initialized)return !1;
		if (m.initCore.initialized = !0, m.options = m.options || {}, m.options.hashChangeInterval = m.options.hashChangeInterval || 100, m.options.safariPollInterval = m.options.safariPollInterval || 500, m.options.doubleCheckInterval = m.options.doubleCheckInterval || 500, m.options.storeInterval = m.options.storeInterval || 1e3, m.options.busyDelay = m.options.busyDelay || 250, m.options.debug = m.options.debug || !1, m.options.initialTitle = m.options.initialTitle || d.title, m.intervalList = [], m.clearAllIntervals = function () {
				var a, b = m.intervalList;
				if ("undefined" != typeof b && null !== b) {
					for (a = 0; a < b.length; a++)j(b[a]);
					m.intervalList = null
				}
			}, m.debug = function () {
				(m.options.debug || !1) && m.log.apply(m, arguments)
			}, m.log = function () {
				var a, b, e, f, g, h = "undefined" != typeof c && "undefined" != typeof c.log && "undefined" != typeof c.log.apply, i = d.getElementById("log");
				for (h ? (f = Array.prototype.slice.call(arguments), a = f.shift(), "undefined" != typeof c.debug ? c.debug.apply(c, [a, f]) : c.log.apply(c, [a, f])) : a = "\n" + arguments[0] + "\n", b = 1, e = arguments.length; e > b; ++b) {
					if (g = arguments[b], "object" == typeof g && "undefined" != typeof k)try {
						g = k.stringify(g)
					} catch (j) {
					}
					a += "\n" + g + "\n"
				}
				return i ? (i.value += a + "\n-----\n", i.scrollTop = i.scrollHeight - i.clientHeight) : h || l(a), !0
			}, m.getInternetExplorerMajorVersion = function () {
				var a = m.getInternetExplorerMajorVersion.cached = "undefined" != typeof m.getInternetExplorerMajorVersion.cached ? m.getInternetExplorerMajorVersion.cached : function () {
					for (var a = 3, b = d.createElement("div"), c = b.getElementsByTagName("i"); (b.innerHTML = "<!--[if gt IE " + ++a + "]><i></i><![endif]-->") && c[0];);
					return a > 4 ? a : !1
				}();
				return a
			}, m.isInternetExplorer = function () {
				var a = m.isInternetExplorer.cached = "undefined" != typeof m.isInternetExplorer.cached ? m.isInternetExplorer.cached : Boolean(m.getInternetExplorerMajorVersion());
				return a
			}, m.emulated = {
				pushState: !Boolean(a.history && a.history.pushState && a.history.replaceState && !/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(e.userAgent) && !/AppleWebKit\/5([0-2]|3[0-2])/i.test(e.userAgent)),
				hashChange: Boolean(!("onhashchange" in a || "onhashchange" in d) || m.isInternetExplorer() && m.getInternetExplorerMajorVersion() < 8)
			}, m.enabled = !m.emulated.pushState, m.bugs = {
				setHash: Boolean(!m.emulated.pushState && "Apple Computer, Inc." === e.vendor && /AppleWebKit\/5([0-2]|3[0-3])/.test(e.userAgent)),
				safariPoll: Boolean(!m.emulated.pushState && "Apple Computer, Inc." === e.vendor && /AppleWebKit\/5([0-2]|3[0-3])/.test(e.userAgent)),
				ieDoubleCheck: Boolean(m.isInternetExplorer() && m.getInternetExplorerMajorVersion() < 8),
				hashEscape: Boolean(m.isInternetExplorer() && m.getInternetExplorerMajorVersion() < 7)
			}, m.isEmptyObject = function (a) {
				for (var b in a)return !1;
				return !0
			}, m.cloneObject = function (a) {
				var b, c;
				return a ? (b = k.stringify(a), c = k.parse(b)) : c = {}, c
			}, m.getRootUrl = function () {
				var a = d.location.protocol + "//" + (d.location.hostname || d.location.host);
				return d.location.port && (a += ":" + d.location.port), a += "/"
			}, m.getBaseHref = function () {
				var a = d.getElementsByTagName("base"), b = null, c = "";
				return 1 === a.length && (b = a[0], c = b.href.replace(/[^\/]+$/, "")), c = c.replace(/\/+$/, ""), c && (c += "/"), c
			}, m.getBaseUrl = function () {
				var a = m.getBaseHref() || m.getBasePageUrl() || m.getRootUrl();
				return a
			}, m.getPageUrl = function () {
				var a, b = m.getState(!1, !1), c = (b || {}).url || d.location.href;
				return a = c.replace(/\/+$/, "").replace(/[^\/]+$/, function (a) {
					return /\./.test(a) ? a : a + "/"
				})
			}, m.getBasePageUrl = function () {
				var a = d.location.href.replace(/[#\?].*/, "").replace(/[^\/]+$/, function (a) {
						return /[^\/]$/.test(a) ? "" : a
					}).replace(/\/+$/, "") + "/";
				return a
			}, m.getFullUrl = function (a, b) {
				var c = a, d = a.substring(0, 1);
				return b = "undefined" == typeof b ? !0 : b, /[a-z]+\:\/\//.test(a) || (c = "/" === d ? m.getRootUrl() + a.replace(/^\/+/, "") : "#" === d ? m.getPageUrl().replace(/#.*/, "") + a : "?" === d ? m.getPageUrl().replace(/[\?#].*/, "") + a : b ? m.getBaseUrl() + a.replace(/^(\.\/)+/, "") : m.getBasePageUrl() + a.replace(/^(\.\/)+/, "")), c.replace(/\#$/, "")
			}, m.getShortUrl = function (a) {
				var b = a, c = m.getBaseUrl(), d = m.getRootUrl();
				return m.emulated.pushState && (b = b.replace(c, "")), b = b.replace(d, "/"), m.isTraditionalAnchor(b) && (b = "./" + b), b = b.replace(/^(\.\/)+/g, "./").replace(/\#$/, "")
			}, m.store = {}, m.idToState = m.idToState || {}, m.stateToId = m.stateToId || {}, m.urlToId = m.urlToId || {}, m.storedStates = m.storedStates || [], m.savedStates = m.savedStates || [], m.normalizeStore = function () {
				m.store.idToState = m.store.idToState || {}, m.store.urlToId = m.store.urlToId || {}, m.store.stateToId = m.store.stateToId || {}
			}, m.getState = function (a, b) {
				"undefined" == typeof a && (a = !0), "undefined" == typeof b && (b = !0);
				var c = m.getLastSavedState();
				return !c && b && (c = m.createStateObject()), a && (c = m.cloneObject(c), c.url = c.cleanUrl || c.url), c
			}, m.getIdByState = function (a) {
				var b, c = m.extractId(a.url);
				if (!c)if (b = m.getStateString(a), "undefined" != typeof m.stateToId[b])c = m.stateToId[b]; else if ("undefined" != typeof m.store.stateToId[b])c = m.store.stateToId[b]; else {
					for (; c = (new Date).getTime() + String(Math.random()).replace(/\D/g, ""), "undefined" != typeof m.idToState[c] || "undefined" != typeof m.store.idToState[c];);
					m.stateToId[b] = c, m.idToState[c] = a
				}
				return c
			}, m.normalizeState = function (a) {
				var b, c;
				return a && "object" == typeof a || (a = {}), "undefined" != typeof a.normalized ? a : (a.data && "object" == typeof a.data || (a.data = {}), b = {}, b.normalized = !0, b.title = a.title || "", b.url = m.getFullUrl(m.unescapeString(a.url || d.location.href)), b.hash = m.getShortUrl(b.url), b.data = m.cloneObject(a.data), b.id = m.getIdByState(b), b.cleanUrl = b.url.replace(/\??\&_suid.*/, ""), b.url = b.cleanUrl, c = !m.isEmptyObject(b.data), (b.title || c) && (b.hash = m.getShortUrl(b.url).replace(/\??\&_suid.*/, ""), /\?/.test(b.hash) || (b.hash += "?"), b.hash += "&_suid=" + b.id), b.hashedUrl = m.getFullUrl(b.hash), (m.emulated.pushState || m.bugs.safariPoll) && m.hasUrlDuplicate(b) && (b.url = b.hashedUrl), b)
			}, m.createStateObject = function (a, b, c) {
				var d = {data: a, title: b, url: c};
				return d = m.normalizeState(d)
			}, m.getStateById = function (a) {
				a = String(a);
				var c = m.idToState[a] || m.store.idToState[a] || b;
				return c
			}, m.getStateString = function (a) {
				var b, c, d;
				return b = m.normalizeState(a), c = {data: b.data, title: a.title, url: a.url}, d = k.stringify(c)
			}, m.getStateId = function (a) {
				var b, c;
				return b = m.normalizeState(a), c = b.id
			}, m.getHashByState = function (a) {
				var b, c;
				return b = m.normalizeState(a), c = b.hash
			}, m.extractId = function (a) {
				var b, c, d;
				return c = /(.*)\&_suid=([0-9]+)$/.exec(a), d = c ? c[1] || a : a, b = c ? String(c[2] || "") : "", b || !1
			}, m.isTraditionalAnchor = function (a) {
				var b = !/[\/\?\.]/.test(a);
				return b
			}, m.extractState = function (a, b) {
				var c, d, e = null;
				return b = b || !1, c = m.extractId(a), c && (e = m.getStateById(c)), e || (d = m.getFullUrl(a), c = m.getIdByUrl(d) || !1, c && (e = m.getStateById(c)), !e && b && !m.isTraditionalAnchor(a) && (e = m.createStateObject(null, null, d))), e
			}, m.getIdByUrl = function (a) {
				var c = m.urlToId[a] || m.store.urlToId[a] || b;
				return c
			}, m.getLastSavedState = function () {
				return m.savedStates[m.savedStates.length - 1] || b
			}, m.getLastStoredState = function () {
				return m.storedStates[m.storedStates.length - 1] || b
			}, m.hasUrlDuplicate = function (a) {
				var b, c = !1;
				return b = m.extractState(a.url), c = b && b.id !== a.id
			}, m.storeState = function (a) {
				return m.urlToId[a.url] = a.id, m.storedStates.push(m.cloneObject(a)), a
			}, m.isLastSavedState = function (a) {
				var b, c, d, e = !1;
				return m.savedStates.length && (b = a.id, c = m.getLastSavedState(), d = c.id, e = b === d), e
			}, m.saveState = function (a) {
				return m.isLastSavedState(a) ? !1 : (m.savedStates.push(m.cloneObject(a)), !0)
			}, m.getStateByIndex = function (a) {
				var b = null;
				return b = "undefined" == typeof a ? m.savedStates[m.savedStates.length - 1] : 0 > a ? m.savedStates[m.savedStates.length + a] : m.savedStates[a]
			}, m.getHash = function () {
				var a = m.unescapeHash(d.location.hash);
				return a
			}, m.unescapeString = function (b) {
				for (var c, d = b; c = a.unescape(d), c !== d;)d = c;
				return d
			}, m.unescapeHash = function (a) {
				var b = m.normalizeHash(a);
				return b = m.unescapeString(b)
			}, m.normalizeHash = function (a) {
				var b = a.replace(/[^#]*#/, "").replace(/#.*/, "");
				return b
			}, m.setHash = function (a, b) {
				var c, e, f;
				return b !== !1 && m.busy() ? (m.pushQueue({
					scope: m,
					callback: m.setHash,
					args: arguments,
					queue: b
				}), !1) : (c = m.escapeHash(a), m.busy(!0), e = m.extractState(a, !0), e && !m.emulated.pushState ? m.pushState(e.data, e.title, e.url, !1) : d.location.hash !== c && (m.bugs.setHash ? (f = m.getPageUrl(), m.pushState(null, null, f + "#" + c, !1)) : d.location.hash = c), m)
			}, m.escapeHash = function (b) {
				var c = m.normalizeHash(b);
				return c = a.escape(c), m.bugs.hashEscape || (c = c.replace(/\%21/g, "!").replace(/\%26/g, "&").replace(/\%3D/g, "=").replace(/\%3F/g, "?")), c
			}, m.getHashByUrl = function (a) {
				var b = String(a).replace(/([^#]*)#?([^#]*)#?(.*)/, "$2");
				return b = m.unescapeHash(b)
			}, m.setTitle = function (a) {
				var b, c = a.title;
				c || (b = m.getStateByIndex(0), b && b.url === a.url && (c = b.title || m.options.initialTitle));
				try {
					d.getElementsByTagName("title")[0].innerHTML = c.replace("<", "&lt;").replace(">", "&gt;").replace(" & ", " &amp; ")
				} catch (e) {
				}
				return d.title = c, m
			}, m.queues = [], m.busy = function (a) {
				if ("undefined" != typeof a ? m.busy.flag = a : "undefined" == typeof m.busy.flag && (m.busy.flag = !1), !m.busy.flag) {
					h(m.busy.timeout);
					var b = function () {
						var a, c, d;
						if (!m.busy.flag)for (a = m.queues.length - 1; a >= 0; --a)c = m.queues[a], 0 !== c.length && (d = c.shift(), m.fireQueueItem(d), m.busy.timeout = g(b, m.options.busyDelay))
					};
					m.busy.timeout = g(b, m.options.busyDelay)
				}
				return m.busy.flag
			}, m.busy.flag = !1, m.fireQueueItem = function (a) {
				return a.callback.apply(a.scope || m, a.args || [])
			}, m.pushQueue = function (a) {
				return m.queues[a.queue || 0] = m.queues[a.queue || 0] || [], m.queues[a.queue || 0].push(a), m
			}, m.queue = function (a, b) {
				return "function" == typeof a && (a = {callback: a}), "undefined" != typeof b && (a.queue = b), m.busy() ? m.pushQueue(a) : m.fireQueueItem(a), m
			}, m.clearQueue = function () {
				return m.busy.flag = !1, m.queues = [], m
			}, m.stateChanged = !1, m.doubleChecker = !1, m.doubleCheckComplete = function () {
				return m.stateChanged = !0, m.doubleCheckClear(), m
			}, m.doubleCheckClear = function () {
				return m.doubleChecker && (h(m.doubleChecker), m.doubleChecker = !1), m
			}, m.doubleCheck = function (a) {
				return m.stateChanged = !1, m.doubleCheckClear(), m.bugs.ieDoubleCheck && (m.doubleChecker = g(function () {
					return m.doubleCheckClear(), m.stateChanged || a(), !0
				}, m.options.doubleCheckInterval)), m
			}, m.safariStatePoll = function () {
				var b, c = m.extractState(d.location.href);
				if (!m.isLastSavedState(c))return b = c, b || (b = m.createStateObject()), m.Adapter.trigger(a, "popstate"), m
			}, m.back = function (a) {
				return a !== !1 && m.busy() ? (m.pushQueue({
					scope: m,
					callback: m.back,
					args: arguments,
					queue: a
				}), !1) : (m.busy(!0), m.doubleCheck(function () {
					m.back(!1)
				}), n.go(-1), !0)
			}, m.forward = function (a) {
				return a !== !1 && m.busy() ? (m.pushQueue({
					scope: m,
					callback: m.forward,
					args: arguments,
					queue: a
				}), !1) : (m.busy(!0), m.doubleCheck(function () {
					m.forward(!1)
				}), n.go(1), !0)
			}, m.go = function (a, b) {
				var c;
				if (a > 0)for (c = 1; a >= c; ++c)m.forward(b); else {
					if (!(0 > a))throw new Error("History.go: History.go requires a positive or negative integer passed.");
					for (c = -1; c >= a; --c)m.back(b)
				}
				return m
			}, m.emulated.pushState) {
			var o = function () {
			};
			m.pushState = m.pushState || o, m.replaceState = m.replaceState || o
		} else m.onPopState = function (b, c) {
			var e, f, g = !1, h = !1;
			return m.doubleCheckComplete(), e = m.getHash(), e ? (f = m.extractState(e || d.location.href, !0), f ? m.replaceState(f.data, f.title, f.url, !1) : (m.Adapter.trigger(a, "anchorchange"), m.busy(!1)), m.expectedStateId = !1, !1) : (g = m.Adapter.extractEventData("state", b, c) || !1, h = g ? m.getStateById(g) : m.expectedStateId ? m.getStateById(m.expectedStateId) : m.extractState(d.location.href), h || (h = m.createStateObject(null, null, d.location.href)), m.expectedStateId = !1, m.isLastSavedState(h) ? (m.busy(!1), !1) : (m.storeState(h), m.saveState(h), m.setTitle(h), m.Adapter.trigger(a, "statechange"), m.busy(!1), !0))
		}, m.Adapter.bind(a, "popstate", m.onPopState), m.pushState = function (b, c, d, e) {
			if (m.getHashByUrl(d) && m.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
			if (e !== !1 && m.busy())return m.pushQueue({
				scope: m,
				callback: m.pushState,
				args: arguments,
				queue: e
			}), !1;
			m.busy(!0);
			var f = m.createStateObject(b, c, d);
			return m.isLastSavedState(f) ? m.busy(!1) : (m.storeState(f), m.expectedStateId = f.id, n.pushState(f.id, f.title, f.url), m.Adapter.trigger(a, "popstate")), !0
		}, m.replaceState = function (b, c, d, e) {
			if (m.getHashByUrl(d) && m.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
			if (e !== !1 && m.busy())return m.pushQueue({
				scope: m,
				callback: m.replaceState,
				args: arguments,
				queue: e
			}), !1;
			m.busy(!0);
			var f = m.createStateObject(b, c, d);
			return m.isLastSavedState(f) ? m.busy(!1) : (m.storeState(f), m.expectedStateId = f.id, n.replaceState(f.id, f.title, f.url), m.Adapter.trigger(a, "popstate")), !0
		};
		if (f) {
			try {
				m.store = k.parse(f.getItem("History.store")) || {}
			} catch (p) {
				m.store = {}
			}
			m.normalizeStore()
		} else m.store = {}, m.normalizeStore();
		m.Adapter.bind(a, "beforeunload", m.clearAllIntervals), m.Adapter.bind(a, "unload", m.clearAllIntervals), m.saveState(m.storeState(m.extractState(d.location.href, !0))), f && (m.onUnload = function () {
			var a, b;
			try {
				a = k.parse(f.getItem("History.store")) || {}
			} catch (c) {
				a = {}
			}
			a.idToState = a.idToState || {}, a.urlToId = a.urlToId || {}, a.stateToId = a.stateToId || {};
			for (b in m.idToState)m.idToState.hasOwnProperty(b) && (a.idToState[b] = m.idToState[b]);
			for (b in m.urlToId)m.urlToId.hasOwnProperty(b) && (a.urlToId[b] = m.urlToId[b]);
			for (b in m.stateToId)m.stateToId.hasOwnProperty(b) && (a.stateToId[b] = m.stateToId[b]);
			m.store = a, m.normalizeStore(), f.setItem("History.store", k.stringify(a))
		}, m.intervalList.push(i(m.onUnload, m.options.storeInterval)), m.Adapter.bind(a, "beforeunload", m.onUnload), m.Adapter.bind(a, "unload", m.onUnload)), m.emulated.pushState || (m.bugs.safariPoll && m.intervalList.push(i(m.safariStatePoll, m.options.safariPollInterval)), ("Apple Computer, Inc." === e.vendor || "Mozilla" === (e.appCodeName || "")) && (m.Adapter.bind(a, "hashchange", function () {
			m.Adapter.trigger(a, "popstate")
		}), m.getHash() && m.Adapter.onDomLoad(function () {
			m.Adapter.trigger(a, "hashchange")
		})))
	}, m.init()
}(window), function (a) {
	"function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
	function b(a) {
		if (a.minTime && (a.minTime = t(a.minTime)), a.maxTime && (a.maxTime = t(a.maxTime)), a.durationTime && "function" != typeof a.durationTime && (a.durationTime = t(a.durationTime)), "now" == a.scrollDefault ? a.scrollDefault = t(new Date) : a.scrollDefault ? a.scrollDefault = t(a.scrollDefault) : a.minTime && (a.scrollDefault = a.minTime), a.scrollDefault && (a.scrollDefault = e(a.scrollDefault, a)), a.timeFormat.match(/[gh]/) && (a._twelveHourTime = !0), a.disableTimeRanges.length > 0) {
			for (var b in a.disableTimeRanges)a.disableTimeRanges[b] = [t(a.disableTimeRanges[b][0]), t(a.disableTimeRanges[b][1])];
			a.disableTimeRanges = a.disableTimeRanges.sort(function (a, b) {
				return a[0] - b[0]
			});
			for (var b = a.disableTimeRanges.length - 1; b > 0; b--)a.disableTimeRanges[b][0] <= a.disableTimeRanges[b - 1][1] && (a.disableTimeRanges[b - 1] = [Math.min(a.disableTimeRanges[b][0], a.disableTimeRanges[b - 1][0]), Math.max(a.disableTimeRanges[b][1], a.disableTimeRanges[b - 1][1])], a.disableTimeRanges.splice(b, 1))
		}
		return a
	}

	function c(b) {
		var c = b.data("timepicker-settings"), e = b.data("timepicker-list");
		if (e && e.length && (e.remove(), b.data("timepicker-list", !1)), c.useSelect) {
			e = a("<select />", {"class": "ui-timepicker-select"});
			var g = e
		} else {
			e = a("<ul />", {"class": "ui-timepicker-list"});
			var g = a("<div />", {"class": "ui-timepicker-wrapper", tabindex: -1});
			g.css({display: "none", position: "absolute"}).append(e)
		}
		if (c.noneOption)if (c.noneOption === !0 && (c.noneOption = c.useSelect ? "Time..." : "None"), a.isArray(c.noneOption)) {
			for (var h in c.noneOption)if (parseInt(h, 10) === h) {
				var j = d(c.noneOption[h], c.useSelect);
				e.append(j)
			}
		} else {
			var j = d(c.noneOption, c.useSelect);
			e.append(j)
		}
		c.className && g.addClass(c.className), null === c.minTime && null === c.durationTime || !c.showDuration || (g.addClass("ui-timepicker-with-duration"), g.addClass("ui-timepicker-step-" + c.step));
		var l = c.minTime;
		"function" == typeof c.durationTime ? l = t(c.durationTime()) : null !== c.durationTime && (l = c.durationTime);
		var m = null !== c.minTime ? c.minTime : 0, o = null !== c.maxTime ? c.maxTime : m + v - 1;
		m >= o && (o += v), o === v - 1 && -1 !== c.timeFormat.indexOf("H") && (o = v);
		for (var p = c.disableTimeRanges, u = 0, w = p.length, h = m; o >= h; h += 60 * c.step) {
			var y = h, z = s(y, c.timeFormat);
			if (c.useSelect) {
				var A = a("<option />", {value: z});
				A.text(z)
			} else {
				var A = a("<li />");
				A.data("time", 86400 >= y ? y : y % 86400), A.text(z)
			}
			if ((null !== c.minTime || null !== c.durationTime) && c.showDuration) {
				var B = r(h - l, c.step);
				if (c.useSelect)A.text(A.text() + " (" + B + ")"); else {
					var C = a("<span />", {"class": "ui-timepicker-duration"});
					C.text(" (" + B + ")"), A.append(C)
				}
			}
			w > u && (y >= p[u][1] && (u += 1), p[u] && y >= p[u][0] && y < p[u][1] && (c.useSelect ? A.prop("disabled", !0) : A.addClass("ui-timepicker-disabled"))), e.append(A)
		}
		if (g.data("timepicker-input", b), b.data("timepicker-list", g), c.useSelect)e.val(f(b.val(), c)), e.on("focus", function () {
			a(this).data("timepicker-input").trigger("showTimepicker")
		}), e.on("blur", function () {
			a(this).data("timepicker-input").trigger("hideTimepicker")
		}), e.on("change", function () {
			n(b, a(this).val(), "select")
		}), b.hide().after(e); else {
			var D = c.appendTo;
			"string" == typeof D ? D = a(D) : "function" == typeof D && (D = D(b)), D.append(g), k(b, e), e.on("mousedown", "li", function () {
				b.off("focus.timepicker"), b.on("focus.timepicker-ie-hack", function () {
					b.off("focus.timepicker-ie-hack"), b.on("focus.timepicker", x.show)
				}), i(b) || b[0].focus(), e.find("li").removeClass("ui-timepicker-selected"), a(this).addClass("ui-timepicker-selected"), q(b) && (b.trigger("hideTimepicker"), g.hide())
			})
		}
	}

	function d(b, c) {
		var d, e, f;
		return "object" == typeof b ? (d = b.label, e = b.className, f = b.value) : "string" == typeof b ? d = b : a.error("Invalid noneOption value"), c ? a("<option />", {
			value: f,
			"class": e,
			text: d
		}) : a("<li />", {"class": e, text: d}).data("time", f)
	}

	function e(b, c) {
		if (a.isNumeric(b) || (b = t(b)), null === b)return null;
		var d = b % (60 * c.step);
		return d >= 30 * c.step ? b += 60 * c.step - d : b -= d, b
	}

	function f(a, b) {
		return a = e(a, b), null !== a ? s(a, b.timeFormat) : void 0
	}

	function g() {
		return new Date(1970, 1, 1, 0, 0, 0)
	}

	function h(b) {
		var c = a(b.target), d = c.closest(".ui-timepicker-input");
		0 === d.length && 0 === c.closest(".ui-timepicker-wrapper").length && (x.hide(), a(document).unbind(".ui-timepicker"))
	}

	function i(a) {
		var b = a.data("timepicker-settings");
		return (window.navigator.msMaxTouchPoints || "ontouchstart" in document) && b.disableTouchKeyboard
	}

	function j(b, c, d) {
		if (!d && 0 !== d)return !1;
		var e = b.data("timepicker-settings"), f = !1, g = 30 * e.step;
		return c.find("li").each(function (b, c) {
			var e = a(c);
			if ("number" == typeof e.data("time")) {
				var h = e.data("time") - d;
				return Math.abs(h) < g || h == g ? (f = e, !1) : void 0
			}
		}), f
	}

	function k(a, b) {
		b.find("li").removeClass("ui-timepicker-selected");
		var c = t(m(a), a.data("timepicker-settings"));
		if (null !== c) {
			var d = j(a, b, c);
			if (d) {
				var e = d.offset().top - b.offset().top;
				(e + d.outerHeight() > b.outerHeight() || 0 > e) && b.scrollTop(b.scrollTop() + d.position().top - d.outerHeight()), d.addClass("ui-timepicker-selected")
			}
		}
	}

	function l(b) {
		if ("" !== this.value) {
			{
				var c = a(this);
				c.data("timepicker-list")
			}
			if (!c.is(":focus") || b && "change" == b.type) {
				var d = t(this.value);
				if (null === d)return void c.trigger("timeFormatError");
				var e = c.data("timepicker-settings"), f = !1;
				if (null !== e.minTime && d < e.minTime ? f = !0 : null !== e.maxTime && d > e.maxTime && (f = !0), a.each(e.disableTimeRanges, function () {
						return d >= this[0] && d < this[1] ? (f = !0, !1) : void 0
					}), e.forceRoundTime) {
					var g = d % (60 * e.step);
					g >= 30 * e.step ? d += 60 * e.step - g : d -= g
				}
				var h = s(d, e.timeFormat);
				f ? n(c, h, "error") && c.trigger("timeRangeError") : n(c, h)
			}
		}
	}

	function m(a) {
		return a.is("input") ? a.val() : a.data("ui-timepicker-value")
	}

	function n(a, b, c) {
		if (a.is("input")) {
			a.val(b);
			var d = a.data("timepicker-settings");
			d.useSelect && a.data("timepicker-list").val(f(b, d))
		}
		return a.data("ui-timepicker-value") != b ? (a.data("ui-timepicker-value", b), "select" == c ? a.trigger("selectTime").trigger("changeTime").trigger("change") : "error" != c && a.trigger("changeTime"), !0) : (a.trigger("selectTime"), !1)
	}

	function o(b) {
		var c = a(this), d = c.data("timepicker-list");
		if (!d || !d.is(":visible")) {
			if (40 != b.keyCode)return !0;
			x.show.call(c.get(0)), d = c.data("timepicker-list"), i(c) || c.focus()
		}
		switch (b.keyCode) {
			case 13:
				return q(c) && x.hide.apply(this), b.preventDefault(), !1;
			case 38:
				var e = d.find(".ui-timepicker-selected");
				return e.length ? e.is(":first-child") || (e.removeClass("ui-timepicker-selected"), e.prev().addClass("ui-timepicker-selected"), e.prev().position().top < e.outerHeight() && d.scrollTop(d.scrollTop() - e.outerHeight())) : (d.find("li").each(function (b, c) {
					return a(c).position().top > 0 ? (e = a(c), !1) : void 0
				}), e.addClass("ui-timepicker-selected")), !1;
			case 40:
				return e = d.find(".ui-timepicker-selected"), 0 === e.length ? (d.find("li").each(function (b, c) {
					return a(c).position().top > 0 ? (e = a(c), !1) : void 0
				}), e.addClass("ui-timepicker-selected")) : e.is(":last-child") || (e.removeClass("ui-timepicker-selected"), e.next().addClass("ui-timepicker-selected"), e.next().position().top + 2 * e.outerHeight() > d.outerHeight() && d.scrollTop(d.scrollTop() + e.outerHeight())), !1;
			case 27:
				d.find("li").removeClass("ui-timepicker-selected"), x.hide();
				break;
			case 9:
				x.hide();
				break;
			default:
				return !0
		}
	}

	function p(b) {
		var c = a(this), d = c.data("timepicker-list");
		if (!d || !d.is(":visible"))return !0;
		if (!c.data("timepicker-settings").typeaheadHighlight)return d.find("li").removeClass("ui-timepicker-selected"), !0;
		switch (b.keyCode) {
			case 96:
			case 97:
			case 98:
			case 99:
			case 100:
			case 101:
			case 102:
			case 103:
			case 104:
			case 105:
			case 48:
			case 49:
			case 50:
			case 51:
			case 52:
			case 53:
			case 54:
			case 55:
			case 56:
			case 57:
			case 65:
			case 77:
			case 80:
			case 186:
			case 8:
			case 46:
				k(c, d);
				break;
			default:
				return
		}
	}

	function q(a) {
		var b = a.data("timepicker-settings"), c = a.data("timepicker-list"), d = null, e = c.find(".ui-timepicker-selected");
		if (e.hasClass("ui-timepicker-disabled"))return !1;
		if (e.length && (d = e.data("time")), null !== d)if ("string" == typeof d)a.val(d); else {
			var f = s(d, b.timeFormat);
			n(a, f, "select")
		}
		return !0
	}

	function r(a, b) {
		a = Math.abs(a);
		var c, d, e = Math.round(a / 60), f = [];
		return 60 > e ? f = [e, w.mins] : (c = Math.floor(e / 60), d = e % 60, 30 == b && 30 == d && (c += w.decimal + 5), f.push(c), f.push(1 == c ? w.hr : w.hrs), 30 != b && d && (f.push(d), f.push(w.mins))), f.join(" ")
	}

	function s(a, b) {
		if (null !== a) {
			var c = new Date(u.valueOf() + 1e3 * a);
			if (!isNaN(c.getTime())) {
				for (var d, e, f = "", g = 0; g < b.length; g++)switch (e = b.charAt(g)) {
					case"a":
						f += c.getHours() > 11 ? w.pm : w.am;
						break;
					case"A":
						f += c.getHours() > 11 ? w.PM : w.AM;
						break;
					case"g":
						d = c.getHours() % 12, f += 0 === d ? "12" : d;
						break;
					case"G":
						f += c.getHours();
						break;
					case"h":
						d = c.getHours() % 12, 0 !== d && 10 > d && (d = "0" + d), f += 0 === d ? "12" : d;
						break;
					case"H":
						d = c.getHours(), a === v && (d = 24), f += d > 9 ? d : "0" + d;
						break;
					case"i":
						var h = c.getMinutes();
						f += h > 9 ? h : "0" + h;
						break;
					case"s":
						a = c.getSeconds(), f += a > 9 ? a : "0" + a;
						break;
					case"\\":
						g++, f += b.charAt(g);
						break;
					default:
						f += e
				}
				return f
			}
		}
	}

	function t(a, b) {
		if ("" === a)return null;
		if (!a || a + 0 == a)return a;
		if ("object" == typeof a)return 3600 * a.getHours() + 60 * a.getMinutes() + a.getSeconds();
		a = a.toLowerCase();
		{
			var c;
			new Date(0)
		}
		if (c = a.match(/^([0-2]?[0-9])\W?([0-5][0-9])?\W?([0-5][0-9])?\s*([pa]?)m?$/), !c)return null;
		var d = parseInt(1 * c[1], 10), e = c[4], f = d;
		e && (f = 12 == d ? "p" == c[4] ? 12 : 0 : d + ("p" == c[4] ? 12 : 0));
		var g = 1 * c[2] || 0, h = 1 * c[3] || 0, i = 3600 * f + 60 * g + h;
		if (!e && b && b._twelveHourTime && b.scrollDefault) {
			var j = i - b.scrollDefault;
			0 > j && j >= v / -2 && (i = (i + v / 2) % v)
		}
		return i
	}

	var u = g(), v = 86400, w = {
		am: "am",
		pm: "pm",
		AM: "AM",
		PM: "PM",
		decimal: ".",
		mins: "mins",
		hr: "hr",
		hrs: "hrs"
	}, x = {
		init: function (d) {
			return this.each(function () {
				var e = a(this), f = [];
				for (var g in a.fn.timepicker.defaults)e.data(g) && (f[g] = e.data(g));
				var h = a.extend({}, a.fn.timepicker.defaults, f, d);
				h.lang && (w = a.extend(w, h.lang)), h = b(h), e.data("timepicker-settings", h), e.addClass("ui-timepicker-input"), h.useSelect ? c(e) : (e.prop("autocomplete", "off"), e.on("click.timepicker focus.timepicker", x.show), e.on("change.timepicker", l), e.on("keydown.timepicker", o), e.on("keyup.timepicker", p), l.call(e.get(0)))
			})
		}, show: function (b) {
			var d = a(this), e = d.data("timepicker-settings");
			if (b) {
				if (!e.showOnFocus)return !0;
				b.preventDefault()
			}
			if (e.useSelect)return void d.data("timepicker-list").focus();
			i(d) && d.blur();
			var f = d.data("timepicker-list");
			if (!d.prop("readonly") && (f && 0 !== f.length && "function" != typeof e.durationTime || (c(d), f = d.data("timepicker-list")), !f.is(":visible"))) {
				x.hide(), f.show();
				var g = {};
				g.left = "rtl" == e.orientation ? d.offset().left + d.outerWidth() - f.outerWidth() + parseInt(f.css("marginLeft").replace("px", ""), 10) : d.offset().left + parseInt(f.css("marginLeft").replace("px", ""), 10), g.top = d.offset().top + d.outerHeight(!0) + f.outerHeight() > a(window).height() + a(window).scrollTop() ? d.offset().top - f.outerHeight() + parseInt(f.css("marginTop").replace("px", ""), 10) : d.offset().top + d.outerHeight() + parseInt(f.css("marginTop").replace("px", ""), 10), f.offset(g);
				var k = f.find(".ui-timepicker-selected");
				if (k.length || (m(d) ? k = j(d, f, t(m(d))) : e.scrollDefault && (k = j(d, f, e.scrollDefault))), k && k.length) {
					var l = f.scrollTop() + k.position().top - k.outerHeight();
					f.scrollTop(l)
				} else f.scrollTop(0);
				return a(document).on("touchstart.ui-timepicker mousedown.ui-timepicker", h), e.closeOnWindowScroll && a(document).on("scroll.ui-timepicker", h), d.trigger("showTimepicker"), this
			}
		}, hide: function () {
			var b = a(this), c = b.data("timepicker-settings");
			return c && c.useSelect && b.blur(), a(".ui-timepicker-wrapper:visible").each(function () {
				var b = a(this), c = b.data("timepicker-input"), d = c.data("timepicker-settings");
				d && d.selectOnBlur && q(c), b.hide(), c.trigger("hideTimepicker")
			}), this
		}, option: function (d, e) {
			return this.each(function () {
				var f = a(this), g = f.data("timepicker-settings"), h = f.data("timepicker-list");
				if ("object" == typeof d)g = a.extend(g, d); else if ("string" == typeof d && "undefined" != typeof e)g[d] = e; else if ("string" == typeof d)return g[d];
				g = b(g), f.data("timepicker-settings", g), h && (h.remove(), f.data("timepicker-list", !1)), g.useSelect && c(f)
			})
		}, getSecondsFromMidnight: function () {
			return t(m(this))
		}, getTime: function (a) {
			var b = this, c = m(b);
			if (!c)return null;
			a || (a = new Date);
			var d = t(c), e = new Date(a);
			return e.setHours(d / 3600), e.setMinutes(d % 3600 / 60), e.setSeconds(d % 60), e.setMilliseconds(0), e
		}, setTime: function (a) {
			var b = this, c = b.data("timepicker-settings");
			if (c.forceRoundTime)var d = f(a, c); else var d = s(t(a), c.timeFormat);
			return n(b, d), b.data("timepicker-list") && k(b, b.data("timepicker-list")), this
		}, remove: function () {
			var a = this;
			if (a.hasClass("ui-timepicker-input")) {
				var b = a.data("timepicker-settings");
				return a.removeAttr("autocomplete", "off"), a.removeClass("ui-timepicker-input"), a.removeData("timepicker-settings"), a.off(".timepicker"), a.data("timepicker-list") && a.data("timepicker-list").remove(), b.useSelect && a.show(), a.removeData("timepicker-list"), this
			}
		}
	};
	a.fn.timepicker = function (b) {
		return this.length ? x[b] ? this.hasClass("ui-timepicker-input") ? x[b].apply(this, Array.prototype.slice.call(arguments, 1)) : this : "object" != typeof b && b ? void a.error("Method " + b + " does not exist on jQuery.timepicker") : x.init.apply(this, arguments) : this
	}, a.fn.timepicker.defaults = {
		className: null,
		minTime: null,
		maxTime: null,
		durationTime: null,
		step: 30,
		showDuration: !1,
		showOnFocus: !0,
		timeFormat: "g:ia",
		scrollDefault: null,
		selectOnBlur: !1,
		disableTouchKeyboard: !1,
		forceRoundTime: !1,
		appendTo: "body",
		orientation: "ltr",
		disableTimeRanges: [],
		closeOnWindowScroll: !1,
		typeaheadHighlight: !0,
		noneOption: !1
	}
}), !function (a, b) {
	"use strict";
	var c, d, e = a, f = e.document, g = e.navigator, h = e.setTimeout, i = e.encodeURIComponent, j = e.ActiveXObject, k = e.Error, l = e.Number.parseInt || e.parseInt, m = e.Number.parseFloat || e.parseFloat, n = e.Number.isNaN || e.isNaN, o = e.Math.round, p = e.Date.now, q = e.Object.keys, r = e.Object.defineProperty, s = e.Object.prototype.hasOwnProperty, t = e.Array.prototype.slice, u = function () {
		var a = function (a) {
			return a
		};
		if ("function" == typeof e.wrap && "function" == typeof e.unwrap)try {
			var b = f.createElement("div"), c = e.unwrap(b);
			1 === b.nodeType && c && 1 === c.nodeType && (a = e.unwrap)
		} catch (d) {
		}
		return a
	}(), v = function (a) {
		return t.call(a, 0)
	}, w = function () {
		var a, c, d, e, f, g, h = v(arguments), i = h[0] || {};
		for (a = 1, c = h.length; c > a; a++)if (null != (d = h[a]))for (e in d)s.call(d, e) && (f = i[e], g = d[e], i !== g && g !== b && (i[e] = g));
		return i
	}, x = function (a) {
		var b, c, d, e;
		if ("object" != typeof a || null == a)b = a; else if ("number" == typeof a.length)for (b = [], c = 0, d = a.length; d > c; c++)s.call(a, c) && (b[c] = x(a[c])); else {
			b = {};
			for (e in a)s.call(a, e) && (b[e] = x(a[e]))
		}
		return b
	}, y = function (a, b) {
		for (var c = {}, d = 0, e = b.length; e > d; d++)b[d] in a && (c[b[d]] = a[b[d]]);
		return c
	}, z = function (a, b) {
		var c = {};
		for (var d in a)-1 === b.indexOf(d) && (c[d] = a[d]);
		return c
	}, A = function (a) {
		if (a)for (var b in a)s.call(a, b) && delete a[b];
		return a
	}, B = function (a, b) {
		if (a && 1 === a.nodeType && a.ownerDocument && b && (1 === b.nodeType && b.ownerDocument && b.ownerDocument === a.ownerDocument || 9 === b.nodeType && !b.ownerDocument && b === a.ownerDocument))do {
			if (a === b)return !0;
			a = a.parentNode
		} while (a);
		return !1
	}, C = function (a) {
		var b;
		return "string" == typeof a && a && (b = a.split("#")[0].split("?")[0], b = a.slice(0, a.lastIndexOf("/") + 1)), b
	}, D = function (a) {
		var b, c;
		return "string" == typeof a && a && (c = a.match(/^(?:|[^:@]*@|.+\)@(?=http[s]?|file)|.+?\s+(?: at |@)(?:[^:\(]+ )*[\(]?)((?:http[s]?|file):\/\/[\/]?.+?\/[^:\)]*?)(?::\d+)(?::\d+)?/), c && c[1] ? b = c[1] : (c = a.match(/\)@((?:http[s]?|file):\/\/[\/]?.+?\/[^:\)]*?)(?::\d+)(?::\d+)?/), c && c[1] && (b = c[1]))), b
	}, E = function () {
		var a, b;
		try {
			throw new k
		} catch (c) {
			b = c
		}
		return b && (a = b.sourceURL || b.fileName || D(b.stack)), a
	}, F = function () {
		var a, c, d;
		if (f.currentScript && (a = f.currentScript.src))return a;
		if (c = f.getElementsByTagName("script"), 1 === c.length)return c[0].src || b;
		if ("readyState" in c[0])for (d = c.length; d--;)if ("interactive" === c[d].readyState && (a = c[d].src))return a;
		return "loading" === f.readyState && (a = c[c.length - 1].src) ? a : (a = E()) ? a : b
	}, G = function () {
		var a, c, d, e = f.getElementsByTagName("script");
		for (a = e.length; a--;) {
			if (!(d = e[a].src)) {
				c = null;
				break
			}
			if (d = C(d), null == c)c = d; else if (c !== d) {
				c = null;
				break
			}
		}
		return c || b
	}, H = function () {
		var a = C(F()) || G() || "";
		return a + "ZeroClipboard.swf"
	}, I = {
		bridge: null,
		version: "0.0.0",
		pluginType: "unknown",
		disabled: null,
		outdated: null,
		unavailable: null,
		deactivated: null,
		overdue: null,
		ready: null
	}, J = "11.0.0", K = {}, L = {}, M = null, N = {
		ready: "Flash communication is established",
		error: {
			"flash-disabled": "Flash is disabled or not installed",
			"flash-outdated": "Flash is too outdated to support ZeroClipboard",
			"flash-unavailable": "Flash is unable to communicate bidirectionally with JavaScript",
			"flash-deactivated": "Flash is too outdated for your browser and/or is configured as click-to-activate",
			"flash-overdue": "Flash communication was established but NOT within the acceptable time limit"
		}
	}, O = {
		swfPath: H(),
		trustedDomains: a.location.host ? [a.location.host] : [],
		cacheBust: !0,
		forceEnhancedClipboard: !1,
		flashLoadTimeout: 3e4,
		autoActivate: !0,
		bubbleEvents: !0,
		containerId: "global-zeroclipboard-html-bridge",
		containerClass: "global-zeroclipboard-container",
		swfObjectId: "global-zeroclipboard-flash-bridge",
		hoverClass: "zeroclipboard-is-hover",
		activeClass: "zeroclipboard-is-active",
		forceHandCursor: !1,
		title: null,
		zIndex: 999999999
	}, P = function (a) {
		if ("object" == typeof a && null !== a)for (var b in a)if (s.call(a, b))if (/^(?:forceHandCursor|title|zIndex|bubbleEvents)$/.test(b))O[b] = a[b]; else if (null == I.bridge)if ("containerId" === b || "swfObjectId" === b) {
			if (!cb(a[b]))throw new Error("The specified `" + b + "` value is not valid as an HTML4 Element ID");
			O[b] = a[b]
		} else O[b] = a[b];
		return "string" == typeof a && a ? s.call(O, a) ? O[a] : void 0 : x(O)
	}, Q = function () {
		return {
			browser: y(g, ["userAgent", "platform", "appName"]),
			flash: z(I, ["bridge"]),
			zeroclipboard: {version: Fb.version, config: Fb.config()}
		}
	}, R = function () {
		return !!(I.disabled || I.outdated || I.unavailable || I.deactivated)
	}, S = function (a, b) {
		var c, d, e, f = {};
		if ("string" == typeof a && a)e = a.toLowerCase().split(/\s+/); else if ("object" == typeof a && a && "undefined" == typeof b)for (c in a)s.call(a, c) && "string" == typeof c && c && "function" == typeof a[c] && Fb.on(c, a[c]);
		if (e && e.length) {
			for (c = 0, d = e.length; d > c; c++)a = e[c].replace(/^on/, ""), f[a] = !0, K[a] || (K[a] = []), K[a].push(b);
			if (f.ready && I.ready && Fb.emit({type: "ready"}), f.error) {
				var g = ["disabled", "outdated", "unavailable", "deactivated", "overdue"];
				for (c = 0, d = g.length; d > c; c++)if (I[g[c]] === !0) {
					Fb.emit({type: "error", name: "flash-" + g[c]});
					break
				}
			}
		}
		return Fb
	}, T = function (a, b) {
		var c, d, e, f, g;
		if (0 === arguments.length)f = q(K); else if ("string" == typeof a && a)f = a.split(/\s+/); else if ("object" == typeof a && a && "undefined" == typeof b)for (c in a)s.call(a, c) && "string" == typeof c && c && "function" == typeof a[c] && Fb.off(c, a[c]);
		if (f && f.length)for (c = 0, d = f.length; d > c; c++)if (a = f[c].toLowerCase().replace(/^on/, ""), g = K[a], g && g.length)if (b)for (e = g.indexOf(b); -1 !== e;)g.splice(e, 1), e = g.indexOf(b, e); else g.length = 0;
		return Fb
	}, U = function (a) {
		var b;
		return b = "string" == typeof a && a ? x(K[a]) || null : x(K)
	}, V = function (a) {
		var b, c, d;
		return a = db(a), a && !jb(a) ? "ready" === a.type && I.overdue === !0 ? Fb.emit({
			type: "error",
			name: "flash-overdue"
		}) : (b = w({}, a), ib.call(this, b), "copy" === a.type && (d = pb(L), c = d.data, M = d.formatMap), c) : void 0
	}, W = function () {
		if ("boolean" != typeof I.ready && (I.ready = !1), !Fb.isFlashUnusable() && null === I.bridge) {
			var a = O.flashLoadTimeout;
			"number" == typeof a && a >= 0 && h(function () {
				"boolean" != typeof I.deactivated && (I.deactivated = !0), I.deactivated === !0 && Fb.emit({
					type: "error",
					name: "flash-deactivated"
				})
			}, a), I.overdue = !1, nb()
		}
	}, X = function () {
		Fb.clearData(), Fb.blur(), Fb.emit("destroy"), ob(), Fb.off()
	}, Y = function (a, b) {
		var c;
		if ("object" == typeof a && a && "undefined" == typeof b)c = a, Fb.clearData(); else {
			if ("string" != typeof a || !a)return;
			c = {}, c[a] = b
		}
		for (var d in c)"string" == typeof d && d && s.call(c, d) && "string" == typeof c[d] && c[d] && (L[d] = c[d])
	}, Z = function (a) {
		"undefined" == typeof a ? (A(L), M = null) : "string" == typeof a && s.call(L, a) && delete L[a]
	}, $ = function (a) {
		return "undefined" == typeof a ? x(L) : "string" == typeof a && s.call(L, a) ? L[a] : void 0
	}, _ = function (a) {
		if (a && 1 === a.nodeType) {
			c && (xb(c, O.activeClass), c !== a && xb(c, O.hoverClass)), c = a, wb(a, O.hoverClass);
			var b = a.getAttribute("title") || O.title;
			if ("string" == typeof b && b) {
				var d = mb(I.bridge);
				d && d.setAttribute("title", b)
			}
			var e = O.forceHandCursor === !0 || "pointer" === yb(a, "cursor");
			Cb(e), Bb()
		}
	}, ab = function () {
		var a = mb(I.bridge);
		a && (a.removeAttribute("title"), a.style.left = "0px", a.style.top = "-9999px", a.style.width = "1px", a.style.top = "1px"), c && (xb(c, O.hoverClass), xb(c, O.activeClass), c = null)
	}, bb = function () {
		return c || null
	}, cb = function (a) {
		return "string" == typeof a && a && /^[A-Za-z][A-Za-z0-9_:\-\.]*$/.test(a)
	}, db = function (a) {
		var b;
		if ("string" == typeof a && a ? (b = a, a = {}) : "object" == typeof a && a && "string" == typeof a.type && a.type && (b = a.type), b) {
			!a.target && /^(copy|aftercopy|_click)$/.test(b.toLowerCase()) && (a.target = d), w(a, {
				type: b.toLowerCase(),
				target: a.target || c || null,
				relatedTarget: a.relatedTarget || null,
				currentTarget: I && I.bridge || null,
				timeStamp: a.timeStamp || p() || null
			});
			var e = N[a.type];
			return "error" === a.type && a.name && e && (e = e[a.name]), e && (a.message = e), "ready" === a.type && w(a, {
				target: null,
				version: I.version
			}), "error" === a.type && (/^flash-(disabled|outdated|unavailable|deactivated|overdue)$/.test(a.name) && w(a, {
				target: null,
				minimumVersion: J
			}), /^flash-(outdated|unavailable|deactivated|overdue)$/.test(a.name) && w(a, {version: I.version})), "copy" === a.type && (a.clipboardData = {
				setData: Fb.setData,
				clearData: Fb.clearData
			}), "aftercopy" === a.type && (a = qb(a, M)), a.target && !a.relatedTarget && (a.relatedTarget = eb(a.target)), a = fb(a)
		}
	}, eb = function (a) {
		var b = a && a.getAttribute && a.getAttribute("data-clipboard-target");
		return b ? f.getElementById(b) : null
	}, fb = function (a) {
		if (a && /^_(?:click|mouse(?:over|out|down|up|move))$/.test(a.type)) {
			var c = a.target, d = "_mouseover" === a.type && a.relatedTarget ? a.relatedTarget : b, g = "_mouseout" === a.type && a.relatedTarget ? a.relatedTarget : b, h = Ab(c), i = e.screenLeft || e.screenX || 0, j = e.screenTop || e.screenY || 0, k = f.body.scrollLeft + f.documentElement.scrollLeft, l = f.body.scrollTop + f.documentElement.scrollTop, m = h.left + ("number" == typeof a._stageX ? a._stageX : 0), n = h.top + ("number" == typeof a._stageY ? a._stageY : 0), o = m - k, p = n - l, q = i + o, r = j + p, s = "number" == typeof a.movementX ? a.movementX : 0, t = "number" == typeof a.movementY ? a.movementY : 0;
			delete a._stageX, delete a._stageY, w(a, {
				srcElement: c,
				fromElement: d,
				toElement: g,
				screenX: q,
				screenY: r,
				pageX: m,
				pageY: n,
				clientX: o,
				clientY: p,
				x: o,
				y: p,
				movementX: s,
				movementY: t,
				offsetX: 0,
				offsetY: 0,
				layerX: 0,
				layerY: 0
			})
		}
		return a
	}, gb = function (a) {
		var b = a && "string" == typeof a.type && a.type || "";
		return !/^(?:(?:before)?copy|destroy)$/.test(b)
	}, hb = function (a, b, c, d) {
		d ? h(function () {
			a.apply(b, c)
		}, 0) : a.apply(b, c)
	}, ib = function (a) {
		if ("object" == typeof a && a && a.type) {
			var b = gb(a), c = K["*"] || [], d = K[a.type] || [], f = c.concat(d);
			if (f && f.length) {
				var g, h, i, j, k, l = this;
				for (g = 0, h = f.length; h > g; g++)i = f[g], j = l, "string" == typeof i && "function" == typeof e[i] && (i = e[i]), "object" == typeof i && i && "function" == typeof i.handleEvent && (j = i, i = i.handleEvent), "function" == typeof i && (k = w({}, a), hb(i, j, [k], b))
			}
			return this
		}
	}, jb = function (a) {
		var b = a.target || c || null, e = "swf" === a._source;
		delete a._source;
		var f = ["flash-disabled", "flash-outdated", "flash-unavailable", "flash-deactivated", "flash-overdue"];
		switch (a.type) {
			case"error":
				-1 !== f.indexOf(a.name) && w(I, {
					disabled: "flash-disabled" === a.name,
					outdated: "flash-outdated" === a.name,
					unavailable: "flash-unavailable" === a.name,
					deactivated: "flash-deactivated" === a.name,
					overdue: "flash-overdue" === a.name,
					ready: !1
				});
				break;
			case"ready":
				var g = I.deactivated === !0;
				w(I, {disabled: !1, outdated: !1, unavailable: !1, deactivated: !1, overdue: g, ready: !g});
				break;
			case"beforecopy":
				d = b;
				break;
			case"copy":
				var h, i, j = a.relatedTarget;
				!L["text/html"] && !L["text/plain"] && j && (i = j.value || j.outerHTML || j.innerHTML) && (h = j.value || j.textContent || j.innerText) ? (a.clipboardData.clearData(), a.clipboardData.setData("text/plain", h), i !== h && a.clipboardData.setData("text/html", i)) : !L["text/plain"] && a.target && (h = a.target.getAttribute("data-clipboard-text")) && (a.clipboardData.clearData(), a.clipboardData.setData("text/plain", h));
				break;
			case"aftercopy":
				Fb.clearData(), b && b !== vb() && b.focus && b.focus();
				break;
			case"_mouseover":
				Fb.focus(b), O.bubbleEvents === !0 && e && (b && b !== a.relatedTarget && !B(a.relatedTarget, b) && kb(w({}, a, {
					type: "mouseenter",
					bubbles: !1,
					cancelable: !1
				})), kb(w({}, a, {type: "mouseover"})));
				break;
			case"_mouseout":
				Fb.blur(), O.bubbleEvents === !0 && e && (b && b !== a.relatedTarget && !B(a.relatedTarget, b) && kb(w({}, a, {
					type: "mouseleave",
					bubbles: !1,
					cancelable: !1
				})), kb(w({}, a, {type: "mouseout"})));
				break;
			case"_mousedown":
				wb(b, O.activeClass), O.bubbleEvents === !0 && e && kb(w({}, a, {type: a.type.slice(1)}));
				break;
			case"_mouseup":
				xb(b, O.activeClass), O.bubbleEvents === !0 && e && kb(w({}, a, {type: a.type.slice(1)}));
				break;
			case"_click":
				d = null, O.bubbleEvents === !0 && e && kb(w({}, a, {type: a.type.slice(1)}));
				break;
			case"_mousemove":
				O.bubbleEvents === !0 && e && kb(w({}, a, {type: a.type.slice(1)}))
		}
		return /^_(?:click|mouse(?:over|out|down|up|move))$/.test(a.type) ? !0 : void 0
	}, kb = function (a) {
		if (a && "string" == typeof a.type && a) {
			var b, c = a.target || null, d = c && c.ownerDocument || f, g = {
				view: d.defaultView || e,
				canBubble: !0,
				cancelable: !0,
				detail: "click" === a.type ? 1 : 0,
				button: "number" == typeof a.which ? a.which - 1 : "number" == typeof a.button ? a.button : d.createEvent ? 0 : 1
			}, h = w(g, a);
			c && d.createEvent && c.dispatchEvent && (h = [h.type, h.canBubble, h.cancelable, h.view, h.detail, h.screenX, h.screenY, h.clientX, h.clientY, h.ctrlKey, h.altKey, h.shiftKey, h.metaKey, h.button, h.relatedTarget], b = d.createEvent("MouseEvents"), b.initMouseEvent && (b.initMouseEvent.apply(b, h), b._source = "js", c.dispatchEvent(b)))
		}
	}, lb = function () {
		var a = f.createElement("div");
		return a.id = O.containerId, a.className = O.containerClass, a.style.position = "absolute", a.style.left = "0px", a.style.top = "-9999px", a.style.width = "1px", a.style.height = "1px", a.style.zIndex = "" + Db(O.zIndex), a
	}, mb = function (a) {
		for (var b = a && a.parentNode; b && "OBJECT" === b.nodeName && b.parentNode;)b = b.parentNode;
		return b || null
	}, nb = function () {
		var a, b = I.bridge, c = mb(b);
		if (!b) {
			var d = ub(e.location.host, O), g = "never" === d ? "none" : "all", h = sb(O), i = O.swfPath + rb(O.swfPath, O);
			c = lb();
			var j = f.createElement("div");
			c.appendChild(j), f.body.appendChild(c);
			var k = f.createElement("div"), l = "activex" === I.pluginType;
			k.innerHTML = '<object id="' + O.swfObjectId + '" name="' + O.swfObjectId + '" width="100%" height="100%" ' + (l ? 'classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"' : 'type="application/x-shockwave-flash" data="' + i + '"') + ">" + (l ? '<param name="movie" value="' + i + '"/>' : "") + '<param name="allowScriptAccess" value="' + d + '"/><param name="allowNetworking" value="' + g + '"/><param name="menu" value="false"/><param name="wmode" value="transparent"/><param name="flashvars" value="' + h + '"/></object>', b = k.firstChild, k = null, u(b).ZeroClipboard = Fb, c.replaceChild(b, j)
		}
		return b || (b = f[O.swfObjectId], b && (a = b.length) && (b = b[a - 1]), !b && c && (b = c.firstChild)), I.bridge = b || null, b
	}, ob = function () {
		var a = I.bridge;
		if (a) {
			var b = mb(a);
			b && ("activex" === I.pluginType && "readyState" in a ? (a.style.display = "none", function c() {
				if (4 === a.readyState) {
					for (var d in a)"function" == typeof a[d] && (a[d] = null);
					a.parentNode && a.parentNode.removeChild(a), b.parentNode && b.parentNode.removeChild(b)
				} else h(c, 10)
			}()) : (a.parentNode && a.parentNode.removeChild(a), b.parentNode && b.parentNode.removeChild(b))), I.ready = null, I.bridge = null, I.deactivated = null
		}
	}, pb = function (a) {
		var b = {}, c = {};
		if ("object" == typeof a && a) {
			for (var d in a)if (d && s.call(a, d) && "string" == typeof a[d] && a[d])switch (d.toLowerCase()) {
				case"text/plain":
				case"text":
				case"air:text":
				case"flash:text":
					b.text = a[d], c.text = d;
					break;
				case"text/html":
				case"html":
				case"air:html":
				case"flash:html":
					b.html = a[d], c.html = d;
					break;
				case"application/rtf":
				case"text/rtf":
				case"rtf":
				case"richtext":
				case"air:rtf":
				case"flash:rtf":
					b.rtf = a[d], c.rtf = d
			}
			return {data: b, formatMap: c}
		}
	}, qb = function (a, b) {
		if ("object" != typeof a || !a || "object" != typeof b || !b)return a;
		var c = {};
		for (var d in a)if (s.call(a, d)) {
			if ("success" !== d && "data" !== d) {
				c[d] = a[d];
				continue
			}
			c[d] = {};
			var e = a[d];
			for (var f in e)f && s.call(e, f) && s.call(b, f) && (c[d][b[f]] = e[f])
		}
		return c
	}, rb = function (a, b) {
		var c = null == b || b && b.cacheBust === !0;
		return c ? (-1 === a.indexOf("?") ? "?" : "&") + "noCache=" + p() : ""
	}, sb = function (a) {
		var b, c, d, f, g = "", h = [];
		if (a.trustedDomains && ("string" == typeof a.trustedDomains ? f = [a.trustedDomains] : "object" == typeof a.trustedDomains && "length" in a.trustedDomains && (f = a.trustedDomains)), f && f.length)for (b = 0, c = f.length; c > b; b++)if (s.call(f, b) && f[b] && "string" == typeof f[b]) {
			if (d = tb(f[b]), !d)continue;
			if ("*" === d) {
				h.length = 0, h.push(d);
				break
			}
			h.push.apply(h, [d, "//" + d, e.location.protocol + "//" + d])
		}
		return h.length && (g += "trustedOrigins=" + i(h.join(","))), a.forceEnhancedClipboard === !0 && (g += (g ? "&" : "") + "forceEnhancedClipboard=true"), "string" == typeof a.swfObjectId && a.swfObjectId && (g += (g ? "&" : "") + "swfObjectId=" + i(a.swfObjectId)), g
	}, tb = function (a) {
		if (null == a || "" === a)return null;
		if (a = a.replace(/^\s+|\s+$/g, ""), "" === a)return null;
		var b = a.indexOf("//");
		a = -1 === b ? a : a.slice(b + 2);
		var c = a.indexOf("/");
		return a = -1 === c ? a : -1 === b || 0 === c ? null : a.slice(0, c), a && ".swf" === a.slice(-4).toLowerCase() ? null : a || null
	}, ub = function () {
		var a = function (a) {
			var b, c, d, e = [];
			if ("string" == typeof a && (a = [a]), "object" != typeof a || !a || "number" != typeof a.length)return e;
			for (b = 0, c = a.length; c > b; b++)if (s.call(a, b) && (d = tb(a[b]))) {
				if ("*" === d) {
					e.length = 0, e.push("*");
					break
				}
				-1 === e.indexOf(d) && e.push(d)
			}
			return e
		};
		return function (b, c) {
			var d = tb(c.swfPath);
			null === d && (d = b);
			var e = a(c.trustedDomains), f = e.length;
			if (f > 0) {
				if (1 === f && "*" === e[0])return "always";
				if (-1 !== e.indexOf(b))return 1 === f && b === d ? "sameDomain" : "always"
			}
			return "never"
		}
	}(), vb = function () {
		try {
			return f.activeElement
		} catch (a) {
			return null
		}
	}, wb = function (a, b) {
		if (!a || 1 !== a.nodeType)return a;
		if (a.classList)return a.classList.contains(b) || a.classList.add(b), a;
		if (b && "string" == typeof b) {
			var c = (b || "").split(/\s+/);
			if (1 === a.nodeType)if (a.className) {
				for (var d = " " + a.className + " ", e = a.className, f = 0, g = c.length; g > f; f++)d.indexOf(" " + c[f] + " ") < 0 && (e += " " + c[f]);
				a.className = e.replace(/^\s+|\s+$/g, "")
			} else a.className = b
		}
		return a
	}, xb = function (a, b) {
		if (!a || 1 !== a.nodeType)return a;
		if (a.classList)return a.classList.contains(b) && a.classList.remove(b), a;
		if ("string" == typeof b && b) {
			var c = b.split(/\s+/);
			if (1 === a.nodeType && a.className) {
				for (var d = (" " + a.className + " ").replace(/[\n\t]/g, " "), e = 0, f = c.length; f > e; e++)d = d.replace(" " + c[e] + " ", " ");
				a.className = d.replace(/^\s+|\s+$/g, "")
			}
		}
		return a
	}, yb = function (a, b) {
		var c = e.getComputedStyle(a, null).getPropertyValue(b);
		return "cursor" !== b || c && "auto" !== c || "A" !== a.nodeName ? c : "pointer"
	}, zb = function () {
		var a, b, c, d = 1;
		return "function" == typeof f.body.getBoundingClientRect && (a = f.body.getBoundingClientRect(), b = a.right - a.left, c = f.body.offsetWidth, d = o(b / c * 100) / 100), d
	}, Ab = function (a) {
		var b = {left: 0, top: 0, width: 0, height: 0};
		if (a.getBoundingClientRect) {
			var c, d, g, h = a.getBoundingClientRect();
			"pageXOffset" in e && "pageYOffset" in e ? (c = e.pageXOffset, d = e.pageYOffset) : (g = zb(), c = o(f.documentElement.scrollLeft / g), d = o(f.documentElement.scrollTop / g));
			var i = f.documentElement.clientLeft || 0, j = f.documentElement.clientTop || 0;
			b.left = h.left + c - i, b.top = h.top + d - j, b.width = "width" in h ? h.width : h.right - h.left, b.height = "height" in h ? h.height : h.bottom - h.top
		}
		return b
	}, Bb = function () {
		var a;
		if (c && (a = mb(I.bridge))) {
			var b = Ab(c);
			w(a.style, {
				width: b.width + "px",
				height: b.height + "px",
				top: b.top + "px",
				left: b.left + "px",
				zIndex: "" + Db(O.zIndex)
			})
		}
	}, Cb = function (a) {
		I.ready === !0 && (I.bridge && "function" == typeof I.bridge.setHandCursor ? I.bridge.setHandCursor(a) : I.ready = !1)
	}, Db = function (a) {
		if (/^(?:auto|inherit)$/.test(a))return a;
		var b;
		return "number" != typeof a || n(a) ? "string" == typeof a && (b = Db(l(a, 10))) : b = a, "number" == typeof b ? b : "auto"
	}, Eb = function (a) {
		function b(a) {
			var b = a.match(/[\d]+/g);
			return b.length = 3, b.join(".")
		}

		function c(a) {
			return !!a && (a = a.toLowerCase()) && (/^(pepflashplayer\.dll|libpepflashplayer\.so|pepperflashplayer\.plugin)$/.test(a) || "chrome.plugin" === a.slice(-13))
		}

		function d(a) {
			a && (i = !0, a.version && (l = b(a.version)), !l && a.description && (l = b(a.description)), a.filename && (k = c(a.filename)))
		}

		var e, f, h, i = !1, j = !1, k = !1, l = "";
		if (g.plugins && g.plugins.length)e = g.plugins["Shockwave Flash"], d(e), g.plugins["Shockwave Flash 2.0"] && (i = !0, l = "2.0.0.11"); else if (g.mimeTypes && g.mimeTypes.length)h = g.mimeTypes["application/x-shockwave-flash"], e = h && h.enabledPlugin, d(e); else if ("undefined" != typeof a) {
			j = !0;
			try {
				f = new a("ShockwaveFlash.ShockwaveFlash.7"), i = !0, l = b(f.GetVariable("$version"))
			} catch (n) {
				try {
					f = new a("ShockwaveFlash.ShockwaveFlash.6"), i = !0, l = "6.0.21"
				} catch (o) {
					try {
						f = new a("ShockwaveFlash.ShockwaveFlash"), i = !0, l = b(f.GetVariable("$version"))
					} catch (p) {
						j = !1
					}
				}
			}
		}
		I.disabled = i !== !0, I.outdated = l && m(l) < m(J), I.version = l || "0.0.0", I.pluginType = k ? "pepper" : j ? "activex" : i ? "netscape" : "unknown"
	};
	Eb(j);
	var Fb = function () {
		return this instanceof Fb ? void("function" == typeof Fb._createClient && Fb._createClient.apply(this, v(arguments))) : new Fb
	};
	r(Fb, "version", {value: "2.1.6", writable: !1, configurable: !0, enumerable: !0}), Fb.config = function () {
		return P.apply(this, v(arguments))
	}, Fb.state = function () {
		return Q.apply(this, v(arguments))
	}, Fb.isFlashUnusable = function () {
		return R.apply(this, v(arguments))
	}, Fb.on = function () {
		return S.apply(this, v(arguments))
	}, Fb.off = function () {
		return T.apply(this, v(arguments))
	}, Fb.handlers = function () {
		return U.apply(this, v(arguments))
	}, Fb.emit = function () {
		return V.apply(this, v(arguments))
	}, Fb.create = function () {
		return W.apply(this, v(arguments))
	}, Fb.destroy = function () {
		return X.apply(this, v(arguments))
	}, Fb.setData = function () {
		return Y.apply(this, v(arguments))
	}, Fb.clearData = function () {
		return Z.apply(this, v(arguments))
	}, Fb.getData = function () {
		return $.apply(this, v(arguments))
	}, Fb.focus = Fb.activate = function () {
		return _.apply(this, v(arguments))
	}, Fb.blur = Fb.deactivate = function () {
		return ab.apply(this, v(arguments))
	}, Fb.activeElement = function () {
		return bb.apply(this, v(arguments))
	};
	var Gb = 0, Hb = {}, Ib = 0, Jb = {}, Kb = {};
	w(O, {autoActivate: !0});
	var Lb = function (a) {
		var b = this;
		b.id = "" + Gb++, Hb[b.id] = {
			instance: b,
			elements: [],
			handlers: {}
		}, a && b.clip(a), Fb.on("*", function (a) {
			return b.emit(a)
		}), Fb.on("destroy", function () {
			b.destroy()
		}), Fb.create()
	}, Mb = function (a, b) {
		var c, d, e, f = {}, g = Hb[this.id] && Hb[this.id].handlers;
		if ("string" == typeof a && a)e = a.toLowerCase().split(/\s+/); else if ("object" == typeof a && a && "undefined" == typeof b)for (c in a)s.call(a, c) && "string" == typeof c && c && "function" == typeof a[c] && this.on(c, a[c]);
		if (e && e.length) {
			for (c = 0, d = e.length; d > c; c++)a = e[c].replace(/^on/, ""), f[a] = !0, g[a] || (g[a] = []), g[a].push(b);
			if (f.ready && I.ready && this.emit({type: "ready", client: this}), f.error) {
				var h = ["disabled", "outdated", "unavailable", "deactivated", "overdue"];
				for (c = 0, d = h.length; d > c; c++)if (I[h[c]]) {
					this.emit({type: "error", name: "flash-" + h[c], client: this});
					break
				}
			}
		}
		return this
	}, Nb = function (a, b) {
		var c, d, e, f, g, h = Hb[this.id] && Hb[this.id].handlers;
		if (0 === arguments.length)f = q(h); else if ("string" == typeof a && a)f = a.split(/\s+/); else if ("object" == typeof a && a && "undefined" == typeof b)for (c in a)s.call(a, c) && "string" == typeof c && c && "function" == typeof a[c] && this.off(c, a[c]);
		if (f && f.length)for (c = 0, d = f.length; d > c; c++)if (a = f[c].toLowerCase().replace(/^on/, ""), g = h[a], g && g.length)if (b)for (e = g.indexOf(b); -1 !== e;)g.splice(e, 1), e = g.indexOf(b, e); else g.length = 0;
		return this
	}, Ob = function (a) {
		var b = null, c = Hb[this.id] && Hb[this.id].handlers;
		return c && (b = "string" == typeof a && a ? c[a] ? c[a].slice(0) : [] : x(c)), b
	}, Pb = function (a) {
		if (Ub.call(this, a)) {
			"object" == typeof a && a && "string" == typeof a.type && a.type && (a = w({}, a));
			var b = w({}, db(a), {client: this});
			Vb.call(this, b)
		}
		return this
	}, Qb = function (a) {
		a = Wb(a);
		for (var b = 0; b < a.length; b++)if (s.call(a, b) && a[b] && 1 === a[b].nodeType) {
			a[b].zcClippingId ? -1 === Jb[a[b].zcClippingId].indexOf(this.id) && Jb[a[b].zcClippingId].push(this.id) : (a[b].zcClippingId = "zcClippingId_" + Ib++, Jb[a[b].zcClippingId] = [this.id], O.autoActivate === !0 && Xb(a[b]));
			var c = Hb[this.id] && Hb[this.id].elements;
			-1 === c.indexOf(a[b]) && c.push(a[b])
		}
		return this
	}, Rb = function (a) {
		var b = Hb[this.id];
		if (!b)return this;
		var c, d = b.elements;
		a = "undefined" == typeof a ? d.slice(0) : Wb(a);
		for (var e = a.length; e--;)if (s.call(a, e) && a[e] && 1 === a[e].nodeType) {
			for (c = 0; -1 !== (c = d.indexOf(a[e], c));)d.splice(c, 1);
			var f = Jb[a[e].zcClippingId];
			if (f) {
				for (c = 0; -1 !== (c = f.indexOf(this.id, c));)f.splice(c, 1);
				0 === f.length && (O.autoActivate === !0 && Yb(a[e]), delete a[e].zcClippingId)
			}
		}
		return this
	}, Sb = function () {
		var a = Hb[this.id];
		return a && a.elements ? a.elements.slice(0) : []
	}, Tb = function () {
		this.unclip(), this.off(), delete Hb[this.id]
	}, Ub = function (a) {
		if (!a || !a.type)return !1;
		if (a.client && a.client !== this)return !1;
		var b = Hb[this.id] && Hb[this.id].elements, c = !!b && b.length > 0, d = !a.target || c && -1 !== b.indexOf(a.target), e = a.relatedTarget && c && -1 !== b.indexOf(a.relatedTarget), f = a.client && a.client === this;
		return d || e || f ? !0 : !1
	}, Vb = function (a) {
		if ("object" == typeof a && a && a.type) {
			var b = gb(a), c = Hb[this.id] && Hb[this.id].handlers["*"] || [], d = Hb[this.id] && Hb[this.id].handlers[a.type] || [], f = c.concat(d);
			if (f && f.length) {
				var g, h, i, j, k, l = this;
				for (g = 0, h = f.length; h > g; g++)i = f[g], j = l, "string" == typeof i && "function" == typeof e[i] && (i = e[i]), "object" == typeof i && i && "function" == typeof i.handleEvent && (j = i, i = i.handleEvent), "function" == typeof i && (k = w({}, a), hb(i, j, [k], b))
			}
			return this
		}
	}, Wb = function (a) {
		return "string" == typeof a && (a = []), "number" != typeof a.length ? [a] : a
	}, Xb = function (a) {
		if (a && 1 === a.nodeType) {
			var b = function (a) {
				(a || (a = e.event)) && ("js" !== a._source && (a.stopImmediatePropagation(), a.preventDefault()), delete a._source)
			}, c = function (c) {
				(c || (c = e.event)) && (b(c), Fb.focus(a))
			};
			a.addEventListener("mouseover", c, !1), a.addEventListener("mouseout", b, !1), a.addEventListener("mouseenter", b, !1), a.addEventListener("mouseleave", b, !1), a.addEventListener("mousemove", b, !1), Kb[a.zcClippingId] = {
				mouseover: c,
				mouseout: b,
				mouseenter: b,
				mouseleave: b,
				mousemove: b
			}
		}
	}, Yb = function (a) {
		if (a && 1 === a.nodeType) {
			var b = Kb[a.zcClippingId];
			if ("object" == typeof b && b) {
				for (var c, d, e = ["move", "leave", "enter", "out", "over"], f = 0, g = e.length; g > f; f++)c = "mouse" + e[f], d = b[c], "function" == typeof d && a.removeEventListener(c, d, !1);
				delete Kb[a.zcClippingId]
			}
		}
	};
	Fb._createClient = function () {
		Lb.apply(this, v(arguments))
	}, Fb.prototype.on = function () {
		return Mb.apply(this, v(arguments))
	}, Fb.prototype.off = function () {
		return Nb.apply(this, v(arguments))
	}, Fb.prototype.handlers = function () {
		return Ob.apply(this, v(arguments))
	}, Fb.prototype.emit = function () {
		return Pb.apply(this, v(arguments))
	}, Fb.prototype.clip = function () {
		return Qb.apply(this, v(arguments))
	}, Fb.prototype.unclip = function () {
		return Rb.apply(this, v(arguments))
	}, Fb.prototype.elements = function () {
		return Sb.apply(this, v(arguments))
	}, Fb.prototype.destroy = function () {
		return Tb.apply(this, v(arguments))
	}, Fb.prototype.setText = function (a) {
		return Fb.setData("text/plain", a), this
	}, Fb.prototype.setHtml = function (a) {
		return Fb.setData("text/html", a), this
	}, Fb.prototype.setRichText = function (a) {
		return Fb.setData("application/rtf", a), this
	}, Fb.prototype.setData = function () {
		return Fb.setData.apply(this, v(arguments)), this
	}, Fb.prototype.clearData = function () {
		return Fb.clearData.apply(this, v(arguments)), this
	}, Fb.prototype.getData = function () {
		return Fb.getData.apply(this, v(arguments))
	}, "function" == typeof define && define.amd ? define(function () {
		return Fb
	}) : "object" == typeof module && module && "object" == typeof module.exports && module.exports ? module.exports = Fb : a.ZeroClipboard = Fb
}(function () {
	return this || window
}()), APP.buildLayout = function (a) {
	setTimeout(function () {
		APP.resizeCanvas(a)
	}, 1e3), /iPhone|iPad|iPod/i.test(navigator.userAgent) && (a.find("select, input[type=text], input[type=email], input[type=password]").focus(function () {
		$("body").removeClass("ios-keyboard-on").addClass("ios-keyboard-on")
	}), a.find("select, input[type=text], input[type=email], input[type=password]").blur(function () {
		$("body").removeClass("ios-keyboard-on")
	}))
}, $(window).resize(function () {
	APP.resizeCanvas($(window))
}), APP.resizeCanvasExtra = function () {
}, APP.resizeCanvas = function (a) {
	$("body").hasClass("skip-resize-canvas") || (window_height = a.height(), window_width = a.width(), center_width = 940, navigator.userAgent.match(/(iPhone|iPod)/) && (window_height += 60), APP.resizeCanvasExtra(a))
}, function () {
	APP.notifications = [], APP.getUserFeed = function (a, b, c, d) {
		var e;
		return e = $.ajax({type: "GET", url: "/notifications/load/" + a + "/" + b}).done(function (a) {
			return d(a)
		}).fail(function () {
			return window.console.log("there was an error")
		})
	}, APP.updateNotifications = function (a, b) {
		var c;
		return c = $.ajax({
			type: "POST",
			url: "/notifications/status/" + b,
			data: {notifications: a},
			done: function () {
				return 9 === b && $(".notification-list").empty(), window.console.log("all done")
			},
			fail: function () {
				return window.console.log("there was an error")
			}
		})
	}, APP.updateNotificationCount = function () {
		var a, b;
		return $(".header-nav-list .navbar-alerts").length > 0 ? (a = $(".header-nav-list .navbar-alerts").attr("data-id"), b = $.ajax({
			type: "GET",
			url: "/notifications/load/" + a,
			success: function (a) {
				return window.console.log(a), $(".navbar-alerts .badge").text(a.newcount), parseInt(a.newcount) > 0 ? $(".navbar-alerts .badge").show() : $(".navbar-alerts .badge").hide()
			},
			error: function () {
				return window.console.log("there was an error")
			}
		})) : void 0
	}, jQuery(function () {
		return "undefined" != typeof Pusher && null !== Pusher && null != APP.userData.id ? APP.webSockets.channel = APP.webSockets.pusher.subscribe(APP.userData.id) : void 0
	})
}.call(this), function () {
	window.S3Upload = function () {
		function a(a) {
			null == a && (a = {});
			for (option in a)this[option] = a[option];
			_.extend(this, a), this.file_dom_selector && this.handleFileSelect($(this.file_dom_selector).get(0))
		}

		return a.prototype.onFinishS3Put = function (a) {
			return window.console.log("base.onFinishS3Put()", a)
		}, a.prototype.onProgress = function (a, b) {
			return window.console.log("base.onProgress()", a, b)
		}, a.prototype.onError = function (a) {
			return window.console.log("base.onError()", a)
		}, a.prototype.handleFileSelect = function (a) {
			var b, c, d, e, f, g;
			for (this.onProgress(0, "Upload started."), c = a.files, d = [], g = [], e = 0, f = c.length; f > e; e++)b = c[e], g.push(this.uploadFile(b));
			return g
		}, a.prototype.createCORSRequest = function (a, b) {
			var c;
			return c = new XMLHttpRequest, null != c.withCredentials ? c.open(a, b, !0) : "undefined" != typeof XDomainRequest ? (c = new XDomainRequest, c.open(a, b)) : c = null, c
		}, a.prototype.executeOnSignedUrl = function (a, b) {
			var c, d;
			return c = this, d = new XMLHttpRequest, d.open("GET", this.s3_sign_put_url + "?s3_object_type=" + a.type + "&s3_object_name=" + this.s3_object_name, !0), d.overrideMimeType("text/plain; charset=x-user-defined"), d.onreadystatechange = function () {
				var a;
				if (4 === this.readyState && 200 === this.status) {
					try {
						a = JSON.parse(this.responseText)
					} catch (d) {
						return c.onError('Signing server returned some ugly/empty JSON: "' + this.responseText + '"'), !1
					}
					return b(decodeURIComponent(a.signed_request), a.url)
				}
				return 4 === this.readyState && 200 !== this.status ? c.onError("Could not contact request signing server. Status = " + this.status) : void 0
			}, d.send()
		}, a.prototype.uploadToS3 = function (a, b, c) {
			if (window.console.log(a), a.size < 3e6) {
				var d, e;
				return d = this, e = this.createCORSRequest("PUT", b), e ? (e.onload = function () {
					return 200 === e.status ? (d.onProgress(100, "Upload completed."), d.onFinishS3Put(c)) : d.onError("Upload error: " + e.status)
				}, e.onerror = function () {
					return d.onError("XHR error.")
				}, e.upload.onprogress = function (a) {
					var b;
					return a.lengthComputable ? (b = Math.round(a.loaded / a.total * 100), d.onProgress(b, 100 === b ? "Finalizing." : "Uploading.")) : void 0
				}) : this.onError("CORS not supported"), e.setRequestHeader("Content-Type", a.type), e.setRequestHeader("x-amz-acl", "public-read"), e.send(a)
			}
			$("#uploading").hide(), window.console.log("file es my largo")
		}, a.prototype.uploadFile = function (a) {
			var b;
			return b = this, this.executeOnSignedUrl(a, function (c, d) {
				return b.uploadToS3(a, c, d)
			})
		}, a
	}()
}.call(this), function () {
	APP.enableScreens = function (a) {
		var b, c, d;
		return c = a.find(".screen"), c.length > 0 && (APP.bindScreenControls(a), c.first().show(), c.length > 1 && (d = 0, b = APP.URLParameter("screen"), "" !== b && parseInt(b) > 0 && (d = parseInt(b)), History.pushState({screen: 0}, window.page_title, ""), d > 0 && c.eq(d).hasClass("allow-screen-url"))) ? (History.pushState({screen: d}, window.page_title, "?screen=" + d), APP.renderScreens(d), c.eq(d).trigger("onURLScreenLoad")) : void 0
	}, APP.bindScreenControls = function (a) {
		return a.find(".btn.next").click(function (b) {
			var c, d;
			return b.preventDefault(), c = ".screen", null != a.attr("id") && (c = "#" + a.attr("id") + " .screen"), d = $(this).parents(".screen").index(c) + 1, d < $(c).length ? $(this).hasClass("disabled") ? !1 : $(this).hasClass("delay") ? setTimeout(function () {
				return History.pushState({screen: d}, window.page_title, "")
			}, 3e3) : $(this).hasClass("short-delay") ? setTimeout(function () {
				return History.pushState({screen: d}, window.page_title, "")
			}, 700) : History.pushState({screen: d}, window.page_title, "") : void 0
		}), a.find(".btn.back").click(function (b) {
			var c, d;
			return b.preventDefault(), c = ".screen", null != a.attr("id") && (c = "#" + a.attr("id") + " .screen"), d = $(this).parents(".screen").index(c) - 1, d >= 0 ? $(this).hasClass("disabled") ? !1 : $(this).hasClass("delay") ? setTimeout(function () {
				return History.pushState({screen: d}, window.page_title, "")
			}, 3e3) : $(this).hasClass("short-delay") ? setTimeout(function () {
				return History.pushState({screen: d}, window.page_title, "")
			}, 700) : History.pushState({screen: d}, window.page_title, "") : void 0
		}), a.find(".btn-group.next .btn").click(function (b) {
			var c, d;
			return b.preventDefault(), c = ".screen", null != a.attr("id") && (c = "#" + a.attr("id") + " .screen"), d = $(this).parents(".screen").index(c) + 1, d < $(c).length ? $(this).hasClass("disabled") ? !1 : $(this).parents(".btn-group").hasClass("delay") ? setTimeout(function () {
				return History.pushState({screen: d}, window.page_title, "")
			}, 3e3) : $(this).parents(".btn-group").hasClass("short-delay") ? setTimeout(function () {
				return History.pushState({screen: d}, window.page_title, "")
			}, 700) : History.pushState({screen: d}, window.page_title, "") : void 0
		}), a.find(".btn-jump").each(function () {
			var b;
			return b = $(this).attr("href"), $(this).click(function (c) {
				return c.preventDefault(), b < a.find(".screen").length ? $(this).hasClass("disabled") ? !1 : $(this).parents(".btn-group").hasClass("delay") ? setTimeout(function () {
					return APP.jumpToScreen(b)
				}, 3e3) : $(this).parents(".btn-group").hasClass("short-delay") ? setTimeout(function () {
					return APP.jumpToScreen(b)
				}, 700) : APP.jumpToScreen(b) : void 0
			}), a.find(".screen").eq(b).bind("onURLScreenLoad", function () {
				return a.find(".btn.btn-jump[href=" + b + "]").click()
			})
		})
	}, APP.jumpToScreen = function (a) {
		var b;
		return History.pushState({screen: a}, window.page_title, ""), b = $(".btn.btn-jump[href=" + a + "]"), b.parents("ul.nav").length > 0 ? (b.parents("ul.nav").find("li").removeClass("active"), b.parent().removeClass("active").addClass("active")) : ($(".btn.btn-jump").removeClass("active"), b.removeClass("active").addClass("active"))
	}, APP.renderScreens = function (a) {
		var b, c, d;
		return $(".btn-jump").removeClass("selected"), $(".btn-jump[href=" + a + "]").addClass("selected"), c = $(".screen"), b = $(".screen:visible"), b.trigger("afterLeaveScreen", [a]), c.hide(), d = c.eq(a), d.show(), APP.resizeCanvas($(window)), APP.scrollToTop(), d.trigger("afterShowScreen"), null != APP.renderScreen ? APP.renderScreen(a) : void 0
	}
}.call(this), function () {
	$(function () {
		return APP.buildPageArtist($("body.page-artist"))
	}), APP.buildPageArtist = function (a) {
		return 1 === a.length ? ($(".header-menu-container.toggle").click(function (a) {
			var b;
			return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
				top: $(this).outerHeight(),
				left: $(this).offset().left
			})
		}), a.find(".video-list a").click(function (a) {
			return a.stopPropagation(), a.preventDefault(), $("#video-container").empty(), $("#video-container").html('<iframe width="100%" src="https://www.youtube.com/embed/' + $(this).attr("href") + '" frameborder="0" allowfullscreen></iframe>'), $("#video-title").text($(this).attr("data-title")), $(".video-list a").removeClass("active"), $(this).addClass("active")
		}), a.find(".facebook-like-count").each(function () {
			var a, b;
			return a = $(this), $(this).attr("data-fb-id") ? b = $.ajax({
				type: "GET",
				url: "https://graph.facebook.com/" + $(this).attr("data-fb-id") + "?access_token=680471355390831|9909089909570810914c585070853e8b",
				cache: !1,
				data: {}
			}).done(function (b) {
				return a.text(APP.numberWithCommas(b.likes))
			}).fail(function () {
				return window.console.log("Request failed.")
			}) : void 0
		}), a.find(".fb-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-url")), b = "https://www.facebook.com/sharer/sharer.php?u=" + c, window.open(b, "", "width=550, height=225")
		}), a.find(".twitter-follower-count").each(function () {
			var a, b;
			return a = $(this), $(this).attr("data-twitter-screenname") ? b = $.ajax({
				type: "GET",
				url: "https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=" + $(this).attr("data-twitter-screenname"),
				cache: !1,
				dataType: "jsonp",
				crossDomain: !0,
				data: {}
			}).done(function (b) {
				return a.text(APP.numberWithCommas(b[0].followers_count))
			}).fail(function () {
				return window.console.log("Request failed.")
			}) : void 0
		}), a.find(".twitter-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-message")), b = "http://twitter.com/home?status=" + c, window.open(b, "", "width=550, height=225")
		}), a.on("click", function (a) {
			return $(".header-menu-container.toggle").each(function () {
				var b;
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
			}), $(".dropdown").each(function () {
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : $(this).removeClass("open")
			})
		}), APP.buildAdditionalComponents = function (a) {
			return a.find(".flexslider-nav").flexslider({
				animation: "slide",
				controlNav: "thumbnails",
				move: 3,
				itemWidth: 42
			})
		}, APP.resizeCanvasExtra = function () {
			return $(".header-menu-container.toggle").each(function () {
				var a;
				return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
			})
		}) : void 0
	}
}.call(this), function () {
	$(function () {
		return APP.buildPageCommunity($("body.page-community"))
	}), APP.buildPageCommunity = function (a) {
		return 1 === a.length ? ($(".header-menu-container.toggle").click(function (a) {
			var b;
			return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
				top: $(this).outerHeight(),
				left: $(this).offset().left
			})
		}), $(".dropdown .current-value").click(function (a) {
			return a.stopPropagation(), a.preventDefault(), $(".dropdown").toggleClass("open")
		}), $(".dropdown li a").click(function (a) {
			var b, c;
			return a.stopPropagation(), a.preventDefault(), c = $(this).attr("data-value"), b = $(this).parents("form"), b.find("input[name=category]").val(c), b.find(".current-value").text($(this).text()), $(".dropdown").removeClass("open")
		}), $(".featured-artist-list li .info-container").click(function (a) {
			return null != $(this).attr("href") ? (a.stopPropagation(), a.preventDefault(), self.location = $(this).attr("href")) : void 0
		}), a.on("click", function (a) {
			return $(".header-menu-container.toggle").each(function () {
				var b;
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
			}), $(".dropdown").each(function () {
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : $(this).removeClass("open")
			})
		}), a.find(".twitter-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-message")), b = "http://twitter.com/home?status=" + c, window.open(b, "", "width=550, height=225")
		}), a.find(".fb-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-url")), b = "https://www.facebook.com/sharer/sharer.php?u=" + c, window.open(b, "", "width=550, height=225")
		}), APP.buildAdditionalComponents = function (a) {
			return a.find(".link-talent-form").click(function () {
				var a, b, c, d, e;
				return b = "", d = $(".talent-form-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").removeClass("wide-medium").addClass("wide-medium"), $("#content-modal").modal("show")
			}), a.find("form#form-talent").unbind("onDone"), a.find("form#form-talent").bind("onDone", function () {
				var a, b, c, d, e;
				return b = "", d = $(".thank-you-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").modal("show")
			}), a.find(".has-link").click(function () {
				return null != $(this).attr("href") ? self.location = $(this).attr("href") : void 0
			})
		}, APP.resizeCanvasExtra = function () {
			return $(".header-menu-container.toggle").each(function () {
				var a;
				return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
			})
		}, APP.YTPlayer.onPlayerStateChange = function (a) {
			return a.data === YT.PlayerState.ENDED ? APP.hideContentModal() : void 0
		}) : void 0
	}
}.call(this), function () {
	$(function () {
		return APP.buildPageLanding($("body.page-landing"))
	}), APP.buildPageLanding = function (a) {
		return 1 === a.length ? ($(".header-menu-container.toggle").click(function (a) {
			var b;
			return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
				top: $(this).outerHeight(),
				left: $(this).offset().left
			})
		}), $(".dropdown .current-value").click(function (a) {
			return a.stopPropagation(), a.preventDefault(), $(".dropdown").toggleClass("open")
		}), $(".dropdown li a").click(function (a) {
			var b, c;
			return a.stopPropagation(), a.preventDefault(), c = $(this).attr("data-value"), b = $(this).parents("form"), b.find("input[name=category]").val(c), b.find(".current-value").text($(this).text()), $(".dropdown").removeClass("open")
		}), $(".featured-artist-list li .info-container").click(function (a) {
			return null != $(this).attr("href") ? (a.stopPropagation(), a.preventDefault(), self.location = $(this).attr("href")) : void 0
		}), a.on("click", function (a) {
			return $(".header-menu-container.toggle").each(function () {
				var b;
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
			}), $(".dropdown").each(function () {
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : $(this).removeClass("open")
			})
		}), APP.buildAdditionalComponents = function (a) {
			return a.find(".link-talent-form").click(function () {
				var a, b, c, d, e;
				return b = "", d = $(".talent-form-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").removeClass("wide-medium").addClass("wide-medium"), $("#content-modal").modal("show")
			}), a.find("form#form-talent").unbind("onDone"), a.find("form#form-talent").bind("onDone", function () {
				var a, b, c, d, e;
				return b = "", d = $(".thank-you-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").modal("show")
			}), a.find(".has-link").click(function () {
				return null != $(this).attr("href") ? self.location = $(this).attr("href") : void 0
			})
		}, APP.resizeCanvasExtra = function () {
			return $(".header-menu-container.toggle").each(function () {
				var a;
				return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
			})
		}, APP.YTPlayer.onPlayerStateChange = function (a) {
			return a.data === YT.PlayerState.ENDED ? APP.hideContentModal() : void 0
		}) : void 0
	}
}.call(this), function () {
	$(function () {
		return APP.buildPageProfile($("body.page-profile"))
	}), APP.buildPageProfile = function (a) {
		return 1 === a.length ? ($(".header-menu-container.toggle").click(function (a) {
			var b;
			return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
				top: $(this).outerHeight(),
				left: $(this).offset().left
			})
		}), a.find(".video-list a").click(function (a) {
			return a.stopPropagation(), a.preventDefault(), $("#video-container").empty(), $("#video-container").html('<iframe width="100%" src="https://www.youtube.com/embed/' + $(this).attr("href") + '" frameborder="0" allowfullscreen></iframe>'), $("#video-title").text($(this).attr("data-title")), $(".video-list a").removeClass("active"), $(this).addClass("active")
		}), a.find(".facebook-like-count").each(function () {
			var a, b;
			return a = $(this), $(this).attr("data-fb-id") ? b = $.ajax({
				type: "GET",
				url: "https://graph.facebook.com/" + $(this).attr("data-fb-id") + "?access_token=680471355390831|9909089909570810914c585070853e8b",
				cache: !1,
				data: {}
			}).done(function (b) {
				return a.text(APP.numberWithCommas(b.likes))
			}).fail(function () {
				return window.console.log("Request failed.")
			}) : void 0
		}), a.find(".fb-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-url")), b = "https://www.facebook.com/sharer/sharer.php?u=" + c, window.open(b, "", "width=550, height=225")
		}), a.find(".twitter-follower-count").each(function () {
			var a, b;
			return a = $(this), $(this).attr("data-twitter-screenname") ? b = $.ajax({
				type: "GET",
				url: "https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=" + $(this).attr("data-twitter-screenname"),
				cache: !1,
				dataType: "jsonp",
				crossDomain: !0,
				data: {}
			}).done(function (b) {
				return a.text(APP.numberWithCommas(b[0].followers_count))
			}).fail(function () {
				return window.console.log("Request failed.")
			}) : void 0
		}), a.find(".twitter-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-message")), b = "http://twitter.com/home?status=" + c, window.open(b, "", "width=550, height=225")
		}), a.on("click", function (a) {
			return $(".header-menu-container.toggle").each(function () {
				var b;
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
			}), $(".dropdown").each(function () {
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : $(this).removeClass("open")
			})
		}), APP.form_offset = $("#floating-form").offset(), APP.ceiling = APP.form_offset.top - 122, APP.form_height = $("#booking-form").height(), APP.adjustFormPosition = function () {
			var a;
			return a = $("footer").offset(), $("#floating-form").css($(window).scrollTop() + APP.form_height + 100 > a.top ? {
				position: "absolute",
				top: a.top - APP.form_height - 70
			} : $(window).scrollTop() > APP.ceiling ? {position: "fixed", top: 122} : {
				position: "absolute",
				top: APP.form_offset.top
			})
		}, $(window).scroll(function () {
			return APP.adjustFormPosition()
		}), APP.adjustFormPosition(), a.find(".twitter-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-message")), b = "http://twitter.com/home?status=" + c, window.open(b, "", "width=550, height=225")
		}), a.find(".fb-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-url")), b = "https://www.facebook.com/sharer/sharer.php?u=" + c, window.open(b, "", "width=550, height=225")
		}), APP.buildAdditionalComponents = function (a) {
			return a.find(".flexslider-nav").flexslider({
				animation: "slide",
				controlNav: "thumbnails",
				move: 3,
				itemWidth: 42
			}), a.find(".link-talent-form").click(function () {
				var a, b, c, d, e;
				return b = "", d = $(".talent-form-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").removeClass("wide-medium").addClass("wide-medium"), $("#content-modal").modal("show")
			}), a.find("form#form-talent").unbind("onDone"), a.find("form#form-talent").bind("onDone", function () {
				var a, b, c, d, e;
				return b = "", d = $(".thank-you-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").modal("show")
			})
		}, APP.resizeCanvasExtra = function () {
			return $(".header-menu-container.toggle").each(function () {
				var a;
				return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
			})
		}) : void 0
	}
}.call(this), function () {
	$(function () {
		return APP.buildPageProfileList($("body.page-profile-list"))
	}), APP.buildPageProfileList = function (a) {
		return 1 === a.length ? ($(".header-menu-container.toggle").click(function (a) {
			var b;
			return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
				top: $(this).outerHeight(),
				left: $(this).offset().left
			})
		}), $(".dropdown .current-value").click(function (a) {
			return a.stopPropagation(), a.preventDefault(), $(".dropdown").toggleClass("open")
		}), $(".dropdown li a").click(function (a) {
			var b, c;
			return a.stopPropagation(), a.preventDefault(), c = $(this).attr("data-value"), b = $(this).parents("form"), b.find("input[name=category]").val(c), b.find(".current-value").text($(this).text()), $(".dropdown").removeClass("open")
		}), $(".featured-profile-list li .info-container").click(function (a) {
			return null != $(this).attr("href") ? (a.stopPropagation(), a.preventDefault(), self.location = $(this).attr("href")) : void 0
		}), a.on("click", function (a) {
			return $(".header-menu-container.toggle").each(function () {
				var b;
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
			}), $(".dropdown").each(function () {
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : $(this).removeClass("open")
			})
		}), a.find(".twitter-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-message")), b = "http://twitter.com/home?status=" + c, window.open(b, "", "width=550, height=225")
		}), a.find(".fb-share-button").click(function (a) {
			var b, c;
			return a.preventDefault(), a.stopPropagation(), c = encodeURIComponent($(this).attr("data-share-url")), b = "https://www.facebook.com/sharer/sharer.php?u=" + c, window.open(b, "", "width=550, height=225")
		}), APP.buildAdditionalComponents = function (a) {
			return a.find(".link-talent-form").click(function () {
				var a, b, c, d, e;
				return b = "", d = $(".talent-form-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").removeClass("wide-medium").addClass("wide-medium"), $("#content-modal").modal("show")
			}), a.find("form#form-talent").unbind("onDone"), a.find("form#form-talent").bind("onDone", function () {
				var a, b, c, d, e;
				return b = "", d = $(".thank-you-template").html(), e = Handlebars.compile(d), a = {}, b = e(a), c = $("#content-modal .modal-body"), c.html(b), APP.buildComponents(c), $("#content-modal .modal-header-content").html(""), $("#content-modal").removeClass("hidden-header").addClass("hidden-header"), $("#content-modal").modal("show")
			}), a.find(".has-link").click(function () {
				return null != $(this).attr("href") ? self.location = $(this).attr("href") : void 0
			})
		}, APP.resizeCanvasExtra = function () {
			return $(".header-menu-container.toggle").each(function () {
				var a;
				return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
			})
		}, APP.YTPlayer.onPlayerStateChange = function (a) {
			return a.data === YT.PlayerState.ENDED ? APP.hideContentModal() : void 0
		}) : void 0
	}
}.call(this), function () {
	$(function () {
		return APP.buildPageSubscription($("body.page-subscription"))
	}), APP.buildPageSubscription = function (a) {
		return 1 === a.length ? ($(".header-menu-container.toggle").click(function (a) {
			var b;
			return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
				top: $(this).outerHeight(),
				left: $(this).offset().left
			})
		}), APP.payment_plans = ["semiannual", "annual", "monthly"], APP.payment_plan_index = 0, a.find(".link-billing-switch").click(function (a) {
			var b, c, d, e, f, g;
			for (a.stopPropagation(), a.preventDefault(), d = $(this).parents(".plan-container"), APP.payment_plan_index = APP.payment_plan_index < APP.payment_plans.length - 1 ? APP.payment_plan_index + 1 : 0, g = [], c = e = 0, f = APP.payment_plans.length; f >= 0 ? f >= e : e >= f; c = f >= 0 ? ++e : --e)b = APP.payment_plans[c], g.push(c === APP.payment_plan_index ? d.removeClass(b).addClass(b) : d.removeClass(b));
			return g
		}), a.on("click", function (a) {
			return $(".header-menu-container.toggle").each(function () {
				var b;
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
			}), $(".dropdown").each(function () {
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : $(this).removeClass("open")
			})
		}), APP.buildAdditionalComponents = function () {
		}, APP.resizeCanvasExtra = function () {
			return $(".header-menu-container.toggle").each(function () {
				var a;
				return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
			})
		}) : void 0
	}
}.call(this), function () {
	$(function () {
		return APP.buildPageSubscriptionConfirmation($("body.page-subscription-confirmation"))
	}), APP.buildPageSubscriptionConfirmation = function (a) {
		return 1 === a.length ? ($(".header-menu-container.toggle").click(function (a) {
			var b;
			return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
				top: $(this).outerHeight(),
				left: $(this).offset().left
			})
		}), a.on("click", function (a) {
			return $(".header-menu-container.toggle").each(function () {
				var b;
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
			}), $(".dropdown").each(function () {
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : $(this).removeClass("open")
			})
		}), APP.buildAdditionalComponents = function () {
		}, APP.resizeCanvasExtra = function () {
			return $(".header-menu-container.toggle").each(function () {
				var a;
				return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
			})
		}) : void 0
	}
}.call(this), function () {
	$(function () {
		return APP.buildPageWhy($("body.page-why"))
	}), APP.buildPageWhy = function (a) {
		return 1 === a.length ? ($(".header-menu-container.toggle").click(function (a) {
			var b;
			return a.stopPropagation(), a.preventDefault(), b = $($(this).attr("data-toggle")), $(this).toggleClass("toggled"), $(".dropdown").removeClass("open"), b.toggleClass("hidden"), b.offset({
				top: $(this).outerHeight(),
				left: $(this).offset().left
			})
		}), a.on("click", function (a) {
			return $(".header-menu-container.toggle").each(function () {
				var b;
				return $(this).is(a.target) || 0 !== $(this).has(a.target).length ? void 0 : (b = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), b.addClass("hidden"))
			})
		}), APP.buildAdditionalComponents = function () {
		}, APP.resizeCanvasExtra = function () {
			return $(".header-menu-container.toggle").each(function () {
				var a;
				return a = $($(this).attr("data-toggle")), $(this).removeClass("toggled"), a.addClass("hidden")
			})
		}) : void 0
	}
}.call(this), function () {
	"undefined" != typeof ZeroClipboard && null !== ZeroClipboard && ZeroClipboard.config({swfPath: "/swf/ZeroClipboard.swf"}), jQuery(function () {
		return APP.buildComponents($("body")), APP.buildLayout($("body")), History.Adapter.bind(window, "popstate", function () {
			var a;
			return a = History.getState().data.screen, null == a && (a = 0), APP.renderScreens(a)
		}), APP.resizeCanvas($(window)), APP.scrollToTop()
	})
}.call(this);