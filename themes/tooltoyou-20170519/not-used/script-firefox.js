jQuery(document).ready(function ($) {
	$(".header-menu-container.toggle").click(function () {
		if ($(".header-menu-container.toggle").hasClass("toggled")) {
			// Menu is shown; hide it
			$(".header-menu-container.toggle").removeClass("toggled");
			$(".toggle-menu.header-toggle-menu-menu").addClass("hidden");
		} else {
			// Menu is hidden; show it
			$(".header-menu-container.toggle").addClass("toggled");
			$(".toggle-menu.header-toggle-menu-menu").removeClass("hidden");

			position_menu_desktop();
		}
	});

	/*
	$(window).click(function () {
		if ($(".dropdown").hasClass("toggled")) {
			// Menu is shown; hide it
			$(".header-menu-container.toggle").removeClass("toggled");
			$(".toggle-menu.header-toggle-menu-menu").addClass("hidden");
		} else {
			// Menu is hidden; show it
			$(".header-menu-container.toggle").addClass("toggled");
			$(".toggle-menu.header-toggle-menu-menu").removeClass("hidden");

			position_menu_desktop();
		}
	});
	*/

	$(".dropdown .current-value").click(function () {
		return $(".dropdown").toggleClass("open")
	});

	$(".dropdown li a").click(function (a) {
		var b, c;
		return c = $(this).attr("data-value"), b = $(this).parents("form"), b.find("input[name=category]").val(c), b.find(".current-value").text($(this).text()), $(".dropdown").removeClass("open")
	});


});

jQuery(window).resize(function () {
	position_menu_desktop();
});

position_menu_desktop = function () {
	// Move the menu under the menu button
	// var element = jQuery( "div[data-toggle='#header-menu-toggle-menu']" );
	var element = jQuery("#header-menu-desktop-anchor");
	var offset = element.offset();
	var height = element.outerHeight();
	var width = element.outerWidth();
	var top = (offset.top + height) + "px";
	var left = (offset.left) + "px";

	jQuery("#header-menu-toggle-menu").css({
		"position": "absolute",
		"left": left,
		"top": top
	});

	console.log("offset.top: " + offset.top);
	console.log("offset.left: " + offset.left);
	console.log("height: " + height);
	console.log("width: " + width);
	console.log("top: " + top);
	console.log("left: " + left);

};
